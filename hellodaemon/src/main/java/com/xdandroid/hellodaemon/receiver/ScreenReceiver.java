package com.xdandroid.hellodaemon.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by snow on 16/07/2019.
 */

public class ScreenReceiver extends BroadcastReceiver {

    private static boolean isScreenOn = true;
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null) {
            if (action.equals(Intent.ACTION_SCREEN_ON)) {
                isScreenOn = true;
            } else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                isScreenOn = false;
            }
        }
    }

    public static boolean IsScreenOn(){
        return isScreenOn;
    }
}