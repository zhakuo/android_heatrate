package com.xdandroid.hellodaemon;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.os.Build;
import android.util.Log;

import com.workout.base.service.HelloService;

import static com.xdandroid.hellodaemon.WatchDogService.HASH_CODE;

/**
 * Android 5.0+ 使用的 JobScheduler.
 * 运行在 :watch 子进程中.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class JobSchedulerService extends JobService {

    @Override
    public boolean onStartJob(JobParameters params) {
        if (!DaemonEnv.sInitialized) return false;

        //启动守护服务，运行在:watch子进程中
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            Log.d("HelloService","JobSchedulerService startServiceMayBind ");
            //启动守护服务，运行在:watch子进程中
            DaemonEnv.startServiceMayBind(WatchDogService.class);
        }

//        Log.d("HelloService","JobSchedulerService onStartJob ");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            JobInfo.Builder builder = new JobInfo.Builder(HASH_CODE, new ComponentName(DaemonEnv.sApp, JobSchedulerService.class));

            builder.setMinimumLatency(DaemonEnv.getWakeUpInterval());
            builder.setOverrideDeadline(DaemonEnv.getWakeUpInterval() + 5000);

            builder.setPersisted(true);
            JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
            scheduler.schedule(builder.build());

            HelloService.tellMeToStartService(DaemonEnv.sApp);
        }

        DaemonEnv.startServiceMayBind(DaemonEnv.sServiceClass);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}