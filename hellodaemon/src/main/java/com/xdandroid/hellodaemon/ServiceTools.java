package com.xdandroid.hellodaemon;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

import java.util.List;

public class ServiceTools {

    public static boolean wetherServiceAlive(Context context, String className) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceInfos = activityManager.getRunningServices(150);

        if (null == serviceInfos || serviceInfos.size() < 1) {
            return false;
        }

        for (int i = 0; i < serviceInfos.size(); i++) {

            if (serviceInfos.get(i).service.getPackageName().equals(context.getPackageName())&&serviceInfos.get(i).service.getClassName().contains(className)) {
                isRunning = true;

                break;
            }
        }
        return isRunning ;
    }

}
