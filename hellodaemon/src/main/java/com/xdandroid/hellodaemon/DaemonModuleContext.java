package com.xdandroid.hellodaemon;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.workout.base.TimeUtil;
import com.xdandroid.hellodaemon.service.KeepAliveService;

import org.greenrobot.eventbus.EventBus;


public class DaemonModuleContext {

    private static Context sContext;
    private static boolean sDebug;

    private static Handler sHandler = new Handler(Looper.getMainLooper()) {

    };

    public static void init(Context context, boolean debug) {
        sContext = context;
        sDebug = debug;
        initService();
    }

    public static Context getContext() {
        return sContext;
    }

    public static boolean isDebug() {
        return sDebug;
    }

    public static void initService() {
        // 守护进程初始化
        DaemonEnv.initialize(sContext, KeepAliveService.class, (int) TimeUtil.MINUTE_IN_MILLIS);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            //启动守护服务，运行在:watch子进程中
            DaemonEnv.startServiceMayBind(WatchDogService.class);
        }else{
            // 启动Service
            try{
                sContext.startService(KeepAliveService.getEntryIntent(getContext(), KeepAliveService.ENTRY_MAIN));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public static void postDelay(Runnable runnable, long delay) {
        sHandler.postDelayed(runnable, delay);
    }



}
