package com.xdandroid.hellodaemon.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.xdandroid.hellodaemon.DaemonModuleContext;
import com.xdandroid.hellodaemon.utils.SdkUtil;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by chenhewen on 2018/7/28.
 */

public class WorkoutServiceDispatcher {

    private static Map<String, BaseServiceListener> sMap = new HashMap<>();

    public static void startService(BaseServiceListener listener) {
        Context context = DaemonModuleContext.getContext();
        Intent intent = listener.getIntent();
        checkIntent(intent);
        String action = listener.getAction();
        checkAction(action);

        sMap.put(action, listener);
        if (SdkUtil.atLeast26()) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    public static void setPending(BaseServiceListener listener) {
        Intent intent = listener.getIntent();
        checkIntent(intent);
        String action = listener.getAction();
        checkAction(action);

        sMap.put(action, listener);
    }

    public void onStartCommand(Service service, Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            BaseServiceListener listener = sMap.remove(action);
            if (listener != null) {
                listener.onStartCommand(service, intent);
            }
        }

    }

    private static void checkIntent(Intent intent) {
        if (intent == null) {
            throw new IllegalArgumentException("intent of service listener should not be null");
        }
    }

    private static void checkAction(String action) {
        if (TextUtils.isEmpty(action)) {
            throw new IllegalArgumentException("action of intent should not be empty");
        }
    }

}
