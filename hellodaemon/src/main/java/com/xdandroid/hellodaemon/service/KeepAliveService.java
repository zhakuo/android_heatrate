package com.xdandroid.hellodaemon.service;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.workout.base.receiver.event.ScreenOnEvent;
import com.workout.base.receiver.event.TimeChangedEvent;
import com.workout.base.service.HelloService;
import com.xdandroid.hellodaemon.AbsWorkService;
import com.xdandroid.hellodaemon.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * KeepAliveService
 * Created by chenhewen on 2018/7/10.
 */

public class KeepAliveService extends AbsWorkService {

    private static final String ENTRY = "entry";

    public static final int ENTRY_MAIN = 0;
    public static final int ENTRY_ALARM = 1;
    public static final int ENTRY_SCREEN_OFF = 2;
    public static final int ENTRY_TIME_CHANGE = 3;
    public static final int ENTRY_DISPATCHER = 4;

    public static final int HASH_CODE = 3;

    private AlarmManager mAlarmManager;
    private WorkoutServiceDispatcher mWorkoutServiceDispatcher;

    public static Intent getEntryIntent(Context context, int entry) {
        Intent intent = new Intent(context, KeepAliveService.class);
        intent.putExtra(ENTRY, entry);

        return intent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
        mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mWorkoutServiceDispatcher = new WorkoutServiceDispatcher();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTimeChange(TimeChangedEvent event) {
        if (enableDaemon()) {
            Intent entryIntent = KeepAliveService.getEntryIntent(getApplicationContext(), KeepAliveService.ENTRY_TIME_CHANGE);
            startService(entryIntent);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onScreenOnOff(ScreenOnEvent event) {
        if (enableDaemon()) {
            Intent entryIntent = KeepAliveService.getEntryIntent(getApplicationContext(), KeepAliveService.ENTRY_SCREEN_OFF);
            startService(entryIntent);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (mWorkoutServiceDispatcher != null) {
            mWorkoutServiceDispatcher.onStartCommand(this, intent);
        }

        try{

//            Log.d("HelloService","HelloService oncreate ");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                String channel;
                Notification.Builder nbuilder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    channel = createChannel();
                    nbuilder = new Notification.Builder(this, channel);
                }else {
                    channel = "";
                    nbuilder = new Notification.Builder(this);
                }


                nbuilder.setSmallIcon(com.workout.base.R.drawable.ic_icon);
                startForeground(HASH_CODE, nbuilder.build());
                startService(new Intent(this, KeepLiveInnerService.class));
            }else{
                startForeground(HASH_CODE, new Notification());
            }

        }catch (Exception e){

            e.printStackTrace();
        }
        return START_STICKY;
    }

    @NonNull
    @TargetApi(26)
    private synchronized String createChannel() {
        try {
            NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

            String name = "Workout Reminder";
            int importance = NotificationManager.IMPORTANCE_LOW;

            NotificationChannel mChannel = new NotificationChannel("workout girl channel", name, importance);

            mChannel.enableLights(true);
            mChannel.setLightColor(Color.BLUE);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            } else {
                stopSelf();
            }
        }catch (Exception e ){
            e.printStackTrace();
        }
        return "workout girl channel";
    }

    @Override
    public Boolean shouldStopService(Intent intent, int flags, int startId) {
        return false;
    }

    @Override
    public void startWork(Intent intent, int flags, int startId) {

    }

    @Override
    public void stopWork(Intent intent, int flags, int startId) {

    }

    @Override
    public Boolean isWorkRunning(Intent intent, int flags, int startId) {
        return true;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent, Void alwaysNull) {
        return null;
    }

    @Override
    public void onServiceKilled(Intent rootIntent) {

    }

    public static boolean enableDaemon() {
//        return Build.VERSION.SDK_INT < Build.VERSION_CODES.O;
        return true;
    }

    public static class KeepLiveInnerService extends Service{
        @Override
        public IBinder onBind(Intent intent){
            return null;
        }
        @Override
        public void onCreate(){
            super.onCreate();
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                Notification.Builder nbuilder = new Notification.Builder(this);
                nbuilder.setSmallIcon(R.drawable.ic_icon);
                startForeground(HASH_CODE, nbuilder.build());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        stopForeground(true);
                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        notificationManager.cancel(HASH_CODE);
                        stopSelf();
                    }
                }, 200);
            }
        }
    }

}
