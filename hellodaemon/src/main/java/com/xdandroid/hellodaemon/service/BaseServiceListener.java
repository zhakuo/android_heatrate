package com.xdandroid.hellodaemon.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.xdandroid.hellodaemon.DaemonModuleContext;


/**
 * Created by chenhewen on 2018/7/28.
 */

public abstract class BaseServiceListener {

    public Intent createServiceIntent() {
        Context context = DaemonModuleContext.getContext();
        return KeepAliveService.getEntryIntent(context, KeepAliveService.ENTRY_DISPATCHER);
    }

    public abstract String getAction();

    public Intent getIntent() {
        String action = getAction();
        if (!TextUtils.isEmpty(action)) {
            Intent serviceIntent = createServiceIntent();
            serviceIntent.setAction(action);
            return serviceIntent;
        }

        return null;
    }

    public abstract void onStartCommand(Service service, Intent intent);
}
