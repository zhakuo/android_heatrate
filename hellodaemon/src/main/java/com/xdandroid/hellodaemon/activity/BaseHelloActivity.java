package com.xdandroid.hellodaemon.activity;

import android.app.Activity;
import android.os.Bundle;

import com.workout.base.service.HelloService;

public class BaseHelloActivity extends Activity {

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);


    }

    protected void onResume() {
        super.onResume();
        HelloService.tellMeToStartService(this);
        finish();
    }

}

