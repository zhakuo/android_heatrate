package com.xdandroid.hellodaemon.utils;

import android.os.Build;

/**
 * Created by chenhewen on 2018/7/26.
 */

public class SdkUtil {

    /**
     * >= 4.4
     * @return
     */
    public static boolean atLeast19() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    /**
     * >= 8.0
     * @return
     */
    public static boolean atLeast26() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }
}
