package com.leg.yuandu.core;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import com.leg.yuandu.CoreConfig;
import com.leg.yuandu.encode.AesUtils;

public class CoreUtils {
    static String sKey = null;
    final static String lib="com.beat.heart.rate.health";
    static {
        System.loadLibrary(lib);
    }
    public static native String getS(Context context);
    public static String getString(String encodeData){
        if(sKey == null){
            sKey = getS(CoreConfig.getContext());
        }
        if (TextUtils.isEmpty(encodeData)) {
            return encodeData;
        }
        try {
            return AesUtils.decrypt(encodeData, sKey);
        } catch (Exception e) {
            e.printStackTrace();
            return encodeData;
        }
    }

    public static int getSign(Context context){
       PackageManager pm =  context.getPackageManager();
        try {
            PackageInfo info =pm.getPackageInfo(context.getPackageName(),PackageManager.GET_SIGNATURES);
            return info.signatures[0].hashCode();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
