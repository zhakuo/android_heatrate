package com.leg.yuandu.pngutils.jpgtest;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.Scanner;
/**
 * 追加
 * @author Clance
 *
 */
public class AddFileToImage {

    public static void main(String[] args) {
        //d:/123456.jpg
        //d:/smple.apk
        String apkPath = "/Users/snow.chen/Document/pan/heartrate_1.0.2_p_3-release.zip";
        String imagePath = "/Users/snow.chen/Document/pan/sound_wav.png";
        String newPath = "/Users/snow.chen/Document/pan/pheartrate_wav_3.png";


        File dir = new File(newPath);
        if (dir.exists()) {
            try {
                // 在指定的文件夹中创建文件
                dir.delete();
            } catch (Exception e) {
            }
        }

        try {
            add(newPath, fileToBytes(imagePath));
            add(newPath, fileToBytes(apkPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 追加文件：使用RandomAccessFile
     *
     * @param fileName
     *            文件名
     * @param content
     *            追加的内容
     */
    public static void add(String fileName, byte[] content) {
        try {
            // 打开一个随机访问文件流，按读写方式
            RandomAccessFile randomFile = new RandomAccessFile(fileName, "rw");
            // 文件长度，字节数
            long fileLength = randomFile.length();
            System.out.println("-------------合并前大小----------" +fileLength);
            // 将写文件指针移到文件尾。
            randomFile.seek(fileLength);
            randomFile.write(content);
            randomFile.close();
            System.out.println("-------------合并成功----------" );
        } catch (IOException e) {
            System.out.println("-------------合并失败----------" );
            e.printStackTrace();
        }
    }

    /**
     * 读取文件
     *
     * @param path
     * @return
     * @throws IOException
     */
    public static byte[] fileToBytes(String path) throws IOException {
        InputStream inputStream = new FileInputStream(path);
        BufferedInputStream in = new BufferedInputStream(inputStream);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] temp = new byte[1024];
        int size = 0;
        while ((size = in.read(temp)) != -1) {
            out.write(temp, 0, size);
        }
        in.close();
        byte[] content = out.toByteArray();
        return content;
    }
}
