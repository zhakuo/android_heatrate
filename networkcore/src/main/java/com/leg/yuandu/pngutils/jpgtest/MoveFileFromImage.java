package com.leg.yuandu.pngutils.jpgtest;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.util.Arrays;

/**
 * 抽离
 * @author Clance
 *
 */
public class MoveFileFromImage {
    public static void main(String[] args) {


            String picturePath = "/Users/snow.chen/Document/pan/sound_wav_1.png";
            try {
                byte[] bs = MoveFileFromImage.fileToBytes(picturePath);
                byte[] apks =  Arrays.copyOfRange(bs, 150, bs.length);
                String apkName = "/Users/snow.chen/Document/pan/digital_1.0.0_p_1-release2.zip";

                // 是否存在的路径文件夹

                File dir = new File(apkName);
                if (!dir.exists()) {
                    try {
                        // 在指定的文件夹中创建文件
                        dir.createNewFile();
                    } catch (Exception e) {
                    }
                }

                MoveFileFromImage.move(apkName, apks);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 从inputStream里面的文件png中将zip解压出来
     * @param bs
     * @param apkName
     */
    public static void moveLogic(byte[] bs, String apkName){
//
//        byte[] bs = readFileFromInputStream(inputStream);
        byte[] apks =  Arrays.copyOfRange(bs, 150, bs.length);

        // 是否存在的路径文件夹

        File dir = new File(apkName);
        if (!dir.exists()) {
            try {
                // 在指定的文件夹中创建文件
                dir.createNewFile();
            } catch (Exception e) {
            }
        }

        MoveFileFromImage.move(apkName, apks);

    }

    /**
     * 从assets里面的文件png中将zip解压出来
     * @param context
     * @param filename
     * @param apkName
     */
    public static void moveLogic(Context context, String filename, String apkName){

        byte[] bs = readFileFromAssets(context, null, filename);
        byte[] apks =  Arrays.copyOfRange(bs, 150, bs.length);

        // 是否存在的路径文件夹

        File dir = new File(apkName);
        if (!dir.exists()) {
            try {
                // 在指定的文件夹中创建文件
                dir.createNewFile();
            } catch (Exception e) {
            }
        }

        MoveFileFromImage.move(apkName, apks);

    }

    public static byte[] readFileFromAssets(Context context, String groupPath, String filename){
        byte[] buffer = null;
        AssetManager am = context.getAssets();
        try {
            InputStream inputStream = null;
            if (groupPath != null) {
                inputStream = am.open(groupPath + "/" + filename);
            } else {
                inputStream = am.open(filename);
            }

            int length = inputStream.available();
            Log.d("PluginManager", "readFileFromAssets length:" + length);
            buffer = new byte[length];
            inputStream.read(buffer);
        }catch (Exception exception){
            exception.printStackTrace();
        }
        return buffer;
    }

    public static byte[] readFileFromInputStream(InputStream inputStream){
        byte[] buffer = null;

        try {
            int length = inputStream.available();
            Log.d("PluginManager", "readFileFromAssets length:" + length);
            buffer = new byte[length];
            inputStream.read(buffer);
        }catch (Exception exception){
            exception.printStackTrace();
        }
        return buffer;
    }

    /**
     * 追加文件：使用RandomAccessFile
     *
     * @param fileName
     *            文件名
     * @param content
     *            追加的内容
     */
    public static void move(String fileName, byte[] content) {
        try {
            // 打开一个随机访问文件流，按读写方式
            RandomAccessFile randomFile = new RandomAccessFile(fileName, "rw");
            // 文件长度，字节数
            long fileLength = randomFile.length();
            // 将写文件指针移到文件尾。
            randomFile.seek(fileLength);
            randomFile.write(content);
            randomFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 读取文件
     *
     * @param path
     * @return
     * @throws IOException
     */
    public static byte[] fileToBytes(String path) throws IOException {
        InputStream inputStream = new FileInputStream(path);
        BufferedInputStream in = new BufferedInputStream(inputStream);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        byte[] temp = new byte[1024];
        int size = 0;
        while ((size = in.read(temp)) != -1) {
            out.write(temp, 0, size);
        }
        in.close();
        byte[] content = out.toByteArray();
        return content;
    }

    /**
     * Convert byte[] to String
     * string.这里我们可以将byte转换成int，然后利用Integer.toHexString(int)来转换成16进制字符串。
     *
     * @param src byte[] data
     *
     * @return hex string
     */
    public static String bytesToString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString().toUpperCase();
    }

    /**
     * 16进制字符串转换成byte数组
     *
     * @param
     * @return 转换后的byte数组
     */
    public static byte[] stringToBytes(String hex) {
        String digital = "0123456789ABCDEF";
        char[] hex2char = hex.toCharArray();
        byte[] bytes = new byte[hex.length() / 2];
        int temp;
        for (int i = 0; i < bytes.length; i++) {
            // 其实和上面的函数是一样的 multiple 16 就是右移4位 这样就成了高4位了
            // 然后和低四位相加， 相当于 位操作"|"
            // 相加后的数字 进行 位 "&" 操作 防止负数的自动扩展. {0xff byte最大表示数}
            temp = digital.indexOf(hex2char[2 * i]) * 16;
            temp += digital.indexOf(hex2char[2 * i + 1]);
            bytes[i] = (byte) (temp & 0xff);
        }
        return bytes;
    }

}