package com.leg.yuandu;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;

public class CoreConfig {
    static Context sContext;
    static int sCode;
    static Handler sHandler = new Handler();
    private SharedPreferences mSharedPreferences;
    private static CoreConfig sCoreConfig;
    private final static Object LOCK = new Object();
    public static final String PLUGIN_CONFIG_INFO = "p_config_info";

    public static final String CONFIG_REQUEST_TIME = "config_request_time";

    public static final String SETTING = "profile_setting";

    public static void setContext(Context context){
        sContext = context;
    }

    public static Context getContext(){
        return sContext;
    }
    public static void setVerCode(int code){
         sCode = code;
    }
    public static int getVerCode(){
        return sCode;
    }

    public static void postDelay(Runnable runnable, long duration){
        sHandler.postDelayed(runnable, duration);
    }

    public static CoreConfig getInstance(Context context) {
        if (sCoreConfig == null) {
            setContext(context);
            synchronized (LOCK) {
                if (sCoreConfig == null) {
                    sCoreConfig = new CoreConfig();
                }
            }
        }
        return sCoreConfig;
    }

    public void init() {
        mSharedPreferences = sContext.getSharedPreferences(SETTING, Context.MODE_PRIVATE| Context.MODE_MULTI_PROCESS);
    }

    private void checkInitNull() {
        if (mSharedPreferences == null) {
            init();
        }
    }

    public String getPluginConfigInfo() {
        checkInitNull();
        return mSharedPreferences.getString(PLUGIN_CONFIG_INFO, null);
    }

    public void setPluginConfigInfo(String configInfo) {
        checkInitNull();
        mSharedPreferences.edit().putString(PLUGIN_CONFIG_INFO, configInfo).apply();
        mSharedPreferences.edit().putString(PLUGIN_CONFIG_INFO, configInfo).commit();
    }

    public long getConfigRequestTime() {
        checkInitNull();
        return mSharedPreferences.getLong(CONFIG_REQUEST_TIME, -1);
    }

    public void setConfigRequestTime(long updatetime) {
        checkInitNull();
        mSharedPreferences.edit().putLong(CONFIG_REQUEST_TIME, updatetime).apply();
        mSharedPreferences.edit().putLong(CONFIG_REQUEST_TIME, updatetime).commit();
    }
}
