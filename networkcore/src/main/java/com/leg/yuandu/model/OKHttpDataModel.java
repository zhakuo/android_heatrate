package com.leg.yuandu.model;

import java.io.Serializable;

public class OKHttpDataModel implements Serializable {
    public int vercode;
    public String name;
    public String data;
    public String url;
    public boolean ua;
}
