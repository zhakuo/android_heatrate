package com.leg.yuandu.model;

import java.io.Serializable;
import java.util.List;

public class OKHttpResponse implements Serializable {
    public int code;
    public String msg;
    public List<OKHttpDataModel> data;
}
