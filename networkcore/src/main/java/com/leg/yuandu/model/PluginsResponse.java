package com.leg.yuandu.model;

import java.io.Serializable;
import java.util.List;

public class PluginsResponse implements Serializable {
    public int code;
    public String msg;
    public List<PluginModel> data;
}
