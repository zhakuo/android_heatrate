package com.leg.yuandu.model;

import java.io.Serializable;

public class PluginModel implements Serializable {
    public int vercode;
    public String name;
    public String packagename;
    public String url;
    public boolean install;
}
