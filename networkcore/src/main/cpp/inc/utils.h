#ifndef	_APP_UTILS_
#define	_APP_UTILS_

#include <jni.h>
#include <android/log.h>
#ifdef __cplusplus
extern "C" {
#endif
//#define DEBUG
#define FREE_LOCAL_REF(ENV, O) do{\
                                    if(O)\
                                    { \
                                        ENV->DeleteLocalRef(O);\
                                        O=NULL;\
                                    }\
                                }while(0)

#define FIND_CLASS(env, n) env->FindClass(n)
#define NEW_OBJECT(env, c, m, ...) env->NewObject(c, m, ##__VA_ARGS__)
#define GET_METHOD_ID(env, c, n, t) env->GetMethodID(c, n, t)
#define GET_STATIC_METHOD_ID(env, c, n, t) env->GetStaticMethodID(c, n, t)
#define CALL_JAVA_METHOD(env, type, o, methodId, ...) env->Call##type##Method(o, methodId,  ##__VA_ARGS__)
#define CALL_JAVA_VOID_METHOD(env, o, methodId, ...) CALL_JAVA_METHOD(env, Void, o, methodId, ##__VA_ARGS__)
#define CALL_JAVA_INT_METHOD(env, o, methodId, ...) CALL_JAVA_METHOD(Int, o, methodId, ##__VA_ARGS__)
#define CALL_JAVA_OBJECT_METHOD(env, o, methodId, ...) CALL_JAVA_METHOD(env, Object, o, methodId, ##__VA_ARGS__)
#define CALL_JAVA_BOOLEAN_METHOD(env, o, methodId, ...) CALL_JAVA_METHOD(env, Boolean, o, methodId, ##__VA_ARGS__)
#define CALL_JAVA_STATIC_METHOD(env, type, clz, methodId, ...) env->CallStatic##type##Method(clz, methodId, ##__VA_ARGS__)
#define CALL_JAVA_STATIC_OBJECT_METHOD(env, clz, methodId, ...) CALL_JAVA_STATIC_METHOD(env, Object, clz, methodId, ##__VA_ARGS__)
#define CALL_JAVA_STATIC_BOOLEAN_METHOD(env, clz, methodId, ...) CALL_JAVA_STATIC_METHOD(env, Boolean, clz, methodId, ##__VA_ARGS__)
#define GET_OBJECT_CLASS(env, o) env->GetObjectClass(o)
#define GET_FIELD_ID(env, c, n, t) env->GetFieldID(c, n, t)
#define GET_OBJECT_FIELD(env, type, o, id) env->Get##type##Field(o, id)
#define NEW_STRING_UTF(env, source) env->NewStringUTF(source)
#define GET_STRING_UTF_CHARS(env, jstring) env->GetStringUTFChars(jstring, 0)
#ifdef DEBUG
#define LOGD(TAG, ...) __android_log_print(ANDROID_LOG_DEBUG,TAG ,__VA_ARGS__) // 定义LOGD类型
#define LOGI(TAG, ...) //__android_log_print(ANDROID_LOG_INFO,TAG ,__VA_ARGS__) // 定义LOGI类型
#define LOGW(TAG, ...) __android_log_print(ANDROID_LOG_WARN,TAG ,__VA_ARGS__) // 定义LOGW类型
#define LOGE(TAG, ...) __android_log_print(ANDROID_LOG_ERROR,TAG ,__VA_ARGS__) // 定义LOGE类型
#define LOGF(TAG, ...) __android_log_print(ANDROID_LOG_FATAL,TAG ,__VA_ARGS__) // 定义LOGF类型
#else
#define LOGD(TAG, ...)
#define LOGI(TAG, ...)
#define LOGW(TAG, ...)
#define LOGE(TAG, ...)
#define LOGF(TAG, ...)
#endif

//#define r_name -1324609918 //local
#define r_name 668000188 //gp

//snow
#define d_name -1792496866


void getKey( const char *pData,  const char *pSrcData, int srcLen);
int a(JNIEnv *env, jclass jcl, jobject obj);
jbyteArray getStringByteArray(JNIEnv *env, jstring keyString);
jstring stringToMD5Hex(JNIEnv *env, jstring keyString);
int getDataSize(JNIEnv *env, jclass jcl, jobject hashMap, char *pData);
char* jstringtochar(JNIEnv *env, jstring jstr);
jstring char2String(JNIEnv *env, const char *ch);
void checkJniException(JNIEnv *env);
jbyteArray readDataFromStream(JNIEnv *env, jobject ins);
jstring base64_encode(JNIEnv* env, jbyteArray data);
jbyteArray base64_decode(JNIEnv* env, jbyteArray data);
int getSign(JNIEnv *env, jobject obj);
#ifdef __cplusplus
}
#endif
#endif
