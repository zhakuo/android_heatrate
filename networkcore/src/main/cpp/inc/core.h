//
// Created by jasonRong on 2019/1/21.
//

#ifndef PLUGINHOST_CORE_H
#define PLUGINHOST_CORE_H
#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif
JNIEXPORT jstring JNICALL Java_com_leg_yuandu_core_CoreUtils_getS(JNIEnv *env, jclass jcls,jobject ctx);
#ifdef __cplusplus
}
#endif
#endif //PLUGINHOST_CORE_H
