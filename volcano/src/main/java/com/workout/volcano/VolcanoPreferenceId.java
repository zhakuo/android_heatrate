package com.workout.volcano;

public class VolcanoPreferenceId {

    /**
     * 首次安装时间, 该preference对应global_sp
     */
    public static final String FIRST_INSTALL_MILLIS = "first_install_millis";

    public static final String LAST_DISCONNECT_MILLIS = "last_disconnect_millis";

    public static final String VOLCANO_ENABLE = "volcano_enable";

    public static final String VOLCANO_DISABLE_MILLIS = "volcano_disable_millis";

    public static final String VOLCANO_DISABLE_TIMES = "volcano_disable_times";

    public static final String VOLCANO_RESTRICTED = "volcano_restricted";

    public static final String VOLCANO_RESTRICTED_DEV_OPTS_ENABLE = "volcano_restricted_dev_opts_enable";

    public static final String VOLCANO_RESTRICTED_DEBUGGING_ENABLE = "volcano_restricted_debugging_enable";

    public static final String VOLCANO_RESTRICTED_USB_CHARGING_ENABLE = "volcano_restricted_usb_charging_enable";

    public static final String VOLCANO_RESTRICTED_SIM_CARD_ENABLE = "volcano_restricted_sim_card_enable";

    public static final String VOLCANO_RESTRICTED_PASS_ENOUGH_DAYS_ENABLE = "volcano_restricted_pass_enough_days_enable";

    public static final String VOLCANO_RESTRICTED_NETWORK_TIME_ENABLE = "volcano_restricted_network_time_enable";

    public static final String VOLCANO_REMOTE_ENABLE = "volcano_remote_enable";
}
