package com.workout.volcano.remote;

import android.util.Log;

import com.workout.base.RemoteConfigId;
import com.workout.base.TimeUtil;
import com.workout.base.iplogic.IpUtils;
import com.workout.base.preferences.SPConstant;
import com.workout.base.preferences.SharedPreferencesUtil;
import com.workout.volcano.logic.spec.BuyUserLogic;

public class VolcanoRemoteManager {

    private static final class VolcanoRemoteManagerHolder {
        private static VolcanoRemoteManager sVolcanoRemoteManager = new VolcanoRemoteManager();
    }

    public static VolcanoRemoteManager getInstance() {
        return VolcanoRemoteManagerHolder.sVolcanoRemoteManager;
    }

    private VolcanoRemoteManager() {

    }

    public VolcanoRemoteBean getRemoteConfig() {
        String volcanoConfig = getValue(RemoteConfigId.volcano_config_key);
        String volcanoOrgConfig = getValue(RemoteConfigId.volcano_org_config_key);
        if(!BuyUserLogic.isPromoteUser() && !IpUtils.getInstance().checkSpeciaCountry()){
            volcanoConfig = volcanoOrgConfig;
            Log.d("adiplimit", " remote config use orgnic config :" +  volcanoConfig );
        }else{
            Log.d("adiplimit", " remote config use buy config :" + volcanoConfig );
        }
        VolcanoRemoteBean bean = null;
        if(volcanoConfig != null && volcanoConfig != ""){
            try {
                bean = parse(volcanoConfig);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }else{

        }


        if (bean == null) {
            try {
                bean = parse(RemoteConfigId.volcano_config_value);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }

        return bean;
    }

    private VolcanoRemoteBean parse(String volcanoConfig) {
        String[] split = volcanoConfig.split(",");
        //0 开关，1 安装延迟多久出，2 更新延迟多久出， 3 最大重启次数，4 间隔多久重启
        boolean volcanoSwitch = "1".equals(split[0]);
        long volcanoNewInstallDelayMillis = Integer.parseInt(split[1]) * TimeUtil.HOUR_IN_MILLIS;
        long volcanoUpdateDelayMillis = Integer.parseInt(split[2]) * TimeUtil.HOUR_IN_MILLIS;
        int maxReopenCount = Integer.parseInt(split[3]);
        long reopenGapMillis = Integer.parseInt(split[4]) * TimeUtil.DAY_IN_MILLIS;

        return new VolcanoRemoteBean(volcanoConfig, volcanoSwitch, volcanoNewInstallDelayMillis, volcanoUpdateDelayMillis, maxReopenCount, reopenGapMillis);
    }

    public static class VolcanoRemoteBean {
        private String mOriginalStr;

        private boolean mVolcanoSwitch;
        private long mVolcanoNewInstallDelayMillis;
        private long mVolcanoUpdateDelayMillis;
        private int mMaxReopenCount;
        private long mReopenGapMillis;


        public VolcanoRemoteBean(String originalStr, boolean volcanoSwitch, long volcanoNewInstallDelayMillis, long volcanoUpdateDelayMillis, int maxReopenCount, long reopenGapMillis) {
            mOriginalStr = originalStr;
            mVolcanoSwitch = volcanoSwitch;
            mVolcanoNewInstallDelayMillis = volcanoNewInstallDelayMillis;
            mVolcanoUpdateDelayMillis = volcanoUpdateDelayMillis;
            mMaxReopenCount = maxReopenCount;
            mReopenGapMillis = reopenGapMillis;
        }

        public String getOriginalStr() {
            return mOriginalStr;
        }

        public boolean isVolcanoSwitchOpen() {
            return mVolcanoSwitch;
        }

        public long getVolcanoNewInstallDelayMillis() {
            return mVolcanoNewInstallDelayMillis;
        }

        public long getVolcanoUpdateDelayMillis() {
            return mVolcanoUpdateDelayMillis;
        }

        public int getMaxReopenCount() {
            return mMaxReopenCount;
        }

        public long getReopenGapMillis() {
            return mReopenGapMillis;
        }
    }

    /**
     * 获取SharedPreferences 的string型值
     * @param key
     * @return
     */
    public static String getValue(String key){
        String value = SharedPreferencesUtil.get(SPConstant.DEFAULT_SHAREPREFERENCES_FILE).getString(key, "");
        return value;
    }
}
