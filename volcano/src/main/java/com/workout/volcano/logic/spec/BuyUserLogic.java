package com.workout.volcano.logic.spec;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.workout.base.BuildConfig;
import com.workout.base.global.GlobalPreferenceFileName;
import com.workout.volcano.VolcanoContext;

public class BuyUserLogic implements IVolcanoLogic {

    public final static String KEY_IS_BUY_USER = "key_is_buy_user";
    public final static String KEY_IS_SUB_USER = "key_is_sub_user";
    public final static String KEY_IS_SUB_PRODUCT = "key_is_sub_product";
    public final static String KEY_IS_SUB_USED = "key_is_sub_used";
    public final static String KEY_IS_REMOVEDADS_USED = "key_is_removeads";

    @Override
    public boolean isPass(Context context) {

        return isPromoteUser();
    }

    public static void setPromoteUser(boolean isPromoteUser) {
        SharedPreferences sharedPreferences =  VolcanoContext.getContext().getSharedPreferences(
                GlobalPreferenceFileName.BASE_SP_FILE_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(KEY_IS_BUY_USER, isPromoteUser).commit();
    }

    public static boolean isPromoteUser() {

        SharedPreferences sharedPreferences = VolcanoContext.getContext().getSharedPreferences(
                GlobalPreferenceFileName.BASE_SP_FILE_NAME, Context.MODE_PRIVATE);
        boolean isPromoteUser = sharedPreferences.getBoolean(KEY_IS_BUY_USER, false);

        if (BuildConfig.DEBUG) {
            Log.d("adiplimit", "isPromoteUser() isPromoteUser =" + isPromoteUser);
        }
        return isPromoteUser;
    }

    public static void setSubUser(boolean isSubUser, String product) {
        SharedPreferences sharedPreferences =  VolcanoContext.getContext().getSharedPreferences(
                GlobalPreferenceFileName.BASE_SP_FILE_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(KEY_IS_SUB_USER, isSubUser).commit();
        sharedPreferences.edit().putString(KEY_IS_SUB_PRODUCT, product).commit();
        sharedPreferences.edit().putBoolean(KEY_IS_SUB_USED, true).commit();

    }

    /**
     * 判断是否订阅用户
     * @return
     */
    public static boolean isSubUser() {

        SharedPreferences sharedPreferences = VolcanoContext.getContext().getSharedPreferences(
                GlobalPreferenceFileName.BASE_SP_FILE_NAME, Context.MODE_PRIVATE);
        boolean isSubUser = sharedPreferences.getBoolean(KEY_IS_SUB_USER, false);

        //TODO
        /*if(com.workout.base.GlobalConst.DEBUG) {
            Llog.d("adiplimit", "isSubUser() isSubUser =" + isSubUser);
            return true;
        }*/
        return isSubUser;
    }

    /**
     * 判断曾经是否订阅过
     * @return
     */
    public static boolean getSubUsed() {

        SharedPreferences sharedPreferences = VolcanoContext.getContext().getSharedPreferences(
                GlobalPreferenceFileName.BASE_SP_FILE_NAME, Context.MODE_PRIVATE);


        return sharedPreferences.getBoolean(KEY_IS_SUB_USED, false);
    }

    public static String getSubProduct() {

        SharedPreferences sharedPreferences = VolcanoContext.getContext().getSharedPreferences(
                GlobalPreferenceFileName.BASE_SP_FILE_NAME, Context.MODE_PRIVATE);
        String isSubUser = sharedPreferences.getString(KEY_IS_SUB_PRODUCT, null);

        return isSubUser;
    }

    public static String getPromoteUserChannel() {

        SharedPreferences sharedPreferences= VolcanoContext.getContext().getSharedPreferences(
                GlobalPreferenceFileName.BASE_SP_FILE_NAME, Context.MODE_PRIVATE);

        String buyUserChannel = sharedPreferences.getString("user_type","");

        return buyUserChannel;
    }

    public static void savePromoteUserChannel(String buyUserChannel) {
        if (TextUtils.isEmpty(buyUserChannel)) {
            return;
        }
        SharedPreferences sharedPreferences=  VolcanoContext.getContext().getSharedPreferences(
                GlobalPreferenceFileName.BASE_SP_FILE_NAME, Context.MODE_PRIVATE);

        sharedPreferences.edit().putString("user_type",buyUserChannel).apply();
        sharedPreferences.edit().putString("user_type",buyUserChannel).commit();

    }

    public static void saveIsRemoveads(boolean isRemove) {

        SharedPreferences sharedPreferences=  VolcanoContext.getContext().getSharedPreferences(
                GlobalPreferenceFileName.BASE_SP_FILE_NAME, Context.MODE_PRIVATE);

        sharedPreferences.edit().putBoolean(KEY_IS_REMOVEDADS_USED, isRemove).apply();
        sharedPreferences.edit().putBoolean(KEY_IS_REMOVEDADS_USED, isRemove).commit();

    }

    public static boolean getIsRemoveads() {

        SharedPreferences sharedPreferences = VolcanoContext.getContext().getSharedPreferences(
                GlobalPreferenceFileName.BASE_SP_FILE_NAME, Context.MODE_PRIVATE);
        boolean isRemove = sharedPreferences.getBoolean(KEY_IS_REMOVEDADS_USED, false);
        return isRemove;
    }

    public static boolean isProductTrialUser() {
        return true;
    }

    public static boolean shouldNotShowAds(){
        if(!isSubUser() && !getIsRemoveads()){
            return false;
        }else{
            return true;
        }
    }


}
