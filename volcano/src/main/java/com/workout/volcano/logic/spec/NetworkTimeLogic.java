package com.workout.volcano.logic.spec;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.workout.base.GlobalConst;
import com.workout.base.Logger;
import com.workout.base.TimeUtil;
import com.workout.base.global.GlobalPreferenceFileName;
import com.workout.base.iplogic.IpUtils;
import com.workout.volcano.VolcanoContext;

import java.net.NetworkInterface;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;

public class NetworkTimeLogic implements IVolcanoLogic {

    public static final long SAME_TIME_GAP = TimeUtil.SECOND_IN_MILLIS * 60 * 24 ;
    public static final long HOUR_TIME_GAP = TimeUtil.SECOND_IN_MILLIS * 60 * 1 ;

    public static final String IS_VPN  = "is_vpn";
    public static final String UPDATE_TIME  = "update_ime";
    public static final String NET_TIME  = "net_ime";



    public static Date getNetTime () {
//        String webUrl = "http://www.ntsc.ac.cn";//中国科学院国家授时中心
        String webUrl = "http://www.google.com";
        try {
            long lastTime = getUpdateTime();
            long currentTime = System.currentTimeMillis();
//            Logger.d("adiplimit", "< HOUR_TIME_GAP " + currentTime + "  lasttime " + lastTime);

            if(Math.abs(currentTime - lastTime) < HOUR_TIME_GAP ){

                return new Date(getNetWorkTime());
            }

            URL url = new URL(webUrl);
            URLConnection uc = url.openConnection();
            uc.setReadTimeout(15000);
            uc.setConnectTimeout(15000);
            uc.connect();
            long correctTime = uc.getDate();
            //保存临时值，1小时请求间隔
            saveNetWorkTime(correctTime);
            saveUpdateTime(System.currentTimeMillis());

            return new Date(correctTime);
        } catch (Exception e) {
            e.printStackTrace();
            //拿前一天的时间
            long lastday = (System.currentTimeMillis() - SAME_TIME_GAP);
            if(GlobalConst.DEBUG){
                lastday = System.currentTimeMillis();
            }
            //保存临时值，1小时请求间隔
            saveNetWorkTime(lastday);
            saveUpdateTime(System.currentTimeMillis());
            return new Date(lastday);
        }
    }

    @Override
    public boolean isPass(Context context) {
        //先判断是否有过vpn记录
        if(!getHasVpn()){
            if(isVpnConnected()){
                saveHasVpn(true);
//                Logger.d("adiplimit", "network has vpn ");
                if(VolcanoContext.isDebug()){
//                    Logger.d("adiplimit", "network has vpn , here is debug , return true ");
                    return true;
                }
                return false;
            }
        }else{
            if(VolcanoContext.isDebug()){
//                Logger.d("adiplimit", "network once has vpn before ");
                return true;
            }

            return false;
        }

        //是否与google接口时间差在24小时内
        Date netTime = getNetTime();
        if (netTime != null) {
            if(VolcanoContext.isDebug()){
                if(Math.abs(netTime.getTime() - System.currentTimeMillis()) > SAME_TIME_GAP)
//                    Logger.d("adiplimit", "time is change, warning!");
                return true;
            }
            return Math.abs(netTime.getTime() - System.currentTimeMillis()) < SAME_TIME_GAP;
        }

        return false;
    }

    public void saveHasVpn(boolean has) {
        SharedPreferences mPreferences = VolcanoContext.getContext().getSharedPreferences(GlobalPreferenceFileName.BASE_SP_FILE_NAME,Context.MODE_PRIVATE);
        mPreferences.edit().putBoolean(IS_VPN, has).apply();
        mPreferences.edit().putBoolean(IS_VPN, has).commit();
    }

    public boolean getHasVpn() {
        SharedPreferences mPreferences = VolcanoContext.getContext().getSharedPreferences(GlobalPreferenceFileName.BASE_SP_FILE_NAME,Context.MODE_PRIVATE);
        return mPreferences.getBoolean(IS_VPN, false);
    }

    /**
     * 网络更新时间
     * @param time
     */
    public static void saveUpdateTime(long time) {
        SharedPreferences mPreferences = VolcanoContext.getContext().getSharedPreferences(GlobalPreferenceFileName.BASE_SP_FILE_NAME,Context.MODE_PRIVATE);
        mPreferences.edit().putLong(UPDATE_TIME, time).apply();
        mPreferences.edit().putLong(UPDATE_TIME, time).commit();
    }

    public static long getUpdateTime() {
        SharedPreferences mPreferences = VolcanoContext.getContext().getSharedPreferences(GlobalPreferenceFileName.BASE_SP_FILE_NAME,Context.MODE_PRIVATE);
        return mPreferences.getLong(UPDATE_TIME, 0l);
    }

    /**
     * 网络时间保存，以保证性能，不频繁请求网络
     * @param time
     */
    public static void saveNetWorkTime(long time) {
        SharedPreferences mPreferences = VolcanoContext.getContext().getSharedPreferences(GlobalPreferenceFileName.BASE_SP_FILE_NAME,Context.MODE_PRIVATE);
        mPreferences.edit().putLong(NET_TIME, time).apply();
        mPreferences.edit().putLong(NET_TIME, time).commit();
    }

    public static long getNetWorkTime() {
        SharedPreferences mPreferences = VolcanoContext.getContext().getSharedPreferences(GlobalPreferenceFileName.BASE_SP_FILE_NAME,Context.MODE_PRIVATE);
        return mPreferences.getLong(NET_TIME, 0l);
    }

    public static boolean isVpnConnected() {
        if (Build.VERSION.SDK_INT >= 9) {
            try {
                //更改vpn状态为一天一更新
                if(IpUtils.getInstance().isSaveVpn()){
//                    Log.i("adiplimit", "upgrade period time is one day, the vpn state is  " + IpUtils.getInstance().getVpnState());
                    return IpUtils.getInstance().getVpnState();
                }else{
                    Enumeration<NetworkInterface> niList = NetworkInterface.getNetworkInterfaces();
                    if (niList != null) {
                        Iterator it = Collections.list(niList).iterator();
                        while (it.hasNext()) {
                            NetworkInterface intf = (NetworkInterface) it.next();
                            if (intf.isUp() && intf.getInterfaceAddresses().size() != 0) {
                                if(VolcanoContext.isDebug()){
//                                    Log.i("adiplimit", "here is debug model , return false ");
                                    IpUtils.getInstance().saveVpnState(false);
                                    IpUtils.getInstance().saveVpnUpdatetime(System.currentTimeMillis());
                                    return false;
                                }
                                if ("tun0".equals(intf.getName()) || "ppp0".equals(intf.getName())) {
//                                    Log.i("adiplimit", "It`s vpn channel , return true ");
                                    IpUtils.getInstance().saveVpnState(true);
                                    IpUtils.getInstance().saveVpnUpdatetime(System.currentTimeMillis());
                                    return true;
                                }
                            }
                        }
                    }
                }



            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        IpUtils.getInstance().saveVpnState(false);
        IpUtils.getInstance().saveVpnUpdatetime(System.currentTimeMillis());
        return false;
    }
}