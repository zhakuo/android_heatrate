package com.workout.volcano.logic.spec;

import android.content.Context;

import com.workout.base.TimeUtil;

public class EnoughDaysLogic implements IVolcanoLogic {

    public static final int ENOUGH_DAY = 3;

    @Override
    public boolean isPass(Context context) {
        return TimeUtil.isInstallDaysAfter(ENOUGH_DAY);
    }
}
