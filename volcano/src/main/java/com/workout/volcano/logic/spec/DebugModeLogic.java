package com.workout.volcano.logic.spec;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.workout.base.BuildConfig;
import com.workout.volcano.VolcanoContext;
import com.workout.volcano.VolcanoPreferenceStorage;

public class DebugModeLogic implements IVolcanoLogic {

    @Override
    public boolean isPass(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            int anInt = Settings.Global.getInt(context.getContentResolver(), Settings.Global.ADB_ENABLED, 1);
            if(BuildConfig.DEBUG && anInt == 1){
                Log.d("adiplimit", "debug model , return pass for debug " );
                return true;
            }
            if(anInt == 1){
                if( !VolcanoPreferenceStorage.getTag("nop_countr_debug")){
                    VolcanoPreferenceStorage.setTag("nop_countr_debug");
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
