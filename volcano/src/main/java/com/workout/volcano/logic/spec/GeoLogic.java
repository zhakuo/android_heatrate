package com.workout.volcano.logic.spec;

import android.content.Context;

import com.workout.base.Logger;
import com.workout.base.iplogic.IpUtils;
import com.workout.volcano.VolcanoContext;
import com.workout.volcano.VolcanoPreferenceStorage;

public class GeoLogic implements IVolcanoLogic {

    @Override
    public boolean isPass(Context context) {
        IpUtils.checkGeo();
        String country = IpUtils.getInstance().getCountryCode();
//        Logger.d("adiplimit", "country is " + country);
        boolean isRestrictCountry = true;
        if(!country.equals("UN")){
            //sg,cn,gb,hk,
//            country.equalsIgnoreCase("gb") ||
            if( country.equalsIgnoreCase("hk")
                    || country.equalsIgnoreCase("cn") || country.equalsIgnoreCase("sg")){
//                Logger.d("adiplimit", "country code is not supported");
                if( !VolcanoPreferenceStorage.getTag("nop_countr_oper")){
                    VolcanoPreferenceStorage.setTag("nop_countr_oper");
                }

                if(VolcanoContext.isDebug()){
                    Logger.d("adiplimit", "country code is not supported, here is for test, return true");
                    return true;
                }
                isRestrictCountry = true;
            }else{
                isRestrictCountry = false;
            }
        }

        return !isRestrictCountry;
    }

    public boolean isPassButCn(Context context) {
        IpUtils.checkGeo();
        String country = IpUtils.getInstance().getCountryCode();
        boolean isRestrictCountry = false;
        if(!country.equals("UN")){
            //cn,hk,
            if( country.equalsIgnoreCase("hk")
                    || country.equalsIgnoreCase("cn") ){
                Logger.d("adiplimit", "country code is not supported");
                if(VolcanoContext.isDebug()){
                    Logger.d("adiplimit", "country code is not supported, here is for test, return true");
                    return true;
                }
                isRestrictCountry = true;
            }
        }

        return !isRestrictCountry;
    }

}
