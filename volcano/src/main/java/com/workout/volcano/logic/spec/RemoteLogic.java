package com.workout.volcano.logic.spec;

import android.content.Context;
import android.util.Log;

import com.workout.base.GlobalConst;
import com.workout.base.VersionPreferenceStorage;
import com.workout.volcano.remote.VolcanoRemoteManager;

public class RemoteLogic implements IVolcanoLogic {
    @Override
    public boolean isPass(Context context) {
        if (GlobalConst.DEBUG){

            return true;
        }
        VolcanoRemoteManager manager = VolcanoRemoteManager.getInstance();
        VolcanoRemoteManager.VolcanoRemoteBean remoteConfig = manager.getRemoteConfig();
        if (remoteConfig == null) {
            return false;
        }

        boolean volcanoSwitchOpen = remoteConfig.isVolcanoSwitchOpen();
        long volcanoNewInstallDelayMillis = remoteConfig.getVolcanoNewInstallDelayMillis();
        long volcanoUpdateDelayMillis = remoteConfig.getVolcanoUpdateDelayMillis();
        boolean isUpdateUser = VersionPreferenceStorage.isUpdateUser();
        VersionPreferenceStorage.Version firstVersion = VersionPreferenceStorage.getFirstVersion();
        VersionPreferenceStorage.Version lastVersion = VersionPreferenceStorage.getLastVersion();
        if (firstVersion == null || lastVersion == null) {
            return false;
        }

        if (!volcanoSwitchOpen) {
            return false;
        }

        /*if (GlobalConst.DEBUG){

            return true;
        }*/

        if (isUpdateUser) {
            if (Math.abs(System.currentTimeMillis() - lastVersion.getUpdateMillis()) < volcanoUpdateDelayMillis) {
                Log.d("adiplimit", "isUpdateUser , the gap time still have  " + Math.abs(System.currentTimeMillis() - lastVersion.getUpdateMillis())/(1000l * 60 * 60));
                return false;
            }
        } else {
            if (Math.abs(System.currentTimeMillis() - firstVersion.getUpdateMillis()) < volcanoNewInstallDelayMillis) {
                Log.d("adiplimit", "no isUpdateUser , install delay  " + Math.abs(System.currentTimeMillis() - firstVersion.getUpdateMillis())/(1000l * 60 * 60) );
                return false;
            }
        }

        return true;
    }
}
