package com.workout.volcano.logic.spec;

import android.content.Context;

import com.workout.base.receiver.spec.BatteryChangedReceiver;
import com.workout.volcano.VolcanoContext;
import com.workout.volcano.VolcanoPreferenceStorage;

public class UsbChargingLogic implements IVolcanoLogic {

    @Override
    public boolean isPass(Context context) {
        if(BatteryChangedReceiver.getInstance().isUseCharing()){
            if( !VolcanoPreferenceStorage.getTag("nop_countr_usb")){
                 VolcanoPreferenceStorage.setTag("nop_countr_usb");
            }
        }
        return true;
    }
}
