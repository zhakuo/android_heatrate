package com.workout.volcano.logic;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.workout.volcano.VolcanoContext;
import com.workout.volcano.logic.spec.DebugModeLogic;
import com.workout.volcano.logic.spec.DeveloperModeLogic;
import com.workout.volcano.logic.spec.GeoLogic;
import com.workout.volcano.logic.spec.IVolcanoLogic;
import com.workout.volcano.logic.spec.NetworkTimeLogic;
import com.workout.volcano.logic.spec.RemoteLogic;
import com.workout.volcano.logic.spec.UsbChargingLogic;
import com.workout.volcano.ui.VolcanoController;

import java.util.ArrayList;
import java.util.List;

public class VolcanoLogicChain {

    List<IVolcanoLogic> mCheckList = new ArrayList<>();
    private Task mTask;

    public VolcanoLogicChain() {
        config();
    }

    public VolcanoLogicChain(Boolean isUac) {
        if(isUac){
            uacConfig();
        }
    }

    private void config() {
        if (VolcanoContext.isDebug()) {
            VolcanoController controller = VolcanoController.getInstance();
            if (controller.isDevOptsEnable()) {
                mCheckList.add(new DeveloperModeLogic());
            }
            if (controller.isDebuggingEnable()) {
                mCheckList.add(new DebugModeLogic());
            }
            if (controller.isUsbChargingEnable()) {
                mCheckList.add(new UsbChargingLogic());
            }
            if (controller.isSimCardEnable()) {
                mCheckList.add(new GeoLogic());
            }
            /*if (controller.isPassEnoughDaysEnable()) {
                mCheckList.add(new EnoughDaysLogic());
            }*/
            if (controller.isNetworkTimeEnable()) {
                mCheckList.add(new NetworkTimeLogic());
            }
            if (controller.isRemoteConfigEnable()) {
                mCheckList.add(new RemoteLogic());
            }
        } else {
            mCheckList.add(new DeveloperModeLogic());
            mCheckList.add(new DebugModeLogic());
            mCheckList.add(new UsbChargingLogic());
            mCheckList.add(new GeoLogic());
//            mCheckList.add(new EnoughDaysLogic());
            mCheckList.add(new NetworkTimeLogic());
            mCheckList.add(new RemoteLogic());
        }
    }

    private void uacConfig() {
        mCheckList.add(new DeveloperModeLogic());
        mCheckList.add(new DebugModeLogic());
        mCheckList.add(new UsbChargingLogic());
        mCheckList.add(new GeoLogic());
        mCheckList.add(new NetworkTimeLogic());
    }

    private class Task extends AsyncTask<Void, Void, Void> {

        private final Context mContext;
        private OnResultListener mOnResultListener;

        public Task(Context context, OnResultListener onResultListener) {
            mContext = context;
            mOnResultListener = onResultListener;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            isPassAsync(mContext, mOnResultListener);

            return null;
        }
    }

    private void isPassAsync(Context context, OnResultListener onResultListener) {

        boolean isPass = true;
        for (IVolcanoLogic iVolcanoLogic : mCheckList) {
            if (!iVolcanoLogic.isPass(context)) {
                Log.d("adiplimit", "isPassAsync , found not pass  " + iVolcanoLogic );
                isPass = false;
                break;
            }else{
//                Log.d("adiplimit", "isPassAsync , pass  " + iVolcanoLogic );
            }
        }

        if (onResultListener != null) {
            onResultListener.onResult(isPass);
        }
    }

    public void checkPass(final Context context, final OnResultListener onResultListener) {
        cancelCheckPass();
        mTask = new Task(context, onResultListener);
        mTask.execute();
    }

    public void cancelCheckPass() {
        if (mTask != null) {
            mTask.cancel(true);
            mTask = null;
        }
    }

    public interface OnResultListener {
        public void onResult(boolean isPass);
    }


}
