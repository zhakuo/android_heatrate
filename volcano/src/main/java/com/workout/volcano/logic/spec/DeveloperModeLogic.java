package com.workout.volcano.logic.spec;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.workout.base.BuildConfig;
import com.workout.volcano.VolcanoContext;
import com.workout.volcano.VolcanoPreferenceStorage;

public class DeveloperModeLogic implements IVolcanoLogic {

    @Override
    public boolean isPass(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            int anInt = Settings.Secure.getInt(context.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED , 1);
            if(BuildConfig.DEBUG && anInt == 1){
                Log.d("adiplimit", "Developer model , return pass for debug " );
                return true;
            }
            if(anInt != 1){
                if( !VolcanoPreferenceStorage.getTag("nop_countr_developer")){
                    VolcanoPreferenceStorage.setTag("nop_countr_developer");
                }
            }
            return true;
        }else{
            if( !VolcanoPreferenceStorage.getTag("nop_countr_developer")){
                VolcanoPreferenceStorage.setTag("nop_countr_developer");
            }
        }

        return false;
    }
}
