package com.workout.volcano.logic.spec;

import android.content.Context;

public interface IVolcanoLogic {

    boolean isPass(Context context);
}
