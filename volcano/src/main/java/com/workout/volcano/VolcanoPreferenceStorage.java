package com.workout.volcano;

import android.content.Context;
import android.content.SharedPreferences;

/**
 *
 * SharedPreference
 * {@link VolcanoPreferenceId}
 *
 * Created by chenhewen on 2018/6/28.
 */
public class VolcanoPreferenceStorage {

    private static final String VOLCANO_SP_FILE_NAME = "volcano_sp";

    public static SharedPreferences getSp() {
        Context appContext = VolcanoContext.getContext();
        return appContext.getSharedPreferences(VOLCANO_SP_FILE_NAME, Context.MODE_PRIVATE);
    }

    public static void setTag(String tag) {
        SharedPreferences sharedPreferences = getSp();
        sharedPreferences.edit().putBoolean(tag, true).commit();
    }

    public static boolean getTag(String tag) {
        SharedPreferences sharedPreferences = getSp();
        return sharedPreferences.getBoolean(tag, false);
    }
}
