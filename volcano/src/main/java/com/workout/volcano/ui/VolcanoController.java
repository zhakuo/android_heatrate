package com.workout.volcano.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.workout.base.BaseModuleContext;
import com.workout.base.GlobalConst;
import com.workout.base.ILifeCircleManager;
import com.workout.base.Logger;
import com.workout.base.RemoteConfigId;
import com.workout.base.SportObj;
import com.workout.base.ads.AdvertParamConfig;
import com.workout.base.incident.AOPNativeEvent;
import com.workout.base.incident.AOPSAVEEvent;
import com.workout.base.incident.RefreshVolcanoEvent;
import com.workout.base.preferences.SPConstant;
import com.workout.base.receiver.event.BatteryChangedEvent;
import com.workout.base.receiver.event.BatteryConnectedChangedEvent;
import com.workout.base.receiver.event.ScreenOnEvent;
import com.workout.base.receiver.event.TimeChangedEvent;
import com.workout.base.receiver.event.UserPresentEvent;
import com.workout.base.receiver.spec.BatteryChangedReceiver;
import com.workout.base.service.HelloService;
import com.workout.volcano.VolcanoContext;
import com.workout.volcano.VolcanoPreferenceId;
import com.workout.volcano.VolcanoPreferenceStorage;
import com.workout.volcano.logic.VolcanoLogicChain;
import com.workout.volcano.logic.spec.BuyUserLogic;
import com.workout.volcano.remote.VolcanoRemoteManager;
import com.workout.volcano.ui.time.EruptionTime;
import com.workout.volcano.ui.time.EruptionTimeManager;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Random;

public class VolcanoController implements ILifeCircleManager {

    private BatteryChangedReceiver mBatteryStatus;
    private EruptionTimeManager mEruptionTimeManager;
    private SharedPreferences mSp;
    private VolcanoLogicChain mVolcanoLogicChain;
    private VolcanoRemoteManager mVolcanoRemoteManager;

    private static final class VolcanoControllerHolder {
        private static VolcanoController sVolcanoController = new VolcanoController();
    }

    public static VolcanoController getInstance() {
        return VolcanoControllerHolder.sVolcanoController;
    }

    private VolcanoController() {

    }

    @Override
    public void onCreate(Context context) {
        EventBus.getDefault().register(this);
        mSp = VolcanoPreferenceStorage.getSp();
        mBatteryStatus = BatteryChangedReceiver.getInstance();
        mEruptionTimeManager = new EruptionTimeManager();
        mVolcanoLogicChain = new VolcanoLogicChain();
        mVolcanoRemoteManager = VolcanoRemoteManager.getInstance();

        reopenVolcanoEnableIfNeed();
    }

    @Override
    public void onDestroy(Context context) {
        EventBus.getDefault().unregister(this);
    }

    public void changeLogic() {
        if (mVolcanoLogicChain != null) {
            mVolcanoLogicChain.cancelCheckPass();
        }

        mVolcanoLogicChain = new VolcanoLogicChain();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserInteract(UserPresentEvent event) {
        if (mVolcanoLogicChain != null) {
            mVolcanoLogicChain.cancelCheckPass();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onScreenOnOrOff(ScreenOnEvent event) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            startVolcanoIfCharging();
        }else{
            Log.d("adiplimit", "startVolcano  event.isEnable() " +event.isEnable()  );
            if(!event.isEnable()){

                startVolcanoIfCharging();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBatteryPowerChanged(BatteryChangedEvent event) {
        if (isVolcanoEnable()) {
            mEruptionTimeManager.addOnePctPower();
            EventBus.getDefault().post(RefreshVolcanoEvent.BATTERY_CHANGED);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBatteryConnecedChanged(BatteryConnectedChangedEvent event) {
        if (isVolcanoEnable()) {
            if (event.isEnable()) {
                onConnectedToPower();
            } else {
                onDisconnectedFromPower();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTimeChanged(TimeChangedEvent timeChangedEvent) {
        if (isVolcanoEnable()) {
            EventBus.getDefault().post(RefreshVolcanoEvent.TIME_CHANGED);
        }
    }

    /**
     * 连接上充电线
     */
    private void onConnectedToPower() {
        Log.d("adiplimit", "startVolcano  onConnectedToPower "   );
        startVolcano();
        mEruptionTimeManager.connect();
        EventBus.getDefault().post(RefreshVolcanoEvent.CONNECTED);
    }

    /**
     * 断开充电线
     */
    private void onDisconnectedFromPower() {
        mEruptionTimeManager.disconnect();
        EventBus.getDefault().post(RefreshVolcanoEvent.DISCONNECTED);
    }

    /**
     * 启动Volcano
     */
    private void startVolcano() {
        if (isVolcanoEnable() && !BuyUserLogic.shouldNotShowAds()) {
            Log.d("adiplimit", "startVolcano  isVolcanoEnable " + true  );

            HelloService.tellMeToStartService(VolcanoContext.getContext());

            if (!isVolcanoRestricted()) {
                startActivity();
            } else {
                checkPass(new VolcanoLogicChain.OnResultListener() {
                    @Override
                    public void onResult(boolean isPass) {
                        if (isPass) {

                            startActivity();
                        }
                    }
                });
            }
        }
    }

    private void startActivity() {
        Log.d("adiplimit", "startVolcano  startActivity "   );
//        if(AdvertParamConfig.isAdmobReachMax()){
//            return ;
//        }


        //记录打开了锁屏
        SPConstant.saveVolcanoHasOpen(true);
        Context context = VolcanoContext.getContext();

        Log.d("adiplimit", "startVolcano  postEvent "   );

        String id = VolcanoRemoteManager.getValue(RemoteConfigId.ban_id_config);
        if(GlobalConst.DEBUG){
//            bannerTopOnPlacementID = "b5ee8ae48f1578"; //Vungle
            id = "b5f7fd2c9a50a8"; //Unityads
        }
        SportObj.setBanId(id);

        getChargingLeftMillis();

        BaseModuleContext.postEvent(new AOPSAVEEvent(context));

        /*if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {

            if( DailyPartActivity.islive ){
                return ;
            }
            Log.d("adiplimit", "startActivity , DailyPartActivity  "  );
            context.startActivity(DailyPartActivity.getEntryIntent(context, DailyPartActivity.ENTRY_MAIN));

        }else{
            try {
                Log.d("adiplimit", "startActivity , DailyPartActivity intent "  );
                Intent intent = DailyPartActivity.getEntryIntent(context, DailyPartActivity.ENTRY_MAIN);
                DailyPActivity.startPPage(context, intent);
            }catch ( Exception e){
                e.printStackTrace();
            }
        }*/
    }

    /**
     * 如果在充电，启动Volcano
     */
    private void startVolcanoIfCharging() {
        Log.d("adiplimit", "startVolcano  startVolcanoIfCharging "   );
        if (mBatteryStatus.isConnected()) {
            startVolcano();
        }
    }

    /**
     * 获取充电剩余时间
     * @return 当返回值为{@link EruptionTime#UNKNOWN_TIME}， 意味着尚未计算出时间，
     * 否则返回long值
     */
    public long getChargingLeftMillis() {
        long avg = mEruptionTimeManager.getAvgTime();
        Log.d("snow", "getChargingLeftMillis avg = " + avg);
        if (avg == EruptionTime.UNKNOWN_TIME) {
            return EruptionTime.UNKNOWN_TIME;
        }
        int batteryPct = mBatteryStatus.getBatteryPct();
        Log.d("snow", "getChargingLeftMillis batteryPct = " + batteryPct);
        return avg * (100 - batteryPct);
    }

    private void checkPass(VolcanoLogicChain.OnResultListener onResultListener) {
        Context context = VolcanoContext.getContext();
        mVolcanoLogicChain.checkPass(context, onResultListener);
    }

    private void reopenVolcanoEnableIfNeed() {
        VolcanoRemoteManager.VolcanoRemoteBean remoteConfig = mVolcanoRemoteManager.getRemoteConfig();
        if (remoteConfig == null) {
            return;
        }
        if (!remoteConfig.isVolcanoSwitchOpen()) {
            Log.d("adiplimit", "isVolcanoSwitchOpen =" );
            return;
        }
        if (isVolcanoEnable()) {
            Log.d("adiplimit", "isVolcanoEnable =" );
            return;
        }
        if (getVolcanoDisableTimes() > remoteConfig.getMaxReopenCount()) {
            Log.d("adiplimit", "getVolcanoDisableTimes =" );
            return;
        }

        if (Math.abs(System.currentTimeMillis() - getVolcanoDisableMillis()) < remoteConfig.getReopenGapMillis()) {
            return;
        }
        saveVolcanoEnable(true);
    }


    public void saveVolcanoEnable(boolean enable) {
        mSp.edit().putBoolean(VolcanoPreferenceId.VOLCANO_ENABLE, enable).apply();
    }

    public boolean isVolcanoEnable() {
        return mSp.getBoolean(VolcanoPreferenceId.VOLCANO_ENABLE, true);
    }

    public void saveVolcanoDisableMillis() {
        mSp.edit().putLong(VolcanoPreferenceId.VOLCANO_DISABLE_MILLIS, System.currentTimeMillis()).apply();
    }


    public long getVolcanoDisableMillis() {
        return mSp.getLong(VolcanoPreferenceId.VOLCANO_DISABLE_MILLIS, 0);
    }

    public void saveVolcanoDisableTimes() {
        int volcanoDisableTimes = getVolcanoDisableTimes();
        mSp.edit().putInt(VolcanoPreferenceId.VOLCANO_DISABLE_TIMES, volcanoDisableTimes + 1).apply();
    }

    public int getVolcanoDisableTimes() {
        return mSp.getInt(VolcanoPreferenceId.VOLCANO_DISABLE_TIMES, 0);
    }


    public void saveVolcanoRestricted(boolean restricted) {
        mSp.edit().putBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED, restricted).apply();
    }

    public boolean isVolcanoRestricted() {
        return mSp.getBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED, true);
    }

    public void saveDevOptsEnable(boolean enable) {
        mSp.edit().putBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_DEV_OPTS_ENABLE, enable).apply();
    }

    public boolean isDevOptsEnable() {
        return mSp.getBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_DEV_OPTS_ENABLE, true);
    }

    public void saveDebuggingEnable(boolean enable) {
        mSp.edit().putBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_DEBUGGING_ENABLE, enable).apply();
    }

    public boolean isDebuggingEnable() {
        return mSp.getBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_DEBUGGING_ENABLE, true);
    }

    public void saveUsbChargingEnable(boolean enable) {
        mSp.edit().putBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_USB_CHARGING_ENABLE, enable).apply();
    }

    public boolean isUsbChargingEnable() {
        return mSp.getBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_USB_CHARGING_ENABLE, true);
    }

    public void saveSimCardEnable(boolean enable) {
        mSp.edit().putBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_SIM_CARD_ENABLE, enable).apply();
    }

    public boolean isSimCardEnable() {
        return mSp.getBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_SIM_CARD_ENABLE, true);
    }

    public void savePassEnoughDaysEnable(boolean enable) {
        mSp.edit().putBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_PASS_ENOUGH_DAYS_ENABLE, enable).apply();
    }

    public boolean isPassEnoughDaysEnable() {
        return mSp.getBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_PASS_ENOUGH_DAYS_ENABLE, true);
    }

    public void saveNetworkTimeEnable(boolean enable) {
        mSp.edit().putBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_NETWORK_TIME_ENABLE, enable).apply();
    }

    public boolean isNetworkTimeEnable() {
        return mSp.getBoolean(VolcanoPreferenceId.VOLCANO_RESTRICTED_NETWORK_TIME_ENABLE, true);
    }

    public void saveRemoteConfigEnable(boolean enable) {
        mSp.edit().putBoolean(VolcanoPreferenceId.VOLCANO_REMOTE_ENABLE, enable).apply();
    }

    public boolean isRemoteConfigEnable() {
        return mSp.getBoolean(VolcanoPreferenceId.VOLCANO_REMOTE_ENABLE, true);
    }
}
