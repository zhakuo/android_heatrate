package com.workout.volcano.ui;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

import com.workout.base.BaseModuleContext;
import com.workout.base.incident.APPChooseEvent;
import com.workout.volcano.VolcanoContext;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;


/**
 * 打开1像素、打开锁屏
 */
public class DailyPActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("snow", " DailyPActivity before postEvent  " );

        Intent intent = this.getIntent();
        Intent passIntent = intent.getParcelableExtra("intent_param");
        this.startActivity(passIntent);

        finish();
    }

    protected void onResume() {
        super.onResume();

        finish();
    }



    @UiThread
    @MainThread
    /**
     * context context 或者activity
     * event 打开场景广告的event
     */
    public static void startPPage(@NonNull final Context context, Intent mIntent) {

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            //Log.d("snowdd", "DailyPActivity startPPage sdk 28 startActivity");
            context.startActivity(mIntent);
            return;
        }

        Context applicationContext = context.getApplicationContext();
        Intent intent = new Intent(applicationContext, DailyPActivity.class);
        intent.setAction("inner_action");
        Bundle bundle = new Bundle();
        bundle.putParcelable("intent_param", mIntent);
        intent.putExtras(bundle);

        final PendingIntent activity = PendingIntent.getActivity(applicationContext, 10102, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        boolean z = false;
        try {
            //Log.d("snowdd", "startPage  try " );
            activity.send();
            z = true;
            //Log.d("snowdd", " startPage unused z " );
        } catch (Exception unused) {
            Log.d("snow", " startPage unused " + unused.getMessage());
        }
        if (!z) {
            intent.setFlags(Intent.FLAG_RECEIVER_FOREGROUND);
            try {
                applicationContext.startActivity(intent);
            } catch (Exception unused2) {
                unused2.printStackTrace();
            }
        }
        if (Looper.myLooper() != Looper.getMainLooper()) {
            VolcanoContext.postRunOnUiThread(new Runnable() {
                @Override
                public void run() {

                    DailyNotification.openPending(context, activity);

                }
            });

        }else{

            DailyNotification.openPending(applicationContext, activity);

        }

    }
}
