package com.workout.volcano.ui.time;

import android.os.SystemClock;

import java.util.LinkedList;
import java.util.List;

public class EruptionTime {

    public static final long UNKNOWN_TIME = -1;

    private final FixedLengthList mFixedLengthList;

    public EruptionTime() {
        mFixedLengthList = new FixedLengthList(2);
    }

    public void add() {
        long millis = SystemClock.elapsedRealtime();
        mFixedLengthList.add(millis);
    }

    public void clear() {
        mFixedLengthList.clear();
    }

    public long getAvg() {
        return mFixedLengthList.getAvg();
    }

    private class FixedLengthList {

        private int mFixedLength;
        private List<Long> mList = new LinkedList<>();

        public FixedLengthList(int fixedLength) {
            if (fixedLength <= 1) {
                throw new IllegalArgumentException("fixedLength should be at least 2, now is " + fixedLength);
            }
            mFixedLength = fixedLength;
        }

        public void add(long element) {
            if (mList.size() >= mFixedLength) {
                mList.remove(0);
            }

            mList.add(element);
        }

        public void clear() {
            mList.clear();
        }

        public long getAvg() {
            if (mList.size() < mFixedLength) {
                return UNKNOWN_TIME;
            }

            int size = mList.size();
            long total = mList.get(size - 1) - mList.get(0);

            return total / (size - 1);
        }
    }
}
