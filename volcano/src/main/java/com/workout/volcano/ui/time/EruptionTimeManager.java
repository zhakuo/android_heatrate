package com.workout.volcano.ui.time;

import android.content.SharedPreferences;
import android.os.SystemClock;

import com.workout.base.TimeUtil;
import com.workout.volcano.VolcanoPreferenceId;
import com.workout.volcano.VolcanoPreferenceStorage;

public class EruptionTimeManager {

    public static final long CONTINUE_TIME_OUT = TimeUtil.SECOND_IN_MILLIS * 15;

    private final EruptionTime mEruptionTime;
    private final SharedPreferences mSp;

    public EruptionTimeManager() {
        mEruptionTime = new EruptionTime();
        mSp = VolcanoPreferenceStorage.getSp();
    }

    public void connect() {
        long disconnectMillis = retrieveDisconnectMillis();
        // 超过某个时间限度，我们认为电量的时间处于一个无效的状态
        if (getMillis() - disconnectMillis > CONTINUE_TIME_OUT) {
            mEruptionTime.clear();
        } else {
//            mEruptionTime.resume();
        }
    }

    public void disconnect() {
//        mEruptionTime.pause();
        saveDisconnectMillis(getMillis());
    }

    public void addOnePctPower() {
        mEruptionTime.add();
    }

    public long getAvgTime() {
        return mEruptionTime.getAvg();
    }

    public void invalidate() {
        mEruptionTime.clear();
        saveDisconnectMillis(0);
    }

    public long getMillis() {
        return SystemClock.elapsedRealtime();
    }

    private void saveDisconnectMillis(long millis) {
        mSp.edit().putLong(VolcanoPreferenceId.LAST_DISCONNECT_MILLIS, millis).apply();
    }

    private long retrieveDisconnectMillis() {
        return mSp.getLong(VolcanoPreferenceId.LAST_DISCONNECT_MILLIS, 0);
    }

}
