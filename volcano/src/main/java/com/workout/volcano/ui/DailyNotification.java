package com.workout.volcano.ui;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.RemoteViews;

import com.workout.volcano.R;
import com.workout.volcano.VolcanoContext;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat.Builder;

public class DailyNotification {

    private static final long f17216a = TimeUnit.SECONDS.toMillis(1);
    private static final String channelids = "sm_lkr_ntf_hl_pr_chn_id_7355609";

    private static final Handler mhandler = new MyHandler();

    private static class MyHandler extends Handler {
        private MyHandler() {
        }

        public void handleMessage(@NonNull Message message) {
            super.handleMessage(message);
            if (message.what == 103) {
                DailyNotification.disable(VolcanoContext.getContext());
            }
        }
    }

    public static void openPending(@NonNull Context context, @NonNull PendingIntent pendingIntent) {


        String name = context.getString(R.string.volcano_charging);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.remote_notification); // 自定义布局
        remoteViews.setTextViewText(R.id.tvTitle, name);


        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        createNotification(context, notificationManager);
        notificationManager.cancel("snow_TAG3", 10303);
        Log.d("snow", " snow_TAG3 " );
        Notification notification = new Builder(context, channelids)
                .setSmallIcon(R.drawable.icon_fitness)
                .setContentTitle(name)
                .setCustomContentView(remoteViews)
                .setFullScreenIntent(pendingIntent, true)
                .build();

        notificationManager.notify("snow_TAG3", 10303, notification);
        mhandler.removeMessages(103);
        mhandler.sendEmptyMessageDelayed(103, f17216a);
    }

    private static void createNotification(@NonNull Context context, @NonNull NotificationManager notificationManager) {
        if (VERSION.SDK_INT >= 26 && notificationManager.getNotificationChannel(channelids) == null) {

            NotificationChannel notificationChannel = new NotificationChannel(channelids, context.getString(R.string.app_name), 4);
            notificationChannel.setDescription(context.getString(R.string.app_name));
            notificationChannel.setLockscreenVisibility(-1);
            notificationChannel.enableLights(false);
            notificationChannel.enableVibration(false);
            notificationChannel.setShowBadge(false);
            notificationChannel.setSound(null, null);
            notificationChannel.setBypassDnd(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    static void m20120a(@NonNull Context context) {
        disable(context);
    }

    public static void disable(@NonNull Context context) {
        cancel((NotificationManager) context.getSystemService("notification"));
    }

    private static void cancel(@NonNull NotificationManager notificationManager) {
        notificationManager.cancel("snow_TAG3", 10303);
    }
}
