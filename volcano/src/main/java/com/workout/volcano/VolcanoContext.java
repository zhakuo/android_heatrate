package com.workout.volcano;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

public class VolcanoContext {
    private static Context sContext;
    private static boolean sDebug;

    public static void init(Context context, boolean debug) {
        sContext = context;
        sDebug = debug;
    }

    public static Context getContext() {
        return sContext;
    }

    public static boolean isDebug() {
        return sDebug;
    }

    private static Handler sHandler = new Handler(Looper.getMainLooper()) {

    };

    /**
     * 提交一个Runable到UI线程执行<br>
     *
     * @param r
     */
    public static void postRunOnUiThread(Runnable r) {
        postRunnableByHandler(sHandler, r);
    }

    /**
     * 提交一个Runable到UI线程执行<br>
     *
     * @param r
     */
    public static void postRunOnUiThread(Runnable r, long delay) {
        postRunnableByHandler(sHandler, r, delay);
    }

    private static void postRunnableByHandler(Handler handler, Runnable r) {
        handler.post(r);
    }

    private static void postRunnableByHandler(Handler handler, Runnable r, long delay) {
        handler.postDelayed(r, delay);
    }
}
