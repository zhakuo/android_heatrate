package com.ironsource.sdk.base;

import android.content.ComponentName;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.ironsource.sdk.internal.ADProxyImpl;
import com.ironsource.sdk.internal.IAttachable;
import com.ironsource.sdk.utils.ADConstants;

import androidx.appcompat.app.AppCompatActivity;
import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class ProxyAppCompActivity extends AppCompatActivity implements IAttachable {
    protected IAD mRemoteActivity;
    private ADProxyImpl impl = new ADProxyImpl(this);
    private boolean mForPoxy = false;

    @Override
    final protected void onCreate(Bundle savedInstanceState) {
        mForPoxy = getIntent().getBooleanExtra(ADConstants.EXTRA_PROXY, false);
        //Log.i("snow","proxy activity is....."+mForPoxy);
        if (!mForPoxy) beforeSuperOnCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        if (!mForPoxy) onActivityCreate(savedInstanceState);
        impl.onCreate(getIntent());
    }

    protected void beforeSuperOnCreate(Bundle savedInstanceState) {

    }

    protected void onActivityCreate(Bundle savedInstanceState) {

    }

    @Override
    public void attach(IAD proxyActivity, ActivityManager pluginManager) {
        mRemoteActivity = proxyActivity;
    }

    @Override
    public void setContentView(int layoutResID) {
        if (mForPoxy) {
            View v =impl.getLayoutInflater().inflate(layoutResID, null);
            super.setContentView(v);
        } else {
            super.setContentView(layoutResID);
        }

    }

    @Override
    public AssetManager getAssets() {
        if (mForPoxy) return impl.getAssets() == null ? super.getAssets() : impl.getAssets();
        return super.getAssets();
    }

    @Override
    public Resources getResources() {
        if (mForPoxy)
            return impl.getResources() == null ? super.getResources() : impl.getResources();
        return super.getResources();
    }

    @Override
    public Resources.Theme getTheme() {

        Resources.Theme theme = super.getTheme();
        Resources.Theme ptheme = impl.getTheme();

//        //Log.i("snow","get theme 1....."+theme);
//        //Log.i("snow","get theme 2....."+ptheme);
//        //Log.i("snow","get theme mForPoxy....."+mForPoxy);

        if(mForPoxy) return ptheme == null ? theme : ptheme;


        return theme;
    }

    @Override
    public ClassLoader getClassLoader() {
        if (mForPoxy) return impl.getClassLoader();
        return super.getClassLoader();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mForPoxy) mRemoteActivity.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        if (mForPoxy) mRemoteActivity.onStart();
        super.onStart();
    }

    @Override
    protected void onRestart() {
        if (mForPoxy) mRemoteActivity.onRestart();
        super.onRestart();
    }

    @Override
    protected void onResume() {
        if (mForPoxy) mRemoteActivity.onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (mForPoxy) mRemoteActivity.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (mForPoxy) mRemoteActivity.onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mForPoxy) mRemoteActivity.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mForPoxy) mRemoteActivity.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (mForPoxy) mRemoteActivity.onRestoreInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (mForPoxy) mRemoteActivity.onNewIntent(intent);
        super.onNewIntent(intent);
    }

    @Override
    public void onBackPressed() {
        if (mForPoxy) mRemoteActivity.onBackPressed();
        super.onBackPressed();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean r = super.onTouchEvent(event);
        if (mForPoxy) return mRemoteActivity.onTouchEvent(event);
        return r;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        boolean r = super.onKeyUp(keyCode, event);
        if (mForPoxy) return mRemoteActivity.onKeyUp(keyCode, event);
        return r;
    }

    @Override
    public void onWindowAttributesChanged(WindowManager.LayoutParams params) {
        if (mRemoteActivity != null) mRemoteActivity.onWindowAttributesChanged(params);
        super.onWindowAttributesChanged(params);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (mForPoxy) mRemoteActivity.onWindowFocusChanged(hasFocus);
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mForPoxy) mRemoteActivity.onCreateOptionsMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mForPoxy) mRemoteActivity.onOptionsItemSelected(item);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public ComponentName startService(Intent service) {
        return super.startService(service);
    }


    @Override
    public Object getSystemService(String name) {

        if (mForPoxy && LAYOUT_INFLATER_SERVICE.equals(name)) {
            LayoutInflater l = impl.getLayoutInflater();
            if (l != null) {
                return l;
            }
        }
        return super.getSystemService(name);
    }
}
