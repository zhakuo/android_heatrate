package com.ironsource.sdk.base;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.ironsource.sdk.internal.ADProxyImpl;
import com.ironsource.sdk.internal.IAttachable;
import com.ironsource.sdk.utils.ADConstants;
import com.ironsource.sdk.utils.KeyUtils;
import com.workout.base.incident.RefreshVolcanoEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.michaelrocks.paranoid.Obfuscate;


@Obfuscate
public class IronsourceActivity extends Activity implements IAttachable {
    protected IAD mRemoteActivity;
    private ADProxyImpl impl = new ADProxyImpl(this);
    private boolean mForPoxy = false;
    private KeyUtils mKeyUtils = null;

    @Override
    final protected void onCreate(Bundle savedInstanceState) {
        mForPoxy = getIntent().getBooleanExtra(ADConstants.EXTRA_PROXY, false);

        if (mForPoxy){
            impl.attach(getIntent());
            impl.beforeSuperOnCreate(getIntent());
        }else{
            beforeSuperOnCreate(savedInstanceState);
        }

        super.onCreate(savedInstanceState);
        if(mForPoxy){
            try {
                impl.onCreate(getIntent());
            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            onActivityCreate(savedInstanceState);
        }

        initKey();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
//        if(mForPoxy){
//            impl.attach(ActivityManager.getInstance(this).getPendingIntent());
//            ActivityManager.getInstance(this).removePendingIntent();
//        }
    }

    protected void beforeSuperOnCreate(Bundle savedInstanceState) {

    }

    protected void onActivityCreate(Bundle savedInstanceState) {

    }

    @Override
    public void attach(IAD proxyActivity, ActivityManager pluginManager) {
        mRemoteActivity = proxyActivity;
    }

    @Override
    public void setContentView(int layoutResID) {
        if (mForPoxy) {
            View v =impl.getLayoutInflater().inflate(layoutResID, null);
            super.setContentView(v);
        } else {
            super.setContentView(layoutResID);
        }

    }

    @Override
    public AssetManager getAssets() {
        if (mForPoxy) return impl.getAssets() == null ? super.getAssets() : impl.getAssets();
        return super.getAssets();
    }

    @Override
    public Resources getResources() {
        if (mForPoxy)
            return impl.getResources() == null ? super.getResources() : impl.getResources();
        return super.getResources();
    }

    @Override
    public Resources.Theme getTheme() {

        Resources.Theme theme = super.getTheme();
        Resources.Theme ptheme = impl.getTheme();

        if(mForPoxy) return ptheme == null ? theme : ptheme;


        return theme;
    }

    @Override
    public ClassLoader getClassLoader() {
        if (mForPoxy) return impl.getClassLoader();
        return super.getClassLoader();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mForPoxy) mRemoteActivity.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        if (mForPoxy) mRemoteActivity.onStart();
        super.onStart();
    }

    @Override
    protected void onRestart() {
        if (mForPoxy) mRemoteActivity.onRestart();
        super.onRestart();
    }

    @Override
    protected void onResume() {
        if (mForPoxy) mRemoteActivity.onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (mForPoxy) mRemoteActivity.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (mForPoxy) mRemoteActivity.onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mForPoxy) mRemoteActivity.onDestroy();
        try{

            if (mKeyUtils!=null){

                mKeyUtils.unregisterReceiver();
            }

        }catch (Exception e){

        }
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mForPoxy) mRemoteActivity.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (mForPoxy) mRemoteActivity.onRestoreInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (mForPoxy) mRemoteActivity.onNewIntent(intent);
        super.onNewIntent(intent);
    }

    @Override
    public void onBackPressed() {


        //Log.i("snow","click back...ad activity.."+mForPoxy);

        if (mForPoxy) {

            mRemoteActivity.onBackPressed();

        }else {

            super.onBackPressed();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean r = super.onTouchEvent(event);
        if (mForPoxy) return mRemoteActivity.onTouchEvent(event);
        return r;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        boolean r = super.onKeyUp(keyCode, event);
        if (mForPoxy) return mRemoteActivity.onKeyUp(keyCode, event);
        return r;
    }

    @Override
    public void onWindowAttributesChanged(WindowManager.LayoutParams params) {
        if (mRemoteActivity != null) mRemoteActivity.onWindowAttributesChanged(params);
        super.onWindowAttributesChanged(params);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (mForPoxy) mRemoteActivity.onWindowFocusChanged(hasFocus);
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mForPoxy) mRemoteActivity.onCreateOptionsMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mForPoxy) mRemoteActivity.onOptionsItemSelected(item);
        return super.onOptionsItemSelected(item);
    }

    private void initKey(){
        try {


            mKeyUtils = new KeyUtils(this, new KeyUtils.OnHomeKeyListener() {
                @Override
                public void onRecentApps() {

                    closeDialogActivity();
                }

                @Override
                public void onLock() {
                }

                @Override
                public void onHome() {

                    closeDialogActivity();



                }


            });

        }catch (Exception e){

        }
    }

    public void closeDialogActivity() {
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                this.finishAndRemoveTask();
            } else {
                this.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ComponentName startService(Intent service) {
        return super.startService(service);
    }


    @Override
    public Object getSystemService(String name) {

        if (mForPoxy && LAYOUT_INFLATER_SERVICE.equals(name)) {
            LayoutInflater l = impl.getLayoutInflater();
            if (l != null) {
                return l;
            }
        }
        return super.getSystemService(name);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefreshUI(RefreshVolcanoEvent event) {

    }
}
