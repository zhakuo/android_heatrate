/*
 * Copyright (C) 2014 singwhatiwanna(任玉刚) <singwhatiwanna@gmail.com>
 *
 * collaborator:zhangjie1980(张杰)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ironsource.sdk.base;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.util.Log;

import com.ironsource.sdk.model.HttpInfo;
import com.ironsource.sdk.utils.ADConstants;

import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class BaseADService extends Service implements IServiceExt {

    public static final String TAG = "Plugin";
    private Service mProxyService;
    private HttpInfo mPluginPackage;
    protected Service that = this;
    protected int mFrom = ADConstants.FROM_INTERNAL;
    
    @Override
    public void attach(Service proxyService, HttpInfo pluginPackage) {
        // TODO Auto-generated method stub
        Log.d(TAG, TAG + " attach");
        mProxyService = proxyService;
        mPluginPackage = pluginPackage;
        that = mProxyService;
        mFrom = ADConstants.FROM_EXTERNAL;
    }
    
    protected boolean isInternalCall() {
        return mFrom == ADConstants.FROM_INTERNAL;
    }
    
    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        return START_STICKY_COMPATIBILITY;
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onLowMemory() {
        // TODO Auto-generated method stub
    }

    @Override
    public void onTrimMemory(int level) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onUnbind(Intent intent) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void onRebind(Intent intent) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        // TODO Auto-generated method stub
    }

}
