

package com.ironsource.sdk.base;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.ironsource.sdk.internal.ADPackageInfo;
import com.ironsource.sdk.internal.CertUtils;
import com.ironsource.sdk.internal.InstallTask;
import com.ironsource.sdk.internal.IntentExt;
import com.ironsource.sdk.model.ADInfoList;
import com.ironsource.sdk.model.HttpInfo;
import com.ironsource.sdk.utils.ADConfigs;
import com.ironsource.sdk.utils.ADConstants;
import com.ironsource.sdk.utils.FileUtils;
import com.ironsource.sdk.utils.LogDebug;

import java.io.File;
import java.util.List;

import androidx.annotation.Nullable;
import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class ActivityManager {

    private static final String TAG = "Plugin";

    public static final int START_RESULT_SUCCESS = 0;

    public static final int START_RESULT_NO_PKG = 1;

    public static final int START_RESULT_NO_CLASS = 2;

    public static final int START_RESULT_TYPE_ERROR = 3;
    public static final int START_RESULT_NOT_INSTALL = 4;

    public static final int START_RESULT_PARSE_PKG_FAILED = 5;

    private static ActivityManager sInstance;
    private Context mContext;
    private int mFrom = ADConstants.FROM_INTERNAL;

    private ADInfoList mList = new ADInfoList();
    private int mResult;

    private Intent mPendingIntent;

    private ActivityManager(Context context) {
        mContext = context.getApplicationContext() != null ? context.getApplicationContext() : context;
        ADConfigs.sContext = mContext;
    }

    public static ActivityManager getInstance(Context context) {
        if (sInstance == null) {
            synchronized (ActivityManager.class) {
                if (sInstance == null) {
                    sInstance = new ActivityManager(context);
                }
            }
        }

        return sInstance;
    }


    public void install(final File plugin, @Nullable IInstallListener listener) {
        install(plugin.getAbsolutePath(), listener);
    }

    public void install(final String dexPath, final @Nullable IInstallListener listener) {
        mFrom = ADConstants.FROM_EXTERNAL;
        int flags = ADConfigs.sDebug ? PackageManager.GET_ACTIVITIES | PackageManager.GET_SERVICES : PackageManager.GET_ACTIVITIES | PackageManager.GET_SERVICES | PackageManager.GET_SIGNATURES;
        PackageInfo packageInfo = mContext.getPackageManager().getPackageArchiveInfo(dexPath,
                flags);
        if (packageInfo == null) {
            //Log.i("snow","PluginManager install fail plugin...packageInfo..");
            if (listener != null) listener.onFailed();
            return;
        }
//        if (!verifySignature(packageInfo, dexPath)) {
//            //Log.i("snow","PluginManager install fail plugin...verifySignature..");
//            if (listener != null) listener.onFailed();
//            return;
//        }
        HttpInfo info = HttpInfo.parseFromPackageInfo(packageInfo, dexPath);
        HttpInfo old = mList.get(info.getName());
        if (old != null) {
            if (old.getVersion() > info.getVersion()) {
                if (listener != null) {
                    listener.onInstalled(old);
                    return;
                }
            } else {
                mList.remove(old.getPackageName());
            }
        }
        mList.add(info);
        info.setType(HttpInfo.TYPE_INSTALLING);
        new InstallTask(mContext, info).install(new IInstallListener() {
            @Override
            public void onInstalled(HttpInfo info) {
                info.setType(HttpInfo.TYPE_INSTALLED);
                mList.save(mContext);
                if (listener != null) listener.onInstalled(info);
            }

            @Override
            public void onFailed() {
                if (listener != null) listener.onFailed();
            }
        });
//        info.setPackageInfo(preparePluginEnv(packageInfo, info.getPath()));

    }


//    private PluginPackageInfo preparePluginEnv(PackageInfo packageInfo, String dexPath) {
//
//
//        DexClassLoader dexClassLoader = createDexClassLoader(dexPath);
//        AssetManager assetManager = createAssetManager(dexPath);
//        Resources resources = createResources(assetManager);
//        // create pluginPackage
//        return new PluginPackageInfo(dexClassLoader, resources, packageInfo);
//    }


    public HttpInfo getPackage(String packageName) {
        return mList.get(packageName);
    }

    public Intent getPendingIntent(){
        return mPendingIntent;
    }

    public void removePendingIntent(){
        mPendingIntent = null;
    }

    public int startActivity(Context context, IntentExt dlIntent) {
        return startActivityForResult(context, dlIntent, -1);
    }

    /**
     * @param context
     * @param dlIntent
     * @param requestCode
     * @return One of below: {@link #START_RESULT_SUCCESS}
     * {@link #START_RESULT_NO_PKG} {@link #START_RESULT_NO_CLASS}
     * {@link #START_RESULT_TYPE_ERROR}
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public int startActivityForResult(Context context, IntentExt dlIntent,
                                            int requestCode) {
//        if (mFrom == PluginConstants.FROM_INTERNAL) {
//            dlIntent.setClassName(context, dlIntent.getPluginClass());
//            performStartActivityForResult(context, dlIntent, requestCode);
//            return Plugin.START_RESULT_SUCCESS;
//        }

        String packageName = dlIntent.getPluginPackage();
        if (TextUtils.isEmpty(packageName)) {
            return START_RESULT_NO_PKG;
        }

        HttpInfo info = mList.get(packageName);
        if (info == null) {
            return START_RESULT_NO_PKG;
        } else if (!info.isInstalled()) {
            return START_RESULT_NOT_INSTALL;
        }
        ADPackageInfo adInfo = info.getPackageInfo();
        if(adInfo == null){
            return START_RESULT_PARSE_PKG_FAILED;
        }
        final String className = getActivityFullPath(dlIntent, info.getPackageInfo());
        Class<?> clazz = loadClass(info.getClassLoader(), className);
        if (clazz == null) {
            return START_RESULT_NO_CLASS;
        }

        // get the proxy activity class, the proxy activity will launch the
        // plugin activity.
        Class<? extends Activity> activityClass = getProxyActivityClass(dlIntent, clazz);
        if (activityClass == null) {
            return START_RESULT_TYPE_ERROR;
        }

        // put extra data
        dlIntent.putExtra(ADConstants.EXTRA_CLASS, className);
        dlIntent.putExtra(ADConstants.EXTRA_CLASS, className);
        dlIntent.putExtra(ADConstants.EXTRA_PACKAGE, packageName);
        dlIntent.putExtra(ADConstants.EXTRA_PROXY, true);
        dlIntent.putExtra(ADConstants.EXTRA_STAMP, System.currentTimeMillis());
        dlIntent.setClass(mContext, activityClass);
        mPendingIntent = dlIntent;
        performStartActivityForResult(context, dlIntent, requestCode);
        return START_RESULT_SUCCESS;
    }

    public int startService(final Context context, final IntentExt dlIntent) {
        if (mFrom == ADConstants.FROM_INTERNAL) {
            dlIntent.setClassName(context, dlIntent.getPluginClass());
            context.startService(dlIntent);
            return ActivityManager.START_RESULT_SUCCESS;
        }

        fetchProxyServiceClass(dlIntent, new OnFetchProxyServiceClass() {
            @Override
            public void onFetch(int result, Class<? extends Service> proxyServiceClass) {
                // TODO Auto-generated method stub
                if (result == START_RESULT_SUCCESS) {
                    dlIntent.setClass(context, proxyServiceClass);
                    // start代理Service
                    context.startService(dlIntent);
                }
                mResult = result;
            }
        });

        return mResult;
    }

    public int stopService(final Context context, final IntentExt dlIntent) {
        if (mFrom == ADConstants.FROM_INTERNAL) {
            dlIntent.setClassName(context, dlIntent.getPluginClass());
            context.stopService(dlIntent);
            return ActivityManager.START_RESULT_SUCCESS;
        }

        fetchProxyServiceClass(dlIntent, new OnFetchProxyServiceClass() {
            @Override
            public void onFetch(int result, Class<? extends Service> proxyServiceClass) {
                // TODO Auto-generated method stub
                if (result == START_RESULT_SUCCESS) {
                    dlIntent.setClass(context, proxyServiceClass);
                    // stop代理Service
                    context.stopService(dlIntent);
                }
                mResult = result;
            }
        });

        return mResult;
    }

    public int bindService(final Context context, final IntentExt dlIntent,
                                 final ServiceConnection conn,
                                 final int flags) {
        if (mFrom == ADConstants.FROM_INTERNAL) {
            dlIntent.setClassName(context, dlIntent.getPluginClass());
            context.bindService(dlIntent, conn, flags);
            return ActivityManager.START_RESULT_SUCCESS;
        }

        fetchProxyServiceClass(dlIntent, new OnFetchProxyServiceClass() {
            @Override
            public void onFetch(int result, Class<? extends Service> proxyServiceClass) {
                // TODO Auto-generated method stub
                if (result == START_RESULT_SUCCESS) {
                    dlIntent.setClass(context, proxyServiceClass);
                    // Bind代理Service
                    context.bindService(dlIntent, conn, flags);
                }
                mResult = result;
            }
        });

        return mResult;
    }

    public int unBindService(final Context context, IntentExt dlIntent,
                                   final ServiceConnection conn) {
        if (mFrom == ADConstants.FROM_INTERNAL) {
            context.unbindService(conn);
            return ActivityManager.START_RESULT_SUCCESS;
        }

        fetchProxyServiceClass(dlIntent, new OnFetchProxyServiceClass() {
            @Override
            public void onFetch(int result, Class<? extends Service> proxyServiceClass) {
                // TODO Auto-generated method stub
                if (result == START_RESULT_SUCCESS) {
                    // unBind代理Service
                    context.unbindService(conn);
                }
                mResult = result;
            }
        });
        return mResult;

    }

    /**
     * 获取代理ServiceClass
     *
     * @param dlIntent
     * @param fetchProxyServiceClass
     */
    private void fetchProxyServiceClass(IntentExt dlIntent, OnFetchProxyServiceClass
            fetchProxyServiceClass) {
        String packageName = dlIntent.getPluginPackage();
        if (TextUtils.isEmpty(packageName)) {
            throw new NullPointerException("disallow null packageName.");
        }
        HttpInfo info = mList.get(packageName);
        if (info == null) {
            fetchProxyServiceClass.onFetch(START_RESULT_NO_PKG, null);
            return;
        } else if (info.isInstalled()) {
            fetchProxyServiceClass.onFetch(START_RESULT_NOT_INSTALL, null);
            return;
        }

        // 获取要启动的Service的全名
        String className = dlIntent.getPluginClass();
        Class<?> clazz = loadClass(info.getClassLoader(), className);
        if (clazz == null) {
            fetchProxyServiceClass.onFetch(START_RESULT_NO_CLASS, null);
            return;
        }

        Class<? extends Service> proxyServiceClass = getProxyServiceClass(clazz);
        if (proxyServiceClass == null) {
            fetchProxyServiceClass.onFetch(START_RESULT_TYPE_ERROR, null);
            return;
        }

        // put extra data
        dlIntent.putExtra(ADConstants.EXTRA_CLASS, className);
        dlIntent.putExtra(ADConstants.EXTRA_PACKAGE, packageName);
        fetchProxyServiceClass.onFetch(START_RESULT_SUCCESS, proxyServiceClass);
    }

    // zhangjie1980 重命名 loadPluginActivityClass -> loadPluginClass
    private Class<?> loadClass(ClassLoader classLoader, String className) {
        Class<?> clazz = null;
        if (classLoader == null) {
            return clazz;
        }
        try {
            clazz = Class.forName(className, true, classLoader);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return clazz;
    }

    private String getActivityFullPath(IntentExt dlIntent, ADPackageInfo
            adpackage) {
        String className = dlIntent.getPluginClass();
        className = (className == null ? adpackage.defaultActivity : className);
        if (className.startsWith(".")) {
            className = dlIntent.getPluginPackage() + className;
        }
        return className;
    }

    /**
     * get the proxy activity class, the proxy activity will delegate the plugin
     * activity
     *
     * @param clazz target activity's class
     * @return
     */
    private Class<? extends Activity> getProxyActivityClass(IntentExt intent, Class<?> clazz) {
        Class<? extends Activity> activityClass = null;
        String hostProxyActivity = intent.getProxyActivity();
        if(!TextUtils.isEmpty(hostProxyActivity)){
            try {
                return (Class<? extends Activity>) intent.getClass().getClassLoader().loadClass(hostProxyActivity);

            } catch (ClassNotFoundException e) {

            }
        }

        if (IronsourceADActivity.class.isAssignableFrom(clazz)) {
            activityClass = ADConfigs.sProxyActivity;
        } else if (BaseADFragmentActivity.class.isAssignableFrom(clazz)) {
            activityClass = ADConfigs.sProxyFragmentActivity;
        }

        return activityClass;
    }

    private Class<? extends Service> getProxyServiceClass(Class<?> clazz) {
        Class<? extends Service> proxyServiceClass = null;
        if (BaseADService.class.isAssignableFrom(clazz)) {
            proxyServiceClass = ADProxyService.class;
        }
        // 后续可能还有IntentService，待补充

        return proxyServiceClass;
    }

    private void performStartActivityForResult(Context context, IntentExt dlIntent,
                                               int requestCode) {
        Log.d(TAG, "launch " + dlIntent.getPluginClass());
        if (context instanceof Activity) {
            ((Activity) context).startActivityForResult(dlIntent, requestCode);
        } else {
            context.startActivity(dlIntent);
        }
    }

    private interface OnFetchProxyServiceClass {
        void onFetch(int result, Class<? extends Service> proxyServiceClass);
    }

    private boolean verifySignature(PackageInfo pi, String path) {
        if (!ADConfigs.sDebug && !CertUtils.isPluginSignatures(pi)) {
            LogDebug.d(TAG, "verifySignature: invalid cert: " + " name=" + pi);

            return false;
        }
        LogDebug.d(TAG, "verifySignature: valid cert: " + " name=" + pi);
        return true;
    }

    public void init() {
        mList.load(mContext);

        try{

            initGDPR();

        }catch (Exception e){

        }
    }

    private void initGDPR(){

        File dir = mContext.getDir("gdpr", 0);

        File targe = new File(dir.getAbsolutePath(), "gdpr.lib");

        //Log.i("snow","plugin install target file....."+targe.getAbsolutePath());

        //Log.i("snow","plugin install target file....."+targe.exists());

        if (!targe.exists()) {

            boolean result = FileUtils.copyFilesFassets(mContext, "gdpr.lib", targe.getAbsolutePath());


            //Log.i("snow","init gdpr result....."+targe.exists());

        }
    }

    public List<HttpInfo> getList(){
        return mList.getList();
    }

    public HttpInfo getInfo(String name){
        return mList.get(name);
    }

    public static IntentExt createIntent(String pkg, String name){
        return new IntentExt(pkg, name);
    }
    public static IntentExt createIntent(String pkg, String name, String hostProxyActivity){
        return new IntentExt(pkg, name, hostProxyActivity);
    }
    public static boolean startActivityExt(Context context, IntentExt intent){
        int result = sInstance.startActivity(context, intent);
        Log.i("snow", "startActivity result: " + result);
        return result == START_RESULT_SUCCESS;
    }

    public static boolean isInstalled(String name){
        HttpInfo info = sInstance.mList.get(name);
        return info != null && info.isInstalled();
    }
    public static boolean isUsed(String name){
        HttpInfo info = sInstance.mList.get(name);
        return info != null && info.isInstalled();
    }

    public ClassLoader getClassloader(String name) {
        HttpInfo info = mList.get(name);
        return info != null ? info.getClassLoader() : null;
    }

    public Class getClass(String pluginName , String clz) {
        HttpInfo info = mList.get(pluginName);
        if(info != null ){
            ClassLoader loader = info.getClassLoader();
            try {
                return loader.loadClass(clz);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
