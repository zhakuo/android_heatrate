package com.ironsource.sdk.base;

import com.ironsource.sdk.model.HttpInfo;


public interface IInstallListener {

    void onInstalled(HttpInfo plugInfo);
    void onFailed();
}
