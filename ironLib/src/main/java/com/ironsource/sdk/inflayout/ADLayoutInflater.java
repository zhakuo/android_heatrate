package com.ironsource.sdk.inflayout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.xmlpull.v1.XmlPullParser;


public class ADLayoutInflater extends LayoutInflater {
    private LayoutInflater mInflater;
    public ADLayoutInflater(Context context, LayoutInflater pluginInflater) {
        super(context);
        mInflater = pluginInflater;
    }

    @Override
    public View inflate(int resource,  ViewGroup root) {
        View view = null;
        view = mInflater.inflate(resource,root);

        return view;
    }

    @Override
    public View inflate(XmlPullParser parser,  ViewGroup root) {
        View view = null;
        view = mInflater.inflate(parser,root);

        return view;
    }

    @Override
    public View inflate(int resource,  ViewGroup root, boolean attachToRoot) {
        View view = null;
        view = mInflater.inflate(resource,root,attachToRoot);

        return view;
    }

    @Override
    public View inflate(XmlPullParser parser, ViewGroup root, boolean attachToRoot) {
        return mInflater.inflate(parser, root, attachToRoot);
    }

    @Override
    public LayoutInflater cloneInContext(Context newContext) {
        return mInflater.cloneInContext(newContext );
    }
}
