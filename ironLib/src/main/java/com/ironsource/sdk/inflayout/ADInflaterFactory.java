package com.ironsource.sdk.inflayout;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import java.lang.reflect.InvocationTargetException;


public class ADInflaterFactory implements LayoutInflater.Factory {

    private LayoutInflater.Factory mOriginal;
    private ClassLoader mClassLoader;

    public ADInflaterFactory(LayoutInflater.Factory original, ClassLoader classLoader) {
        mOriginal = original;
        mClassLoader = classLoader;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        View view = null;
        if (name.contains(".")) {
            try {
                view = (View) Class.forName(name, true, this.mClassLoader).getConstructor(new Class[]{Context.class, AttributeSet.class}).newInstance(new Object[]{context, attrs});
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            if (view != null) {
                return view;
            }
            if (mOriginal != null) {
                view = mOriginal.onCreateView(name, context, attrs);
            }
            if (view != null) {
                return view;
            }

            if (mOriginal != null) {
                view = mOriginal.onCreateView(name, context, attrs);
            }
        }
        if (view == null && mOriginal != null) {
            view = mOriginal.onCreateView(name, context, attrs);
        }
        return view;
    }
}
