package com.ironsource.sdk.nginx;

import android.content.Context;
import android.content.Intent;


public interface INginxAction {
    Object callNginx(Context context, String url, Intent intent, Object arg);
}
