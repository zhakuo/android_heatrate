package com.ironsource.sdk.nginx;


public interface INginxUrl {
    String SCHEMA = "web";
    String WEBHOST = "nginx";
    String NGINX_DOMAIN = SCHEMA + "://" + WEBHOST;
}
