package com.ironsource.sdk.nginx;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class NginxEntry {
    private Class mTargetClz = null;
    private INginxAction mAction = null;

    public NginxEntry(String url, Class target) {
        mTargetClz = target;
    }

    public NginxEntry(INginxAction action) {
        mAction = action;
    }

    public void startActivity(Context context,String url, Intent intent) {
        Intent result = getNginxIntent(url, intent);
        if (mTargetClz != null) {
            result.setClass(context, mTargetClz);
        }
        if (context instanceof Application) {
            result.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(result);
    }

    public Object callNginx(Context context, String url, Intent intent, Object arg) {
        Intent result = getNginxIntent(url, intent);
        return mAction.callNginx(context, url,result,arg);
    }

    private Intent getNginxIntent(String url, Intent intent) {
        intent = intent != null ? intent : new Intent();
        parseUrlToIntent(intent, url);
        return intent;

    }

    private void parseUrlToIntent(Intent intent, String url) {
        try {
            Uri uri = Uri.parse(url);
            if (uri != null) {
                String query = uri.getEncodedQuery();
                String[] params = query.split("&");
                if (params != null && params.length > 0) {
                    for (String str : params) {
                        String[] pair = str.split("=");
                        if (pair != null && pair.length == 2) {
                            intent.putExtra(pair[0], pair[1]);
                        }
                    }
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }
}
