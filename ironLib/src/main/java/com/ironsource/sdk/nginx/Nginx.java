package com.ironsource.sdk.nginx;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import java.util.HashMap;

public class Nginx {
    private static Nginx sInstancesInstance = new Nginx();
    private HashMap<String, NginxEntry> mNginxMap = new HashMap<>(5);

    private Nginx() {
    }

    public static Nginx get(){
        return sInstancesInstance;
    }

    public void registerNginx(String url, NginxEntry action) {
        if (!TextUtils.isEmpty(url) && action != null) {
            mNginxMap.put(url, action);
        }
    }

    /*public void startHostActivity(Context context, String url, Intent intent) {
        NginxEntry action = getEntry(url);
        if (action != null) {
            action.startActivity(context, url,intent);
        }
    }*/

    public Object nginx(Context context, String url, Intent intent, Object org) {
        NginxEntry entry = getEntry(url);
        if (entry != null) {
            return entry.callNginx(context,url, intent, org);
        }
        return null;
    }
    private NginxEntry getEntry(String url){
        String key = url;
        if(url.contains("?")){
            key = url.substring(0,url.indexOf("?") );
        }
        return mNginxMap.get(key);
    }
    public Object nginx(Context context, String url, Intent intent) {
        return nginx(context, url, intent, null);
    }
    public Object nginx(Context context, String url) {
        return nginx(context, url, null, null);
    }
}
