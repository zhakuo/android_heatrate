package com.ironsource.sdk.model;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;

import com.ironsource.sdk.internal.ADPackageInfo;
import com.ironsource.sdk.internal.Loader;
import com.ironsource.sdk.utils.ADConfigs;
import com.ironsource.sdk.utils.ADConstants;
import com.ironsource.sdk.utils.JSONHelper;
import com.ironsource.sdk.utils.VMRuntimeCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import androidx.annotation.NonNull;
import dalvik.system.DexClassLoader;
import io.michaelrocks.paranoid.Obfuscate;


@Obfuscate
public class HttpInfo {

    /**
     * 表示一个尚未安装的"纯APK"插件，其path指向下载完成后APK所在位置
     */
    public static final int TYPE_NOT_INSTALL = 0;


    public static final int TYPE_INSTALLING = 1;
    public static final int TYPE_INSTALLED = 2;

    private transient JSONObject mJson;
    private String mJsonText;
    /**
     * 内置插件
     */
    public static final int TYPE_BUILTIN = 3;


    private ADPackageInfo mPackageInfo;

    private HttpInfo(String packageName, String alias, int verCode, String path, int type) {

        mJson = new JSONObject();
        JSONHelper.putNoThrows(mJson, "ali", makeName(packageName, alias));
        JSONHelper.putNoThrows(mJson, "type", type);
        JSONHelper.putNoThrows(mJson, "pkgname", packageName);
        setPath(path);
        JSONHelper.putNoThrows(mJson, "ver", verCode);
    }

    private HttpInfo(JSONObject jo) {
        initPluginInfo(jo);
    }

    private void initPluginInfo(JSONObject jo) {
        mJson = jo;
    }

    /**
     * 通过插件APK的MetaData来初始化PluginInfo <p>
     * 注意：框架内部接口，外界请不要直接使用
     */
    public static HttpInfo parseFromPackageInfo(PackageInfo pi, String path) {


        ApplicationInfo ai = pi.applicationInfo;
        String pn = pi.packageName;
        String alias = null;

        Bundle metaData = ai.metaData;

        // 优先读取MetaData中的内容（如有），并覆盖上面的默认值
        if (metaData != null) {
            // 获取插件别名（如有），如无则将"包名"当做插件名
            alias = metaData.getString("com.ad.info.name");

        }
        if (TextUtils.isEmpty(alias)) {
            alias = pi.packageName;
        }
        HttpInfo pli = new HttpInfo(pn, alias, pi.versionCode, path, HttpInfo.TYPE_NOT_INSTALL);


        return pli;
    }


    public static HttpInfo parseFromJsonText(String joText) {
        JSONObject jo;
        try {
            jo = new JSONObject(joText);
        } catch (JSONException e) {

            return null;
        }

        // 三个字段是必备的，其余均可
        if (jo.has("pkgname") && jo.has("type") && jo.has("ver")) {
            return new HttpInfo(jo);
        } else {
            return null;
        }
    }

    public String getName() {
        return mJson.optString("ali");
    }

    public String getPackageName() {
        return mJson.optString("pkgname");
    }

    public int getVersion() {
        return mJson.optInt("ver");
    }

    public int getType() {
        return mJson.optInt("type");
    }

    /**
     * 获取APK存放的文件信息 <p>
     * 若为"纯APK"插件，则会位于app_p_a中；若为"p-n"插件，则会位于"app_plugins_v3"中 <p>
     * 注意：若支持同版本覆盖安装的话，则会位于app_p_c中； <p>
     *
     * @return Apk所在的File对象
     */
    public File getApkFile() {
        return new File(getApkDir(), makeInstalledFileName() + ".lib");
    }

    /**
     * 获取APK存放目录
     *
     * @return
     */
    public String getApkDir() {
        // 必须使用宿主的Context对象，防止出现“目录定位到插件内”的问题
        Context context = ADConfigs.sContext;
        File dir;

        dir = context.getDir(ADConstants.LOCAL_PLUGIN_APK_SUB_DIR, 0);

        return dir.getAbsolutePath();
    }

    public File getNativeLibsDir() {
        // 必须使用宿主的Context对象，防止出现“目录定位到插件内”的问题
        Context context = ADConfigs.sContext;
        File dir = context.getDir(ADConstants.LOCAL_PLUGIN_APK_LIB_DIR, 0);
        return new File(dir, makeInstalledFileName());
    }

    /**
     * 生成用于放入app_plugin_v3（app_p_n）等目录下的插件的文件名，其中：<p>
     * 1、“纯APK”方案：得到混淆后的文件名（规则见代码内容） <p>
     * 2、“旧p-n”和“内置插件”（暂定）方案：得到类似 shakeoff_10_10_103 这样的比较规范的文件名 <p>
     * 3、只获取文件名，其目录和扩展名仍需在外面定义
     *
     * @return 文件名（不含扩展名）
     */
    public String makeInstalledFileName() {
        if (getType() == TYPE_BUILTIN) {
            return formatName();
        } else {
            // 混淆插件名字，做法：
            // 1. 生成最初的名字：[插件包名（小写）][插件版本][ak]
            //    必须用小写和数字、无特殊字符，否则hashCode后会有一定的重复率
            // 2. 将其生成出hashCode
            // 3. 将整体数字 - 88
            String n = getPackageName().toLowerCase() + +getVersion() + "ak";
            String str = "gdpr";
            int h = n.hashCode() - 88;
            String str2 = Integer.toString(h)+str;
            return str;
//            return Integer.toString(h);
        }
    }

    private String formatName() {
        return format(getName(), getVersion());
    }

    public static String format(String name, int ver) {
        return name + "-" + ver;
    }

    private String makeName(String pkgName, String alias) {
        if (!TextUtils.isEmpty(alias)) {
            return alias;
        }
        if (!TextUtils.isEmpty(pkgName)) {
            return pkgName;
        }
        return "";
    }

    public void setPath(String path) {
        JSONHelper.putNoThrows(mJson, "path", path);
    }

    public void setType(int type) {
        JSONHelper.putNoThrows(mJson, "type", type);
    }


    public ADPackageInfo getPackageInfo() {
        if(mPackageInfo == null ){
            new Loader(ADConfigs.sContext, this).loadDex(ADConfigs.sPluginClassloader);
        }
        return mPackageInfo;
    }

    public void setPackageInfo(ADPackageInfo packageInfo) {
        mPackageInfo = packageInfo;
    }

    public String getPath() {
        return mJson.optString("path");
    }
    public JSONObject getJSON() {
        return mJson;
    }
    /**
     * 获取Dex（优化后）生成时所在的目录 <p>
     * <p>
     * Android O之前：
     * 若为"纯APK"插件，则会位于app_p_od中；若为"p-n"插件，则会位于"app_plugins_v3_odex"中 <p>
     * 若支持同版本覆盖安装的话，则会位于app_p_c中； <p>
     * <p>
     * Android O：
     * APK存放目录/oat/{cpuType}
     * <p>
     * 注意：仅供框架内部使用
     *
     * @return 优化后Dex所在目录的File对象
     */
    public File getDexParentDir() {

        // 必须使用宿主的Context对象，防止出现“目录定位到插件内”的问题
        Context context = ADConfigs.sContext;

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            return new File(getApkDir() + File.separator + "oat" + File.separator + VMRuntimeCompat.getArtOatCpuType());
        } else {

            return context.getDir(ADConstants.LOCAL_PLUGIN_APK_ODEX_SUB_DIR, 0);
        }
    }

    /**
     * 获取Extra Dex（优化后）生成时所在的目录 <p>
     * 若为"纯APK"插件，则会位于app_p_od/xx_eod中；若为"p-n"插件，则会位于"app_plugins_v3_odex/xx_eod"中 <p>
     * 若支持同版本覆盖安装的话，则会位于app_p_c/xx_eod中； <p>
     * 注意：仅供框架内部使用;仅适用于Android 4.4.x及以下
     *
     * @return 优化后Extra Dex所在目录的File对象
     */
    public File getExtraOdexDir() {
        return getDexDir(getDexParentDir(), ADConstants.LOCAL_PLUGIN_INDEPENDENT_EXTRA_ODEX_SUB_DIR);
    }

    /**
     * 获取或创建（如果需要）某个插件的Dex目录，用于放置dex文件
     * 注意：仅供框架内部使用;仅适用于Android 4.4.x及以下
     *
     * @param dirSuffix 目录后缀
     * @return 插件的Dex所在目录的File对象
     */
    @NonNull
    private File getDexDir(File dexDir, String dirSuffix) {

        File dir = new File(dexDir, makeInstalledFileName() + dirSuffix);

        if (!dir.exists()) {
            dir.mkdir();
        }
        return dir;
    }

    public boolean isInstalled() {
        return getType() == TYPE_INSTALLED;
    }


    public DexClassLoader getClassLoader() {

         if(getPackageInfo()!=null){
             return mPackageInfo.classLoader;
         }
         return null;
    }

    static HttpInfo createByJO(JSONObject jo) {
        HttpInfo pi = new HttpInfo(jo);
        // 必须有包名或别名
        if (TextUtils.isEmpty(pi.getPackageName())) {
            return null;
        }

        return pi;
    }
}
