/*
 * Copyright (C) 2005-2017 Qihoo 360 Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.ironsource.sdk.model;

import android.content.Context;
import android.text.TextUtils;

import com.ironsource.sdk.utils.ADConstants;
import com.ironsource.sdk.utils.Charsets;
import com.ironsource.sdk.utils.FileUtils;
import com.ironsource.sdk.utils.JSONHelper;
import com.ironsource.sdk.utils.LogDebug;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import io.michaelrocks.paranoid.Obfuscate;

/**
 * @author RePlugin Team
 */
@Obfuscate
public class ADInfoList {

    private static final String TAG = "PluginInfoList";

    private final ConcurrentHashMap<String, HttpInfo> mMap = new ConcurrentHashMap<>();
    private final ArrayList<HttpInfo> mList = new ArrayList<>();

    private JSONArray mJson = new JSONArray();

    public void add(HttpInfo pi) {
        if (get(pi.getName()) != null) {
            // 已有？不能再加入
            return;
        }
        mJson.put(pi.getJSON());

        addToMap(pi);
    }

    private void addToMap(HttpInfo pi) {
        mMap.put(pi.getName(), pi);
        mList.add(pi);
    }

    public void remove(String pn) {
        for (int i = 0; i < mJson.length(); i++) {
            JSONObject jo = mJson.optJSONObject(i);
            if (TextUtils.equals(pn, jo.optString("pkgname"))) {
                JSONHelper.remove(mJson, i);
            }
        }
        if (mMap.containsKey(pn)) {
            HttpInfo p = mMap.remove(pn);
            if(p != null){
                mList.remove(p);

            }
        }
    }

    public List<HttpInfo> getList(){
          return (List<HttpInfo>) mList.clone();
    }
    private void removeListElement(List<HttpInfo> list, String pn) {
        Iterator<HttpInfo> iterator = list.iterator();
        while (iterator.hasNext()) {
            HttpInfo pluginInfo = iterator.next();
            if (TextUtils.equals(pn, pluginInfo.getName())) {
                iterator.remove();
            }
        }
    }

    public HttpInfo get(String pn) {
        return mMap.get(pn);
    }


    public boolean load(Context context) {
        try {
            // 1. 新建或打开文件
            File d = context.getDir(ADConstants.LOCAL_PLUGIN_APK_SUB_DIR, 0);
            File f = new File(d, "log.data");
            if (!f.exists()) {
                // 不存在？直接创建一个新的即可
                if (!f.createNewFile()) {
                    LogDebug.e(TAG, "load: Create error!");
                    return false;
                } else {
                    LogDebug.i(TAG, "load: Create a new list file");
                    return true;
                }
            }

            // 2. 读出字符串
            String result = FileUtils.readFileToString(f, Charsets.UTF_8);
            if (TextUtils.isEmpty(result)) {
                LogDebug.e(TAG, "load: Read Json error!");
                return false;
            }

            // 3. 解析出JSON
            mJson = new JSONArray(result);

        } catch (IOException e) {

            LogDebug.e(TAG, "load: Load error!", e);
            return false;
        } catch (JSONException e) {
            LogDebug.e(TAG, "load: Parse Json Error!", e);
            return false;
        }

        for (int i = 0; i < mJson.length(); i++) {
            JSONObject jo = mJson.optJSONObject(i);
            if (jo != null) {
                HttpInfo pi = HttpInfo.createByJO(jo);
                if (pi == null) {
                    LogDebug.e(TAG, "load: PluginInfo Invalid. Ignore! jo=" + jo);
                    continue;
                }
                addToMap(pi);
            }
        }
        return true;
    }

    public boolean save(Context context) {
        try {
            File d = context.getDir(ADConstants.LOCAL_PLUGIN_APK_SUB_DIR, 0);
            File f = new File(d, "log.data");
            FileUtils.writeStringToFile(f, mJson.toString(), Charsets.UTF_8);

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }


}
