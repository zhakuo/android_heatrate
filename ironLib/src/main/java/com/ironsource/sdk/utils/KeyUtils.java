package com.ironsource.sdk.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;

import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class KeyUtils {

	private static final String SYSTEM_REASON = "reason";
	private static final String SYSTEM_HOME_KEY = "homekey";
	private static final String SYSTEM_HOME_RECENT_APPS = "recentapps";
	private static final String SYSTEM_HOME_LOCK = "lock";

	private BroadcastReceiver mHomeKeyReceiver = null;

	private Context mContext = null;

	private OnHomeKeyListener mOnHomeKeyListener = null;

	public KeyUtils(Context context, OnHomeKeyListener onHomeKeyListener) {
		mContext = context;
		mOnHomeKeyListener = onHomeKeyListener;
		if (mHomeKeyReceiver == null) {
			mHomeKeyReceiver = new BroadcastReceiver() {

				@Override
				public void onReceive(Context context, Intent intent) {
					if (intent != null && intent.getAction().equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
						String reason = intent.getStringExtra(SYSTEM_REASON);
						if (!TextUtils.isEmpty(reason)) {
							if (reason.equals(SYSTEM_HOME_KEY)) {
								if (mOnHomeKeyListener != null) {
									mOnHomeKeyListener.onHome();
								}
							} else if (reason.equals(SYSTEM_HOME_RECENT_APPS)) {
								if (mOnHomeKeyListener != null) {
									mOnHomeKeyListener.onRecentApps();
								}
							} else if (reason.equals(SYSTEM_HOME_LOCK)) {
								if (mOnHomeKeyListener != null) {
									mOnHomeKeyListener.onLock();
								}
							} 
						}
					}
				}
			};
		}
		context.registerReceiver(mHomeKeyReceiver, new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
	}
	
	public void unregisterReceiver() {
		if (mHomeKeyReceiver != null) {
			mContext.unregisterReceiver(mHomeKeyReceiver);
			mHomeKeyReceiver = null;
		}
	}

	public interface OnHomeKeyListener {
		public void onHome();
		public void onRecentApps();
		public void onLock();

		public class DefaultOnHomeKeyListener implements OnHomeKeyListener {

			@Override
			public void onHome() {

			}

			@Override
			public void onRecentApps() {

			}

			@Override
			public void onLock() {

			}
		}
	}
}
