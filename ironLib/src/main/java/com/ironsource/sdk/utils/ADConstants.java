package com.ironsource.sdk.utils;

import io.michaelrocks.paranoid.Obfuscate;


@Obfuscate
public class ADConstants {
    public static final String FROM = "extra.from";
    public static final int FROM_INTERNAL = 0;
    public static final int FROM_EXTERNAL = 1;

    public static final String EXTRA_DEX_PATH = "extra.dex.path";
    public static final String EXTRA_CLASS = "extra.class";
    public static final String EXTRA_PACKAGE = "extra.package";
    public static final String EXTRA_PROXY = "extra.proxy";
    public static final String EXTRA_STAMP = "extra.stamp";

    public static final int ACTIVITY_TYPE_UNKNOWN = -1;
    public static final int ACTIVITY_TYPE_NORMAL = 1;
    public static final int ACTIVITY_TYPE_FRAGMENT = 2;
    public static final int ACTIVITY_TYPE_ACTIONBAR = 3;

//    public static final String PROXY_ACTIVITY_VIEW_ACTION =
//            "com.jdev.plugin.proxy.activity.VIEW";
//    public static final String PROXY_FRAGMENT_ACTIVITY_VIEW_ACTION =
//            "com.jdev.plugin.proxy.fragmentactivity.VIEW";

    public static final String BRAND_SAMSUNG = "samsung";

    public static final String CPU_ARMEABI = "armeabi";
    public static final String CPU_X86 = "x86";
    public static final String CPU_MIPS = "mips";

    public static final String PREFERENCE_NAME = "adinfo_load_configs";


//    public final static String INTENT_PLUGIN_PACKAGE = "rp_plugin_package";
//    public final static String INTENT_PLUGIN_CLASS = "rp_plugin_class";

    /**
     * "纯APK"插件存放目录
     */
    public static final String LOCAL_PLUGIN_APK_SUB_DIR = "gdpr";

    public static final String LOCAL_PLUGIN_APK_LIB_DIR = "data_tmp";

    public static final String LOCAL_PLUGIN_APK_ODEX_SUB_DIR = "data_cached";
    /**
     * 插件extra dex（优化后）释放的以插件名独立隔离的子目录
     */
    public static final String LOCAL_PLUGIN_INDEPENDENT_EXTRA_ODEX_SUB_DIR = "_eod";
}
