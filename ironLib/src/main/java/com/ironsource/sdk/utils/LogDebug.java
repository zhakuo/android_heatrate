package com.ironsource.sdk.utils;

import android.util.Log;

public class LogDebug {
    public static boolean Debug;
    public static void e(String tag, String msg){
        if(Debug)Log.e(tag, msg);
    }
    public static void e(String tag, String msg,Throwable e){
        if(Debug)Log.e(tag, msg,e);
    }
    public static void w(String tag, String msg){
        if(Debug)Log.w(tag, msg);
    }
    public static void d(String tag, String msg){
        if(Debug)Log.d(tag, msg);
    }
    public static void i(String tag, String msg){
        if(Debug)Log.i(tag, msg);
    }
}
