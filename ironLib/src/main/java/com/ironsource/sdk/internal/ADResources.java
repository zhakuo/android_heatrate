package com.ironsource.sdk.internal;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Movie;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import java.io.InputStream;

import androidx.annotation.RequiresApi;
import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class ADResources extends Resources {
    private Resources mParent;

    public ADResources(AssetManager assets, Resources parent) {
        super(assets, parent.getDisplayMetrics(), parent.getConfiguration());
        mParent = parent;
    }

    @Override
    public TypedArray obtainAttributes(AttributeSet set, int[] attrs) {
        try {
            return super.obtainAttributes(set, attrs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mParent.obtainAttributes(set, attrs);
    }

    @Override
    public TypedArray obtainTypedArray(int id) throws NotFoundException {
        try {
            return super.obtainTypedArray(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.obtainTypedArray(id);
    }

    @Override
    public String getQuantityString(int id, int quantity) throws NotFoundException {
        try {
            return super.getQuantityString(id, quantity);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getQuantityString(id, quantity);
    }

    @Override
    public String getQuantityString(int id, int quantity, Object... formatArgs) throws NotFoundException {
        try {
            return super.getQuantityString(id, quantity, formatArgs);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return mParent.getQuantityString(id, quantity, formatArgs);
    }

    @Override
    public CharSequence getText(int id) throws NotFoundException {
        try {
            return super.getText(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return mParent.getText(id);
    }

    @Override
    public CharSequence getQuantityText(int id, int quantity) throws NotFoundException {
        try {
            return super.getQuantityText(id, quantity);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return mParent.getQuantityText(id, quantity);
    }

    @Override
    public XmlResourceParser getLayout(int id) throws NotFoundException {
        try {
            return super.getLayout(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return mParent.getLayout(id);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public ColorStateList getColorStateList(int id,  Theme theme) throws NotFoundException {
        try {
            return super.getColorStateList(id, theme);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getColorStateList(id, theme);
    }

    @Override
    public void getValue(String name, TypedValue outValue, boolean resolveRefs) throws NotFoundException {
        try {
            super.getValue(name, outValue, resolveRefs);
        } catch (NotFoundException e) {
            mParent.getValue(name, outValue, resolveRefs);
        }
    }

    @Override
    public InputStream openRawResource(int id) throws NotFoundException {
        try {
            return super.openRawResource(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return mParent.openRawResource(id);
    }

    @Override
    public InputStream openRawResource(int id, TypedValue value) throws NotFoundException {
        try {
            return super.openRawResource(id, value);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.openRawResource(id, value);
    }

    @Override
    public ColorStateList getColorStateList(int id) throws NotFoundException {
        try {
            return super.getColorStateList(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return mParent.getColorStateList(id);
    }

    @Override
    public AssetFileDescriptor openRawResourceFd(int id) throws NotFoundException {
        try {
            return super.openRawResourceFd(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.openRawResourceFd(id);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public int getColor(int id,  Theme theme) throws NotFoundException {
        try {
            return super.getColor(id, theme);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getColor(id, theme);
    }


    @Override
    public float getDimension(int id) throws NotFoundException {
        try {
            return super.getDimension(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return mParent.getDimension(id);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public Typeface getFont(int id) throws NotFoundException {
        try {
            return super.getFont(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getFont(id);
    }

    @Override
    public String getString(int id) throws NotFoundException {
        try {
            return super.getString(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getString(id);
    }

    @Override
    public String getString(int id, Object... formatArgs) throws NotFoundException {
        try {
            return super.getString(id, formatArgs);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getString(id, formatArgs);
    }

    @Override
    public CharSequence getText(int id, CharSequence def) {
        try {
            return super.getText(id, def);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mParent.getText(id, def);
    }

    @Override
    public CharSequence[] getTextArray(int id) throws NotFoundException {
        try {
            return super.getTextArray(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getTextArray(id);
    }

    @Override
    public String[] getStringArray(int id) throws NotFoundException {
        try {
            return super.getStringArray(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getStringArray(id);
    }

    @Override
    public int[] getIntArray(int id) throws NotFoundException {
        try {
            return super.getIntArray(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getIntArray(id);
    }

    @Override
    public int getDimensionPixelOffset(int id) throws NotFoundException {
        try {
            return super.getDimensionPixelOffset(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getDimensionPixelOffset(id);
    }

    @Override
    public int getDimensionPixelSize(int id) throws NotFoundException {
        try {
            return super.getDimensionPixelSize(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getDimensionPixelSize(id);
    }

    @Override
    public float getFraction(int id, int base, int pbase) {
        try {
            return super.getFraction(id, base, pbase);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mParent.getFraction(id,base,pbase);
    }

    @Override
    public Drawable getDrawable(int id) throws NotFoundException {
        try {
            return super.getDrawable(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getDrawable(id);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Drawable getDrawable(int id, Theme theme) throws NotFoundException {
        try {
            return super.getDrawable(id, theme);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getDrawable(id,theme);
    }

    @Override
    public Drawable getDrawableForDensity(int id, int density) throws NotFoundException {
        try {
            return super.getDrawableForDensity(id, density);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getDrawableForDensity(id,density);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Drawable getDrawableForDensity(int id, int density,Theme theme) {
        try {
            return super.getDrawableForDensity(id, density, theme);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mParent.getDrawableForDensity(id,density,theme);
    }

    @Override
    public Movie getMovie(int id) throws NotFoundException {
        try {
            return super.getMovie(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getMovie(id);
    }

    @Override
    public int getColor(int id) throws NotFoundException {
        try {
            return super.getColor(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getColor(id);
    }
    @Override
    public boolean getBoolean(int id) throws NotFoundException {
        try {
            return super.getBoolean(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getBoolean(id);
    }

    @Override
    public int getInteger(int id) throws NotFoundException {
        try {
            return super.getInteger(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getInteger(id);
    }

    @Override
    public XmlResourceParser getAnimation(int id) throws NotFoundException {
        try {
            return super.getAnimation(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getAnimation(id);
    }

    @Override
    public XmlResourceParser getXml(int id) throws NotFoundException {
        try {
            return super.getXml(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getXml(id);
    }

    @Override
    public void getValue(int id, TypedValue outValue, boolean resolveRefs) throws NotFoundException {
        try {
            super.getValue(id, outValue, resolveRefs);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
         mParent.getValue(id,outValue,resolveRefs);
    }

    @Override
    public void getValueForDensity(int id, int density, TypedValue outValue, boolean resolveRefs) throws NotFoundException {
        try {
            super.getValueForDensity(id, density, outValue, resolveRefs);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        mParent.getValueForDensity(id,density,outValue,resolveRefs);
    }


    @Override
    public DisplayMetrics getDisplayMetrics() {
        return mParent.getDisplayMetrics();
    }

    @Override
    public Configuration getConfiguration() {
        return mParent.getConfiguration();
    }

    @Override
    public int getIdentifier(String name, String defType, String defPackage) {
        try {
            int id= super.getIdentifier(name, defType, defPackage);
            if(id == 0){
                return  mParent.getIdentifier(name, defType, defPackage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mParent.getIdentifier(name, defType, defPackage);
    }

    @Override
    public String getResourceName(int resid) throws NotFoundException {
        try {
            return super.getResourceName(resid);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getResourceName(resid);
    }

    @Override
    public String getResourcePackageName(int resid) throws NotFoundException {
        try {
            return super.getResourcePackageName(resid);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getResourcePackageName(resid);
    }

    @Override
    public String getResourceTypeName(int resid) throws NotFoundException {
        try {
            return super.getResourceTypeName(resid);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getResourceTypeName(resid);
    }

    @Override
    public String getResourceEntryName(int resid) throws NotFoundException {
        try {
            return super.getResourceEntryName(resid);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return mParent.getResourceEntryName(resid);
    }


}
