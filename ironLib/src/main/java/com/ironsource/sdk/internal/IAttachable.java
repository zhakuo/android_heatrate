package com.ironsource.sdk.internal;

import com.ironsource.sdk.base.ActivityManager;
import com.ironsource.sdk.base.IAD;

public interface IAttachable {
    public void attach(IAD proxyActivity, ActivityManager pluginManager);

}
