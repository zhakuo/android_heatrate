package com.ironsource.sdk.internal;

import android.content.Intent;
import android.os.Parcelable;

import com.ironsource.sdk.utils.ADConfigs;

import java.io.Serializable;

import io.michaelrocks.paranoid.Obfuscate;


@Obfuscate
public class IntentExt extends Intent {


    private String mPluginPackage;
    private String mPluginClass;
    private String mProxyActivity;
    public IntentExt() {
        super();
    }

    public IntentExt(String pluginPackage) {
        super();
        this.mPluginPackage = pluginPackage;
    }

    public IntentExt(String pluginPackage, String pluginClass) {
        super();
        this.mPluginPackage = pluginPackage;
        this.mPluginClass = pluginClass;
    }
    public IntentExt(String pluginPackage, String pluginClass, String proxyActivity) {
        super();
        this.mPluginPackage = pluginPackage;
        this.mPluginClass = pluginClass;
        mProxyActivity = proxyActivity;
    }
    public IntentExt(String pluginPackage, Class<?> clazz, String proxyActivity) {
        super();
        this.mPluginPackage = pluginPackage;
        this.mPluginClass = clazz.getName();
        mProxyActivity = proxyActivity;
    }


    public IntentExt(String pluginPackage, Class<?> clazz) {
        super();
        this.mPluginPackage = pluginPackage;
        this.mPluginClass = clazz.getName();
    }


    public String getPluginPackage() {
        return mPluginPackage;
    }

    public void setPluginPackage(String pluginPackage) {
        this.mPluginPackage = pluginPackage;
    }

    public String getPluginClass() {
        return mPluginClass;
    }

    public void setPluginClass(String pluginClass) {
        this.mPluginClass = pluginClass;
    }

    public void setPluginClass(Class<?> clazz) {
        this.mPluginClass = clazz.getName();
    }

    @Override
    public Intent putExtra(String name, Parcelable value) {
        setupExtraClassLoader(value);
        return super.putExtra(name, value);
    }

    @Override
    public Intent putExtra(String name, Serializable value) {
        setupExtraClassLoader(value);
        return super.putExtra(name, value);
    }

    private void setupExtraClassLoader(Object value) {
        ClassLoader pluginLoader = value.getClass().getClassLoader();
        ADConfigs.sPluginClassloader = pluginLoader;
        setExtrasClassLoader(pluginLoader);
    }

    public String getProxyActivity() {
        return mProxyActivity;
    }
}
