package com.ironsource.sdk.internal;

import com.ironsource.sdk.utils.ADConfigs;
import com.ironsource.sdk.utils.LogDebug;
import com.ironsource.sdk.utils.ReflectUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import dalvik.system.DexClassLoader;
import io.michaelrocks.paranoid.Obfuscate;


@Obfuscate
public class ADLoader extends DexClassLoader {
    public static String TAG = "PluginClassLoader";
    private final ClassLoader mHostClassLoader;

    private static Method sLoadClassMethod;

    private String mPluginName;

    public ADLoader(String dexPath, String optimizedDirectory, String librarySearchPath, ClassLoader parent) {
        super(dexPath, optimizedDirectory, librarySearchPath, parent);
        mHostClassLoader = ADConfigs.sPluginClassloader;
        initMethods(mHostClassLoader);
    }

    private static void initMethods(ClassLoader cl) {
        Class<?> clz = cl.getClass();
        if (sLoadClassMethod == null) {
            sLoadClassMethod = ReflectUtils.getMethod(clz, "loadClass", String.class, Boolean.TYPE);
            if (sLoadClassMethod == null) {
                throw new NoSuchMethodError("loadClass");
            }
        }
    }

    @Override
    protected Class<?> loadClass(String className, boolean resolve) throws ClassNotFoundException {
        // 插件自己的Class。从自己开始一直到BootClassLoader，采用正常的双亲委派模型流程，读到了就直接返回
        Class<?> pc = null;
        ClassNotFoundException cnfException = null;
        // 若插件里没有此类，则会从宿主ClassLoader中找，找到了则直接返回
        // 注意：需要读取isUseHostClassIfNotFound开关。默认为关闭的。可参见该开关的说明

        try {
            pc = super.loadClass(className, resolve);
            if (pc != null) {
                // 只有开启“详细日志”才会输出，防止“刷屏”现象
                return pc;
            }
        } catch (ClassNotFoundException e) {
            // Do not throw "e" now
            cnfException = e;

            try {
                return loadClassFromHost(className, resolve);
            } catch (ClassNotFoundException e1) {
                // Do not throw "e1" now
                cnfException = e1;

                LogDebug.e(TAG, "loadClass ClassNotFoundException, from HostClassLoader&&PluginClassLoader, cn=" + className + ", pluginName=" + mPluginName);
            }

        }


        // At this point we can throw the previous exception
        if (cnfException != null) {
            throw cnfException;
        }
        return null;
    }

    private Class<?> loadClassFromHost(String className, boolean resolve) throws ClassNotFoundException {
        Class<?> c;
        try {
            c = (Class<?>) sLoadClassMethod.invoke(mHostClassLoader, className, resolve);
            // 只有开启“详细日志”才会输出，防止“刷屏”现象
            LogDebug.w(TAG, "loadClass: load host class, cn=" + className + ", cz=" + c);
        } catch (IllegalAccessException e) {
            // Just rethrow
            throw new ClassNotFoundException("Calling the loadClass method failed (IllegalAccessException)", e);
        } catch (InvocationTargetException e) {
            // Just rethrow
            throw new ClassNotFoundException("Calling the loadClass method failed (InvocationTargetException)", e);
        }
        return c;
    }
}
