package com.ironsource.sdk.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.ironsource.sdk.base.IInstallListener;
import com.ironsource.sdk.model.HttpInfo;
import com.ironsource.sdk.utils.FileUtils;
import com.ironsource.sdk.utils.LogDebug;
import com.ironsource.sdk.utils.SoManager;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class InstallTask implements Runnable {
    private static Executor sExecutor = Executors.newSingleThreadExecutor();
    static String TAG = "InstallTask";
    private HttpInfo mPluginInfo;
    private Context mContext;
    private IInstallListener mListener;
    private PackageInfo mPackageInfo;

    public InstallTask(Context context, HttpInfo pluginInfo) {
        mPluginInfo = pluginInfo;
        mContext = context;
    }

    public void install(IInstallListener listener) {
        mListener = listener;
        sExecutor.execute(this);
    }

    private void copySoLib() {
        String soDir = mPluginInfo.getNativeLibsDir().getAbsolutePath();
        SoManager.getSoLoader().copyPluginSoLib(mContext, mPluginInfo.getPath(), soDir);
    }

    @Override
    public void run() {
        mPackageInfo = mContext.getPackageManager().getPackageArchiveInfo(mPluginInfo.getPath(),
                PackageManager.GET_ACTIVITIES | PackageManager.GET_SERVICES);
        if (mPackageInfo == null) {
            if (mListener != null) mListener.onFailed();
            return;
        }
        if(moveApk(mPluginInfo.getPath(), mPluginInfo)){
            copySoLib();
        }
        mPluginInfo.setType(HttpInfo.TYPE_INSTALLED);

        mListener.onInstalled(mPluginInfo);
    }

//    private PluginPackageInfo preparePluginEnv(PackageInfo packageInfo, String dexPath) {
//
//
//
//        // create pluginPackage
//        return new PluginPackageInfo(dexClassLoader, resources, packageInfo);
//    }




//    private Resources createResources(AssetManager assetManager) {
//        Resources superRes = mContext.getResources();
//        Resources resources = new Resources(assetManager, superRes.getDisplayMetrics(), superRes.getConfiguration());
//
//        return resources;
//    }

    private boolean moveApk(String path, HttpInfo instPli) {
        File srcFile = new File(path);
        File newFile = instPli.getApkFile();

        // 插件已被释放过一次？通常“同版本覆盖安装”时，覆盖次数超过2次的会出现此问题
        // 此时，直接删除安装路径下的文件即可，这样就可以直接Move/Copy了
        if (newFile.exists()) {
            FileUtils.deleteQuietly(newFile);
        }

        // 将源APK文件移动/复制到安装路径下
        try {
            FileUtils.moveFile(srcFile, newFile);
        } catch (IOException e) {
            LogDebug.e(TAG, "copyOrMoveApk: Copy/Move Failed! src=" + srcFile + "; dest=" + newFile, e);
            return false;
        }

        instPli.setPath(newFile.getAbsolutePath());

        return true;
    }


}
