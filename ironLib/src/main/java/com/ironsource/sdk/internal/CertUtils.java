/*
 * Copyright (C) 2005-2017 Qihoo 360 Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed To in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.ironsource.sdk.internal;

import android.content.pm.PackageInfo;
import android.content.pm.Signature;
import android.text.TextUtils;

import com.ironsource.sdk.utils.StringUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import io.michaelrocks.paranoid.Obfuscate;

/**
 * @author RePlugin Team
 */
@Obfuscate
public class CertUtils {

    /**
     *
     */
    public static final ArrayList<String> SIGNATURES = new ArrayList<String>();

    public static final boolean isPluginSignatures(PackageInfo info) {
        if (info == null) {
            return false;
        }

        if (info.signatures == null) {
            return false;
        }

        for (Signature signature : info.signatures) {
            boolean match = false;
            String md5 = StringUtils.toHexString(md5NonE(signature.toByteArray()));
            for (String element : SIGNATURES) {
                if (TextUtils.equals(md5, element)) {
                    match = true;
                    break;
                }
            }
            if (!match) {
                return false;
            }
        }

        return true;
    }

    public static final byte[] md5(byte buffer[]) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.update(buffer, 0, buffer.length);
        return digest.digest();
    }

    public static final byte[] md5NonE(byte buffer[]) {
        try {
            return md5(buffer);
        } catch (NoSuchAlgorithmException e) {
        }
        return new byte[0];
    }
}
