/*
 * Copyright (C) 2014 singwhatiwanna(任玉刚) <singwhatiwanna@gmail.com>
 *
 * collaborator:田啸,宋思宇,Mr.Simple
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ironsource.sdk.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;

import com.ironsource.sdk.base.ActivityManager;
import com.ironsource.sdk.base.IAD;
import com.ironsource.sdk.inflayout.ADInflaterFactory;
import com.ironsource.sdk.inflayout.ADLayoutInflater;
import com.ironsource.sdk.model.HttpInfo;
import com.ironsource.sdk.utils.ADConfigs;
import com.ironsource.sdk.utils.ADConstants;

import java.lang.reflect.Constructor;

import io.michaelrocks.paranoid.Obfuscate;

/**
 * This is a plugin activity proxy, the proxy will create the plugin activity
 * with reflect, and then call the plugin activity's attach、onCreate method, at
 * this time, the plugin activity is running.
 * 
 * @author mrsimple
 */
@Obfuscate
public class ADProxyImpl extends ContextWrapper {

    private static final String TAG = "DLProxyImpl";

    private String mClass;
    private String mPackageName;

    private HttpInfo mPluginPackage;
    private ActivityManager mPluginManager;

    private Resources mResources;
    private Theme mTheme;
//    private PluginContext mPluginContext;

    private ActivityInfo mActivityInfo;
    private Activity mProxyActivity;
    protected IAD mPluginActivity;
    public ClassLoader mPluginClassLoader;
    private boolean mForPoxy = false;
    private LayoutInflater mInflater;
    public ADProxyImpl(Activity activity) {
        super(ADConfigs.sContext);
        mProxyActivity = activity;
    }
    public void beforeSuperOnCreate(Intent intent){
        if(mPluginPackage == null){
            //出现attach没被调用情况？
            attach(intent);
        }
        initializeActivityInfo();
        handleActivityInfo();
    }

    public void attach(Intent intent){
        intent.setExtrasClassLoader(ADConfigs.sPluginClassloader);
        mPackageName = intent.getStringExtra(ADConstants.EXTRA_PACKAGE);
        mClass = intent.getStringExtra(ADConstants.EXTRA_CLASS);

        mPluginManager = ActivityManager.getInstance(mProxyActivity);
        mPluginPackage = mPluginManager.getPackage(mPackageName);
        mResources = mPluginPackage.getPackageInfo().resources;

    }

    private void initializeActivityInfo() {
        PackageInfo packageInfo = mPluginPackage.getPackageInfo().packageInfo;
        if ((packageInfo.activities != null) && (packageInfo.activities.length > 0)) {
            if (mClass == null) {
                mClass = packageInfo.activities[0].name;
            }

            //Finals 修复主题BUG
            int defaultTheme = packageInfo.applicationInfo.theme;
            for (ActivityInfo a : packageInfo.activities) {
                if (a.name.equals(mClass)) {
                    mActivityInfo = a;
                    // Finals ADD 修复主题没有配置的时候插件异常

                    //s//Log.i("snow","plugin install....get mActivityInfo theme...."+mActivityInfo.theme);
                    //s//Log.i("snow","plugin install....get defaultTheme...."+defaultTheme);

                    mActivityInfo.theme =  android.R.style.Theme_Translucent_NoTitleBar_Fullscreen;

                    if (mActivityInfo.theme == 0) {
                        if (defaultTheme != 0) {
                            mActivityInfo.theme = defaultTheme;
                        } else {
//                            if (Build.VERSION.SDK_INT >= 14) {
//                                mActivityInfo.theme = android.R.style.Theme_DeviceDefault;
//                            } else {
//                                mActivityInfo.theme = android.R.style.Theme;
//                            }
                        }
                    }
                }
            }

        }
    }

    private void handleActivityInfo() {
        Log.d(TAG, "handleActivityInfo, theme=" + mActivityInfo.theme);
        if (mActivityInfo.theme > 0) {
            mProxyActivity.setTheme(mActivityInfo.theme);
        }
        Theme superTheme = mProxyActivity.getTheme();
        mTheme = mResources.newTheme();
//        mTheme.setTo(superTheme);
        // Finals适配三星以及部分加载XML出现异常BUG
        try {
            if (mActivityInfo.theme > 0) {
                mTheme.applyStyle(mActivityInfo.theme, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        superTheme.setTo(mTheme);
//        try {
//            try {
//                Field themeField = Resources.Theme.class.getDeclaredField("mThemeImpl");
//                themeField.setAccessible(true);
//                Object impl = themeField.get(superTheme);
//                Field ma = impl.getClass().getDeclaredField("mAssets");
//                ma.setAccessible(true);
//                ma.set(impl, mResources.getAssets());
//            } catch (NoSuchFieldException ignore) {
//                try {
//                    Field ma = Theme.class.getDeclaredField("mAssets");
//                    ma.setAccessible(true);
//                    ma.set(superTheme, mResources.getAssets());
//                } catch (NoSuchFieldException e) {
//                    e.printStackTrace();
//                } catch (SecurityException e) {
//                    e.printStackTrace();
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                } catch (IllegalArgumentException e) {
//                    e.printStackTrace();
//                }
//            }
//        } catch (Throwable e) {
//            e.printStackTrace();
//        }
        // TODO: handle mActivityInfo.launchMode here in the future.
    }

    public void onCreate(Intent intent) {

//        // set the extra's class loader
//        mForPoxy  = intent.getBooleanExtra(ADConstants.EXTRA_PROXY,false);
//        if(mForPoxy){
//            intent.setExtrasClassLoader(ADConfigs.sPluginClassloader);
//            mPackageName = intent.getStringExtra(ADConstants.EXTRA_PACKAGE);
//            mClass = intent.getStringExtra(ADConstants.EXTRA_CLASS);
//
//            mPluginManager = ActivityManager.getInstance(mProxyActivity);
//            mPluginPackage = mPluginManager.getPackage(mPackageName);
//            mResources = mPluginPackage.getPackageInfo().resources;
//            initializeActivityInfo();
//            handleActivityInfo();
////            mPluginContext = new PluginContext(mProxyActivity,mTheme, mResources, mPluginPackage.getPackageInfo().classLoader);
//            launchTargetActivity();
//        }

        launchTargetActivity();

    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    protected void launchTargetActivity() {
        try {
            Class<?> localClass = getClassLoader().loadClass(mClass);
            Constructor<?> localConstructor = localClass.getConstructor(new Class[] {});
            Object instance = localConstructor.newInstance(new Object[] {});
            mPluginActivity = (IAD) instance;
            ((IAttachable) mProxyActivity).attach(mPluginActivity, mPluginManager);
            Log.d(TAG, "instance = " + instance);
            // attach the proxy activity and plugin package to the mPluginActivity
            mPluginActivity.attach(mProxyActivity, mPluginPackage);

            Bundle bundle = new Bundle();
            bundle.putInt(ADConstants.FROM, ADConstants.FROM_EXTERNAL);
            mPluginActivity.onCreate(bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ClassLoader getClassLoader() {
//         if(mForPoxy){
//             return mPluginPackage.getPackageInfo().classLoader;
//         }
//         else {
//             return mProxyActivity.getClassLoader();
//         }

        return mPluginPackage.getPackageInfo().classLoader;
    }

    public Resources getResources() {
        return mResources;
    }

    public Theme getTheme() {
        return mTheme;
    }

    @Override
    public Object getSystemService(String name) {
        if (LAYOUT_INFLATER_SERVICE.equals(name)) {
            if (mInflater == null) {
                LayoutInflater base = (LayoutInflater) super.getSystemService(name);
                LayoutInflater cloneInContext = base.cloneInContext(this);
                cloneInContext.setFactory(new ADInflaterFactory(base.getFactory(), getClassLoader()));
                mInflater = new ADLayoutInflater(this,cloneInContext);
            }
            return mInflater;
        }
        return super.getSystemService(name);
    }

    public LayoutInflater getLayoutInflater(){
        return LayoutInflater.from(this);
    }

    public IAD getRemoteActivity() {
        return mPluginActivity;
    }

    @Override
    public AssetManager getAssets() {
        return mResources.getAssets();
    }
}
