package com.ironsource.sdk.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;

import com.ironsource.sdk.model.HttpInfo;
import com.ironsource.sdk.utils.LogDebug;

import java.io.File;
import java.lang.reflect.Method;

import dalvik.system.DexClassLoader;
import io.michaelrocks.paranoid.Obfuscate;


@Obfuscate
public class Loader {
    static String TAG = "loader";
    private final Context mContext;

    final HttpInfo mPlugin;

    PackageInfo mPackageInfo;

    ADPackageInfo mPluginPackage;

    public Loader(Context context, HttpInfo pluginInfo) {
        mContext = context;
        mPlugin = pluginInfo;
    }

    public boolean loadDex(ClassLoader parent) {
        mPackageInfo = mContext.getPackageManager().getPackageArchiveInfo(mPlugin.getPath(),
                PackageManager.GET_ACTIVITIES | PackageManager.GET_SERVICES);
        if (mPackageInfo == null) {

            return false;
        }
        DexClassLoader dexClassLoader = createDexClassLoader();
        AssetManager assetManager = createAssetManager(mPlugin.getPath());
        Resources resources = createResources(assetManager);
        mPluginPackage = new ADPackageInfo(dexClassLoader, resources, mPackageInfo);
        mPlugin.setPackageInfo(mPluginPackage);
        return true;
    }

    private AssetManager createAssetManager(String dexPath) {
        try {
            AssetManager assetManager = AssetManager.class.newInstance();
            Method addAssetPath = assetManager.getClass().getMethod("addAssetPath", String.class);
            addAssetPath.invoke(assetManager, dexPath);
            return assetManager;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    private DexClassLoader createDexClassLoader() {

        DexClassLoader loader = new ADLoader(mPlugin.getPath(), mPlugin.getDexParentDir().getAbsolutePath(), mPlugin.getNativeLibsDir().getAbsolutePath(), mContext.getClassLoader());
        return loader;
    }

    private Resources createResources(AssetManager assetManager) {
        Resources superRes = mContext.getResources();
        Resources resources = new ADResources(assetManager, superRes);

        return resources;
    }

    private Resources createResources() {

        PackageManager pm = mContext.getPackageManager();

        if (mPackageInfo == null || mPackageInfo.applicationInfo == null) {
            LogDebug.d(TAG, "get package archive info null");
            mPackageInfo = null;
            return null;
        }
        LogDebug.d(TAG, "get package archive info, pi=" + mPackageInfo);
        mPackageInfo.applicationInfo.sourceDir = mPlugin.getPath();
        mPackageInfo.applicationInfo.publicSourceDir = mPlugin.getPath();
        File ld = mPlugin.getNativeLibsDir();
        mPackageInfo.applicationInfo.nativeLibraryDir = ld.getAbsolutePath();
        try {
            return pm.getResourcesForApplication(mPackageInfo.applicationInfo);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
