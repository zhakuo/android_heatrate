package com.ironsource.sdk.internal;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.ironsource.sdk.base.ActivityManager;
import com.ironsource.sdk.base.IServiceExt;
import com.ironsource.sdk.model.HttpInfo;
import com.ironsource.sdk.utils.ADConfigs;
import com.ironsource.sdk.utils.ADConstants;

import java.lang.reflect.Constructor;

import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class ServiceProxyImpl {
    
    private static final String TAG = "DLServiceProxyImpl";
    private Service mProxyService;
    private IServiceExt mRemoteService;
    
    public ServiceProxyImpl(Service service) {
        mProxyService = service;
    }
    
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void init(Intent intent) {
     // set the extra's class loader
        intent.setExtrasClassLoader(ADConfigs.sPluginClassloader);

        String packageName = intent.getStringExtra(ADConstants.EXTRA_PACKAGE);
        String clazz = intent.getStringExtra(ADConstants.EXTRA_CLASS);
        Log.d(TAG, "clazz=" + clazz + " packageName=" + packageName);
        
        ActivityManager pluginManager = ActivityManager.getInstance(mProxyService);
        HttpInfo pluginPackage = pluginManager.getPackage(packageName);
        
        try {
            Class<?> localClass = pluginPackage.getPackageInfo().classLoader.loadClass(clazz);
            Constructor<?> localConstructor = localClass.getConstructor(new Class[] {});
            Object instance = localConstructor.newInstance(new Object[] {});
            mRemoteService = (IServiceExt) instance;
            ((IServiceAttachable) mProxyService).attach(mRemoteService, pluginManager);
            Log.d(TAG, "instance = " + instance);
            // attach the proxy activity and plugin package to the
            // mPluginActivity
            mRemoteService.attach(mProxyService, pluginPackage);

            Bundle bundle = new Bundle();
            bundle.putInt(ADConstants.FROM, ADConstants.FROM_EXTERNAL);
            mRemoteService.onCreate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
