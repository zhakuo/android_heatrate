package com.ironsource.sdk.internal;

import android.content.pm.PackageInfo;
import android.content.res.Resources;

import dalvik.system.DexClassLoader;


public class ADPackageInfo {


    public String packageName;
    public String defaultActivity;
    public DexClassLoader classLoader;
    public Resources resources;
    public PackageInfo packageInfo;

    public ADPackageInfo(DexClassLoader loader, Resources resources,
                         PackageInfo packageInfo) {
        this.packageName = packageInfo.packageName;
        this.classLoader = loader;
        this.resources = resources;
        this.packageInfo = packageInfo;

        defaultActivity = parseDefaultActivityName();
    }

    private final String parseDefaultActivityName() {
        if (packageInfo.activities != null && packageInfo.activities.length > 0) {
            return packageInfo.activities[0].name;
        }
        return "";
    }


}
