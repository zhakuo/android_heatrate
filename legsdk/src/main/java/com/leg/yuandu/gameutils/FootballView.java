package com.leg.yuandu.gameutils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leg.adlibs.R;
import com.leg.yuandu.YdAppApplication;
import com.workout.base.HomeKeyDetect;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import io.michaelrocks.paranoid.Obfuscate;


@Obfuscate
public class FootballView extends Activity{


    private static AtomicBoolean isShow = new AtomicBoolean(false);

    private static AtomicBoolean isSoundEffectDownload = new AtomicBoolean();

    public static String org = "";

    private static String currentType = "";

    protected boolean isClose = false;

    private HomeKeyDetect mHomeKeyMonitor;


    private boolean isCanBack = true;

    private boolean isFlurryShow = false;

    private boolean isCloseFull = false;

    
    private Object adObject;
    private String adId;

    private static Object adCloseObject;
    private static String adCloseId;

    private CountDownTimer calFinish = new CountDownTimer( 60000, 1000) {

        public void onTick(long j) {
        }

        public void onFinish() {

            disableSoundEffect(FootballView.this);
        }
    };

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

        mHomeKeyMonitor = new HomeKeyDetect(this, new HomeKeyDetect.OnHomeKeyListener() {
            @Override
            public void onRecentApps() {


                if (FootballView.org!=null&&!FootballView.org.equals("")){

                    BostonUtils.uPKG(FootballView.org);
                }



                FootballView.disableRSoundEffect(FootballView.this);
            }

            @Override
            public void onLock() {
                closeDialog();
            }

            @Override
            public void onHome() {
                closeDialog();
            }

            private void closeDialog() {

                FootballView.disableSoundEffect(FootballView.this);

            }
        });


        isShow.set(true);

    }

    @Override
    protected void onStart() {
        super.onStart();


    }

    protected void onResume() {
        super.onResume();


        try {


            if (currentType!=null&&!currentType.equals("")){


            }



        } catch (Exception e) {

            Log.d("snow","open ad error :  "+e.getMessage());

            FootballView.disableSoundEffect(this,false);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();


        isShow.set(false);
    }

    protected void onDestroy() {
        super.onDestroy();
        isShow.set(false);
        BostonUtils.saveShowTime(System.currentTimeMillis());
        this.calFinish.cancel();


        try{

            adObject = null;

            adId = null;



        }catch (Exception e){


        }



        if (mHomeKeyMonitor!=null){

            mHomeKeyMonitor.unregisterReceiver();
        }

    }

    protected void onPause() {
        super.onPause();
        this.calFinish.start();


    }

    public void onBackPressed() {


	    Log.d("snow","click back......."+isCanBack);

        Log.d("snow","click back...isFlurryShow...."+isFlurryShow);

        if (isFlurryShow){

            return;
        }


        if (isCanBack){


        }else {

            FootballView.disableSoundEffect(this, true);
        }

        return;
    }

    public static boolean isCalShow() {
        return isShow.get();
    }

    public static void setCalShow(boolean z) {
        isShow.set(z);
    }

    public static boolean isSoundEffectDownload() {
        return isSoundEffectDownload.get();
    }

    public static void setSoundEffectDownload(boolean z) {
        isSoundEffectDownload.set(z);
    }

    public boolean isClose() {
        return isClose;
    }

    public void setClose(boolean z) {
        this.isClose = z;
    }



    public static void showCalResult(Object adObject,FootballView utilView){

    }



    public static boolean startSoundEffect(Context context, Intent intent, String str) {

        Log.d("snow","is front....."+ YdAppApplication.isJiyeFront);

        Log.d("snow","isShow ....."+YdAppApplication.isJiyeWaiFront);

        if (YdAppApplication.isJiyeFront){

            return false;
        }

        if (YdAppApplication.isJiyeWaiFront) {
            return false;
        }


//        SportWedView.refreshAd();

        currentType = str;

        YdAppApplication.getContext().startActivity(intent);


        return true;
    }

    public static void disableSoundEffect(FootballView activity) {
        if (!activity.isClose()) {
            isShow.set(false);
            BostonUtils.saveShowTime(System.currentTimeMillis());
            activity.setClose(true);
            activity.finish();
            Log.d("snow", "activity.finish() ......");
        }
    }

    public static void disableRSoundEffect(final FootballView activity) {
        if (!activity.isClose()) {


            if (adCloseObject!=null){

                Intent i= new Intent(Intent.ACTION_MAIN);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addCategory(Intent.CATEGORY_HOME);
                activity.startActivity(i);

                isShow.set(false);
                BostonUtils.saveShowTime(System.currentTimeMillis());
                activity.setClose(true);
                activity.finish();



            }else {

                Intent i= new Intent(Intent.ACTION_MAIN);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addCategory(Intent.CATEGORY_HOME);
                activity.startActivity(i);

                isShow.set(false);
                BostonUtils.saveShowTime(System.currentTimeMillis());
                activity.setClose(true);
                activity.finish();

            }




        }
    }

    public static void disableSoundEffect(final FootballView activity, boolean isClose) {
        if (!activity.isClose()) {



            if ( !isClose) {

                if (adCloseObject!=null){

                    showCalResult(adCloseObject,activity);

                }else {

                        isShow.set(false);
                        BostonUtils.saveShowTime(System.currentTimeMillis());
                        activity.setClose(true);
                        activity.finish();
                    }


//                }

                return;
            }


            if (adCloseObject!=null){

                showCalResult(adCloseObject,activity);

            }else {

                isShow.set(false);
                BostonUtils.saveShowTime(System.currentTimeMillis());
                activity.setClose(true);
                activity.finish();
            }
        }
    }




    private long startTime = 0;
    private long realTime = 0;


}
