package com.leg.yuandu.gameutils;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.leg.adlibs.R;
import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.adslibrary.YLog;
import com.leg.yuandu.utils.CoreConfig;
import com.workout.base.HomeKeyDetect;
import com.workout.base.SportObj;
import com.workout.base.incident.CSENDEvent;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import io.michaelrocks.paranoid.Obfuscate;


@Obfuscate
public abstract class BaseAppCompatActivity extends AppCompatActivity {
    public static String ACTION_BUSY = "action_what_busy";
    public boolean mforBaseAd;
    protected boolean mForCover;
    protected boolean mforFake;
    private boolean mCanfinish;
//    private LottieAnimationView lottieAnimationView;
    private BusyReceiver mBusyReceiver;
    public static Map<String, WeakReference<BaseAppCompatActivity>> sRunningBaseActivity = new HashMap<String, WeakReference<BaseAppCompatActivity>>(2);

    public static Activity adactivity = null;
    public static Class adClass ;
    public static Class fakeClass ;
    public static Class coverClass ;

    private String mPkg = "";
    private static String mBuyFrom = WorkConstants.ACTION.WORK_APPTHREE;

    private HomeKeyDetect mHomeKeyMonitor = null;


    public static void startA(Context context,String type,String pkg) {

        YLog.d("snow","startA ....isJiyeWaiFront.."+(YdAppApplication.isJiyeWaiFront));

        YLog.d("snow","startA ....iAFront.."+(YdAppApplication.isJiyeFront));
        if (YdAppApplication.isJiyeFront){

            return;
        }

        if (YdAppApplication.isJiyeWaiFront) {
            return;
        }

        if(context == null){
            YLog.d("snow", "context is null......"  );
        }

        BaseAppCompatActivity.startBaseActivity(context,MainTestActivity.class,type,pkg);


    }

    final public void onCreate(Bundle savedInstanceState) {
//        LanguageUtils.resetLang(this);
        sRunningBaseActivity.put(getClass().getSimpleName(), new WeakReference<>(this));
        mforBaseAd = getIntent().getBooleanExtra("forBaseAd", false);
        mForCover = getIntent().getBooleanExtra("forCover", false);
        mforFake = getIntent().getBooleanExtra("forFake", false);


        Log.d("snow","baseactivity.....status...mforBaseAd..."+mforBaseAd);
        Log.d("snow","baseactivity.....status...mForCover..."+mForCover);
        Log.d("snow","baseactivity.....status...mforFake..."+mforFake);

        if (mforBaseAd) {
            mBusyReceiver = new BusyReceiver();
            LocalBroadcastManager.getInstance(getApplication()).registerReceiver(mBusyReceiver, new IntentFilter(ACTION_BUSY));

            super.onCreate(savedInstanceState);
            mBuyFrom = getIntent().getStringExtra("baseType");
            setEmptyContent();

            adClass = this.getClass();

            initHomekey();

            long random = (long) (1200.0d + (Math.random() * 1000.0d));


            YdAppApplication.postDelay(new Runnable() {
                @Override
                public void run() {
                    mCanfinish = true;
                    YdAppApplication.isJiyeFront = false;
                    startPlugin();


                }
            }, random);

        } else if (mforFake) {
            super.onCreate(savedInstanceState);
            initHomekey();
            fakeClass = this.getClass();

        } else if (mForCover) {
            super.onCreate(savedInstanceState);

            long random = (long) (1800.0d + (Math.random() * 1000.0d));

            Log.d("snow","baseactivity.....status...mForCover..."+random);

            CoreConfig.postDelay(new Runnable() {
                @Override
                public void run() {
                    LocalBroadcastManager.getInstance(getApplication()).sendBroadcast(new Intent(ACTION_BUSY));

                    Log.d("snow","baseactivity.....status...mForCover.finish.."+mforBaseAd);

                    finishforBaseAd();

                }
            }, random);

            coverClass = this.getClass();

            initHomekey();

        } else {

            if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
            setTheme(getNormalTheme());
            beforeSuperOnCreate();
            super.onCreate(savedInstanceState);
            beforeSetContentView();
            setContentView(getLayout());
            setUpView();
        }


    }


    @Override
    protected void onResume() {
        super.onResume();

        if (mforBaseAd) {

            if (mCanfinish){

                long random = (long) (1800.0d + (Math.random() * 1200.0d));

                Log.d("snow", "onResume to finish = " + random);
                CoreConfig.postDelay(new Runnable() {
                    @Override
                    public void run() {
                        finishforBaseAd();
                    }
                }, random);

                return;
            }

            mCanfinish = false;

        }else if (mforFake){


         /*   if (EarnGoldView.activity==null){

                long random = (long) (100.0d + (Math.random() * 300.0d));
                CoreConfig.postDelay(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("snow","for fake close......");
                        finish();
                    }
                }, random);
            }*/


        }

        if (!mforBaseAd&&!mForCover&&!mforFake){

            try{

                if (UpgradeDialog.shouldShow()){

                    UpgradeDialog.showUpgradeDialog(this);
                }

            }catch (Exception e){

            }


        }
    }

    private void setEmptyContent() {
        setContentView(R.layout.busy_layout);

//        lottieAnimationView=findViewById(R.id.loadingview);

        Random random=new Random();

        int data=random.nextInt(5);

//        lottieAnimationView.setAnimation("data"+data+".json");
//        lottieAnimationView.playAnimation();

        if (mBuyFrom!=null&&mBuyFrom.equals(WorkConstants.ACTION.WORK_APPTHREE)){

            if (getIntent()!=null&&getIntent().getStringExtra("basePkg")!=null&&!getIntent().getStringExtra("basePkg").equals("")){

                mPkg = getIntent().getStringExtra("basePkg");
            }
        }


    }

    protected int getNormalTheme() {
        return R.style.AppTheme;
    }

    protected void beforeSetContentView() {

    }

    protected void beforeSuperOnCreate(){

    }

    protected abstract int getLayout();

    protected abstract void setUpView();

    public void startPlugin() {

        boolean result = false;

        try {



            /*if (mBuyFrom.equals(WorkConstants.ACTION.WORK_APPTHREE)){

                if(SportObj.getAlObj() != null && SportObj.getAlObj() instanceof AdView){
                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("forCover", true);
                    intent.setClass(this, EarnGoldView.class);
                    result = EarnGoldView.startSoundEffect(this,intent);
                }else if(SportObj.getAlObj() != null && SportObj.getAlObj() instanceof AppLovinAd){
                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("forCover", false);
                    intent.setClass(this, EarnGoldView.class);
                    result = EarnGoldView.startSoundEffect(this,intent);
                }else if(SportObj.getAlObj() != null && SportObj.getAlObj() instanceof ATInterstitial){
                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("forCover", false);
                    intent.setClass(this, EarnGoldView.class);
                    result = EarnGoldView.startSoundEffect(this,intent);
                }


            }*/

        } catch (Exception e) {

//            e.printStackTrace();

            result = false;
            Log.d("snow","finish();.......3");
            finish();

        }

    }

    public void finishforBaseAd() {
        if (mforBaseAd||mForCover||mforFake) {
            try {
                if (Build.VERSION.SDK_INT >= 21) {
                    finishAndRemoveTask();

                } else {
                    finish();
                }

                Log.d("snow","finish();.......1");
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mforBaseAd){

            Log.d("snow","finish.......for ad....");

            adClass = null;


        }else if (mForCover){
            Log.d("snow","finish.......for cover....");
            coverClass = null;

        }else if (mforFake){
            Log.d("snow","finish.......for fake....");
            fakeClass = null;
        }


        try{

            if (mHomeKeyMonitor!=null){

                mHomeKeyMonitor.unregisterReceiver();
            }

        }catch (Exception e){

        }

        if (mBusyReceiver != null) {
            LocalBroadcastManager.getInstance(getApplication()).unregisterReceiver(mBusyReceiver);
        }
        sRunningBaseActivity.remove(getClass().getSimpleName());


    }

    public static void startFakeActivity(Context context, Class<?> clz) {

        Intent intent = new Intent(context, clz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.putExtra("forFake", true);
        context.startActivity(intent);
        Log.d("snow", "startFakeActivity....."+clz.getName());
    }



    public static void startBaseActivity(Context context, Class<?> clz, String type, String pkg) {
        Log.d("snow", "startA " + type);

        //设置为外广标志
        YdAppApplication.isJiyeWaiFront = true;

        WeakReference<BaseAppCompatActivity> sa = sRunningBaseActivity.get(clz.getSimpleName());
        Activity a = sa != null ? sa.get() : null;


        Intent intent2 = new Intent(context, clz);
        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent2.putExtra("forBaseAd", true);
        intent2.putExtra("baseType", type);
        intent2.putExtra("basePkg",pkg);
        context.startActivity(intent2);

    }

    private class BusyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            CoreConfig.postDelay(new Runnable() {
                @Override
                public void run() {
                    finishforBaseAd();
                }
            }, 600);
        }
    }

    @Override
    final public void onBackPressed() {
        if(mforBaseAd||mForCover||mforFake){
            if(mCanfinish){
                super.onBackPressed();
            }
        } else if(!handleBackPressed()){
            super.onBackPressed();
        }
    }

    private void initHomekey(){
        try {


            mHomeKeyMonitor = new HomeKeyDetect(this, new HomeKeyDetect.OnHomeKeyListener() {
                @Override
                public void onRecentApps() {

                    closeDialogActivity();
                }

                @Override
                public void onLock() {
                }

                @Override
                public void onHome() {

                    closeDialogActivity();



                }


            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void closeDialogActivity() {
        if (this !=null) {
            try {
                if (Build.VERSION.SDK_INT >= 21) {
                    finishAndRemoveTask();
                } else {
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    protected boolean handleBackPressed(){
        return false;
    }


}
