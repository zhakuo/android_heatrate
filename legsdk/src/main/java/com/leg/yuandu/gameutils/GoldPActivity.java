package com.leg.yuandu.gameutils;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

import com.leg.yuandu.YdAppApplication;
import com.workout.base.ActivityHook;
import com.workout.base.BaseModuleContext;
import com.workout.base.SportObj;
import com.workout.base.incident.APPChooseEvent;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;

/**
 * GoldPActivity 只用来打开uac广告
 */
public class GoldPActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ActivityHook.hookOrientation(this);
        super.onCreate(savedInstanceState);
        Log.d("snow", " GoldPActivity before postEvent  " );

        BaseModuleContext.postEvent(new APPChooseEvent(this));

        finish();
    }

    protected void onResume() {
        super.onResume();

        finish();
    }



    @UiThread
    @MainThread
    /**
     * context context 或者activity
     * event 打开场景广告的event
     */
    public static void startPPage(@NonNull Context context) {

//        Log.d("snow", "GoldPActivity SportObj.getAlObj() = " +SportObj.getAlObj() );
//        Log.d("snow", "GoldPActivity DailyPartActivity.islive = " +DailyPartActivity.islive );
        if(SportObj.getAlObj()==null){
            return;
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            Log.d("snow", "GoldPActivity startPPage sdk 28 APPChooseEvent");
            BaseModuleContext.postEvent(new APPChooseEvent(context));
            return;
        }

        Context applicationContext = context.getApplicationContext();
        Intent intent = new Intent(applicationContext, GoldPActivity.class);
        intent.setAction("inner_action");

        final PendingIntent activity = PendingIntent.getActivity(applicationContext, 10102, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        boolean z = false;
        try {
//            Log.d("snow", "GoldPActivity startPage  try " );
            activity.send();
            z = true;
//            Log.d("snow", "GoldPActivity startPage unused z " );
        } catch (Exception unused) {
            Log.d("snow", "GoldPActivity startPage unused " + unused.getMessage());
        }
        if (!z) {
            intent.setFlags(Intent.FLAG_RECEIVER_FOREGROUND);
            try {
                applicationContext.startActivity(intent);
            } catch (Exception unused2) {
                unused2.printStackTrace();
            }
        }

        if (Looper.myLooper() != Looper.getMainLooper()) {
            YdAppApplication.postRunOnUiThread(new Runnable() {
                @Override
                public void run() {
                    GoldNotification.openPending(YdAppApplication.getContext(), activity);

                }
            });

        }else{

            GoldNotification.openPending(applicationContext, activity);

        }
    }
}

