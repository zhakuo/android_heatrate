package com.leg.yuandu.gameutils;

/**
 * Created by snow on 11/08/2019.
 */

public class WorkConstants {

    public static final String DEFAULT_SHAREPREFERENCES_FILE = "default_sharepreferences_file_name";

    public static final long HOUR_MILLIS = 1000 * 60 * 60;
    public static final long DAY_MILLIS = HOUR_MILLIS * 24;

    public static String dailybu = "ads_dailybu";

    public static String WORK_START = "WORK_START";

    public static final class ACTION {

        public static String WORK_LOOK = "WORK_LOOK";//lock

        public static String WORK_APPTHREE = "WORK_APPTHREE"; // ac

        public static String WORK_ZERO = "WORK_ZERO";   // show times.

        public static String TEMP_ZERO = "TEMP_ZERO";   // temp disable.


    }

    //switch
    public static final class SWITCH {

        public static String WORK_CEND = "WORK_CEND_S";//call end

        public static String WORK_APPTHREE = "WORK_APPTHREE_S";

    }

    //close time
    public static final class SWITCHT {


        public static String TEMP_ZERO = "TEMP_ZERO_CT";   // temp disable.

    }

    //show time
    public static final class TSWITCH {

        public static String WORK_CEND = "WORK_CEND_ST";//call end

        public static String WORK_SQUTWO = "WORK_SQUTWO_ST";

    }
}
