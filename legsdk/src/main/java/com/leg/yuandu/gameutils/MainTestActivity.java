package com.leg.yuandu.gameutils;

import android.content.Intent;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.WindowManager;

import com.leg.adlibs.R;
import com.leg.yuandu.utils.ScreenUtil;
import com.workout.base.ActivityHook;


public class MainTestActivity extends BaseAppCompatActivity {

    @Override
    protected int getLayout() {
        return R.layout.activity_slive;
    }

    @Override
    protected void setUpView() {
        ActivityHook.hookOrientation(this);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = (int) (ScreenUtil.getScreenWidth(this) * 0.8);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(lp);


    }



    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void beforeSetContentView() {
        super.beforeSetContentView();

        Log.d("snow","setup mainactivity anim.....");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }



}
