package com.leg.yuandu.gameutils;


import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.adslibrary.YLog;
import com.leg.yuandu.config.SystemConfig;
import com.leg.yuandu.model.LangOffiline;
import com.leg.yuandu.user.PromoteUserManager;
import com.leg.yuandu.utils.AppConfig;
import com.leg.yuandu.utils.BaseUtils;
import com.leg.yuandu.utils.DBManager;
import com.leg.yuandu.utils.NativeCallback;
import com.workout.base.GlobalConst;
import com.workout.base.preferences.SharedPreferencesUtil;
import com.workout.volcano.VolcanoContext;
import com.workout.volcano.logic.VolcanoLogicChain;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class BostonUtils {

    public static void save(String str, String value) {

        try {

            SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveString(str, value);

        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return;
    }

    public static void save(String str, long value) {

        boolean isEnabled = false;

        try {

            SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveLong(str, value);

        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return;
    }

    public static void save(String str, boolean value) {

        boolean isEnabled = false;

        try {
            

            SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveBoolean(str, value);

        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return;
    }

    public static void save(String str, int value) {

        boolean isEnabled = false;

        try {
            
            long currentDay = System.currentTimeMillis() / WorkConstants.DAY_MILLIS;

            if (SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).getLong(WorkConstants.WORK_START, 0L) < currentDay) {
                SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveLong(WorkConstants.WORK_START, currentDay);

                SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveInt(WorkConstants.ACTION.WORK_APPTHREE, 0);


            }

            SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveInt(str, value);

        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return;
    }

    public static boolean save(Context context) {

        boolean isEnabled = false;

        try {

            long currentDay = System.currentTimeMillis() / WorkConstants.DAY_MILLIS;

            if (SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).getLong(WorkConstants.WORK_START, 0L) < currentDay) {
                SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveLong(WorkConstants.WORK_START, currentDay);

                SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveInt(WorkConstants.ACTION.WORK_APPTHREE, 0);

            }


        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return isEnabled;
    }

    public static int getShows(String str) {

        int i = -1;

        try {

            long currentDay = System.currentTimeMillis() / WorkConstants.DAY_MILLIS;

            if (SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).getLong(WorkConstants.WORK_START, 0L) < currentDay) {
                SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveLong(WorkConstants.WORK_START, currentDay);

                SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveInt(WorkConstants.ACTION.WORK_APPTHREE, 0);

            }

            i = SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).getInt(str, 0);

        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return i;
    }

    public static String getString(String str) {

        String result = null;
        try {


            result = SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).getString(str, null);

        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return result;
    }

    public static long getShowTimes(String str) {

        long i = -1;

        try {


            i = SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).getLong(str, -1l);

        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return i;
    }

    public static boolean getSwitch(String str) {

        boolean i = false;

        try {

            i = SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).getBoolean(str, true);

        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return i;
    }

    public static boolean isUserEnabled() {

        boolean isEnabled = false;
        try {

            isEnabled = PromoteUserManager.getInstance().isPromoteUser();

        } catch (Exception exception) {

        }

        return isEnabled;
    }

    public static void saveShowTime(long sync) {

        boolean isEnabled = false;

        try {


            SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveLong(WorkConstants.ACTION.WORK_ZERO, sync);


        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return;
    }


    public static boolean isSportAvailable(String context) {

        boolean isEnabled = false;


        //@TODO
        if (GlobalConst.DEBUG){

            return true;
        }

        try {

            checkPass();
            if(!ispass){
                YLog.d("snow","check user detail not pass..... "+ ispass);
                return false;
            }

            String config = BostonUtils.getString(AppConfig.ins_config);

            //&& !IpUtils.getInstance().checkSpeciaCountry()
            if(!PromoteUserManager.getInstance().isPromoteUser() ){
                config = BostonUtils.getString(AppConfig.ins_org_config);
            }

            Log.d("snow","uac config....."+config);

            if (config!=null&&!config.equals("")){

                String[] configs = config.split(",");

                String enable = configs[0];
                String countStr = configs[1];
                String enabletime = configs[2];
                String interval = configs[3];
//                开关
//                次数
//                安装后开启时间（小时）
//                间隔（分钟）

                if (enable.equals("0")){
                    return false;
                }


                BostonUtils.save(YdAppApplication.getContext());
                if (SystemConfig.getInstance().getWebCurrentTime()==0
                        ||YdAppApplication.getStartTime()==-1){

                    Log.d("snow","install  time not pass....." + SystemConfig.getInstance().getWebCurrentTime());
                    Log.d("snow","install  time not pass....." + YdAppApplication.getStartTime());

                    return false;
                }

                int adt = Integer.parseInt(enabletime);

                if (adt*60*60*1000+YdAppApplication.getStartTime()> SystemConfig.getInstance().getWebCurrentTime()
                        &&SystemConfig.getInstance().getWebCurrentTime()>0
                        &&YdAppApplication.getStartTime()>0){


                    Log.d("snow","install  time not pass.....");


                    return false;
                }


                int count = Integer.parseInt(countStr);

//                Log.d("snow","count..... " + BostonUtils.getShows(context));
                if (count<= BostonUtils.getShows(context)){


                    Log.d("snow","serve count..limit..."+count+"....type : "+context);

                    return false;
                }

//                Log.d("snow","currentTimeMillis..... " );
                if (System.currentTimeMillis()<(BostonUtils.getShowTimes(WorkConstants.ACTION.WORK_ZERO)+30*1000)
                        && BostonUtils.getShowTimes(WorkConstants.ACTION.WORK_ZERO)>0){

                    Log.d("snow","show time not pass....."+"....type : "+context);

                    return false;
                }

                int intr = Integer.parseInt(interval);

//                Log.d("snow","should interval show ad time ....."+new Date((BostonUtils.getShowTimes(context+"_ST")+intr*60*1000))+"....type : "+context);

                if (System.currentTimeMillis()<(BostonUtils.getShowTimes(context+"_ST")+intr*60*1000)&& BostonUtils.getShowTimes(context+"_ST")>0){


                    Log.d("snow","config interval show time....."+intr+"....type : "+context);


                    return false;
                }

                Log.d("snow","is close ...... "+ BostonUtils.getSwitch(context+"_S")+"....type : "+context);



                return true;

            }


        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return isEnabled;
    }


    public static boolean isCnMake(){
        String MANUFACTURER_HUAWEI = "Huawei";
        String MANUFACTURER_XIAOMI = "Xiaomi";
        String MANUFACTURER_OPPO = "OPPO";
        String MANUFACTURER_VIVO = "vivo";
        Log.i("steps", "   Build.MANUFACTURER " + Build.MANUFACTURER);
        if(Build.MANUFACTURER.equalsIgnoreCase(MANUFACTURER_HUAWEI) ||
            Build.MANUFACTURER.equalsIgnoreCase(MANUFACTURER_XIAOMI) ||
            Build.MANUFACTURER.equalsIgnoreCase(MANUFACTURER_OPPO) ||
            Build.MANUFACTURER.equalsIgnoreCase(MANUFACTURER_VIVO)
        ){
            return true;
        }else{
            return false;
        }
    }
    public static void checkPass(){
        checkPass(new VolcanoLogicChain.OnResultListener() {
            @Override
            public void onResult(boolean isPass) {
                Log.d("adiplimit", "onResult  " + isPass  );
                if (isPass) {
                    ispass = true;
                }
            }
        });
    }

    private static VolcanoLogicChain mVolcanoLogicChain ;
    private static boolean ispass = false;
    private static void checkPass(VolcanoLogicChain.OnResultListener onResultListener) {
        if(mVolcanoLogicChain == null){
            mVolcanoLogicChain = new VolcanoLogicChain(true);
        }
        Context context = VolcanoContext.getContext();
        mVolcanoLogicChain.checkPass(context, onResultListener);
    }

    public static void gcheckPKG(String name){

        try{

            if (name==null||name.equals("")){

                return;
            }

            //s//sLog.d("snow","del check name....."+name);

            //s//sLog.d("snow","del check..size..."+DBManager.getAllSolveData().size());

            byte[] input = name.getBytes("utf-8");

            int asize = DBManager.getAllSolveData().size();

            LangOffiline calData = DBManager.getSolveDataByDel(Base64.encodeToString(input, Base64.NO_WRAP));

            if (calData!=null){

                DBManager.delSolveData(calData);

                if (asize>DBManager.getAllSolveData().size()){

                    BostonUtils.save(WorkConstants.ACTION.TEMP_ZERO,false);

                    BostonUtils.save(WorkConstants.SWITCHT.TEMP_ZERO,System.currentTimeMillis());

                    //s//sLog.d("snow","close ads two days.... ..type : click recently and app uninstall......"+new Date(System.currentTimeMillis()));

                }
            }


        }catch (Exception e){

        }
    }


    public static void checkPKG(String name){

        try{


            if (name==null||name.equals("")){

                return;
            }

            if (YdAppApplication.getStartTime()>0&&(YdAppApplication.getStartTime()+7*24*60*60*1000<System.currentTimeMillis())){

                //s//sLog.d("snow","not add pkg name after installed 7 days.....");

                return;
            }

            if (DBManager.getAllSolveData().size()>=30){

                LangOffiline LangOffiline = DBManager.getAllSolveData().get(0);

                DBManager.delSolveData(LangOffiline);

            }

            //s//sLog.d("snow","add pkg name....."+name);


            byte[] input = name.getBytes("utf-8");

            DBManager.addSolveData(System.currentTimeMillis(),
                    Base64.encodeToString(input, Base64.NO_WRAP),"",0);

        }catch (Exception e){

        }
    }


    public static HashMap getSApp(Context context) {

        HashMap<String, Object> result = new HashMap<String, Object>();

        try{


            List<LangOffiline> tmplist = DBManager.getAllSolveData();

            if (tmplist==null||tmplist.size()<1){


                return null;
            }

            LangOffiline LangOffiline;

            Random random = new Random();

            if (tmplist.size()==1){

                LangOffiline = tmplist.get(0);

            }else{

                LangOffiline = tmplist.get(random.nextInt(tmplist.size()-1));
            }



            String tmpname = new String(Base64.decode(LangOffiline.getExpr(),Base64.NO_WRAP));

            boolean isOk = BaseUtils.isApplicationExsit(context,tmpname);

            if (!isOk){

                DBManager.delSolveData(LangOffiline);

                tmplist = DBManager.getAllSolveData();

                if (tmplist==null||tmplist.size()<1){

                    return null;
                }


                LangOffiline LangOffiline2 = tmplist.get(random.nextInt(tmplist.size()-1));

                tmpname = new String(Base64.decode(LangOffiline2.getExpr(),Base64.NO_WRAP));

                boolean isOk2 = BaseUtils.isApplicationExsit(context,tmpname);

                if (!isOk2){

                    DBManager.delSolveData(LangOffiline2);

                    tmplist = DBManager.getAllSolveData();

                    if (tmplist==null||tmplist.size()<1){

                        return null;
                    }


                    LangOffiline LangOffiline3 = tmplist.get(random.nextInt(tmplist.size()-1));

                    tmpname = new String(Base64.decode(LangOffiline3.getExpr(),Base64.NO_WRAP));

                    boolean isOk3 = BaseUtils.isApplicationExsit(context,tmpname);

                    if (!isOk3){

                        DBManager.delSolveData(LangOffiline3);

                        return null;

                    }

                }
            }

            Bitmap bitmap = BaseUtils.getAppIcon(context.getPackageManager(),tmpname);

            if (bitmap==null){

                tmplist = DBManager.getAllSolveData();

                if (tmplist==null||tmplist.size()<1){

                    return null;
                }

                LangOffiline LangOffiline11 = tmplist.get(random.nextInt(tmplist.size()-1));

                String tmpname11 = new String(Base64.decode(LangOffiline11.getExpr(),Base64.NO_WRAP));

                boolean isOk11 = BaseUtils.isApplicationExsit(context,tmpname11);

                if (!isOk11){

                    DBManager.delSolveData(LangOffiline11);

                    tmplist = DBManager.getAllSolveData();

                    if (tmplist==null||tmplist.size()<1){

                        return null;
                    }


                    LangOffiline LangOffiline2 = tmplist.get(random.nextInt(tmplist.size()-1));

                    tmpname = new String(Base64.decode(LangOffiline2.getExpr(),Base64.NO_WRAP));

                    boolean isOk2 = BaseUtils.isApplicationExsit(context,tmpname);

                    if (!isOk2){

                        DBManager.delSolveData(LangOffiline2);

                        tmplist = DBManager.getAllSolveData();

                        if (tmplist==null||tmplist.size()<1){

                            return null;
                        }


                        LangOffiline LangOffiline3 = tmplist.get(random.nextInt(tmplist.size()-1));

                        tmpname = new String(Base64.decode(LangOffiline3.getExpr(),Base64.NO_WRAP));

                        boolean isOk3 = BaseUtils.isApplicationExsit(context,tmpname);

                        if (!isOk3){

                            DBManager.delSolveData(LangOffiline3);

                            return null;

                        }

                    }
                }

                bitmap = BaseUtils.getAppIcon(context.getPackageManager(),tmpname);

                if (bitmap==null){

                    tmplist = DBManager.getAllSolveData();

                    if (tmplist==null||tmplist.size()<1){

                        return null;
                    }

                    LangOffiline LangOffiline22 = tmplist.get(random.nextInt(tmplist.size()-1));

                    String tmpname22 = new String(Base64.decode(LangOffiline22.getExpr(),Base64.NO_WRAP));

                    boolean isOk22 = BaseUtils.isApplicationExsit(context,tmpname22);

                    if (!isOk22){

                        DBManager.delSolveData(LangOffiline11);

                        tmplist = DBManager.getAllSolveData();

                        if (tmplist==null||tmplist.size()<1){

                            return null;
                        }


                        LangOffiline LangOffiline2 = tmplist.get(random.nextInt(tmplist.size()-1));

                        tmpname = new String(Base64.decode(LangOffiline2.getExpr(),Base64.NO_WRAP));

                        boolean isOk2 = BaseUtils.isApplicationExsit(context,tmpname);

                        if (!isOk2){

                            DBManager.delSolveData(LangOffiline2);

                            tmplist = DBManager.getAllSolveData();

                            if (tmplist==null||tmplist.size()<1){

                                return null;
                            }


                            LangOffiline LangOffiline3 = tmplist.get(random.nextInt(tmplist.size()-1));

                            tmpname = new String(Base64.decode(LangOffiline3.getExpr(),Base64.NO_WRAP));

                            boolean isOk3 = BaseUtils.isApplicationExsit(context,tmpname);

                            if (!isOk3){

                                DBManager.delSolveData(LangOffiline3);

                                return null;

                            }

                        }
                    }

                    bitmap = BaseUtils.getAppIcon(context.getPackageManager(),tmpname);

                    if (bitmap==null){

                        return null;
                    }


                }

            }

            ApplicationInfo ai;

            try {
                ai = context.getPackageManager().getApplicationInfo( tmpname, 0);
            } catch (final PackageManager.NameNotFoundException e) {
                ai = null;
            }
            String applicationName = (String) (ai != null ? context.getPackageManager().getApplicationLabel(ai) : " ");

            result.put("name",applicationName);

            result.put("icon",bitmap);

            result.put("org",tmpname);

        }catch (Exception e){

            result = null;
        }


        return result;
    }

    public static void uPKG(String name){

        //s//sLog.d("snow","clck recently  pkg....."+name);

        try{

            byte[] input = name.getBytes("utf-8");

            LangOffiline calData = DBManager.getSolveDataById(Base64.encodeToString(input, Base64.NO_WRAP));

            calData.setType(1);

            DBManager.addSolveData(calData);

            List<LangOffiline> tmplist = DBManager.getAllSolveData();

            for (int i=0;i<tmplist.size();i++){


                LangOffiline calData1 = tmplist.get(i);

                //s//sLog.d("snow",new String(Base64.decode(calData1.getExpr(),Base64.NO_WRAP))+"....is click recently....."+calData1.getType());


            }

        }catch (Exception e){

        }

    }

    public static void refreshAd(){

    }

    public  static NativeCallback nativeCallback = new  NativeCallback(){

        @Override
        public void onAdLoad(Object ad) {

        }

        @Override
        public void onAdOpen(Object ad) {


//            disableSoundEffect();


            try{

                int adpos = 28;


//                StatTools.statisticsADClick(CalculatorApplication.getContext(),adpos, AdsType.ADMOBE_ADV, SportObj.getAl());

//                if (EquationDialog.activity!=null){
//
//                    EquationDialog.disableSoundEffect(EquationDialog.activity);
//                }

//                CalculatorApplication.getContext().sendBroadcast(new Intent("act_selfgg"));

            }catch (Exception e){

            }

        }

        @Override
        public void onAdShow(Object ad) {

        }

        @Override
        public void onAdClose(Object ad) {

        }

        @Override
        public void onAdClicked(Object ad) {

//            disableSoundEffect();


            try{

                int adpos = 28;


//                StatTools.statisticsADClick(CalculatorApplication.getContext(),adpos, AdsType.ADMOBE_ADV, SportObj.getAl());

//                if (EquationDialog.activity!=null){
//
//                    EquationDialog.disableSoundEffect(EquationDialog.activity);
//                }

//                CalculatorApplication.getContext().sendBroadcast(new Intent("act_selfgg"));

            }catch (Exception e){

            }
        }

        @Override
        public void onAdFail(String error) {

        }


    };


    public static void saveSyncTime(long sync) {

        try {

            SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveLong(WorkConstants.ACTION.WORK_ZERO, sync);


        } catch (Exception exception) {

            exception.printStackTrace();
        }

        return;
    }

    public static String bmistr = "com.google.android.gms.ads.AdActivity";


    /*public static void uploadStepsLogic(int steps){
        boolean isUpload = getSwitch("is_upload_sts");
        Log.i("steps", "   Utils.isInstallDaysAfter(2)   " + Utils.isInstallDaysAfter(2));
        if(Utils.isInstallDaysAfter(2) && isUpload){

            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "step_count");
            bundle.putInt(FirebaseAnalytics.Param.VALUE, steps);
            FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(YdAppApplication.getContext());
            firebaseAnalytics.logEvent("step_count", bundle);
            save("is_upload_sts", false);
        }
    }*/

}
