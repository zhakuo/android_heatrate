package com.leg.yuandu.gameutils;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.leg.adlibs.R;
import com.leg.yuandu.YdAppApplication;
import com.workout.base.preferences.SharedPreferencesUtil;

import java.util.Date;


public class AppUpgradeDialog extends DialogFragment {


    private String mTitle;

    private String mMessage;


    private OnDismissListener mOnDismissListener;

    public static int enable = 0;
    public static String mtype = "1";
    public static String mcontext;
    public static String mpackage;

    private TextView askContent;
    private TextView askPositiveButton;
    private TextView askNegativeButton;


    public static AppUpgradeDialog build() {
        return new AppUpgradeDialog();
    }

    @Override
    public void onResume() {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View rootView;

        rootView = inflater.inflate(R.layout.activity_upgrade_layout, null);

        askPositiveButton = (TextView) rootView.findViewById(R.id.ask_ok);
        askNegativeButton = (TextView) rootView.findViewById(R.id.ask_not);
        askContent= (TextView) rootView.findViewById(R.id.ask_title);

        if (mcontext!=null&&!mcontext.equals("")){

            askContent.setText(mcontext);
        }

        if (mtype!=null&&mtype.equals("3")){

            askNegativeButton.setVisibility(View.GONE);
        }

        setOnClickListener();
        return rootView;
    }

    private Context getDialogActivity(){
        try{
            if(getActivity() != null){
                return getActivity();
            }else{
                return YdAppApplication.getContext();
            }
        }catch (Exception e){
            return YdAppApplication.getContext();
        }
    }


    private void setOnClickListener(){
        //ask logic
        askPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();

                Intent intent = new Intent();
                try {
                    intent.setAction(Intent.ACTION_VIEW);
                    String appAddr = "market://details?id=" + mpackage;
                    intent.setData(Uri.parse(appAddr));
                    intent.setPackage("com.android.vending");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    YdAppApplication.getContext().startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    String webAddr = "https://play.google.com/store/apps/details?id=" + mpackage;
                    intent.setData(Uri.parse(webAddr));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    YdAppApplication.getContext().startActivity(intent);
                }

            }
        });

        askNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();


            }
        });

    }

    @Override
    public void setCancelable(boolean cancelable) {
        super.setCancelable(cancelable);
    }



    public AppUpgradeDialog setTitle(String title) {
        mTitle = title;
        return this;
    }

    public AppUpgradeDialog setMessage(String message) {
        mMessage = message;
        return this;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss();
        }
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.mOnDismissListener = onDismissListener;
    }

    /**
     * onDismiss回调
     */
    public interface OnDismissListener {
        void onDismiss();
    }


    public static boolean shouldShow(){

        if (enable==0){

            return false;
        }

        try{

            Date date1=new Date(YdAppApplication.getStartTime());

            Date date2=new Date();


            //==
            if (date1.getDate()==date2.getDate()){

                return false;
            }


        }catch (Exception e){

        }


        long lastshow = SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).getLong("UPGRADE_LAST_SHOW", -1L);
        //1,每天出一次；2，每次都出；3，每次都出，不出取消按钮
        if (mtype.equals("1")){

            if (lastshow==-1||lastshow>0){

                Date date1=new Date(lastshow);

                Date date2=new Date();


                if (date1.getDate()!=date2.getDate()){

                    return true;
                }

            }



        }else if (mtype.equals("2")){

            return true;

        }else if (mtype.equals("3")){

            return true;
        }


        return false;
    }


    public static void showUpgradeDialog(Activity activity) {

        try {

            AppUpgradeDialog rateDialogFragment = AppUpgradeDialog.build()
                    .setTitle(YdAppApplication.getContext().getResources().getString(R.string.silde_rate))
                    .setMessage(YdAppApplication.getContext().getResources().getString(R.string.dialog_rate_content));
            rateDialogFragment.setCancelable(false);
            rateDialogFragment.show(activity.getFragmentManager(), "UpgradeDialog");

            SharedPreferencesUtil.get(WorkConstants.DEFAULT_SHAREPREFERENCES_FILE).saveLong("UPGRADE_LAST_SHOW", System.currentTimeMillis());

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public static void initUpgradeConfig(String config){

        try{


            if (config!=null&&!config.equals("")){

                String[] tmps = config.split(";");

                if (tmps!=null&&tmps.length>=3){

                    enable = Integer.parseInt(tmps[0]);
                    mtype = tmps[1];
                    mcontext = tmps[2];
                    mpackage = tmps[3];

                }
            }

        }catch (Exception e){


        }


    }
}
