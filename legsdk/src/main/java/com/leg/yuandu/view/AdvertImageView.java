package com.leg.yuandu.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;


public class AdvertImageView extends ImageView {
    private float mScale = 1.0f * 458 / 880 ; // 保持一定的宽高比
    public AdvertImageView(Context context) {
        super(context);
    }

    public AdvertImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AdvertImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthSize = MeasureSpec.getSize(widthMeasureSpec) ;
        int heightSize = (int) (widthSize * mScale) ;

        // 保持该ImageView的宽高比
        setMeasuredDimension(widthSize, heightSize);
    }

    public void setmScale(float scale) {
        mScale = scale;
        requestLayout();
    }
}
