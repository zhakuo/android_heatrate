package com.leg.yuandu.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.leg.yuandu.YdAppApplication;



public class ReminderActivity extends Activity {

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        //sLog.d("snow","startEffectResultActivity onCreate");

        YdAppApplication.postDelay(new Runnable() {
            @Override
            public void run() {

                finish();

            }
        },5);

    }


    public static void startEffectResultActivity(Context context) {

        Intent intent = new Intent(context, ReminderActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

}

