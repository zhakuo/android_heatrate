package com.leg.yuandu.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ironsource.sdk.base.ActivityManager;
import com.ironsource.sdk.internal.IntentExt;
import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.core.Constants;
import com.leg.yuandu.core.CoreUtils;
import com.leg.yuandu.gameutils.WorkConstants;
import com.workout.base.SportObj;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Random;

import io.michaelrocks.paranoid.Obfuscate;


@Obfuscate
public class BaseActivitys extends Activity {

    private Handler mHandler;
    private TextView mTextView;

    private static String mBuyFrom = "";

    private static String mPkg = "";

    public static Activity aAactivity = null;

    private static long mCallTime = -1;
    private static long mCallStart = -1;

    public static void startA(Context context, String type) {


        if (YdAppApplication.isJiyeFront){

            return;
        }

        if (YdAppApplication.isJiyeWaiFront) {
            long current = System.currentTimeMillis();

            if (current-YdAppApplication.mAFrontTime>3*60*1000 && !SportObj.isCalling){

                YdAppApplication.isJiyeWaiFront = false;
            }
            return;
        }

        
        mPkg ="";
        mBuyFrom =type;

        createIntent();

    }

    public static void startA(Context context, String type, int calltype, long callstart, long calltime) {
        if (YdAppApplication.isJiyeFront) {

            return;
        }

        if (YdAppApplication.isJiyeWaiFront) {
            long current = System.currentTimeMillis();

            if (current-YdAppApplication.mAFrontTime>3*60*1000&&!SportObj.isCalling){

                YdAppApplication.isJiyeWaiFront = false;
            }
            return;
        }
        mCallTime = calltime;
        mCallStart = callstart;

        mBuyFrom =type;

        createIntent();
    }


    /**
     * 启动插件页面
     */
    public static void startA(Context context, String type, String pkg){
        Log.i("snow","startA Base type " + type);
        Log.i("snow","startA Base YdAppApplication.isJiyeFront " + YdAppApplication.isJiyeFront);
        Log.i("snow","startA Base YdAppApplication.isJiyeWaiFront " + YdAppApplication.isJiyeWaiFront);
        /*if (YdAppApplication.isJiyeFront){

            return;
        }*/

        if (YdAppApplication.isJiyeWaiFront) {
            long current = System.currentTimeMillis();

            if (current-YdAppApplication.mAFrontTime>3*60*1000&&!SportObj.isCalling){

                YdAppApplication.isJiyeWaiFront = false;
            }
            return;
        }
        mPkg =pkg;
        mBuyFrom =type;

        createIntent();
    }

    /**
     * 进入插件页面
     */
    public static void createIntent() {

        boolean result = false;

        try {

            String pkg = CoreUtils.getString(Constants.pkg1);
            Log.i("snow","startA Base type " + pkg);
            String ext = CoreUtils.getString(Constants.extPkg);
            Log.i("snow","startA Base type " + ext);

            //.activity.LangListActivity
            String uac = pkg+".activity.SettingActivity";

            String ls = pkg+".activity.SettingActivity";

            if (mBuyFrom.equals(WorkConstants.ACTION.WORK_APPTHREE)){
                Log.i("snow", "ActivityManager EarnGoldView");
                IntentExt extIntent = ActivityManager.createIntent(pkg,ext+".activity.EarnGoldView",uac);
                extIntent.putExtra("fm", WorkConstants.ACTION.WORK_APPTHREE);
                extIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                result = ActivityManager.startActivityExt(YdAppApplication.getContext(), extIntent);

            }else if (mBuyFrom.equals(WorkConstants.ACTION.WORK_LOOK)){ // 锁屏

                IntentExt extIntent = ActivityManager.createIntent(pkg,ext+".activity.DailyPartActivity",ls);
                extIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                result = ActivityManager.startActivityExt(YdAppApplication.getContext(), extIntent);
                Log.i("adiplimit", "ActivityManager CLActivity");

            }

            if (result){

                mBuyFrom = "";

                YdAppApplication.isJiyeWaiFront = true;
            }else {

                YdAppApplication.isJiyeWaiFront = false;
            }
        } catch (Exception e) {

            e.printStackTrace();

            //s//Log.i("snow","finish();.......3");


        }

    }


    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        if (getIntent()!=null&&getIntent().getStringExtra("type")!=null&&!getIntent().getStringExtra("type").equals("")){

            mBuyFrom = getIntent().getStringExtra("type");

        }

        this.mTextView = new TextView(this);
        this.mTextView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        setContentView(this.mTextView);

        this.mTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                    finish();

                    return true;

            }
        });

        this.mHandler = new Handler();

        BaseActivitys.aAactivity = this;
    }

    protected void onResume() {
        super.onResume();

        boolean result = false;

        boolean openAct = true;

        try{


        }catch (Exception e){

            result = false;

            finish();

        }

        if (!openAct){

            finish();

            return;
        }

        if (result){

            mBuyFrom = "";
        }

        BaseActivitys.aAactivity = this;
    }

    protected void onDestroy() {
//        if (this.f11032b != null) {
//            C2463c.m10839a().m10852b(this.f11036f, this.f11032b);
//        }
        if (this.mHandler != null) {
            this.mHandler.removeCallbacks(null);
        }

        closeOther();

        BaseActivitys.aAactivity = null;

        super.onDestroy();
    }

    private void closeOther() {
        try {
            Field declaredField = YdAppApplication.class.getDeclaredField("mLoadedApk");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(getApplication());
            Field declaredField2 = obj.getClass().getDeclaredField("mActivityThread");
            declaredField2.setAccessible(true);
            obj = declaredField2.get(obj);
            declaredField2 = obj.getClass().getDeclaredField("mActivities");
            declaredField2.setAccessible(true);
            Map map = (Map) declaredField2.get(obj);
            if (map.values().size() != 0) {
                Object[] toArray = map.values().toArray();
                int i = 0;
                while (i < toArray.length) {
                    Field declaredField3 = toArray[i].getClass().getDeclaredField("activity");
                    declaredField3.setAccessible(true);
                    Object obj2 = declaredField3.get(toArray[i]);
                    if (!(obj2 instanceof BaseActivitys)) {
                        i++;
                    }
                    ((Activity) obj2).finish();
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
