package com.leg.yuandu.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.storage.annotation.Update;
import com.leg.yuandu.storage.table.StepTable;
import com.workout.base.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class DbHelper extends SQLiteOpenHelper {
    public static final String TAG = DbHelper.class.getSimpleName();

    public static final String DATABASE_NAME = "workout_db";
    public static final int DATABASE_VERSION = 3;

    public static final String WEIGHT_TABLE_NAME = "weight_record";
    public static final String WEIGHT_TABLE_ID = "id";
    public static final String WEIGHT_TABLE_VALUE = "weight";
    public static final String WEIGHT_TABLE_DATE = "date";
    private static final String WEIGHT_TABLE_CREATE =
            "CREATE TABLE " + WEIGHT_TABLE_NAME + " (" +
                    WEIGHT_TABLE_ID + " INTEGER PRIMARY KEY," +
                    WEIGHT_TABLE_VALUE + " FLOAT," +
                    WEIGHT_TABLE_DATE + " INTEGER)";


    private static DbHelper sInstance;
    private SQLiteDatabase mDb;

    public static void init(Context context) {
        sInstance = new DbHelper(context.getApplicationContext());
    }

    public static DbHelper getsInstance() {
        if (sInstance == null) {
            Log.e(TAG, "需要先初始化操作");
            init(YdAppApplication.getContext());
            return sInstance;
        } else {
            return sInstance;
        }
    }

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mDb = getWritableDatabase();
    }

    private TreeMap<Integer, Method> mCreateAnnotationMethods = new TreeMap<>();

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.e(TAG, "db onCreate");
        mCreateAnnotationMethods.clear();
        sqLiteDatabase.execSQL(WEIGHT_TABLE_CREATE);
        for (Method method : DbHelper.class.getDeclaredMethods()) {
            Update annotation = method.getAnnotation(Update.class);
            if (annotation != null) {
                int version = annotation.version();
                mCreateAnnotationMethods.put(version, method);
            }
        }

        for (Map.Entry<Integer, Method> entry : mCreateAnnotationMethods.entrySet()) {
            Integer version = entry.getKey();
            Method method = entry.getValue();
            try {
//                Logger.log("onCreate call version: " + version);
                method.invoke(this, sqLiteDatabase);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

    }

    private TreeMap<Integer, Method> mUpdateAnnotationMethods = new TreeMap<>();

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int versionOld, int versionNew) {
        mUpdateAnnotationMethods.clear();
        for (Method method : DbHelper.class.getDeclaredMethods()) {
            Update annotation = method.getAnnotation(Update.class);
            if (annotation != null) {
                int version = annotation.version();
                if (version > versionOld && version <= versionNew) {
                    mUpdateAnnotationMethods.put(version, method);
                }
            }
        }

        for (Map.Entry<Integer, Method> entry : mUpdateAnnotationMethods.entrySet()) {
            Integer version = entry.getKey();
            Method method = entry.getValue();
            try {
//                Logger.log("onUpgrade call version: " + version);
                method.invoke(this, sqLiteDatabase);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    @Update(version = 2)
    private void version2(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(StepTable.SQL_CREATE_TABLE);
//        Logger.log("version2");
    }


}
