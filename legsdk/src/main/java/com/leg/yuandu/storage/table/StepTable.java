package com.leg.yuandu.storage.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.workout.base.GlobalConst;
import com.workout.base.TimeUtil;

import java.util.Map;
import java.util.Random;
import java.util.TreeMap;


/**
 * Created by chenhewen on 2018/7/26.
 */

public class StepTable {

    public static final String TABLE_NAME = "t_step";

    public static final String ID = "id";
    public static final String DATE = "date";
    public static final String STEP_COUNT = "step_count";
    public static final String STEP_TIME = "step_time";

    public static final String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
            DATE + " INTEGER PRIMARY KEY," +
            STEP_COUNT + " INTEGER," +
            STEP_TIME + " INTEGER)";

}
