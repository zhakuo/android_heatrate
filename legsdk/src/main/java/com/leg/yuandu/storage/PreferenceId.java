package com.leg.yuandu.storage;

/**
 * PreferenceId
 * Created by chenhewen on 2018/6/28.
 */

public class PreferenceId {

    /**
     * 当前的进度到第几天
     */
    public static final String CURR_DAY_INDEX = "curr_day_index";

    /**
     * 当前的进度到第几个动作
     */
    public static final String CURR_ACTION_INDEX = "curr_action_index";

    /**
     * 用于计算BMI的用户体重
     */
    public static final String PERSONAL_WEIGHT = "personal_weight";
    /**
     * 用于计算BMI的用户身高
     */
    public static final String PERSONAL_HEIGHT = "personal_height";

    /**
     * 休息开始时间
     */
    public static final String REST_START_MILLIS = "rest_start_millis";

    /**
     * 保活效果（List<Long>)
     */
    public static final String ALIVE_MILLIS = "alive_millis";

    /**
     * reminder一周提醒设置
     */
    public static final String REMINDER_REPEAT_DAYS = "reminder_repeat_days";
    /**
     * reminder闹钟hour
     */
    public static final String REMINDER_ALARM_HOUR = "reminder_alarm_hour";
    /**
     * reminder闹钟minute
     */
    public static final String REMINDER_ALARM_MINUTE = "reminder_alarm_minute";
    /**
     * reminder是否开启
     */
    public static final String REMINDER_ALARM_ON = "reminder_alarm_on";

    /**
     * 运动播报语音级别
     */
    public static final String EXERCISE_SPEAK_LEVEL = "exercise_speak_level";

    /**
     * 记录进入主界面次数
     */
    public static final String APP_OPEN_TIMES = "app_open_times";
    /**
     * 评分引导弹出次数
     */
    public static final String RATE_POP_TIMES = "rate_pop_times";

    /**
     * 健身的背景音乐
     */
    public static final String EXERCISE_BGM_ENABLE = "exercise_bgm_enable";

    /**
     * 首次安装时间
     */
    public static final String FIRST_INSTALL_MILLIS = "first_install_millis";

    /**
     * 健身过程返回主界面全屏广告请求次数
     */
    public static final String DROP_OUT_AD_REQUEST_TIMES = "drop_out_ad_request_times";
    /**
     * 健身完成返回主界面全屏广告请求次数
     */
    public static final String FINISH_AD_REQUEST_TIMES = "finish_ad_request_times";
    /**
     * 动作讲解视频超过15s全屏广告请求次数
     */
    public static final String VIDEO_AD_REQUEST_TIMES = "video_ad_request_times";

    /**
     * Debug页面设置的action重复次数
     */
    public static final String DEBUG_ACTION_REPEAT_TIMES = "debug_action_repeat_times";

    /**
     * 记录开始计步时，系统显示的当前步数
     */
    public static final String STEP_COUNT_SYSTEM_INIT = "step_count_system_init";

    /**
     * 步数目标
     */
    public static final String STEP_TARGET = "step_target";

    /**
     * 步数通知栏是否开启
     */
    public static final String STEP_NOTIFICATION_ENABLE = "step_notification_enable";

    /**
     * 针对8.0以上第一次开启通知栏是否开启
     */
    public static final String STEP_FIRST_NOTIFICATION_ENABLE = "step_first_notification_enable";

    /**
     * 高度选择单位
     */
    public static final String UNIT_HEIGHT_SELECT_METRIC = "unit_height_select_metric";

    /**
     * 体重选择单位
     */
    public static final String UNIT_WEIGHT_SELECT_METRIC = "unit_weight_select_metric";

    /**
     * 距离选择单位
     */
    public static final String UNIT_DISTANCE_SELECT_METRIC = "unit_distance_select_metric";

    /**
     * 未开启的训练日是否已经自动展示过提示对话框了
     */
    public static final String DISABLED_DAY_DIALOG_SHOWN = "disabled_day_dialog_shown";

    /**
     * 上次计步上传时间
     */
    public static final String STEP_COUNT_UPLOAD_MILLIS = "step_count_upload_millis";

    /**
     * 设置页面用户选择的语言id
     */
    public static final String LANGUAGE_SELECTED_ID = "language_selected_id";
}
