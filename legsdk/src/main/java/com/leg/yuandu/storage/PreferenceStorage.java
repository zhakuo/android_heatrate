package com.leg.yuandu.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.leg.yuandu.YdAppApplication;
import com.workout.base.global.GlobalPreferenceFileName;


/**
 *
 * SharedPreference
 * {@link PreferenceId}
 *
 * Created by chenhewen on 2018/6/28.
 */
public class PreferenceStorage {

    /**
     * 获取全局的sharedPreference的实例
     * 如果你使用的仅仅是简单的配置，那么可以考虑放在这里，并在{@link PreferenceId}中统一定义key，以免重复
     * @return
     */
    public static SharedPreferences getGlobalSp() {
        Context appContext = YdAppApplication.getContext();
        return appContext.getSharedPreferences(GlobalPreferenceFileName.GLOBAL_SP_FILE_NAME, Context.MODE_PRIVATE);
    }

    /**
     * 获取reminder的sharedPreference的实例
     * @return
     */
    public static SharedPreferences getReminderSp() {
        Context context = YdAppApplication.getContext();
        return  context.getSharedPreferences(GlobalPreferenceFileName.REMINDER_SP_FILE_NAME, Context.MODE_PRIVATE);
    }
}
