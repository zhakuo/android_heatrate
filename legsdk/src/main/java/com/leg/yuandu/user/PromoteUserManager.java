package com.leg.yuandu.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.adslibrary.YLog;
import com.leg.yuandu.config.SystemConfig;

import com.leg.yuandu.utils.HttpProtocolUtil;
import com.workout.base.preferences.PreferenceUtil;


public class PromoteUserManager {

    private final static String LOG_TAG = PromoteUserManager.class.getSimpleName();

    public final static String[] BUY_CHANNEL_OTHERS = {"bg", "cg"};
    /**
     * 本地用户买量渠道列表, 用于判断是否为买量用户<br>
     */
    public final static String[] BUY_USER_CHANNELS = new String[]{"adwords", "fb",
            "yeahmobi_speed"};
    public final static String KEY_TRACKER_CHANNELS = "KEY_TRACKER_CHANNELS";

    private static PromoteUserManager sInstance;
    private Context mContext;
    private String[] mOnlineBuyUserChannels;

    private PromoteUserManager(Context context) {
        mContext = context;
        checkIdentifyUser();
    }

    /**
     * 初始化单例,在程序启动时调用<br>
     *
     * @param context
     */
    public static void initSingleton(Context context) {
        sInstance = new PromoteUserManager(context);
    }

    /**
     * 获取单例.<br>
     *
     * @return
     */
    public static PromoteUserManager getInstance() {
        if (sInstance == null) {
            initSingleton(YdAppApplication.getContext());
        }
        return sInstance;
    }

    public static PromoteUserManager getInstance(Context context) {
        if (sInstance == null) {
            initSingleton(context);
        }
        return sInstance;
    }

    private void checkIdentifyUser() {
        final String channel = getPromoteUserChannel();

        // 可能为自然渠道 ，无需识别
        if (HttpProtocolUtil.getChannel(mContext).equals(channel)) {
            return;
        }

        // 优先是本地数据判断
        if (isPromoteUser(channel, BUY_USER_CHANNELS)) {
            Log.d(LOG_TAG, "BUY_USER_CHANNELS OK");
            setPromoteUser(true);
            return;
        }

    }


    public void setPromoteUser(boolean isPromoteUser) {
        SharedPreferences sharedPreferences= PreferenceUtil.getSharedPreferences(SystemConfig.SETTING, YdAppApplication.getContext());
        sharedPreferences.edit().putBoolean(SystemConfig.KEY_IS_BUY_USER, isPromoteUser).commit();

    }

    public void setPromoteUser(boolean isPromoteUser,Context context) {
        SharedPreferences sharedPreferences= PreferenceUtil.getSharedPreferences(SystemConfig.SETTING, context);
        sharedPreferences.edit().putBoolean(SystemConfig.KEY_IS_BUY_USER, isPromoteUser).commit();

    }
    /**
     * 判断用户是否为买量用户<br>
     *
     * @return
     */
    public boolean isPromoteUser() {
        SharedPreferences sharedPreferences= PreferenceUtil.getSharedPreferences(SystemConfig.SETTING, YdAppApplication.getContext());
        boolean isLockAsBuyUser = sharedPreferences.getBoolean(SystemConfig.KEY_LOCK_AS_BUY_USER, false);
        if (YLog.isD()) {
            YLog.d(LOG_TAG, "isPromoteUser() isLockAsBuyUser =" + isLockAsBuyUser);
        }
        if (isLockAsBuyUser) {
            return true;
        } else {
            boolean isPromoteUser = sharedPreferences.getBoolean(SystemConfig.KEY_IS_BUY_USER, false);
            if (YLog.isD()) {
                YLog.d(LOG_TAG, "isPromoteUser() isPromoteUser =" + isPromoteUser);
            }
            return isPromoteUser;
        }
    }

    /**
     * 是否为买量用户
     *
     * @param channel     用户当前的渠道
     * @param buyChannels 买量的渠道列表
     * @return
     */
    private boolean isPromoteUser(String channel, String[] buyChannels) {
        if (buyChannels != null) {
            for (final String buy : buyChannels) {
                if (!TextUtils.isEmpty(channel) && channel.equalsIgnoreCase(buy)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取用户的买量来源<br>
     *
     * @return
     */
    public String getPromoteUserChannel() {
        String buyUserChannel = SystemConfig.getInstance().getBuyChannel();
        if (TextUtils.isEmpty(buyUserChannel)) {
            buyUserChannel = HttpProtocolUtil.getChannel(mContext);
        }

        return buyUserChannel;
    }

    /**
     * 保存用户买量渠道源标识<br>
     *
     * @param buyUserChannel
     */
    private void savePromoteUserChannelPrivate(String buyUserChannel) {

        if (TextUtils.isEmpty(buyUserChannel)) {
            return;
        }

        SystemConfig.getInstance().setBuyChannel(buyUserChannel);
    }

    /**
     * 保存买量渠道源的标识<br>
     *
     * @param buyUserChannel
     */
    public void savePromoteUserChannel(final String buyUserChannel) {
        savePromoteUserChannelPrivate(buyUserChannel);

        checkIdentifyUser();
    }


}
