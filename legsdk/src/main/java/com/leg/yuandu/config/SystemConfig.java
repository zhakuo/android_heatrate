package com.leg.yuandu.config;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.adslibrary.YLog;
import com.leg.yuandu.utils.AppUtils;
import com.leg.yuandu.utils.LocationHelper;
import com.workout.base.preferences.PreferenceUtil;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * 设置页面的配置信息
 * <p>
 * Created by biz on 16-4-15.
 */

/**
 * 设置页面的配置信息
 * <p>
 * Created by biz on 16-4-15.
 */
public class SystemConfig {

    public static final long AN_HOUR = 60 * 60 * 1000;
    public static final long ONE_DAY_MILLS = 24 * AN_HOUR;

    private static SystemConfig sSystemConfig;
    private final static Object LOCK = new Object();


    public static final String SETTING = "profile_setting";


    public static final String CHARGING_LOCK_FIRSTTIME = "charging_lock_firsttime";


    public static final String CHARGING_LOCK_USER_SET_FLAG = "charging_lock_user_set_flag";

    public static final String KEY_CHARGELOCK_AD_SHOW_LASTTIME = "chargelock_ad_show_lasttime";

    public final static String KEY_LOCK_AS_BUY_USER = "key_lock_as_buy_user";


    public static final String KEY_102_LAST_UPLOAD_TIMEMILLS = "key_102_last_upload_timemills";
    public static final String KEY_45_LAST_UPLOAD_DATA = "key_45_last_upload_data";

    public static final String PERMISSION_TIP = "permission_tip";

    public static final String SHOW_DISCOVERY_RED = "show_discovery_red";
    public static final String SAVED_DEEPLINK = "saved_deeplink";

    public static final String POWER_SAVER_ENABLE = "power_saver_enable";

    public static final String FULLSCREEN_POP = "fullscreen_pop";

    public static final String BANNER_STATUS = "banner_status";

    public static final String MOPUB_CACHED_TIME = "mopub_cached_time";

    public static final String LAST_LATITUE = "last_latitue";
    public static final String LAST_LONGITUDE = "last_longitude";


    public static final String LOCATION_UPDATETIME = "location_updatetime";

    public static final String WEB_INIT_TIME = "web_init_time";

    public static final String SYS_REAL_TIME = "sys_real_time";

    public static final String WEB_CURRENT_TIME = "web_current_time";

    public static final String LOCK_CODE = "lock_code";

    public static final String KEY_DAILY_FIREBASE_UPLOAD_TIMES = "daily_firebase_upload_times";

    /**
     * 记录买量的渠道源<br>
     * 值类型:String<br>
     */
    public final static String KEY_BUY_USER_CHANNEL = "key_buy_user_channel";
    /**
     * 记录是否为买量用户(成功判定结果后，就不用再判定了，若未判定则保存为空)<br>
     * 值类型:String<br>
     */



    public final static String KEY_IS_BUY_USER = "key_is_buy_user";

    private SharedPreferences mStaticPreferences;

    public static final String CL_FB_SHOW_TIMES = "cl_fb_show_times";


    public static final String PERMISSION_LAST_TIME = "permission_last_time";

    public static final String SHOW_PROMO = "show_promo";


    public static final String SHOW_EDITOR_PROMO = "show_editor_promo";

    public static final String SHOW_MENU_PROMO = "show_menu_promo";

    public static final String CAMPAIGN = "campaign";

    public static final String CURRENT_TIPS = "current_tips";


    public static final String COUNTRY_CODE = "country_code";

    public static final String SELECTED_MODE = "selected_mode";

    public static final String KEY_DAILY_FIREBASE_UPLOAD_USER_TYPE_TIMES = "daily_firebase_upload_user_type_times";


    private SystemConfig() {
        init(YdAppApplication.getContext());
    }


    public static SystemConfig getInstance() {
        if (sSystemConfig == null) {
            synchronized (LOCK) {
                if (sSystemConfig == null) {
                    sSystemConfig = new SystemConfig();
                }
            }
        }
        return sSystemConfig;
    }

    public void init(Context context) {
        mStaticPreferences = PreferenceUtil.getSharedPreferences(SETTING, context);
    }

    public boolean isShowGrantPermissionTips() {
        checkInit();
        return mStaticPreferences.getBoolean(PERMISSION_TIP, false);
    }

    public void setGrantPermissionTips(boolean flag) {
        checkInit();
        mStaticPreferences.edit().putBoolean(PERMISSION_TIP, flag).apply();
        mStaticPreferences.edit().putBoolean(PERMISSION_TIP, flag).commit();
    }

    public boolean isDeeplinkSave() {
        checkInit();
        return mStaticPreferences.getBoolean(SAVED_DEEPLINK, false);
    }

    public void setDeeplinkSave(boolean flag) {
        checkInit();
        mStaticPreferences.edit().putBoolean(SAVED_DEEPLINK, flag).apply();
        mStaticPreferences.edit().putBoolean(SAVED_DEEPLINK, flag).commit();
    }

    public boolean isEnablePowerSaver() {
        checkInit();
        return mStaticPreferences.getBoolean(POWER_SAVER_ENABLE, false);
    }

    public void setEnablePowerSaver(boolean flag) {
        checkInit();
        mStaticPreferences.edit().putBoolean(POWER_SAVER_ENABLE, flag).apply();
        mStaticPreferences.edit().putBoolean(POWER_SAVER_ENABLE, flag).commit();
    }

    public boolean isShowFullscreenPopup() {
        checkInit();
        return mStaticPreferences.getBoolean(FULLSCREEN_POP, false);
    }

    public void setFullscreenPopup(boolean flag) {
        checkInit();
        mStaticPreferences.edit().putBoolean(FULLSCREEN_POP, flag).apply();
        mStaticPreferences.edit().putBoolean(FULLSCREEN_POP, flag).commit();
    }

    public void saveCurrentTips(String campaignName) {
        checkInit();
        mStaticPreferences.edit().putString(CURRENT_TIPS, campaignName).apply();
        mStaticPreferences.edit().putString(CURRENT_TIPS, campaignName).commit();
    }

    public String getCurrentTips() {

        checkInit();

        return mStaticPreferences.getString(CURRENT_TIPS, "");
    }

    private void checkInit() {
        if (mStaticPreferences == null) {
            init(YdAppApplication.getContext());
        }
    }


    public void setBuyChannel(String buyUserChannel) {
        checkInit();
        mStaticPreferences.edit().putString(SystemConfig.KEY_BUY_USER_CHANNEL, buyUserChannel).apply();
    }

    public String getBuyChannel() {
        checkInit();
        return mStaticPreferences.getString(KEY_BUY_USER_CHANNEL, "");
    }

    public int getFirebaseConfigFirestTime() {
        checkInit();
        return mStaticPreferences.getInt(CHARGING_LOCK_FIRSTTIME, -1);
    }

    public void setFirebaseConfigFirestTime(int value) {
        checkInit();
        mStaticPreferences.edit().putInt(SystemConfig.CHARGING_LOCK_FIRSTTIME, value).apply();
    }

    public void setChargelockAdShowLastTime(long timemills) {
        checkInit();
        mStaticPreferences.edit().putLong(SystemConfig.KEY_CHARGELOCK_AD_SHOW_LASTTIME, timemills).apply();
    }

    public long getChargelockAdShowLastTime() {
        checkInit();
        return mStaticPreferences.getLong(KEY_CHARGELOCK_AD_SHOW_LASTTIME, 0);
    }

    public void saveBannerStatus(boolean status) {
        checkInit();
        mStaticPreferences.edit().putBoolean(BANNER_STATUS, status).apply();
        mStaticPreferences.edit().putBoolean(BANNER_STATUS, status).commit();
    }

    public boolean getBannerStatus() {

        checkInit();
        return mStaticPreferences.getBoolean(BANNER_STATUS, false);
    }

    public void saveLocation(double latitue, double longitude) {
        checkInit();
        mStaticPreferences.edit().putString(LAST_LATITUE, latitue + "").apply();
        mStaticPreferences.edit().putString(LAST_LONGITUDE, longitude + "").commit();

        SystemConfig.getInstance().saveLocationUpdatetime(System.currentTimeMillis());

        try {

            YdAppApplication.getContext().sendBroadcast(new Intent("weather_update"));

        } catch (Exception e) {

        }
    }

    public void saveLocationUpdatetime(long updatetime) {
        checkInit();
        mStaticPreferences.edit().putLong(LOCATION_UPDATETIME, updatetime).apply();
        mStaticPreferences.edit().putLong(LOCATION_UPDATETIME, updatetime).commit();
    }

    public boolean isSaveLocation() {
        checkInit();

        long updateime = mStaticPreferences.getLong(LOCATION_UPDATETIME, -1);

        if (updateime + 1 * 24 * 60 * 60 * 1000l > System.currentTimeMillis()) {

            return true;

        } else {

            return false;
        }

    }


    public boolean isCNLocation() {
        checkInit();

        String lat = mStaticPreferences.getString(LAST_LATITUE, "");
        String lon = mStaticPreferences.getString(LAST_LONGITUDE, "");

        if (lat.equals("") || lon.equals("")) {

            return false;
        }

        try {

            Double latD = Double.parseDouble(lat);

            Double lonD = Double.parseDouble(lon);


            if (latD == null || lonD == null) {

                return false;

            } else {

                boolean result = LocationHelper.isInCN(latD, lonD);

                return result;
            }


        } catch (Exception e) {

            return false;
        }

    }

    public String getLat() {

        checkInit();

        String lat = mStaticPreferences.getString(LAST_LATITUE, "");

        return lat;
    }


    public String getLon() {

        checkInit();

        String lon = mStaticPreferences.getString(LAST_LONGITUDE, "");

        return lon;
    }


    public long getWebInitTime() {
        checkInit();
        return mStaticPreferences.getLong(WEB_INIT_TIME, 0);
    }

    public void setWebInitTime(long updatetime) {
        checkInit();
        mStaticPreferences.edit().putLong(WEB_INIT_TIME, updatetime).apply();
        mStaticPreferences.edit().putLong(WEB_INIT_TIME, updatetime).commit();
    }

    public long getWebCurrentTime() {
        checkInit();

        Context context = YdAppApplication.getContext();

        SharedPreferences sp = PreferenceUtil.getSharedPreferences(SystemConfig.SETTING, context);

        int firstVersionCode = sp.getInt("KEY_FIRST_START_VERSION_CODE", -1);
        if (firstVersionCode == -1) {

            sp.edit().putInt("KEY_FIRST_START_VERSION_CODE", AppUtils.getVersionCode(context)).commit();

        } else {

            if (!YLog.isD() && mStaticPreferences.getLong(WEB_CURRENT_TIME, 0) != 0) {

            } else {
                return System.currentTimeMillis();
            }

        }

        return mStaticPreferences.getLong(WEB_CURRENT_TIME, 0);
    }

    public void setWebCurrentTime(long updatetime) {
        checkInit();
        mStaticPreferences.edit().putLong(WEB_CURRENT_TIME, updatetime).apply();
        mStaticPreferences.edit().putLong(WEB_CURRENT_TIME, updatetime).commit();
    }

    public long getSystemRealTime() {
        checkInit();
        return mStaticPreferences.getLong(SYS_REAL_TIME, 0);
    }

    public void setSystemRealTime(long updatetime) {
        checkInit();
        mStaticPreferences.edit().putLong(SYS_REAL_TIME, updatetime).apply();
        mStaticPreferences.edit().putLong(SYS_REAL_TIME, updatetime).commit();
    }

    public static long getWebsiteDatetime() {
        try {
            URL url = new URL("https://www.baidu.com");// 取得资源对象
            URLConnection uc = url.openConnection();// 生成连接对象
            uc.connect();// 发出连接
            long ld = uc.getDate();// 读取网站日期时间

            return ld;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public void saveCountryCode(String campaignName) {
        checkInit();

        YLog.d("snow", "saveCountryCode  : " + campaignName);

        mStaticPreferences.edit().putString(COUNTRY_CODE, campaignName).apply();
        mStaticPreferences.edit().putString(COUNTRY_CODE, campaignName).commit();
    }

    public String getCountryCode() {

        checkInit();

        return mStaticPreferences.getString(COUNTRY_CODE, "");
    }

    public void setDailyFirebaseUploadUsertypeTime(long times) {
        checkInit();
        mStaticPreferences.edit().putLong(SystemConfig.KEY_DAILY_FIREBASE_UPLOAD_USER_TYPE_TIMES, times).apply();
    }

    public long getDailyFirebaseUploadUsertypeTime() {
        checkInit();
        return mStaticPreferences.getLong(SystemConfig.KEY_DAILY_FIREBASE_UPLOAD_USER_TYPE_TIMES, 0);
    }

    public void setLockAds(String code) {
        checkInit();
        mStaticPreferences.edit().putString(SystemConfig.LOCK_CODE, code).apply();
    }

    public String getLockAds() {
        return mStaticPreferences.getString(LOCK_CODE, "mopub_banner");
    }

    public void setDailyFirebaseUploadTime(long times) {
        checkInit();
        mStaticPreferences.edit().putLong(SystemConfig.KEY_DAILY_FIREBASE_UPLOAD_TIMES, times).apply();
    }

    public long getDailyFirebaseUploadTime() {
        checkInit();
        return mStaticPreferences.getLong(SystemConfig.KEY_DAILY_FIREBASE_UPLOAD_TIMES, 0);
    }
}
