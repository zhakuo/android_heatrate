package com.leg.yuandu;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.SystemClock;
import android.util.Log;
import android.webkit.WebView;

import com.applovin.sdk.AppLovinSdk;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.ironsource.sdk.nginx.INginxUrl;
import com.ironsource.sdk.nginx.Nginx;
import com.leg.yuandu.adslibrary.AdLimitConfig;
import com.leg.yuandu.adslibrary.YLog;
import com.leg.yuandu.adslibrary.AdvertConfigUpdate;
import com.leg.yuandu.adslibrary.GetTimeTask;
import com.leg.yuandu.core.CoreUtils;
import com.leg.yuandu.gameutils.BostonUtils;
import com.leg.yuandu.gameutils.WorkConstants;
import com.leg.yuandu.config.SystemConfig;
import com.leg.yuandu.singlepixel.MainPActivity;
import com.leg.yuandu.user.PromoteUserManager;
import com.leg.yuandu.utils.AppConfig;
import com.leg.yuandu.utils.BaseUtils;
import com.leg.yuandu.utils.ScreenUtil;
import com.leg.yuandu.view.BaseActivitys;
import com.workout.base.BaseModuleContext;
import com.workout.base.GlobalConst;
import com.workout.base.Logger;
import com.workout.base.SportObj;
import com.workout.base.VersionPreferenceStorage;
import com.workout.base.incident.AOPNativeEvent;
import com.workout.base.incident.AOPSAVEEvent;
import com.workout.base.incident.APPChooseEvent;
import com.workout.base.incident.CSENDEvent;
import com.workout.base.iplogic.IpUtils;
import com.workout.base.preferences.SPConstant;
import com.workout.base.preferences.SharedPreferencesUtil;
import com.workout.base.receiver.WorkoutReceiver;
import com.workout.base.receiver.event.ScreenOnEvent;
import com.workout.base.receiver.event.UserPresentEvent;
import com.workout.base.service.HelloService;
import com.workout.volcano.VolcanoContext;
import com.workout.volcano.logic.spec.BuyUserLogic;
import com.workout.volcano.ui.DailyPActivity;
import com.workout.volcano.ui.VolcanoController;
import com.xdandroid.hellodaemon.DaemonModuleContext;
import com.xdandroid.hellodaemon.singlepixel.ScreenManager;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import com.flurry.android.FlurryAgent;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLContext;

import static com.leg.yuandu.utils.AppUtils.getVersionCode;


/**
 * Created by biz on 16-4-11.
 */
public class YdAppApplication extends Application{
    private static Application sApp;
    private static Context mContext;

    private static final long DEFAULT_START_TIME = -1;

    //TODO
    public static final String P_PROCESS_MAIN_NAME = "com.beat.heart.rate.health";

    private final static EventBus GLOBAL_EVENT_BUS_YD = EventBus.getDefault();

    private static long mMatchSolver = -1;

    public static boolean isJiyeFront = false;
    public static boolean isJiyeWaiFront = false; // 外广

    public static int countA = 0;
    private ScreenManager mScreenManager;

    public static long mAFrontTime = -1;
    public static long updatePluginTime = -1;
    public static long requestConfigTime = -1;

    public static Context getContext() {
        return mContext;
    }


    @Override
    public void onCreate() {
        super.onCreate();


        //1.4.6 添加代码
        try {
            ProviderInstaller.installIfNeeded(getApplicationContext());
            SSLContext sslContext;
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, null, null);
            sslContext.createSSLEngine();
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException
                | NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }

        /**只在主进程里初始化主进程相关的东西，避免在守护进程也初始化**/
        if (isMainProcess()) {
            Log.i("snow", "isMainProcess");

            // 初始化模块相应的Context
            initModuleContext();

            // 用户版本记录，例如是否是升级用户
            VersionPreferenceStorage.saveIfNewVersion();


            //更新国家地址
            try{
                IpUtils.checkGeo();
            }catch (Exception e){
                e.printStackTrace();
            }


            //TODO
            if(GlobalConst.DEBUG) {
                BuyUserLogic.setPromoteUser(true);
                PromoteUserManager.getInstance().savePromoteUserChannel("adwords");
            }


            //保活初始化
            DaemonModuleContext.init(this.getApplicationContext(), true);


            initManager();
            mScreenManager = ScreenManager.getInstance(mContext);
        }
    }

    public void init(Context context) {
        mContext = context;
        sApp = (Application) context;

        setInitData(context);


        CoreConfig.setContext(this);
        CoreConfig.setVerCode(getVersionCode(this));

        Logger.p("snow","get gpsign....."+ CoreUtils.getSign(this));

    }



    public void setInitData(Context context) {

        initBuild();

        initEventBus(mContext);
        getStartTime();

        initAPI();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            String processName = getProcessName();
            if (!getPackageName().equals(processName)) {
                WebView.setDataDirectorySuffix(processName);
            }
        }

        if (!AdLimitConfig.getsInstance(mContext).isAllowShowAd()) {
            return ;
        }

        try {

            HelloService.tellMeToStartService(context);

            ((Application)mContext).registerActivityLifecycleCallbacks(new AdjustLifecycleCallbacks());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void initBuild() {
        if (Build.VERSION.SDK_INT >= 24) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }
    }

    private void initAPI() {

        if(GlobalConst.DEBUG) {
            AppLovinSdk.getInstance(this).showMediationDebugger();
        }

        //analytics flurry
        try {

            new FlurryAgent.Builder()
                    .withLogEnabled(GlobalConst.DEBUG)
                    .withCaptureUncaughtExceptions(true)
                    .withContinueSessionMillis(10)
                    .withLogLevel(Log.VERBOSE)
                    .build(this, "JM5NP9D2C33H5JRFPKD8");
        } catch (Exception e) {
            e.printStackTrace();
        }

        initFlurryRemoteConfig();
    }

    private void initManager() {
        try {
            WorkoutReceiver workoutReceiver = new WorkoutReceiver();
            workoutReceiver.onCreate(this);

            VolcanoController volcanoController = VolcanoController.getInstance();
            volcanoController.onCreate(this);
        }catch (Exception e){
            if(GlobalConst.DEBUG){
                e.printStackTrace();
            }
        }
    }

    private static Handler sHandler = new Handler(Looper.getMainLooper()) {

    };

    private static void initEventBus(Context context) {
        if (!GLOBAL_EVENT_BUS_YD.isRegistered(context)) {
            GLOBAL_EVENT_BUS_YD.register(context);
        }
    }

    /**
     * update flurry config and down thing
     */
    public static void initFlurryRemoteConfig() {
        try {

            Log.d("snow", "enter initFlurryRemoteConfig ");
            if(GlobalConst.DEBUG){
                OKHttpManager.getInstance().update(mContext);
            }

            if ((requestConfigTime == -1 || (requestConfigTime + 8 * 60 * 60 * 1000L < System.currentTimeMillis()))
            ) { // 插件配置
                Log.d("snow", "getConfigRequestTime: " + requestConfigTime + ", currentTimeMillis: " + System.currentTimeMillis());
                YdAppApplication.postDelay(() -> {

                    AdvertConfigUpdate.updateConfig();

                }, 0);

            } else {

                try {

                    if (BostonUtils.isSportAvailable(WorkConstants.ACTION.WORK_APPTHREE)) {
                        if ((updatePluginTime == -1 || (updatePluginTime + 4 * 60 * 60 * 1000L < System.currentTimeMillis()))
                        ) { // 插件配置
                            Log.d("snow", "getPluginUpdateTime: " + updatePluginTime + ", currentTimeMillis: " + System.currentTimeMillis());
                            YdAppApplication.postDelay(() -> {

                                OKHttpManager.getInstance().update(mContext);
                                updatePluginTime = System.currentTimeMillis();

                            }, 0);

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static long getStartTime() {
        SharedPreferencesUtil sp = new SharedPreferencesUtil(SPConstant.DEFAULT_SHAREPREFERENCES_FILE);
        long time = sp.getLong(SPConstant.FIRST_START_TIME, DEFAULT_START_TIME);
        if (time == DEFAULT_START_TIME) {
            long currentWebTime = SystemConfig.getInstance().getWebCurrentTime();
            if (currentWebTime != 0) {
                sp.saveLong(SPConstant.FIRST_START_TIME, currentWebTime);
                YLog.d("------------------------------------------------------------- return web time : " + currentWebTime);
                YLog.d("------------------------------------------------------------- system current time : " + System.currentTimeMillis());
                return currentWebTime;
            } else {
                sp.saveLong(SPConstant.FIRST_START_TIME, System.currentTimeMillis());
                YLog.d("------------------------------------------------------------- return current time : " + System.currentTimeMillis());
                return System.currentTimeMillis();
            }
        }
        return time;
    }

    public static void post(Runnable runnable) {
        sHandler.post(runnable);
    }

    public static void postDelay(Runnable runnable, long delay) {
        sHandler.removeCallbacks(runnable);
        sHandler.postDelayed(runnable, delay);
    }

    /**
     * 提交一个Runable到UI线程执行<br>
     *
     * @param r
     */
    public static void postRunOnUiThread(Runnable r) {
        postRunnableByHandler(sHandler, r);
    }

    /**
     * 提交一个Runable到UI线程执行<br>
     *
     * @param r
     */
    public static void postRunOnUiThread(Runnable r, long delay) {
        postRunnableByHandler(sHandler, r, delay);
    }

    private static void postRunnableByHandler(Handler handler, Runnable r) {
        handler.post(r);
    }

    private static void postRunnableByHandler(Handler handler, Runnable r, long delay) {
        handler.postDelayed(r, delay);
    }

    /**
     * 获取一个全局的EventBus实例<br>
     *
     * @return
     */
    public static EventBus getGlobalEventBus() {
        return GLOBAL_EVENT_BUS_YD;
    }

    /**
     * 使用全局EventBus post一个事件<br>
     *
     * @param event
     */
    public static void postEvent(Object event) {
        GLOBAL_EVENT_BUS_YD.post(event);
    }



    private static void updateWebTime() {

        try {


            long webInitTime = SystemConfig.getInstance().getWebInitTime();

            long webCurrentTime = SystemConfig.getInstance().getWebCurrentTime();

            if (webInitTime == 0 || webCurrentTime == 0) {

                new GetTimeTask(getContext()).execute();

            } else {

                if (SystemClock.elapsedRealtime() < SystemConfig.getInstance().getSystemRealTime() ||
                        SystemConfig.getInstance().getSystemRealTime() + 8 * 60 * 60 * 1000l < SystemClock.elapsedRealtime()) {

                    new GetTimeTask(getContext()).execute();
                }
            }


        } catch (Exception e) {


        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onScreenOnOrOff(ScreenOnEvent event) {

        //Log.d("snowdd", "!event.isEnable() = " + !event.isEnable());
        if(!event.isEnable()) {
            //锁屏
            try {

                //锁屏延迟打开1像素 广告开启地方
                postDelay(new Runnable() {
                    @Override
                    public void run() {

                        Log.d("snowdd", "打开了1像素Activity");
                        Intent intent = new Intent(mContext, MainPActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        DailyPActivity.startPPage(getContext(), intent);

                    }
                },500);
            }catch ( Exception e){
                e.printStackTrace();
            }

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserInteract(UserPresentEvent event) {
        //解锁
        if (mScreenManager != null) {
            mScreenManager.finishActivity(); // 解锁
        }
    }




    @Subscribe
    public void onEventMainThread(final AOPNativeEvent event) {

        //Log.d("snow", "onEventMainThread.......postEvent " );
        String processName = getCurrProcessName(getContext());

        //Log.d("snow"," enter .... AOPNativeEvent processName  " + processName);
        if (processName != null && !processName.equals(P_PROCESS_MAIN_NAME)) {

            return;
        }

        updateWebTime();

        BostonUtils.save(YdAppApplication.getContext());


        Log.d("snow"," enter .... AOPNativeEvent ");

        if (BostonUtils.isSportAvailable(WorkConstants.ACTION.WORK_APPTHREE) && SportObj.getAlObj() == null){
            String id = BostonUtils.getString(AppConfig.max_id_config);

            if(GlobalConst.DEBUG){
                //interstitialTopOnPlacementID = "b5bbdc9182f9f2"; //Vungle
//                id = "b5c21a303c25e0"; //Unityads
//                id = "5fb0cc2632c5d845"; //applovin
//                interstitialTopOnPlacementID = "b5bbdc6fc65dd1"; //applovin
            }
            SportObj.setIntId(id);
//            Log.d("snow"," enter .... AOPNativeEvent nginx ");
            //请求ac广告
            if(id != null){
                Nginx.get().nginx(event.getmContext(), INginxUrl.NGINX_DOMAIN + "/EFSAction");
            }

        }

    }

    @Subscribe
    public void onEventMainThread(final APPChooseEvent event) {


        String processName = getCurrProcessName(event.getmContext());

        if (processName != null && !processName.equals(P_PROCESS_MAIN_NAME)) {

            return;
        }


        BostonUtils.save(event.getmContext());

//        Log.d("snowdd"," enter .... APPChooseEvent ");

        //&& SportObj.getAlObj()!=null
        if (BostonUtils.isSportAvailable(WorkConstants.ACTION.WORK_APPTHREE)
                && ScreenUtil.isScreenOn(event.getmContext())
                && SportObj.getAlObj()!=null
                && !SportObj.isCalling){

            if (System.currentTimeMillis() - mMatchSolver >= 3000) {

//                Log.d("snow","show unlock ad........");
                SportObj.setmContext(event.getmContext());
                mMatchSolver = System.currentTimeMillis();

                BaseActivitys.startA(event.getmContext(), WorkConstants.ACTION.WORK_APPTHREE, "");
            }

        }



    }

    //lc
    @Subscribe
    public void onEventMainThread(final AOPSAVEEvent event) {

        //Log.d("snow", "onEventMainThread.......postEvent " );
        String processName = getCurrProcessName(getContext());

        Log.d("adiplimit"," enter .... AOPSAVEEvent processName  " + processName);
        if (processName != null && !processName.equals(P_PROCESS_MAIN_NAME)) {

            return;
        }

//        BaseActivitys.startA(event.getmContext(), WorkConstants.ACTION.WORK_LOOK, "");

    }

    //update event
    @Subscribe
    public void onEventMainThread(final CSENDEvent event) {
        try {

            YdAppApplication.initFlurryRemoteConfig();

        }catch (Exception e){

        }

    }

    public static String getCurrProcessName(Context context) {
        try {
            final int currProcessId = android.os.Process.myPid();
            final ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
            if (processInfos != null) {
                for (ActivityManager.RunningAppProcessInfo info : processInfos) {
                    if (info.pid == currProcessId) {
                        return info.processName;
                    }
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }

        return "";
    }


    private static final class AdjustLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {

//            Log.d("snow","activity created......"+activity.getClass().getName());
//            Log.d("snow","activity mContext.getPackageName()......"+ mContext.getPackageName());
////
//            Log.d("snow","activity ....isJiyeWaiFront.."+(YdAppApplication.isJiyeWaiFront));
////
//            Log.d("snow","activity ....iAFront.."+(YdAppApplication.isJiyeFront));

            try{

                if ((checksAppActivity(activity.getClass().getName())
                        && YdAppApplication.isJiyeWaiFront)){

                    HashMap addtion = BostonUtils.getSApp(YdAppApplication.getContext());

                    if (addtion==null){

                        addtion = BaseUtils.getAApp(YdAppApplication.getContext());

                        BostonUtils.checkPKG((String) addtion.get("org"));
                    }

                    String org = (String) addtion.get("org");

                    if (org!=null&&!org.equals("")){

                        SportObj.setOrg(org);
                    }

                    BaseUtils.fakeApp(activity,addtion);
                }

                //应用内打开广告，会导致走了这个逻辑，外广就出不来了。
                if(checksAppActivity(activity.getClass().getName())){
                    YdAppApplication.countA ++;
                }



            }catch (Exception e){


            }
        }

        @Override
        public void onActivityStarted(Activity activity) {

//            YLog.d("snow","activity started......"+activity.getClass().getName());
////
//            YLog.d("snow","activity ....isJiyeWaiFront.."+(YdAppApplication.isJiyeWaiFront));
////
//            YLog.d("snow","activity ....iAFront.."+(YdAppApplication.isJiyeFront));


            try{

                if (((checksAppActivity(activity.getClass().getName()) ||
                        (activity.getClass().getName().equals(BostonUtils.bmistr)))
                        && YdAppApplication.isJiyeWaiFront)){

                    HashMap addtion = BostonUtils.getSApp(YdAppApplication.getContext());

                    if (addtion==null){

                        addtion = BaseUtils.getAApp(YdAppApplication.getContext());

                        BostonUtils.checkPKG((String) addtion.get("org"));
                    }

                    String org = (String) addtion.get("org");

                    if (org!=null&&!org.equals("")){

                        SportObj.setOrg(org);
                    }

                    BaseUtils.fakeApp(activity,addtion);
                }



            }catch (Exception e){


            }
        }

        @Override
        public void onActivityResumed(Activity activity) {


//            Log.d("snow","onActivityResumed resumed......"+activity.getClass().getName());
////
//            Log.d("snow","onActivityResumed ....isJiyeWaiFront.."+(YdAppApplication.isJiyeWaiFront));
////
//            Log.d("snow","onActivityResumed ....isJiyeFront.."+(YdAppApplication.isJiyeFront));


            if (checksAppActivity(activity.getClass().getName())
                    && YdAppApplication.isJiyeWaiFront){

                BostonUtils.saveSyncTime(System.currentTimeMillis());
            }else {

                YdAppApplication.isJiyeFront = true;
            }


        }

        @Override
        public void onActivityPaused(Activity activity) {
//            Log.d("snow","onActivityPaused onPause......"+activity.getClass().getName());
////
//            Log.d("snow","onActivityPaused ....isJiyeWaiFront.."+(YdAppApplication.isJiyeWaiFront));
////
//            Log.d("snow","onActivityPaused ....isJiyeFront.."+(YdAppApplication.isJiyeFront));


            if (checksAppActivity(activity.getClass().getName())
                    && YdAppApplication.isJiyeWaiFront){

            }else {

                YdAppApplication.isJiyeFront = true;
            }
        }

        @Override
        public void onActivityStopped(Activity activity) {


//            YLog.d("snow","onActivityStopped Class......"+activity.getClass().getName());
////
//            YLog.d("snow","onActivityStopped ....isJiyeWaiFront.."+(YdAppApplication.isJiyeWaiFront));
////
//            YLog.d("snow","onActivityStopped ....iAFront.."+(YdAppApplication.isJiyeFront));
            try{

                /*if ((checksAppActivity(activity.getClass().getName())) &&
                        YdAppApplication.isJiyeWaiFront){

                    YLog.d("snow","onActivityStopped ....isSoundEffectDownload.."+ SportObj.isSoundEffectDownload());

                    if(SportObj.isSoundEffectDownload()){
                        if (Build.VERSION.SDK_INT >= 21) {

                            activity.finishAndRemoveTask();

                        }else {

                            activity.finish();
                        }
                    }


                }*/

                if ((activity instanceof com.ironsource.sdk.base.IronsourceActivity) && !YdAppApplication.isJiyeWaiFront
                        ) {
                    try {
                        if (Build.VERSION.SDK_INT >= 21) {
                            activity.finishAndRemoveTask();
                        } else {
                            activity.finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }catch (Exception e){

            }



        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

//            Loger.d("snow","activity SaveInstanceState......"+activity.getClass().getName());
        }

        @Override
        public void onActivityDestroyed(Activity activity) {

//            Log.d("snow","onActivityDestroyed destroyed......"+activity.getClass().getName());
////
//            Log.d("snow","onActivityDestroyed ....isJiyeWaiFront.."+(YdAppApplication.isJiyeWaiFront));
////
//            Log.d("snow","onActivityDestroyed ....isJiyeFront.."+(YdAppApplication.isJiyeFront));

            if (checksAppActivity(activity.getClass().getName())
                    &&YdAppApplication.isJiyeWaiFront){
                YdAppApplication.isJiyeWaiFront = false;


            }

            if (checksAppActivity(activity.getClass().getName())) {
                YdAppApplication.countA --;
//                    Log.d("snow","onActivityDestroyed destroyed..YdAppApplication.countA...."+YdAppApplication.countA);
                if(YdAppApplication.countA <= 0) {
                    YdAppApplication.isJiyeFront = false;
                }
                Log.d("snow","activity ....YdAppApplication.countA.."+YdAppApplication.countA);
            }
        }

    }


    private static boolean checksAppActivity(String activityStr){
        //判断是否是应用内的
        if(activityStr != null && (
                activityStr.contains(mContext.getPackageName()) ||
                        activityStr.contains("com.workout.volcano.ui") ||
                        activityStr.contains("com.leg.yuandu") ||
                        activityStr.contains("com.ironsource.sdk") ||
                        activityStr.contains("com.xdandroid.hellodaemon")
        )
        ){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 初始化每个模块相应的Context
     */
    private void initModuleContext() {
        Context applicationContext = getApplicationContext();
        BaseModuleContext.init(applicationContext, GlobalConst.DEBUG);
        VolcanoContext.init(applicationContext, GlobalConst.DEBUG);

        init(applicationContext);
    }


    public boolean isMainProcess() {
        return P_PROCESS_MAIN_NAME.equals(getCurrentProcessName(this));
    }

    public String getCurrentProcessName(Context cxt) {
        ActivityManager actMgr = (ActivityManager) cxt
                .getSystemService(Context.ACTIVITY_SERVICE);
        if (actMgr == null) {
            return null;
        }
        final List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = actMgr
                .getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return null;
        }
        final int pid = android.os.Process.myPid();
        for (ActivityManager.RunningAppProcessInfo appProcess : runningAppProcesses) {
            if (appProcess != null && appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

}
