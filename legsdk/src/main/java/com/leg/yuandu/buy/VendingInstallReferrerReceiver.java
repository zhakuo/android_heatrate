package com.leg.yuandu.buy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.workout.base.preferences.SPConstant;
import com.workout.base.preferences.SharedPreferencesUtil;


public class VendingInstallReferrerReceiver extends BroadcastReceiver {


    public static final String ACTION_INSTALL_REFERRER = "com.android.vending.INSTALL_REFERRER";
    public static final String EXTRA_REFERRER = "referrer";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getAction() == null) {
            return;
        }
        try {
            SharedPreferencesUtil sp = new SharedPreferencesUtil(SPConstant.DEFAULT_SHAREPREFERENCES_FILE);
            sp.saveBoolean(SPConstant.IS_NEW_USER_FOR_PL, true);
            String campaign = intent.getStringExtra(VendingInstallReferrerReceiver.EXTRA_REFERRER);

            if (campaign == null) {
                campaign = "utm_source=google-play&utm_medium=organic";
            } else {
                campaign = "utm_source=adwords&utm_medium=banner" + campaign;
                campaign = campaign.replace("network", "key_channel");
                campaign = campaign.replace("gclid", "key_click_id");
                campaign = campaign + "&utm_campaign=YDAD66_BR_010719";
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void testActionInstallReferrer(Context context, String referrer) {
        Intent intent = new Intent(ACTION_INSTALL_REFERRER);
        intent.putExtra(EXTRA_REFERRER, referrer);
        new VendingInstallReferrerReceiver().onReceive(context, intent);
    }

    private String getUtmSource(String referrer) {
        if (!TextUtils.isEmpty(referrer)) {
            String[] splits = referrer.split("&");
            if (splits != null && splits.length >= 0) {
                for (String string : splits) {
                    if (string != null && string.contains("utm_source")) {
                        String[] utmSources = string.split("=");
                        if (utmSources != null && utmSources.length > 1) {
                            return utmSources[1];
                        }
                    }
                }
            }
        }
        return null;
    }
}
