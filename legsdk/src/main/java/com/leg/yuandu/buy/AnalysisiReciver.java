package com.leg.yuandu.buy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.leg.yuandu.user.PromoteUserManager;

public class AnalysisiReciver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {

        if (intent == null) {
            return;
        } else {
            String var5 = intent.getStringExtra("referrer");
            String var6 = intent.getAction();
            Bundle b = new Bundle();
            b.putString("gp_value", var5);
            if ("com.android.vending.INSTALL_REFERRER".equals(var6) && !TextUtils.isEmpty(var5)) {
                if (var5.contains("utm_source=google")) {
                    PromoteUserManager.getInstance(context).setPromoteUser(false, context);
                } else {
                    PromoteUserManager.getInstance(context).setPromoteUser(true, context);
                }
            }
        }
    }
}
