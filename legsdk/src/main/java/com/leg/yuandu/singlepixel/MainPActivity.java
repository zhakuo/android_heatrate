package com.leg.yuandu.singlepixel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import com.workout.base.BaseModuleContext;
import com.workout.base.SportObj;
import com.workout.base.incident.AOPNativeEvent;
import com.workout.base.incident.AOPSAVEEvent;
import com.workout.base.incident.APPChooseEvent;
import com.workout.base.service.HelloService;
import com.xdandroid.hellodaemon.singlepixel.ScreenManager;

import static com.leg.yuandu.YdAppApplication.getContext;
import static com.leg.yuandu.YdAppApplication.postDelay;


/**
 * 该Activity的View只要设置为1像素然后设置在Window对象上即可。在Activity的onDestroy周期中进行保活服务的存活判断从而唤醒服务。
 * 运行在:watch进程, 为了提高watch进程的优先级 oom_adj值越小，优先级越高。
 * copy from shihu wang
 */
public class MainPActivity extends Activity  {

    private static final String TAG = "snowdd";

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window mWindow = getWindow();
        mWindow.setGravity(Gravity.LEFT | Gravity.TOP);
        WindowManager.LayoutParams attrParams = mWindow.getAttributes();
        attrParams.x = 0;
        attrParams.y = 0;
        attrParams.height = 1;
        attrParams.width = 1;
        mWindow.setAttributes(attrParams);
        ScreenManager.getInstance(this).setSingleActivity(this);
        Log.d("snowdd", " 1 像素Activity onCreate " ) ;
//        ToastUtil.toast(this," 1 像素Activity onCreate " + SPConstant.isVolcanoHasOpen());
    }

    @Override
    protected void onDestroy() {
        try {
            HelloService.tellMeToStartService(this);
        }catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //如果锁屏打开了，则不请求广告
//        Log.i("snowdd", " 1 像素Activity --- isVolcanoHasOpen " + SPConstant.isVolcanoHasOpen()) ;

        if(isScreenOn()){
            if (SportObj.getAlObj() == null) {
                BaseModuleContext.postEvent(new AOPNativeEvent(this));
            }
            //解锁，出uac
            postDelay(new Runnable() {
                @Override
                public void run() {
                    Log.d("snowdd",  " 1 像素Activity --- 打开uac " ) ;
                    if (SportObj.getAlObj() != null) {
                        BaseModuleContext.postEvent(new APPChooseEvent(MainPActivity.this));

                    }

                    finishSelf();


                }
            },9000);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        Log.d("snowdd", "dispatchTouchEvent finishSelf");
        finishSelf();
        return super.dispatchTouchEvent(motionEvent);
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        Log.d("snowdd", "onTouchEvent finishSelf");
        finishSelf();
        return super.onTouchEvent(motionEvent);
    }


    public void finishSelf() {
        Log.d("snowdd", " 1 像素Activity !isFinishing() = " + !isFinishing() ) ;
//        if (!isFinishing()) {
            //Log.d("snowdd", " 1 像素Activity isFinishing " ) ;
//            finish();
//        }
        finish();
    }

    private boolean isScreenOn() {
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);

        Log.d("snowdd", " isScreenOn powerManager.isScreenOn() = "+  powerManager.isScreenOn()) ;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            Log.d("snowdd", " isScreenOn isInteractive = "+  powerManager.isInteractive()) ;
            return powerManager.isInteractive();
        } else {
            return powerManager.isScreenOn();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
