package com.leg.yuandu.utils;

import android.content.Context;
import android.os.Environment;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author snow
 * @date 16-5-13
 */
public class FileUtils {

    public final static String SDCARD = Environment
            .getExternalStorageDirectory().getPath();
    /**
     * 统一获取raw文件流中数据
     * @param context
     * @param rawId
     * @return
     */
    public static String getShortStrDataFromRaw(Context context, int rawId) {
        String strData = null;
        if (context == null) {
            return strData;
        }
        // 从资源获取流
        InputStream is = null;
        try {
            is = context.getResources().openRawResource(rawId);
            if (is != null) {
                byte[] buffer = new byte[128];
                int len = is.read(buffer); // 读取流内容
                if (len > 0) {
                    strData = new String(buffer, 0, len).trim(); // 生成字符串
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return strData;
    }
}
