package com.leg.yuandu.utils;

import android.text.TextUtils;

import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.adslibrary.YLog;
import com.workout.base.preferences.SPConstant;
import com.workout.base.preferences.SharedPreferencesUtil;


public class ABTest {

	private String mUser;
	
	private boolean mRebuild;

	private SharedPreferencesUtil mSp;
	
	private static ABTest sInstance;

	private int mVersion = 1;

	private final static String TAG = "CLABTextConfig";
	
	public ABTest() {
		mSp = new SharedPreferencesUtil(SPConstant.ABTEST_SP);
		init();
	}
	
	public static ABTest getInstance() {
		if (null == sInstance) {
			sInstance = new ABTest();
		}
		return sInstance;
	}
	
	private void init() {
		String user = mSp.getString(getUserKey(), "");
		if (TextUtils.isEmpty(user)) {
			if (mRebuild) {
				mSp.clear();
				mUser = null;
			}
		}
		mUser = user;
	}

	
	/**
	 * 获取当前测试用户
	 */
	public String getUser() {
		if (TextUtils.isEmpty(mUser)) {
			mUser = mSp.getString(getUserKey(), TestUser.USER_B);
		}
		return mUser;
	}


	private String getUserKey() {
		return mVersion + "_user_type";
	}


}