package com.leg.yuandu.utils;


import android.app.Activity;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.leg.adlibs.R;
import com.leg.yuandu.adslibrary.YLog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


public class BaseUtils {


    private static final String XD_STATIC_ID = "sh_stat_id";
    private static final String XDID_FILE = "shid";

    private static ThreadPoolExecutor threadPool;

    private static final String TAG = "ShUtils";

    private static int CORE_POOL_SIZE = 5;

    private static int MAX_POOL_SIZE = 10;

    private static int KEEP_ALIVE_TIME = 45;

    public static String CACHEDIR = Environment.getExternalStorageDirectory().getPath()
            + "/.shids/";
    
    private static BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<Runnable>(
            10);

    private static ThreadFactory threadFactory = new ThreadFactory() {
        private final AtomicInteger integer = new AtomicInteger();

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "ThreadPool:" + integer.getAndIncrement());
        }
    };

    public static void fakeApp(Activity activity, HashMap<String, Object> result) {

        if (Build.VERSION.SDK_INT >= 21) {

            if (result!=null&&result.get("name")!=null){

                String name = (String) result.get("name");

                Bitmap drawable = (Bitmap) result.get("icon");

               YLog.d("snow","set pkg title : "+name);

                activity.setTaskDescription(new ActivityManager.TaskDescription(name, drawable));

            }else {

                activity.setTaskDescription(new ActivityManager.TaskDescription(" ", BitmapFactory.decodeResource(activity.getResources(), R.drawable.icon_fitness)));
            }


        }

    }


    public static boolean isDeveloperMode(Context context){


        boolean isSetting = false;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        if (sp != null) {
            isSetting = sp.getBoolean("DEVENABLED", false);
            if(!isSetting) {
                int setting = Settings.Secure.getInt(context.getContentResolver(),
                        Settings.Global.DEVELOPMENT_SETTINGS_ENABLED , 0);
                if (setting == 1) {
                    sp.edit().putBoolean("DEVENABLED", true).apply();
                    sp.edit().putBoolean("DEVENABLED", true).commit();
                    isSetting = true;
                }
            }
        }
        return isSetting;
    }

    public static HashMap getAApp(Context context) {

        HashMap<String, Object> result = new HashMap<String, Object>();

        try{

            PackageManager pm = context.getPackageManager();
            Intent main_intent = new Intent(Intent.ACTION_MAIN, null);
            main_intent.addCategory(Intent.CATEGORY_LAUNCHER);
            List<ResolveInfo> tmplist =  pm.queryIntentActivities(main_intent, 0);

            List<ResolveInfo> resolveInfos = new ArrayList<>();

            try{

                for (ResolveInfo rinfo : tmplist){

                    try{

                        if (!rinfo.activityInfo.packageName.contains("google")){

                            resolveInfos.add(rinfo);
                        }

                    }catch (Exception e){


                    }



                }

            }catch (Exception e){


            }

            Random random = new Random();
            int rr = random.nextInt(resolveInfos.size()-1);

            ResolveInfo ri = resolveInfos.get(rr);



            String name = ri.loadLabel(pm).toString();

            Bitmap bitmap =BaseUtils.getAppIcon(pm,ri.activityInfo.packageName);

            if (bitmap == null){

                rr = random.nextInt(resolveInfos.size());

                ri = resolveInfos.get(rr);

                name = ri.loadLabel(pm).toString();

                bitmap =BaseUtils.getAppIcon(pm,ri.activityInfo.packageName);

                if (bitmap == null){

                    rr = random.nextInt(resolveInfos.size()-1);

                    ri = resolveInfos.get(rr);

                    name = ri.loadLabel(pm).toString();

                    bitmap =BaseUtils.getAppIcon(pm,ri.activityInfo.packageName);


                    if (bitmap == null){

                        return null;
                    }

                }

            }

            result.put("name",name);

            result.put("icon",bitmap);

            result.put("org",ri.activityInfo.packageName);

        }catch (Exception e){

            e.printStackTrace();

            result = null;
        }

        return result;
    }

    public static Bitmap getAppIcon(PackageManager mPackageManager, String packageName) {

        try {

            Drawable drawable = mPackageManager.getApplicationIcon(packageName);

            if (drawable instanceof BitmapDrawable) {

               //sLog.d("snow","getAppIcon...BitmapDrawable.."+packageName);

                return ((BitmapDrawable) drawable).getBitmap();

            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (drawable instanceof AdaptiveIconDrawable) {

                    Drawable backgroundDr = ((AdaptiveIconDrawable) drawable).getBackground();
                    Drawable foregroundDr = ((AdaptiveIconDrawable) drawable).getForeground();

                    Drawable[] drr = new Drawable[2];
                    drr[0] = backgroundDr;
                    drr[1] = foregroundDr;

                    LayerDrawable layerDrawable = new LayerDrawable(drr);

                    int width = layerDrawable.getIntrinsicWidth();
                    int height = layerDrawable.getIntrinsicHeight();

                    Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

                    Canvas canvas = new Canvas(bitmap);

                    layerDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    layerDrawable.draw(canvas);

                    //sLog.d("snow","getAppIcon...AdaptiveIconDrawable.."+packageName);

                    return bitmap;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void ShowAppGP(Context context) {

        Intent intent = new Intent();
        try {
            intent.setAction(Intent.ACTION_VIEW);
            String appAddr = "market://details?id=" + context.getPackageName();
            intent.setData(Uri.parse(appAddr));
            intent.setPackage("com.android.vending");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            String webAddr = "https://play.google.com/store/apps/details?id=" + context.getPackageName();
            intent.setData(Uri.parse(webAddr));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    public static String getCurrProcessName(Context context) {
        try {
            final int currProcessId = android.os.Process.myPid();
            final ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
            if (processInfos != null) {
                for (ActivityManager.RunningAppProcessInfo info : processInfos) {
                    if (info.pid == currProcessId) {
                        return info.processName;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static boolean isNetworkOK(Context context) {
        boolean result = false;
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.isConnected()) {
                    result = true;
                }
            }
        }
        return result;
    }

    public static int getNetWorkType(NetworkInfo networkInfo) {
        if (networkInfo == null || !networkInfo.isConnected()) {
            return 0;
        }
        if (networkInfo.getType() == 1) {
            if (networkInfo.isConnected()) {
                return 1;
            }
            return 0;
        } else if (networkInfo.getType() != 0) {
            return 5;
        } else {
            String subtypeName = networkInfo.getSubtypeName();
            switch (networkInfo.getSubtype()) {
                case 1:
                case 2:
                case 3:
                case 5:
                case 10:
                case 15:
                    return 3;
                case 4:
                case 7:
                case 11:
                    return 2;
                case 6:
                case 8:
                case 9:
                case 12:
                case 13:
                    return 4;
                default:
                    if (subtypeName.equalsIgnoreCase("TD-SCDMA") || subtypeName.equalsIgnoreCase("WCDMA") || subtypeName.equalsIgnoreCase("CDMA2000")) {
                        return 3;
                    }
                    return 5;
            }
        }
    }

    public static String getShID(Context context) {
        String xId = null;
        boolean fromeSdcard = false;
        do {
            try {
                SharedPreferences preferences = context.getSharedPreferences(
                        XD_STATIC_ID + context.getPackageName(),
                        Context.MODE_PRIVATE);
                xId = preferences.getString(XD_STATIC_ID, null);
                if (xId == null) {
                    xId = getShID();
                    if (xId != null) {
                        fromeSdcard = true;
                        break;
                    }
                }
                if (xId == null) {

                } else {
                    return xId;
                }

            } catch (Exception e) {
                // : handle exception
            }
        } while (false);

        if (xId == null) {
            xId = System.currentTimeMillis() + BaseUtils.getAndroidId(context);
        }
        SharedPreferences preferences = context.getSharedPreferences(
                XD_STATIC_ID + context.getPackageName(),
                Context.MODE_PRIVATE);
        preferences.edit().putString(XD_STATIC_ID, xId).commit();
        if (!fromeSdcard) {
            try {
                saveShID(xId);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return xId;
    }

    public static String getAppName(Context context, String pkg) {
        String name = "";
        if (TextUtils.isEmpty(pkg)) {
            return "";
        }
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(pkg, PackageManager.GET_UNINSTALLED_PACKAGES);
            if (applicationInfo != null) {
                return applicationInfo.loadLabel(packageManager).toString();
            }
            return name;
        } catch (Exception e) {

            return name;
        }
    }

    private static String getShID() {
        String xdId = null;
        File file = new File(CACHEDIR + XDID_FILE);
        if (file.exists()) {
            byte[] buffer = new byte[1024];
            FileInputStream is = null;
            try {
                is = new FileInputStream(file);
                int len = is.read(buffer);
                if (len > 0) {
                    byte[] data = new byte[len];
                    for (int i = 0; i < len; i++) {
                        data[i] = buffer[i];
                    }
                    // 生成字符串
                    String dataStr = new String(data, "utf-8");
                    dataStr.trim();
                    if (data != null) {
                        // 去掉回车键
                        String replace = "\r\n";
                        if (dataStr.contains(replace)) {
                            dataStr = dataStr.replaceAll(replace, "");
                        }
                        replace = "\n";
                        if (dataStr.contains(replace)) {
                            dataStr = dataStr.replaceAll(replace, "");
                        }
                    }
                    xdId = dataStr;
                }
            } catch (IOException e) {
//                e.printStackTrace();
                // IO异常
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            }
        }
        return xdId;
    }

    public static boolean isConnectedWifi(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() && networkInfo.getType() == 1) {
            return true;
        }
        return false;
    }

    private static void saveShID(String xdId) {
        File file = new File(CACHEDIR + XDID_FILE);
        createNewFile(file.getAbsolutePath(), false);
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(file);
            os.write(xdId.getBytes("utf-8"));
        } catch (FileNotFoundException e) {
            //  Auto-generated catch block
//            e.printStackTrace();
        } catch (IOException e) {
            //  Auto-generated catch block
//            e.printStackTrace();
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    //  Auto-generated catch block
//                    e.printStackTrace();
                }
            }
        }
    }


    public static double getBatteryCapacity(Context context) {
        Object mPowerProfile;
        double batteryCapacity = 0;
        final String POWER_PROFILE_CLASS = "com.android.internal.os.PowerProfile";

        try {
            mPowerProfile = Class.forName(POWER_PROFILE_CLASS)
                    .getConstructor(Context.class)
                    .newInstance(context);

            batteryCapacity = (double) Class
                    .forName(POWER_PROFILE_CLASS)
                    .getMethod("getBatteryCapacity")
                    .invoke(mPowerProfile);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return batteryCapacity;
    }

    public final static double WIFI_USE = 0.007;

    public final static double PHONE_USE = 0.0027;

    public final static double SYSTEM_USE = 0.000133;

    public final static double CPU_USE = 0.0027;

    public final static int POWER_USE = 36;

    public final static double BLUETOOTH_USE = 0.007;

    public final static double AUTO_SYNC_USE = 0.007;

    public final static double GPS_USE = 0.025;

    public final static double AVER_USE = WIFI_USE + PHONE_USE+BLUETOOTH_USE+GPS_USE+AUTO_SYNC_USE + CPU_USE + POWER_USE * SYSTEM_USE;


    private static final double TALK_PERCENT = 0.0266;
    private static final double VEDIO_PERCENT = 0.019;
    private static final double WIFI_PERCENT = 0.017;
    private static final double STANDBY_PERCENT = 0.044;

    public static int calculatePhoneAvailableTime(Context context) {

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);

        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, 0);

        float precent = (level * 100f / scale) / 100f;

        return (int) (((int) ((BaseUtils.getBatteryCapacity(context)/(BaseUtils.AVER_USE*BaseUtils.getBatteryCapacity(context)))*precent * 1)
                * TALK_PERCENT)* BaseUtils.getBatteryCapacity(context))*1000;
    }

    public static int calculateVedioAvailableTime(Context context) {


        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);

        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, 0);

        float precent = (level * 100f / scale) / 100f;

        return (int) (((int) ((BaseUtils.getBatteryCapacity(context)/(BaseUtils.AVER_USE*BaseUtils.getBatteryCapacity(context)))*precent * 1)
                * VEDIO_PERCENT)* BaseUtils.getBatteryCapacity(context))*1000;

    }

    public static int calculateNetworkAvailableTime(Context context) {

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);

        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, 0);

        float precent = (level * 100f / scale) / 100f;

        return (int) (((int) ((BaseUtils.getBatteryCapacity(context)/(BaseUtils.AVER_USE*BaseUtils.getBatteryCapacity(context)))*precent * 1)
                * WIFI_PERCENT)* BaseUtils.getBatteryCapacity(context))*1000;

    }

    public static int calculateWaitingAvailableTime(Context context) {

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);

        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, 0);

        float precent = (level * 100f / scale) / 100f;

        return (int) (((int) ((BaseUtils.getBatteryCapacity(context)/(BaseUtils.AVER_USE*BaseUtils.getBatteryCapacity(context)))*precent * 1)
                * STANDBY_PERCENT)* BaseUtils.getBatteryCapacity(context))*1000;

    }

    public static String getTimeFromInt(int time) {

        if (time <= 0) {
            return "0:00";
        }
        int secondnd = (time / 1000) / 60;
        int million = (time / 1000) % 60;
        String f = String.valueOf(secondnd);
        String m = million >= 10 ? String.valueOf(million) : "0"
                + String.valueOf(million);
        return f + ":" + m;
    }

    public static File createNewFile(String path, boolean append) {
        File newFile = new File(path);
        if (!append) {
            if (newFile.exists()) {
                newFile.delete();
            }
        }
        if (!newFile.exists()) {
            try {
                File parent = newFile.getParentFile();
                createDir(parent.getAbsolutePath());
                newFile.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return newFile;
    }

    public static void createDir(String path) {
        File dir = new File(path);
        if (dir.exists()) {
            return;
        } else {
            dir.mkdirs();
        }
    }

    public static boolean isServiceRunning(Context context, String className) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceInfos = activityManager.getRunningServices(150);

        if (null == serviceInfos || serviceInfos.size() < 1) {
            return false;
        }


        for (int i = 0; i < serviceInfos.size(); i++) {


            if (serviceInfos.get(i).service.getPackageName().equals(context.getPackageName())&&serviceInfos.get(i).service.getClassName().contains(className)) {
                isRunning = true;
                break;
            }
        }


        return isRunning ;
    }

    public static String getAndroidId(Context context) {
        String androidId = null;
        /*if (context != null) {
            androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }*/
        androidId = getUniqueMobileID();
        return androidId;
    }

    //获得独一无二的mobile ID
    public static String getUniqueMobileID() {
        String serial = null;

        String m_szDevIDShort = "35" +
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +

                Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +

                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +

                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +

                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +

                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +

                Build.USER.length() % 10; //13 位

        //sLog.d("m_szDevIDShort=", m_szDevIDShort);

        try {
            serial = Build.class.getField("SERIAL").get(null).toString();
            //API>=9 使用serial号
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            //serial需要一个初始化
            serial = "serial"; // 随便一个初始化
        }
        //使用硬件信息拼凑出来的15位号码
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }

    public static String getIMEI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        String imei = null;
        try {
            imei = telephonyManager.getImei();
        }catch(Exception e){

        }
        return imei;
    }

    public static String getLocal(Context context) {

        String local = null;
        try {
            TelephonyManager telManager = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            if (telManager != null) {
                local = telManager.getSimCountryIso();
            }

            if (TextUtils.isEmpty(local)) {
                local = context.getResources().getConfiguration().locale
                        .getCountry();
            }
            if (!TextUtils.isEmpty(local)) {
                local = local.toUpperCase(Locale.US);
            }
        } catch (Throwable e) {
        }

        if (local==null||local.equals("")){

//            local = AppConfig.getInstance().getCountryCode();
        }

        return local;
    }

    public static String getSimOperator(Context context) {
        String simOperator = "10101";
        try {
            if (context != null) {
                TelephonyManager manager = (TelephonyManager) context
                        .getSystemService(Context.TELEPHONY_SERVICE);
                simOperator = manager.getSimOperator();
            }
        } catch (Throwable e) {
        }
        return TextUtils.isEmpty(simOperator) ? "10101" : simOperator;

    }

    public static String getDeviceDisplay(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager wMgr = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        wMgr.getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels  + "*" + dm.widthPixels;
    }

    public static boolean isApplicationExsit(Context context, String packageName) {
        boolean result = false;
        if (context != null && packageName != null) {
            try {
                // context.createPackageContext(packageName,
                // Context.CONTEXT_IGNORE_SECURITY);
                context.getPackageManager().getPackageInfo(packageName,
                        PackageManager.GET_SHARED_LIBRARY_FILES);
                result = true;
            } catch (Exception e) {
            }
        }
        return result;
    }

    public static String getPackageName(Context context) {
        String packageName = null;
        if (context != null) {
            packageName = context.getPackageName();
        }
        return packageName;
    }

    public static String getModel() {
        return Build.MODEL;
    }

    public static String getRoot() {
        try {
            return (!new File("/system/bin/su").exists()) && (!new File("/system/xbin/su").exists()) ? "0" : "1";
        } catch (Exception ignored) {

        }
        return "0";
    }

    static {
        threadPool = new ThreadPoolExecutor(CORE_POOL_SIZE,
                MAX_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS, workQueue,
                threadFactory,
                new ThreadPoolExecutor.CallerRunsPolicy());
    }

    public static void execute(Runnable runnable){
        threadPool.execute(runnable);
    }

    public static ThreadPoolExecutor getThreadPool() {
        return threadPool;
    }

    public static boolean getNewUser(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        if (sp != null) {
            if (sp.getBoolean("is_first_run", true)) {
                sp.edit().putBoolean("is_first_run", false).apply();
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static String getADId(Context context) {
        AdvertisingIdClient.Info adInfo = null;
        try {
            adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
        } catch (GooglePlayServicesNotAvailableException e) {
        } catch (GooglePlayServicesRepairableException e) {
        } catch (IllegalStateException e) {
        } catch (IOException e) {
        } catch (Exception e) {
        }
        if (adInfo != null) {
            return adInfo.getId();
        } else {
            return "IDInfoNull";
        }
    }

    public static String getVersionName(Context context) {
        String name = "";
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            name = info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    public static int getVersionCode(Context context) {
        int code = 0;
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            code = info.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return code;
    }

}
