package com.leg.yuandu.utils;

import android.content.Context;
import android.os.Handler;

public class CoreConfig {
    static Context sContext;
    static int sCode;
    private static boolean sFront;
    private static boolean isJiyeWaiFront;
    static Handler sHandler = new Handler();
    public static void setContext(Context context){
        sContext = context;
    }

    public static Context getContext(){
        return sContext;
    }
    public static void setVerCode(int code){
        sCode = code;
    }
    public static int getVerCode(){
        return sCode;
    }

    public static void postDelay(Runnable runnable, long duration){
        sHandler.postDelayed(runnable, duration);
    }

    public static void setisJiyeFront(boolean isJiyeFront){
        sFront = isJiyeFront;
    }
    public static boolean isJiyeFront(){
        return sFront;
    }

    public static void setisJiyeWaiFront(boolean isJiyeFront){
        isJiyeWaiFront = isJiyeFront;
    }
    public static boolean isJiyeWaiFront(){
        return isJiyeWaiFront;
    }


}
