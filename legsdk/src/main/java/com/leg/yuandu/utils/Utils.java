package com.leg.yuandu.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.text.TextUtils;

import com.leg.adlibs.R;
import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.storage.PreferenceId;
import com.leg.yuandu.storage.PreferenceStorage;
import com.workout.base.BaseModulePreferenceId;
import com.workout.base.BaseModulePreferenceStorage;
import com.workout.base.GlobalConst;
import com.workout.base.TimeUtil;

import java.math.BigDecimal;

import androidx.core.app.ShareCompat;

public class Utils {

    public static int dp2pix(float dips) {
        return (int) (dips * YdAppApplication.getContext().getResources().getDisplayMetrics().density + 0.5f);
    }

    public static boolean openWorkoutInGooglePlay(Context context) {
        return Utils.openGooglePlay(context, GlobalConst.GOOGLE_PLAY_APP_PREFIX + context.getPackageName(),
                GlobalConst.GOOGLE_PLAY_PREFIX + context.getPackageName());
    }

    public static void shareWorkout(Activity activity, String app_name) {
        String str = app_name + ": " + (GlobalConst.GOOGLE_PLAY_PREFIX + activity.getPackageName());

        ShareCompat.IntentBuilder.from(activity)
                .setType("text/plain")
                .setText(str)
                .startChooser();
    }

    static public boolean openGooglePlay(Context context, String uriString, String urlString) {
        boolean isOk = false;
        if (!TextUtils.isEmpty(uriString)) {
            isOk = openActivitySafely(context, Intent.ACTION_VIEW, uriString, "com.android.vending");
            if (!isOk) {
                isOk = openActivitySafely(context, Intent.ACTION_VIEW, uriString, null);
            }
        }
        if (!isOk) {
            if (!TextUtils.isEmpty(uriString)) {
                isOk = openActivitySafely(context, Intent.ACTION_VIEW, urlString, null);
            }
        }
        return isOk;
    }

    public static boolean openActivitySafely(Context context, String action, String uri, String packageName) {
        boolean isOk = true;
        try {
            Uri uriData = Uri.parse(uri);
            final Intent intent = new Intent(action, uriData);
            if (!TextUtils.isEmpty(packageName)) {
                intent.setPackage(packageName);
            }
            if (!Activity.class.isInstance(context)) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            context.startActivity(intent);
        } catch (Throwable e) {
            e.printStackTrace();
            isOk = false;
        }
        return isOk;
    }

    public static void checkSetFirstInstallTime() {
        SharedPreferences sp = PreferenceStorage.getGlobalSp();
        if (sp.getLong(PreferenceId.FIRST_INSTALL_MILLIS, -1) < 0) {
            sp.edit().putLong(PreferenceId.FIRST_INSTALL_MILLIS, System.currentTimeMillis()).apply();
        }
    }

    public static void copyFirstInstallTimeToBaseModule() {
        SharedPreferences globalSp = PreferenceStorage.getGlobalSp();
        long installMillis = globalSp.getLong(PreferenceId.FIRST_INSTALL_MILLIS, -1);
        SharedPreferences baseSp = BaseModulePreferenceStorage.getBaseSp();
        long baseModuleFirstInstalledMillis = baseSp.getLong(BaseModulePreferenceId.APP_FIRST_INSTALL_MILLIS, -1);
        if (baseModuleFirstInstalledMillis < 0) {
            baseSp.edit().putLong(BaseModulePreferenceId.APP_FIRST_INSTALL_MILLIS, installMillis).apply();
        }

    }

    /**
     * 距初次安装应用的是否已经过了几天
     *
     * @param days
     */
    public static boolean isInstallDaysAfter(int days) {
        SharedPreferences sp = PreferenceStorage.getGlobalSp();
        long currentTime = System.currentTimeMillis();
        long firstInstallTime = sp.getLong(PreferenceId.FIRST_INSTALL_MILLIS, currentTime);
        return (currentTime - firstInstallTime) > days * TimeUtil.DAY_IN_MILLIS;
    }

    public static boolean isInstallDaysBefore(int days) {
        SharedPreferences sp = PreferenceStorage.getGlobalSp();
        long currentTime = System.currentTimeMillis();
        long firstInstallTime = sp.getLong(PreferenceId.FIRST_INSTALL_MILLIS, currentTime);
        return (currentTime - firstInstallTime) < days * TimeUtil.DAY_IN_MILLIS;
    }

    /**
     * 首次安装日期
     * @return millis
     */
    public static long getInstallMillis() {
        SharedPreferences sp = PreferenceStorage.getGlobalSp();
        return sp.getLong(PreferenceId.FIRST_INSTALL_MILLIS, System.currentTimeMillis());
    }

    public static boolean isNetworkOK(Context context) {
        boolean result = false;
        if (context != null) {
            ConnectivityManager cm = (ConnectivityManager) context.getApplicationContext()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                android.net.NetworkInfo networkInfo = cm.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.isConnected()) {
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * 获取屏幕宽度，不受设备物理方向影响
     *
     * @return
     */
    public static int getTrueScreenWidth() {
        int width = Resources.getSystem().getDisplayMetrics().widthPixels;
        int height = Resources.getSystem().getDisplayMetrics().heightPixels;
        return width < height ? width : height;
    }

    /**
     * Round to certain number of decimals
     *
     * @param d
     * @param decimalPlace
     * @return
     */
    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
}
