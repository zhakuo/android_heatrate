package com.leg.yuandu.utils;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.adslibrary.YLog;
import com.leg.yuandu.config.SystemConfig;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 区域判断的工具类
 *
 * @author snow
 */

public class RegionTool {

    private static String sCountry;
    private static Boolean sIsCn = null;
    private static Boolean sIsUs = null;
    private static Boolean sIsSg = null;
    private static Boolean sIsBr = null;
    private static Boolean sIsIn = null;
    private static Boolean sIsFr = null;
    private static Boolean sIsSa = null;
    private static Boolean sIsMy = null;
    private static Boolean sIsGb = null;
    private static Boolean sIsKr = null;
    private static Boolean sIsTh = null;
    private static Boolean sIsTw = null;

    /**
     * 取用户国家码
     * 优先根据sim卡状态判断
     * 若无法获取则取手机语言
     *
     * @return 国家码
     * @author Airu.ljw
     */
    public static String getSimCountry() {
        if (sCountry == null) {
            Context context = YdAppApplication.getContext();
            if (null != context) {
                TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                if (null != manager) {
                    boolean ready = TelephonyManager.SIM_STATE_READY == manager.getSimState();
                    String country = manager.getSimCountryIso();
                    if (!TextUtils.isEmpty(country)) {
                        country = country.toLowerCase();
                        Pattern regex = Pattern.compile("^[a-z]{2}$");
                        Matcher matcher = regex.matcher(country);
                        if (matcher.matches()) {
                            sCountry = country;
                            return sCountry;
                        } else {
                            sCountry = Locale.getDefault().getCountry().toLowerCase();
                            return sCountry;
                        }
                    } else {
                        if (ready) {
                            sCountry = Locale.getDefault().getCountry().toLowerCase();
                            return sCountry;
                        }
                    }
                }
            }
            return Locale.getDefault().getCountry().toLowerCase();
        }
        return sCountry;
    }

    /**
     * 根据Sim卡判断是否是中国用户，如果没有Sim卡，则根据手机语言及设定的时区判断
     *
     * @return
     */
    public static boolean isCnUser() {

        if (YLog.isD()) {

            return false;

        }

        if (sIsCn != null) {
            return sIsCn;
        }
        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();
//                Log.d("AdLimitConfig", "shoudShowAd() oper " + oper);
                if (!TextUtils.isEmpty(oper)) {
                    sIsCn = oper.startsWith("460");
                    return sIsCn;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsCn = "cn".equals(country);
                    return sIsCn;
                }
            }
        }

        boolean location = SystemConfig.getInstance().isCNLocation();

        if (location) {

            return true;
        }
        // 如果没有SIM卡的话simOperator为null，然后获取本地信息进行判断处理
        // 获取当前国家或地区，如果当前手机设置为简体中文-中国，则使用此方法返回CN
        String country = Locale.getDefault().getCountry().toLowerCase();
        if (!TextUtils.isEmpty(country) && "cn".equals(country)) {
            // 如果获取的国家信息是CN，则返回TRUE
            // 增加时区的判断，若语言是CN且时区为中国，才认为是中国用户 -- Airu.ljw
            String zone = TimeZone.getDefault().getDisplayName(Locale.ENGLISH);
            if (zone != null && zone.toUpperCase().contains("CHINA")) {
                return true;
            }
        }
        return false;
    }

    public static boolean isUSUser() {

        if (sIsUs != null) {
            if (sIsUs) {
                YLog.d("snow", "This is US user");
            }
            return sIsUs;
        }

        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();

//                YLog.d("snow", "get oper code ....." + oper);

                if (!TextUtils.isEmpty(oper)) {
                    sIsUs = oper.startsWith("310") || oper.startsWith("311") || oper.startsWith("312") || oper.startsWith("313") || oper.startsWith("314") || oper.startsWith("315")
                            || oper.startsWith("316");

                    YLog.d("snow", "get oper code ....sIsUs." + sIsUs);
                    return sIsUs;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsUs = "US".equalsIgnoreCase(country);
                    return sIsUs;
                }
            }
        }

//        String country = Locale.getDefault().getCountry().toLowerCase();
//        if (!TextUtils.isEmpty(country) && "US".equalsIgnoreCase(country)) {
//
//            return true;
//        }
//
//        if (SystemConfig.getInstance().getCountryCode().equalsIgnoreCase("US")){
//
//            return true;
//        }


        return false;
    }

    public static boolean isSgUser() {

        if (sIsSg != null) {
            if (sIsSg) {
                YLog.d("snow", "This is SG user");
            }
            return sIsSg;
        }

        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();
                if (!TextUtils.isEmpty(oper)) {
                    sIsSg = oper.startsWith("525");
                    return sIsSg;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsSg = "sg".equals(country);
                    return sIsSg;
                }
            }
        }


//        if (SystemConfig.getInstance().getCountryCode().equalsIgnoreCase("SG")){
//
//            return true;
//        }


        return false;
    }

    public static boolean isGbUser() {

        if (sIsGb != null) {
            if (sIsGb) {
                YLog.d("snow", "This is GB user");
            }
            return sIsGb;
        }

        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();
                if (!TextUtils.isEmpty(oper)) {
                    sIsGb = oper.startsWith("234");
                    return sIsGb;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsGb = "gb".equals(country);
                    return sIsGb;
                }
            }
        }

//        String country = Locale.getDefault().getCountry().toLowerCase();
//        if (!TextUtils.isEmpty(country) && "gb".equals(country)) {
//
//            return true;
//        }
//
//        if (SystemConfig.getInstance().getCountryCode().equalsIgnoreCase("GB")){
//
//            return true;
//        }


        return false;
    }

    /**
     * 获取系统的本地信息
     *
     * @return
     */
    public static Locale getLocale() {
        Locale locale = null;
        try {
            locale = YdAppApplication.getContext().getResources().getConfiguration().locale;
        } catch (Exception e) {
        }
        if (locale == null) {
            locale = Locale.getDefault();
        }
        return locale;
    }

    /**
     * 获取系统的“语言-国家”字符串
     *
     * @return
     */
    public static String getLocalString() {
        Locale locale = getLocale();
        String language = locale.getLanguage();
        if (language != null) {
            language = language.toLowerCase();
        }
        String country = locale.getCountry();
        if (country != null) {
            country.toLowerCase();
        }
        return String.format(Locale.US, "%s-%s", language, country);
    }

    /**
     * 获取系统的语言
     *
     * @return
     */
    public static String getLanguage() {
        Locale locale = getLocale();
        String language = locale.getLanguage();
        if (language != null) {
            language = language.toLowerCase();
        }
        return language;
    }

    /**
     * 获取系统的国家
     *
     * @return
     */
    public static String getCountry() {
        Locale locale = getLocale();
        String country = locale.getCountry();
        if (country != null) {
            country.toUpperCase();
        }
        return country;
    }

    /**
     * 是否是中文语言
     *
     * @return
     */
    public static boolean isZhLanguage() {
        return "zh".equals(getLanguage());
    }

    public static boolean isVpnConnected() {
        if (Build.VERSION.SDK_INT >= 9) {
            try {
                Enumeration<NetworkInterface> niList = NetworkInterface.getNetworkInterfaces();
                if (niList != null) {
                    Iterator it = Collections.list(niList).iterator();
                    while (it.hasNext()) {
                        NetworkInterface intf = (NetworkInterface) it.next();
                        if (intf.isUp() && intf.getInterfaceAddresses().size() != 0) {
                            if ("tun0".equals(intf.getName()) || "ppp0".equals(intf.getName())) {
                                return true;
                            }
                        }
                    }
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * 巴西
     *
     * @return
     */
    public static boolean isBRUser() {

        if (sIsBr != null) {
            if (sIsBr) {
                YLog.d("snow", "This is BR user");
            }
            return sIsBr;
        }

        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();
                if (!TextUtils.isEmpty(oper)) {
                    sIsBr = oper.startsWith("724");
                    return sIsBr;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsBr = "br".equals(country);
                    return sIsBr;
                }
            }
        }
        return false;
    }

    /**
     * 印度
     *
     * @return
     */
    public static boolean isINUser() {

        if (sIsIn != null) {
            if (sIsIn) {
                YLog.d("snow", "This is BR user");
            }
            return sIsIn;
        }

        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();
                if (!TextUtils.isEmpty(oper)) {
                    sIsIn = oper.startsWith("404") || oper.startsWith("405") || oper.startsWith("406");
                    return sIsIn;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsIn = "in".equals(country);
                    return sIsIn;
                }
            }
        }
        return false;
    }

    /**
     * 法国
     *
     * @return
     */
    public static boolean isFRUser() {

        if (sIsFr != null) {
            if (sIsFr) {
                YLog.d("snow", "This is BR user");
            }
            return sIsFr;
        }

        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();
                if (!TextUtils.isEmpty(oper)) {
                    sIsFr = oper.startsWith("208");
                    return sIsFr;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsFr = "in".equals(country);
                    return sIsFr;
                }
            }
        }
        return false;
    }

    public static boolean isSAUser() {

        if (sIsSa != null) {
            if (sIsSa) {
                YLog.d("snow", "This is BR user");
            }
            return sIsSa;
        }

        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();
                if (!TextUtils.isEmpty(oper)) {
                    sIsSa = oper.startsWith("420");
                    return sIsSa;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsSa = "in".equals(country);
                    return sIsSa;
                }
            }
        }
        return false;
    }

    /**
     * 马来西亚
     *
     * @return
     */
    public static boolean isMYUser() {

        if (sIsMy != null) {
            if (sIsMy) {
                YLog.d("snow", "This is BR user");
            }
            return sIsMy;
        }

        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();
                if (!TextUtils.isEmpty(oper)) {
                    sIsMy = oper.startsWith("502");
                    return sIsMy;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsMy = "my".equals(country);
                    return sIsMy;
                }
            }
        }
        return false;
    }

    /**
     * kr
     *
     * @return
     */
    public static boolean isKRUser() {

        if (sIsKr != null) {
            if (sIsKr) {
                YLog.d("snow", "This is KR user");
            }
            return sIsKr;
        }

        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();
                if (!TextUtils.isEmpty(oper)) {
                    sIsKr = oper.startsWith("82");
                    return sIsKr;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsKr = "kr".equals(country);
                    return sIsKr;
                }
            }
        }
        return false;
    }


    /**
     * th
     *
     * @return
     */
    public static boolean isTHUser() {

        if (sIsTh != null) {
            if (sIsTh) {
                YLog.d("snow", "This is TH user");
            }
            return sIsTh;
        }

        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();
                if (!TextUtils.isEmpty(oper)) {
                    sIsTh = oper.startsWith("66");
                    return sIsTh;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsTh = "th".equals(country);
                    return sIsTh;
                }
            }
        }
        return false;
    }

    /**
     * tw
     *
     * @return
     */
    public static boolean isTWUser() {

        if (sIsTw != null) {
            if (sIsTw) {
                YLog.d("snow", "This is TH user");
            }
            return sIsTw;
        }

        // 从系统服务上获取了当前网络的MCC(移动国家号)，进而确定所处的国家和地区
        Context context = YdAppApplication.getContext();
        if (null != context) {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (null != manager) {
                String oper = manager.getSimOperator();
                if (!TextUtils.isEmpty(oper)) {
                    sIsTw = oper.startsWith("886");
                    return sIsTw;
                }
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country) && country.length() == 2) {
                    sIsTw = "tw".equals(country);
                    return sIsTw;
                }
            }
        }
        return false;
    }

    public static String getLocal(Context context) {
        if (context == null) {
            return null;
        }
        String local = null;
        try {
            TelephonyManager telManager = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            if (telManager != null) {
                local = telManager.getSimCountryIso();
            }

            if (TextUtils.isEmpty(local)) {
                local = context.getResources().getConfiguration().locale
                        .getCountry();
            }
            if (!TextUtils.isEmpty(local)) {
                local = local.toUpperCase(Locale.US);
            }
        } catch (Throwable e) {
        }

        return local;
    }
}
