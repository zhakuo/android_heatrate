package com.leg.yuandu.utils;

import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


public class TimeUtil {
    public static final long SECOND_IN_MILLIS = 1000L;
    public static final long MINUTE_IN_MILLIS = SECOND_IN_MILLIS * 60;
    public static final long HOUR_IN_MILLIS = MINUTE_IN_MILLIS * 60;
    public static final long DAY_IN_MILLIS = HOUR_IN_MILLIS * 24;

    public static long dayFrom1970(long millis) {
        return (millis + TimeZone.getDefault().getRawOffset()) / TimeUtil.DAY_IN_MILLIS;
    }


    static public String getMillisToTimeStr(long millis) {
        String result;
        long h = TimeUnit.MILLISECONDS.toHours(millis);
        long m = TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1);
        long s = TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1);
        if (h != 0) {
            result = String.format(Locale.US, "%dh %dm %ds", h, m, s);
        } else {
            result = String.format(Locale.US, "%dm %ds", m, s);
        }
        return result;
    }
}
