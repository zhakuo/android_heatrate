package com.leg.yuandu.utils;

import com.leg.yuandu.greendao.LangOffilineDao;
import com.leg.yuandu.model.LangOffiline;

import java.util.List;

public class DBManager {

    public static void addSolveData(long time, String expr, String result,int type) {
        try {
            LangOffiline calData = new LangOffiline(time, expr, result, type);
            SQliteManager.getInstance().getDaoSession().getLangOffilineDao().insertOrReplace(calData);
        }catch (Exception e){

        }
    }

    public static void addSolveData(LangOffiline calData) {

        SQliteManager.getInstance().getDaoSession().getLangOffilineDao().insertOrReplace(calData);
    }

    public static LangOffiline getSolveDataById(String time) {
        return SQliteManager.getInstance().getDaoSession().getLangOffilineDao().queryBuilder()
                .where(LangOffilineDao.Properties.Expr.eq(time))
                .build()
                .unique();
    }

    public static List<LangOffiline> getAllSolveData() {
        return SQliteManager.getInstance().getDaoSession().getLangOffilineDao()
                .queryBuilder()
                .orderAsc(LangOffilineDao.Properties.Time)
                .list();
    }





    public static void delSolveData(LangOffiline calHistory) {
        SQliteManager.getInstance().getDaoSession().getLangOffilineDao().delete(calHistory);
    }

    public static LangOffiline getSolveDataByDel(String time) {
        return SQliteManager.getInstance().getDaoSession().getLangOffilineDao().queryBuilder()
                .where(LangOffilineDao.Properties.Expr.eq(time),LangOffilineDao.Properties.Type.eq(1))
                .build()
                .unique();
    }
}
