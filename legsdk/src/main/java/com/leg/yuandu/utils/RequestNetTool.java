package com.leg.yuandu.utils;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Base64;

import com.leg.yuandu.adslibrary.AdVariable;
import com.leg.yuandu.adslibrary.StoreChannel;
import com.leg.yuandu.adslibrary.YLog;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class RequestNetTool {

    private static RequestNetTool mInstance;
    private ExecutorService mPoolService;

    private ExecutorService mInValidService;

    private ThreadFactory mThreadFactory;

    private MyHandler sUIHandler;

    private RequestNetTool(){
        mThreadFactory = new ThreadFactory() {

            private final AtomicInteger mCount = new AtomicInteger(1);

            public Thread newThread(Runnable r) {
                return new Thread(r, "RequestNetTool Runnable #" + mCount.getAndIncrement());
            }
        };
        mPoolService = Executors.newCachedThreadPool(mThreadFactory);
        mInValidService = Executors.newCachedThreadPool(mThreadFactory);
        sUIHandler = new MyHandler(Looper.getMainLooper());
    }

    public synchronized static RequestNetTool getInstance(){
        if(mInstance == null){
            mInstance = new RequestNetTool();
        }
        return mInstance;
    }


    private static class MyHandler extends Handler {

        public MyHandler(Looper looper){
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    }
}
