package com.leg.yuandu.utils;

import io.michaelrocks.paranoid.Obfuscate;

@Obfuscate
public class AppConfig {


    public static String ce_config = "1,3,70,1,5,1,9";//1.开关 2.次数 3.安装后开启时间（小时）4.间隔（分钟）5. back键比例 6.关闭后重新开启时间（天） 7.关闭展示全屏比例

    public static String ins_config = "ins_config"; //1.开关 2.次数 3.安装后开启时间（小时）4.间隔（分钟）
    public static String ins_org_config = "ins_org_config"; //自然用户：1.开关 2.次数 3.安装后开启时间（小时）4.间隔（分钟）

    public static String int_id_config = "int_id";
    public static String max_id_config = "max_id";
    public static String topOnAppID = "topOnAppID";
    public static String topOnAppKey = "topOnAppKey";


    private AppConfig() {
    }

}
