package com.leg.yuandu.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.leg.yuandu.greendao.DaoMaster;

/**
 * Created by snow on 06/03/2019.
 */

public class UpgradeHelper extends DaoMaster.OpenHelper {

    public UpgradeHelper(Context context, String name) {
        super(context, name);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.d("snow", "Upgrading schema from version " + oldVersion + " to " + newVersion + " by migrating all tables data");

    }

}
