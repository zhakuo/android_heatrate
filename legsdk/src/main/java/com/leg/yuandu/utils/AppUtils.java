package com.leg.yuandu.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.leg.adlibs.BuildConfig;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class AppUtils {
    public static final String pkg = BuildConfig.LIBRARY_PACKAGE_NAME;

    private static String sChannel;

    public static String getpkg(){
        return pkg;
    }

    public static boolean pgck(Context context) {
        if (context.getPackageName().equals(pkg)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean hasSimCard(Context context) {
        TelephonyManager telMgr = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        boolean result = true;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                result = false; // 没有SIM卡
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                result = false;
                break;
        }
        return result;
    }


    private final static FilenameFilter FILENAME_FILTER_NUMS = new FilenameFilter() {

        /**
         * 匹配模式，只要数字
         */
        private Pattern mPattern = Pattern.compile("^[0-9]+$");

        @Override
        public boolean accept(File dir, String filename) {
            return mPattern.matcher(filename).matches();
        }
    };


    /**
     * 获取版本名
     *
     * @return
     */
    public static String getVersionName(Context context) {
        String name = "";
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            name = info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    /**
     * 获取版本号
     *
     * @param context
     * @return
     */
    public static int getVersionCode(Context context) {
        int code = 0;
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            code = info.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return code;
    }

    /**
     * 获取渠道
     */
    public static String getChannel(Context context) {
        if (TextUtils.isEmpty(sChannel)) {
            sChannel = getMyChannel(context);
        }
        return sChannel;
    }

    /**
     * 读取渠道
     */
    public static String getMyChannel(Context context) {
        final String start_flag = "META-INF/channel_";
        String ret = "";
        if (context != null) {
            ApplicationInfo appinfo = context.getApplicationInfo();
            String sourceDir = appinfo.sourceDir;
            ZipFile zipfile = null;
            try {
                zipfile = new ZipFile(sourceDir);
                Enumeration<?> entries = zipfile.entries();
                while (entries.hasMoreElements()) {
                    ZipEntry entry = (ZipEntry) entries.nextElement();
                    String entryName = entry.getName();
                    if (entryName.contains(start_flag)) {
                        ret = entryName.replace(start_flag, "");
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (zipfile != null) {
                    try {
                        zipfile.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return ret.equals("") ? "200" : ret;
    }
}
