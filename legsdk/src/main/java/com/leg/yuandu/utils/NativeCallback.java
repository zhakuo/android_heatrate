package com.leg.yuandu.utils;

public interface NativeCallback {

    void onAdLoad(Object ad);
    void onAdOpen(Object ad);
    void onAdShow(Object ad);
    void onAdClose(Object ad);
    void onAdClicked(Object ad);
    void onAdFail(String error);

}
