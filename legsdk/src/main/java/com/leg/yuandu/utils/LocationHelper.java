package com.leg.yuandu.utils;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.leg.yuandu.adslibrary.YLog;
import com.leg.yuandu.config.SystemConfig;

import org.json.JSONObject;

/**
 * 地理位置帮助类
 *
 * @author snow
 *
 */
public class LocationHelper {
    private static final String TAG = "LocationHelper";
    private Context mContext;
    private LocationManager mLocationManager = null;
    private MyLocationListener [] locationListeners = null;

    public LocationHelper(Context context) {
        this.mContext = context;
        mLocationManager = (LocationManager)mContext.getSystemService(Context.LOCATION_SERVICE);
    }

    public synchronized Location getLocation() {
        // returns null if not available
        if( locationListeners == null )
            return null;
        // location listeners should be stored in order best to worst
        for(int i=0;i<locationListeners.length;i++) {
            Location location = locationListeners[i].getLocation();
            if( location != null )
                return location;
        }

        try{

            Location networkLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (networkLocation!=null){

                return networkLocation;
            }

        }catch (Exception e){

        }


        try{

            Location gpsLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (gpsLocation!=null){

                return gpsLocation;
            }

        }catch (Exception e){

        }


        return null;
    }

    public boolean testHasReceivedLocation() {
        if( locationListeners == null )
            return false;
        for(int i=0;i<locationListeners.length;i++) {
            if( locationListeners[i].test_has_received_location )
                return true;
        }
        return false;
    }

    public synchronized void setupLocationListener() {
        if (YLog.isD())
            YLog.d(TAG, "setupLocationListener");
        // Define a listener that responds to location updates
        // we only set it up if store_location is true, to avoid unnecessarily wasting battery
//        boolean store_location = SettingsManager.getPreferenceLocation();
        if( locationListeners == null ) {
            locationListeners = new MyLocationListener[2];
            locationListeners[0] = new MyLocationListener();
            locationListeners[1] = new MyLocationListener();

            // location listeners should be stored in order best to worst
            // also see https://sourceforge.net/p/opencamera/tickets/1/ - need to check provider is available
            if( mLocationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) ) {
                try {
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locationListeners[1]);
                } catch (Throwable tr) {
//                    Log.e(TAG, "", tr);
                }
            }
            if( mLocationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) ) {
                try {
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListeners[0]);
                } catch (Throwable tr) {
//                    Log.e(TAG, "", tr);
                }
            }
        }
    }

    public synchronized void freeLocationListeners() {
        if (YLog.isD())
            YLog.d(TAG, "freeLocationListeners");
        if( locationListeners != null ) {
            for(int i=0;i<locationListeners.length;i++) {
                try {
                    mLocationManager.removeUpdates(locationListeners[i]);
                } catch (Throwable tr) {
                    Log.e(TAG, "", tr);
                }
                locationListeners[i] = null;
            }
            locationListeners = null;
        }
    }

    public boolean hasLocationListeners() {
        if( this.locationListeners == null )
            return false;
        if( this.locationListeners.length != 2 )
            return false;
        for(int i=0;i<this.locationListeners.length;i++) {
            if( this.locationListeners[i] == null )
                return false;
        }
        return true;
    }

    private class MyLocationListener implements LocationListener {
        private Location location = null;
        public boolean test_has_received_location = false;

        Location getLocation() {
            return location;
        }

        public void onLocationChanged(Location location) {
            if (YLog.isD())
                YLog.d(TAG, "onLocationChanged");
            this.test_has_received_location = true;
            // Android camera source claims we need to check lat/long != 0.0d
            if( location.getLatitude() != 0.0d || location.getLongitude() != 0.0d ) {
                if (YLog.isD()) {
                    YLog.d(TAG, "received location:");
                    YLog.d(TAG, "lat " + location.getLatitude() + " long " + location.getLongitude() + " accuracy " + location.getAccuracy());
                }
                this.location = location;
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch( status ) {
                case LocationProvider.OUT_OF_SERVICE:
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                {
                    if (YLog.isD()) {
                        if( status == LocationProvider.OUT_OF_SERVICE )
                            YLog.d(TAG, "location provider out of service");
                        else if( status == LocationProvider.TEMPORARILY_UNAVAILABLE )
                            YLog.d(TAG, "location provider temporarily unavailable");
                    }
                    this.location = null;
                    this.test_has_received_location = false;
                    break;
                }
            }
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
            if (YLog.isD())
                YLog.d(TAG, "onProviderDisabled");
            this.location = null;
            this.test_has_received_location = false;
        }
    }

    public static boolean isInCN(double latitue,double longitude){


        boolean area1 = LocationHelper.isInArea(latitue,longitude,49.220400f,42.889900f,079.446200f,096.330000f);

        boolean area2 = LocationHelper.isInArea(latitue,longitude,54.141500f,39.374200f,109.687200f,135.000200f);

        boolean area3 = LocationHelper.isInArea(latitue,longitude,42.889900f,29.529700f,073.124600f,124.143255f);

        boolean area4 = LocationHelper.isInArea(latitue,longitude,29.529700f,26.718600f,082.968400f,097.035200f);

        boolean area5 = LocationHelper.isInArea(latitue,longitude,29.529700f,20.414096f,097.025300f,124.367395f);

        boolean area6 = LocationHelper.isInArea(latitue,longitude,20.414096f,17.871542f,107.975793f,111.744104f);

        if (area1 || area2 || area3 || area4 || area5 || area6){


            boolean excludeArea1 = LocationHelper.isInArea(latitue,longitude,25.398623f,21.785006f,119.921265f,122.497559f);

            boolean excludeArea2 = LocationHelper.isInArea(latitue,longitude,22.284000f,20.098800f,101.865200f,106.665000f);

            boolean excludeArea3 = LocationHelper.isInArea(latitue,longitude,21.542200f,20.487800f,106.452500f,108.051000f);

            boolean excludeArea4 = LocationHelper.isInArea(latitue,longitude,55.817500f,50.325700f,109.032300f,119.127000f);

            boolean excludeArea5 = LocationHelper.isInArea(latitue,longitude,55.817500f,49.557400f,127.456800f,137.022700f);

            boolean excludeArea6 = LocationHelper.isInArea(latitue,longitude,44.892200f,42.569200f,131.266200f,137.022700f);


            if (excludeArea1 || excludeArea2 || excludeArea3 || excludeArea4 || excludeArea5 || excludeArea6){

                return false;
            }


            return true;
        }




        return false;
    }

    public static boolean isInArea(double latitue,double longitude,double areaLatitude1,double areaLatitude2,double areaLongitude1,double areaLongitude2){
        if(isInRange(latitue, areaLatitude1, areaLatitude2)){//如果在纬度的范围内
            if(areaLongitude1*areaLongitude2>0){//如果都在东半球或者都在西半球
                if(isInRange(longitude, areaLongitude1, areaLongitude2)){
                    return true;
                }else {
                    return false;
                }
            }else {//如果一个在东半球，一个在西半球
                if(Math.abs(areaLongitude1)+ Math.abs(areaLongitude2)<180){//如果跨越0度经线在半圆的范围内
                    if(isInRange(longitude, areaLongitude1, areaLongitude2)){
                        return true;
                    }else {
                        return false;
                    }
                }else{//如果跨越180度经线在半圆范围内
                    double left = Math.max(areaLongitude1, areaLongitude2);//东半球的经度范围left-180
                    double right = Math.min(areaLongitude1, areaLongitude2);//西半球的经度范围right-（-180）
                    if(isInRange(longitude, left, 180)||isInRange(longitude, 0, right)){
                        return true;
                    }else {
                        return false;
                    }
                }
            }
        }else{
            return false;
        }
    }

    public static boolean isInRange(double point, double left,double right){
        if(point>= Math.min(left, right)&&point<= Math.max(left, right)){
            return true;
        }else {
            return false;
        }

    }

    public static  boolean isLocationEnable (final Context context) {
        LocationManager locationManager
                = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        // 通过GPS卫星定位，定位级别可以精确到街（通过24颗卫星定位，在室外和空旷的地方定位准确、速度快）
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // 通过WLAN或移动网络(3G/2G)确定的位置（也称作AGPS，辅助GPS定位。主要用于在室内或遮盖物（建筑群或茂密的深林等）密集的地方定位）
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network) {
            return true;
        }

        return false;
    }

    public static void requestIPLocation (Context context) {


        try {

            final RequestQueue requestQueue= Volley.newRequestQueue(context);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ip-api.com/json/",
                    new Response.Listener<String>(){

                        @Override
                        public void onResponse(String bs) {

                            if (bs!=null&&!bs.equals("")){

                                try{

                                    JSONObject jsonObject=new JSONObject(bs);

                                    if (jsonObject!=null){

                                        if (jsonObject.getString("status").equals("success")){

                                            Double lat = jsonObject.getDouble("lat");
                                            Double lon = jsonObject.getDouble("lon");

                                            if (lat!=null&&lon!=null){

                                                SystemConfig.getInstance().saveLocation(lat,lon);
                                            }


                                            String country = jsonObject.getString("country");

                                            if (country!=null&&!country.equals("")){

                                                SystemConfig.getInstance().saveCountryCode(country);
                                            }

                                        }
                                    }


                                }catch (Exception e){

                                }



                            }


                        }
                    }, new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError volleyError) {
//                    int statusCode = volleyError.networkResponse.statusCode;
//                    String msg = volleyError.getMessage();
//                    YLog.d(TAG, "uploadAdData onErrorResponse =" + msg + " statusCode=" + statusCode);

                    requestQueue.stop();
                }
            }){

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30 * 1000, 1, 1.0f));
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
