package com.leg.yuandu.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


public class HttpProtocolUtil {
    private static String sChannel;
    public static final String DATA_PROTOCOL_DIVIDER = "||";

    /**
     * 获取包名
     */
    public static String getPackageName(Context context) {
        String packageName = null;
        if (context != null) {
            packageName = context.getPackageName();
        }
        return packageName;
    }


    /**
     * android id
     */
    public static String getAndroidId(Context context) {
        String androidId = null;
        if (context != null) {
            androidId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }
        return androidId;
    }

    /**
     * 日志打印时间
     */
    public static String getLogPrintTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(new Date());
    }

    /**
     * os 用户手机操作系统的版本信息，如android-2.3.7
     */
    public static String getOS() {
        return Build.VERSION.RELEASE;
    }

    /**
     * 用户手机型号，如GT-I9000
     */
    public static String getModel() {
        return Build.MODEL;
    }

    /**
     * 获取国家
     */
    public static String getCountry(Context context) {
        String ret = null;

        try {
            TelephonyManager telManager = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            if (telManager != null) {
                ret = telManager.getSimCountryIso().toUpperCase();
            }
        } catch (Throwable e) {
            //             e.printStackTrace();
        }
        if (ret == null || ret.equals("")) {
            ret = Locale.getDefault().getCountry();
        }
        return ret;
    }

    /**
     * 获取渠道
     */
    public static String getChannel(Context context) {
        if (TextUtils.isEmpty(sChannel)) {
            sChannel = getMyChannel(context);
        }
        return sChannel;
    }

    /**
     * 读取渠道
     */
    public static String getMyChannel(Context context) {
        final String start_flag = "META-INF/channel_";
        String ret = "";
        if (context != null) {
            ApplicationInfo appinfo = context.getApplicationInfo();
            String sourceDir = appinfo.sourceDir;
            ZipFile zipfile = null;
            try {
                zipfile = new ZipFile(sourceDir);
                Enumeration<?> entries = zipfile.entries();
                while (entries.hasMoreElements()) {
                    ZipEntry entry = (ZipEntry) entries.nextElement();
                    String entryName = entry.getName();
                    if (entryName.contains(start_flag)) {
                        ret = entryName.replace(start_flag, "");
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (zipfile != null) {
                    try {
                        zipfile.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return ret.equals("") ? "200" : ret;
    }


    /**
     * 获取运营商
     */
    public static String getOperator(Context mContext) {
        String operator = "";
        try {
            TelephonyManager telManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            operator = telManager.getSimOperator();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return operator;
    }


    public static boolean getNewUser(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        if (sp != null) {
            if (sp.getBoolean("is_first_run", true)) {
                sp.edit().putBoolean("is_first_run", false).apply();
                return true;
            } else {
                return false;
            }
        }
        return false;
    }


    private static final String DEFAULT_RANDOM_DEVICE_ID = "0000000000000000";

    private static final String SHARED_PREFERENCES_DEVICE_ID = "device_id";                                                                        // 存储IMEI号的sharedPreference名称

    private static final String SHARED_PREFERENCES_RANDOM_ID = "random_id";
    public static String getVirtualIMEI(Context context) {
        //如果已保存IMEI或者存在旧的虚拟IMEI号则直接使用。
        String imei = getDeviceIdFromSharedPreference(context);
        // 获取手机的IMEI，并保存下来
        if (context != null) {
            if (null == imei || imei.equals(DEFAULT_RANDOM_DEVICE_ID)) {
                try {
                    imei = getIMEI(context);
                    saveDeviceIdToSharedPreference(context, Long.valueOf(imei));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return imei;
    }

    private static String getIMEI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = null;

        try{
            deviceId = telephonyManager.getDeviceId();
        }catch (Exception e){

        }
        return deviceId;
    }

    /**
     * 获取随机生成的IMEI的方法
     */
    private static String getDeviceIdFromSharedPreference(Context context) {
        if (context == null) {
            return null;
        }

        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARED_PREFERENCES_DEVICE_ID, Context.MODE_PRIVATE);
        return sharedPreferences.getString(SHARED_PREFERENCES_RANDOM_ID, DEFAULT_RANDOM_DEVICE_ID);
    }

    /**
     * 保存随机生成的IMEI的方法
     */
    private static void saveDeviceIdToSharedPreference(Context context, long deviceId) {
        if (context == null) {
            return;
        }

        SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARED_PREFERENCES_DEVICE_ID, Context.MODE_PRIVATE);
        String device = String.valueOf(deviceId);
        sharedPreferences.edit().putString(SHARED_PREFERENCES_RANDOM_ID, device).apply();
    }

}

