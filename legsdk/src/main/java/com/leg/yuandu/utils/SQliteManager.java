package com.leg.yuandu.utils;

import android.util.Log;

import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.greendao.DaoMaster;
import com.leg.yuandu.greendao.DaoSession;

import org.greenrobot.greendao.database.Database;


public class SQliteManager {
    public static final String DB_NAME = "xingqi_db";

    private static SQliteManager instance;
    private static DaoSession mDaoSession;

    private SQliteManager() {
        try {
            UpgradeHelper helper = new UpgradeHelper(YdAppApplication.getContext(), DB_NAME);
            Database database = helper.getWritableDb();
            DaoMaster daoMaster = new DaoMaster(database);
            mDaoSession = daoMaster.newSession();
        }catch (Exception e){
            Log.d("greenDAO", "SQliteManager error " + e);

        }
    }

    public static SQliteManager getInstance() {
        if (instance == null) {
            synchronized (SQliteManager.class) {
                if (instance == null) {
                    instance = new SQliteManager();
                }
            }
        }
        return instance;
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }


}
