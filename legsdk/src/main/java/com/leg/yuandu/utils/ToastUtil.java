package com.leg.yuandu.utils;

import android.content.Context;
import android.widget.Toast;

import com.workout.base.system.SafeToast;

public class ToastUtil {
    public static  void toast(Context context, String log){
        SafeToast.makeText(context, log, Toast.LENGTH_SHORT).show();
    }
}
