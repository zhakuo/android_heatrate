package com.leg.yuandu.promote;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;

import com.kochava.base.AttributionUpdateListener;
import com.kochava.base.Tracker;
import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.config.SystemConfig;
import com.leg.yuandu.user.PromoteUserManager;
import com.workout.volcano.logic.spec.BuyUserLogic;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by snow on 13/07/2019.
 */

public class KochavarHelper {
    
    public static void initTracker(){
        try {
            Tracker.configure(new Tracker.Configuration(YdAppApplication.getContext())
                            .setAppGuid("koheartrate-android-plnf8669")
                            .setAttributionUpdateListener(new AttributionUpdateListener() {
                                @Override
                                public void onAttributionUpdated(@NonNull String attribution) {
                                    try {
                                        String channel = "unattributed";
                                        String KEY_TRACKER_LAST_UPLOAD_DATA = "KEY_TRACKER_LAST_UPLOAD_DATA";

//                                    Log.d("snow", "Install is not attributed.");
                                        if (attribution != null && !attribution.equals("")) {


                                            JSONObject jsonObject = null;
                                            try {
                                                jsonObject = new JSONObject(attribution);


                                                String attributionCallback = null;

                                                try {

                                                    attributionCallback = jsonObject.optString("attribution");

                                                } catch (Exception e) {

                                                }

//                                            Log.d("snow","get attributionCallback   "+attributionCallback);

                                                if (attributionCallback != null && attributionCallback.equals("false")) {


                                                    PromoteUserManager.getInstance().setPromoteUser(false);
                                                    BuyUserLogic.setPromoteUser(false);
                                                } else {


//                                        String agencyNetworkId = jsonObject.optString("agency_network_id"); // identify the agency
//                                        String  agency = jsonObject.optString("agency");// agency name

                                                    String networkId = jsonObject.optString("network_id");
                                                    String tracker = jsonObject.optString("tracker");
                                                    String campaignStr = jsonObject.optString("campaign_name");
                                                    String siteStr = jsonObject.optString("campaign_group_name");
                                                    String creative = jsonObject.optString("adgroup_name");

                                                    String original_request = jsonObject.optString("original_request");

                                                    if (networkId.equals("65") || networkId.contains("2909")) {

                                                        StringBuilder sb = new StringBuilder("utm_source=fb&utm_medium=banner");
                                                        sb.append("&utm_campaign=");
                                                        sb.append(siteStr);
                                                        sb.append("&key_channel=");
                                                        sb.append(campaignStr + "-kochava");
                                                        sb.append("&key_click_id=");
                                                        sb.append(creative);

                                                        PromoteUserManager.getInstance().savePromoteUserChannel("fb");
                                                        BuyUserLogic.setPromoteUser(true);

                                                    } else if (networkId.equals("90") || networkId.equals("5465")) {
                                                        JSONObject originObject = new JSONObject(original_request);
                                                        JSONObject adEvents = originObject.optJSONArray("ad_events") != null ? originObject.optJSONArray("ad_events").getJSONObject(0) : null;
                                                        if (adEvents != null) {
                                                            siteStr = adEvents.optString("campaign_name");
                                                        } else {
                                                            siteStr = jsonObject.optString("campaignid");
                                                        }

                                                        PromoteUserManager.getInstance().savePromoteUserChannel("adwords");
                                                        BuyUserLogic.setPromoteUser(true);
                                                        channel = "adwords";

                                                    } else if (networkId.equals("7117") ) {
                                                        try {

                                                            PromoteUserManager.getInstance().savePromoteUserChannel("adwords");
                                                            BuyUserLogic.setPromoteUser(true);
                                                            channel = "tiktok";
                                                        }catch (Exception e){
                                                            e.printStackTrace();
                                                        }

                                                    }

                                                    PromoteUserManager.getInstance().savePromoteUserChannel("adwords");
                                                    BuyUserLogic.setPromoteUser(true);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        } else {

                                            PromoteUserManager.getInstance().setPromoteUser(false);
                                            BuyUserLogic.setPromoteUser(false);
                                        }


                                    } catch (Exception exception) {
                                        exception.printStackTrace();
                                    }
                                }
                            })
            );
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private static String strToJson(String str) throws JSONException {
        str = str.replace("=","\":\"");
        str = str.replace("&","\",\"");
        str = "{\"" + str +"\"}";
        System.out.println(str);

        return str;
    }

}
