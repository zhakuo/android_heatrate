package com.leg.yuandu.countaction;



import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.adslibrary.YLog;
import com.leg.yuandu.config.SystemConfig;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 需要注意
 * 1、不同产品需要修改OperationCode、getOperationCode、getFunctionId，定义好code以及对应的functionid。
 * 2、不同产品fb广告数不一样，fbCount 要对应
 */
public class CountActionManage implements Serializable {

    private static CountActionManage mInstance;

    private static final int fbCount = 5;

    /**
     * 获取单例
     * @return
     */
    public synchronized static CountActionManage getInstance(){
        if(mInstance == null){
            mInstance = new CountActionManage();
        }

        return mInstance;
    }


    public synchronized void uploadStatByPerMin(){
        int day = getEntrance();
        ArrayList<CountActionBean> result = new ArrayList<CountActionBean>();

        result = CountActionDBHelper.getInstance(YdAppApplication.getContext()).getActionAdByDay(day);
        if(result.size() >= fbCount){
            insertVisiteFb( CountActionManage.OperationCode.visit_fb.toString(), CountActionManage.OperationCode.visit_fb, day);
        }

        result = CountActionDBHelper.getInstance(YdAppApplication.getContext()).getActionByFunction(day);



        /*for(CountActionBean ab : result){
            String key = ab.getOperation();
            String value = ab.getOperationCode();
            UploadOperationBean UploadOperationBean = new UploadOperationBean(YdAppApplication.getContext(), key, value, day);
            UploadOperationBean.setmFunctionId(ab.getFunctionId() + "");
            UploadOperationBean.setmOpertResult(ab.getOperationResult());
            YdUploadTools.statOperaClick(YdAppApplication.getContext(), UploadOperationBean);

            //
            OperationCode code = getOperationCode(value);
            if(code != null) {
                insertOrUpdate(key, code, true, true, day);
            }
            YLog.d("snow", "actionBean " + ab.toString());
        }

        if(result.size() > 0){
            YdUploadTools.uploadOperationDataImediately(YdAppApplication.getContext());
        }*/

    }


    public synchronized void uploadStatByPerDay(){
        int day = getYesterdayEntrance();
        ArrayList<CountActionBean> result = new ArrayList<CountActionBean>();

        result = CountActionDBHelper.getInstance(YdAppApplication.getContext()).getActionAdByDay(day);
        if(result.size() >= fbCount){

            insertVisiteFb( CountActionManage.OperationCode.visit_fb.toString(), CountActionManage.OperationCode.visit_fb, day);
        }

        result = CountActionDBHelper.getInstance(YdAppApplication.getContext()).getActionByDay(day);



        /*for(CountActionBean ab : result){
            String key = ab.getOperation();
            String value = ab.getOperationCode();
            UploadOperationBean UploadOperationBean = new UploadOperationBean(YdAppApplication.getContext(), key, value, day);
            UploadOperationBean.setmFunctionId(ab.getFunctionId() + "");
            UploadOperationBean.setmOpertResult(ab.getOperationResult());
            YdUploadTools.statOperaClick(YdAppApplication.getContext(), UploadOperationBean);

            //
            OperationCode code = getOperationCode(value);
            if(code != null) {
                insertOrUpdate(key, code, true, true, day);
            }
            YLog.d("snow", "actionBean " + ab.toString());
        }

        YdUploadTools.uploadOperationDataImediately(YdAppApplication.getContext());*/
    }

    /**
     * use for insert data when finish upload stat.
     * @param operation
     * @param operationCode
     * @param isAutoincrement
     * @param isUpload
     */
    public synchronized void insertOrUpdate(String operation, OperationCode operationCode, boolean isAutoincrement, boolean isUpload, int datetype){

        CountActionBean bean = new CountActionBean();
        if(datetype == 7){
            return;
        }
        int functionId = getFunctionId(operationCode);
        if(functionId == 1){
            return;
        }

        bean.setFunctionId(functionId);
        bean.setDateType(datetype);
        bean.setOperation(operation);
        bean.setOperationCode(operationCode.toString());
        CountActionDBHelper.getInstance(YdAppApplication.getContext()).insertOrUpdate(bean, isAutoincrement, isUpload);

        return ;
    }

    /**
     *  use for default insert data
     * @param operation
     * @param operationCode
     * @param isAutoincrement
     */
    public synchronized void insertOrUpdate(String operation, OperationCode operationCode, boolean isAutoincrement){
        CountActionBean bean = new CountActionBean();
        int datetype = getEntrance();
        if(datetype == 7){
            return;
        }
        int functionId = getFunctionId(operationCode);
        if(functionId == 1){
            return;
        }

        bean.setFunctionId(functionId);
        bean.setDateType(datetype);
        bean.setOperation(operation);
        bean.setOperationCode(operationCode.toString());
        CountActionDBHelper.getInstance(YdAppApplication.getContext()).insertOrUpdate(bean, isAutoincrement, false);

        return ;
    }

    /**
     *  use for default visited fb
     * @param operation
     * @param operationCode
     */
    public synchronized void insertVisiteFb(String operation, OperationCode operationCode, int datetype){
        CountActionBean bean = new CountActionBean();
        if(datetype == 7){
            return;
        }
        int functionId = getFunctionId(operationCode);
        if(functionId == 1){
            return;
        }

        bean.setFunctionId(functionId);
        bean.setDateType(getEntrance());
        bean.setOperation(operation);
        bean.setOperationCode(operationCode.toString());
        bean.setOperationResult("1");
        CountActionDBHelper.getInstance(YdAppApplication.getContext()).insertAction(bean);

        return ;
    }




    /**
     *  use for default insert ad action data
     * @param operation ad pos
     * @param operationCode ad type , such as fb / admob
     */
    public synchronized void insertOrUpdateAdAction(String operation, Adtype operationCode){
        CountActionBean bean = new CountActionBean();
        int datetype = getEntrance();
        if(datetype == 7){
            return;
        }

        bean.setDateType(getEntrance());
        bean.setOperation(operation);
        bean.setOperationCode(operationCode.toString());
        CountActionDBHelper.getInstance(YdAppApplication.getContext()).insertOrUpdateAdAction(bean);

        return ;
    }

    public synchronized void getActionAdByDay(){
        int day = getEntrance();
        ArrayList<CountActionBean> result = new ArrayList<CountActionBean>();
        result = CountActionDBHelper.getInstance(YdAppApplication.getContext()).getActionAdByDay(day);
        for(CountActionBean ab : result){
            YLog.d("snow", "actionBean " + ab.toString());
        }
    }



    // 如果添加code，需要制定对应的function id，也就是下一个方法
    public static enum OperationCode {
        //用户行为 functionId 2
        equal,
        oncreate,
        //广告监控 functionId 3
        visit_fb,
        //政策组监控 functionId 4
        setting_fb,
        ad_click,
        health_fb,
        news_fb
    }

    private OperationCode getOperationCode(String code){
        if(OperationCode.equal.toString().equals(code)){
            return OperationCode.equal;
        }else if(OperationCode.oncreate.toString().equals(code)){
            return OperationCode.oncreate;
        }else if(OperationCode.visit_fb.toString().equals(code)){
            return OperationCode.visit_fb;
        }else if(OperationCode.setting_fb.toString().equals(code)){
            return OperationCode.setting_fb;
        }else if(OperationCode.ad_click.toString().equals(code)){
            return OperationCode.ad_click;
        }else if(OperationCode.health_fb.toString().equals(code)){
            return OperationCode.health_fb;
        }else if(OperationCode.news_fb.toString().equals(code)){
            return OperationCode.news_fb;
        }
        return null;
    }

    public int getFunctionId(OperationCode operationCode) {
        int functionId = 1;
        switch (operationCode) {
            case equal:
                functionId = 2;
                break;
            case oncreate:
                functionId = 2;
                break;
            case visit_fb:
                functionId = 3;
                break;
            case setting_fb:
                functionId = 4;
                break;
            case ad_click:
                functionId = 4;
                break;
            case health_fb:
                functionId = 4;
                break;
            case news_fb:
                functionId = 4;
                break;
            default: break;
        }
        return functionId;
    }

    // 广告类型
    public static enum Adtype {
        Fb,
        Admob
    }


    public static enum Operation {
        //对应上面的统计code
        app,
        eaual,
        //广告对象写广告的id
    }


    private int getEntrance(){
        long days = (SystemConfig.getInstance().getWebCurrentTime() - YdAppApplication.getStartTime()) / (24 * 60 * 60 *1000l);

        switch ((int)days){
            case 0 : return 0 ;
            case 1 : return 1 ;
            case 2 : return 2 ;
            case 3 : return 3 ;
            case 4 : return 4 ;
            case 5 : return 5 ;
            case 6 : return 6 ;
            default: return 7 ;
        }
    }

    private int getYesterdayEntrance(){
        long days = (SystemConfig.getInstance().getWebCurrentTime() - YdAppApplication.getStartTime()) / (24 * 60 * 60 *1000l);

        int day = 0;
        if(days <= 0){
            day = -1;
        }else if(days > 0){
            day = (int)days - 1;
        }
        switch (day){
            case 0 : return 0 ;
            case 1 : return 1 ;
            case 2 : return 2 ;
            case 3 : return 3 ;
            case 4 : return 4 ;
            case 5 : return 5 ;
            case 6 : return 6 ;
            default: return 7 ;
        }
    }
}
