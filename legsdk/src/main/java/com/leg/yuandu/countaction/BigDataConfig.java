package com.leg.yuandu.countaction;

import android.content.Context;

import com.leg.yuandu.YdAppApplication;
import com.workout.base.preferences.SPConstant;
import com.workout.base.preferences.SharedPreferencesUtil;

/**
 * Created by admin on 2016/12/29.
 */

public class BigDataConfig {
    private Context mContext;
    private static BigDataConfig sInstance;
    private static Object sLock = new Object();

    private int mAdDeepValue = 1 ;

    private final static String TAG = "BigDataConfig";


    private BigDataConfig(Context context) {
        if (context == null) {
            context = YdAppApplication.getContext();
        }
        mContext = context.getApplicationContext();
    }

    public static BigDataConfig getsInstance(Context context) {
        if (sInstance == null) {
            synchronized (sLock) {
                if (sInstance == null) {
                    sInstance = new BigDataConfig(context);
                }
            }
        }

        return sInstance;
    }

    public int getDeepValue() {

        SharedPreferencesUtil sp = new SharedPreferencesUtil(SPConstant.DEFAULT_SHAREPREFERENCES_FILE);

        return sp.getInt(SPConstant.AD_DEEP_VALUE, 1);
    }

}
