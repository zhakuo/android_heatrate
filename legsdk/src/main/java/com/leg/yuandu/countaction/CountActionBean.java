package com.leg.yuandu.countaction;


import java.io.Serializable;

public class CountActionBean implements Serializable {

    private static final long serialVersionUID = 435412132132L;

    protected int functionId;
    protected int dateType;
    protected String operation;
    protected String operationCode;
    protected String operationResult;

    public int getFunctionId() {
        return functionId;
    }

    public void setFunctionId(int functionId) {
        this.functionId = functionId;
    }

    public int getDateType() {
        return dateType;
    }

    public void setDateType(int dateType) {
        this.dateType = dateType;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getOperationResult() {
        return operationResult;
    }

    public void setOperationResult(String operationResult) {
        this.operationResult = operationResult;
    }

    public static CountActionBean create(int mfunctionId, int mdateType, String moperation, String moperationCode, String moperationResult){
        CountActionBean bean = new CountActionBean();
        bean.setFunctionId(mfunctionId);
        bean.setDateType(mdateType);
        bean.setOperation(moperation);
        bean.setOperationCode(moperationCode);
        bean.setOperationResult(moperationResult);
        return bean;
    }


    @Override
    public String toString() {
        return functionId + " " + dateType + " " + operation + "  " + operationCode + "  " + operationResult;
    }
}
