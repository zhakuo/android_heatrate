package com.leg.yuandu.countaction;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

//import com.leg.yuandu.adslibrary.YLog;
//import com.leg.yuandu.steps.annotation.Update;
//import com.leg.yuandu.steps.table.StepTable;

import java.util.ArrayList;


public class CountActionDBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "action_data_1.db"; // 数据库名称
    public static final int VERSION = 1; // 数据库版本 1


    private static CountActionDBHelper mInstance;

    private SQLiteDatabase mDatabase;

    //action table
    public static final String TABLE_ACTION = "action_db";

    public static final String TABLE_ACTION_AD = "action_ad_db";


    //自增的ID 主键
    public static final String COL_ID = "_id";

    public static final String COL_FUNCTIONID = "mfunction_id";
    public static final String COL_DATETYPE = "mdate_type";
    public static final String COL_OPERATION = "moperation";
    public static final String COL_OPERATION_CODE = "moperation_code";
    public static final String COL_OPERATION_RESULT = "moperation_result";
    public static final String COL_IS_UPLOAD = "mis_upload";

    



    public static final String mCreateTableSQL1 = "create table if not exists " + TABLE_ACTION + "(" + COL_ID +
            " Integer primary key autoincrement, " + COL_FUNCTIONID + " Integer, "
            + COL_DATETYPE + " VARCHAR(5), "+ COL_OPERATION + " VARCHAR(50), " + COL_OPERATION_CODE + " VARCHAR(50), "
            + COL_OPERATION_RESULT + " VARCHAR(50), " + COL_IS_UPLOAD + " Integer )  ";

    public static final String mCreateTableSQL2 = "create table if not exists " + TABLE_ACTION_AD + "(" + COL_ID +
            " Integer primary key autoincrement, "
            + COL_DATETYPE + " VARCHAR(5), "+ COL_OPERATION + " VARCHAR(50), " + COL_OPERATION_CODE + " VARCHAR(50), "
            + COL_OPERATION_RESULT + " VARCHAR(50)) " ;



    private CountActionDBHelper(Context context){
        super(context, DB_NAME, null, VERSION);
    }

    /**
     * 获取单例
     * @param context
     * @return
     */
    public synchronized static CountActionDBHelper getInstance(Context context){
        if(mInstance == null){
            mInstance = new CountActionDBHelper(context);
        }

        return mInstance;
    }

    @Override
    public  void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL(mCreateTableSQL1);
            db.execSQL(mCreateTableSQL2);
//            db.execSQL(StepTable.SQL_CREATE_TABLE);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    /**
     * 插入初始化的数据
     */
    private void init(SQLiteDatabase db){

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /*@Update(version = 2)
    private void version2(SQLiteDatabase sqLiteDatabase) {
//        sqLiteDatabase.execSQL(StepTable.SQL_CREATE_TABLE);
//        YLog.d("steps" + "  sql= " + StepTable.SQL_CREATE_TABLE);
    }*/

    /**
     * 获取数据库
     * @return SQLiteDatabase
     */
    public synchronized SQLiteDatabase getDatabase(){
        if(mDatabase == null || !mDatabase.isOpen()){
            mDatabase = getWritableDatabase();
        }
        return mDatabase;
    }
    /**
     * 关闭数据库
     */
    public synchronized void closeDatabase(){
        if(mDatabase != null && mDatabase.isOpen()){
            mDatabase.close();
        }
    }


    public synchronized ArrayList<CountActionBean> getActionByDay(int day){
        Cursor c = null;
        ArrayList<CountActionBean> result = new ArrayList<CountActionBean>();
        try{
            c = getDatabase().query(TABLE_ACTION, new String[]{COL_FUNCTIONID, COL_DATETYPE, COL_OPERATION, COL_OPERATION_CODE, COL_OPERATION_RESULT},
                    COL_DATETYPE + " = ? and " + COL_IS_UPLOAD + " = 0 " , new String[]{ day + ""}, null, null, null);
            if(c.moveToNext()) {

                int functionId = c.getColumnIndex(COL_FUNCTIONID);
                int dateIndex = c.getColumnIndex(COL_DATETYPE);
                int operationIndex = c.getColumnIndex(COL_OPERATION);
                int operationCodeIndex = c.getColumnIndex(COL_OPERATION_CODE);
                int operationResultIndex = c.getColumnIndex(COL_OPERATION_RESULT);
                do {
                    result.add(CountActionBean.create(c.getInt(functionId), c.getInt(dateIndex), c.getString(operationIndex),
                            c.getString(operationCodeIndex), c.getString(operationResultIndex) ));
                } while (c.moveToNext());
            }
        } catch (Throwable e){

        } finally {
            closeDatabase();
            if(c != null){
                c.close();
            }

        }

        return result;
    }

    public synchronized ArrayList<CountActionBean> getActionByFunction(int day){
        Cursor c = null;
        ArrayList<CountActionBean> result = new ArrayList<CountActionBean>();
        try{
            c = getDatabase().query(TABLE_ACTION, new String[]{COL_FUNCTIONID, COL_DATETYPE, COL_OPERATION, COL_OPERATION_CODE, COL_OPERATION_RESULT},
                    COL_DATETYPE + " = ? and " + COL_IS_UPLOAD + " = 0 and " +  COL_FUNCTIONID + " = 4 ", new String[]{ day +"" }, null, null, null);
            if(c.moveToNext()) {

                int functionId = c.getColumnIndex(COL_FUNCTIONID);
                int dateIndex = c.getColumnIndex(COL_DATETYPE);
                int operationIndex = c.getColumnIndex(COL_OPERATION);
                int operationCodeIndex = c.getColumnIndex(COL_OPERATION_CODE);
                int operationResultIndex = c.getColumnIndex(COL_OPERATION_RESULT);
                do {
                    result.add(CountActionBean.create(c.getInt(functionId), c.getInt(dateIndex) , c.getString(operationIndex),
                            c.getString(operationCodeIndex), c.getString(operationResultIndex) ));
                } while (c.moveToNext());
            }
        } catch (Throwable e){

        } finally {
            closeDatabase();
            if(c != null){
                c.close();
            }

        }

        return result;
    }

    /**
     *
     * @param bean action bean , include action information
     * @param isAutoincrement decide operation result auto increment 1
     * @param isUpload if action data is upload,
     */
    public synchronized void insertOrUpdate(CountActionBean bean, boolean isAutoincrement, boolean isUpload){
        Cursor c = null;
        try{
            c = getDatabase().query(TABLE_ACTION, new String[]{COL_ID, COL_OPERATION_RESULT}, COL_FUNCTIONID + " = ? and " + COL_DATETYPE + " = ? and " +
                            COL_OPERATION + " = ? and " + COL_OPERATION_CODE + " = ? "
                    , new String[]{String.valueOf(bean.getFunctionId()), bean.getDateType() + "", bean.getOperation(), bean.getOperationCode()}, null, null, null);

            if(c.moveToNext()) {//update
                ContentValues contentValues = new ContentValues();
                contentValues.put(COL_FUNCTIONID, bean.getFunctionId());
                contentValues.put(COL_DATETYPE, bean.getDateType());
                contentValues.put(COL_OPERATION, bean.getOperation());
                contentValues.put(COL_OPERATION_CODE, bean.getOperationCode());
                int operationResultIndex = c.getColumnIndex(COL_OPERATION_RESULT);
                String operationResult = c.getString(operationResultIndex);
                if(isAutoincrement){
                    int result = Integer.parseInt(operationResult);
                    bean.setOperationResult(Integer.toString(result + 1));
                }
                contentValues.put(COL_OPERATION_RESULT, bean.getOperationResult());
                contentValues.put(COL_IS_UPLOAD, isUpload);


                getDatabase().update(TABLE_ACTION, contentValues, COL_FUNCTIONID + " = ? and " + COL_DATETYPE + " = ? and " +
                        COL_OPERATION + " = ? and " + COL_OPERATION_CODE + " = ? ", new String[]{String.valueOf(bean.getFunctionId()), bean.getDateType() + "", bean.getOperation(), bean.getOperationCode()});
            } else{//insert

                ContentValues contentValues = new ContentValues();
                contentValues.put(COL_FUNCTIONID, bean.getFunctionId());
                contentValues.put(COL_DATETYPE, bean.getDateType());
                contentValues.put(COL_OPERATION, bean.getOperation());
                contentValues.put(COL_OPERATION_CODE, bean.getOperationCode());
                if(isAutoincrement) {
                    contentValues.put(COL_OPERATION_RESULT, 1);
                }else{
                    contentValues.put(COL_OPERATION_RESULT, bean.getOperationResult());
                }
                contentValues.put(COL_IS_UPLOAD, 0);


                getDatabase().insert(TABLE_ACTION, COL_ID, contentValues);
            }
        } catch (Throwable e){
            e.printStackTrace();
        } finally {
            closeDatabase();
            if(c != null){
                c.close();
            }

        }
    }

    /**
     *
     * @param bean action bean , include action information
     */
    public synchronized void insertAction(CountActionBean bean){
        Cursor c = null;
        try{
            c = getDatabase().query(TABLE_ACTION, new String[]{COL_ID},  COL_DATETYPE + " = ? and " +
                            COL_OPERATION + " = ? and " + COL_OPERATION_CODE + " = ? "
                    , new String[]{bean.getDateType() + "", bean.getOperation(), bean.getOperationCode()}, null, null, null);

            if(!c.moveToNext()) {//insert

                ContentValues contentValues = new ContentValues();
                contentValues.put(COL_FUNCTIONID, bean.getFunctionId());
                contentValues.put(COL_DATETYPE, bean.getDateType());
                contentValues.put(COL_OPERATION, bean.getOperation());
                contentValues.put(COL_OPERATION_CODE, bean.getOperationCode());
                contentValues.put(COL_OPERATION_RESULT, bean.getOperationResult());
                contentValues.put(COL_IS_UPLOAD, 0);


                getDatabase().insert(TABLE_ACTION, COL_ID, contentValues);
            }
        } catch (Throwable e){
            e.printStackTrace();
        } finally {
            closeDatabase();
            if(c != null){
                c.close();
            }

        }
    }

    /**
     *
     * @param bean action bean , include action information
     */
    public synchronized void insertOrUpdateAdAction(CountActionBean bean){
        Cursor c = null;
        try{
            c = getDatabase().query(TABLE_ACTION_AD, new String[]{COL_ID},  COL_DATETYPE + " = ? and " +
                            COL_OPERATION + " = ? and " + COL_OPERATION_CODE + " = ? "
                    , new String[]{bean.getDateType() + "", bean.getOperation(), bean.getOperationCode()}, null, null, null);

            if(!c.moveToNext()) {//insert

                ContentValues contentValues = new ContentValues();
                contentValues.put(COL_DATETYPE, bean.getDateType());
                contentValues.put(COL_OPERATION, bean.getOperation());
                contentValues.put(COL_OPERATION_CODE, bean.getOperationCode());
                contentValues.put(COL_OPERATION_RESULT, 1);

                getDatabase().insert(TABLE_ACTION_AD, COL_ID, contentValues);
            }
        } catch (Throwable e){
            e.printStackTrace();
        } finally {
            closeDatabase();
            if(c != null){
                c.close();
            }

        }
    }

    public synchronized ArrayList<CountActionBean> getActionAdByDay(int day){
        Cursor c = null;
        ArrayList<CountActionBean> result = new ArrayList<CountActionBean>();
        try{
            c = getDatabase().query(TABLE_ACTION_AD, new String[]{ COL_DATETYPE, COL_OPERATION, COL_OPERATION_CODE, COL_OPERATION_RESULT},
                    COL_DATETYPE + " = ? " , new String[]{ day + ""}, null, null, null);
            if(c.moveToNext()) {
                int dateIndex = c.getColumnIndex(COL_DATETYPE);
                int operationIndex = c.getColumnIndex(COL_OPERATION);
                int operationCodeIndex = c.getColumnIndex(COL_OPERATION_CODE);
                int operationResultIndex = c.getColumnIndex(COL_OPERATION_RESULT);
                do {
                    result.add(CountActionBean.create(0, c.getInt(dateIndex), c.getString(operationIndex),
                            c.getString(operationCodeIndex), c.getString(operationResultIndex) ));
                } while (c.moveToNext());
            }
        } catch (Throwable e){

        } finally {
            closeDatabase();
            if(c != null){
                c.close();
            }

        }

        return result;
    }


    public synchronized void deleteAction(String operation, String operationCode){
        try{

            getDatabase().delete(TABLE_ACTION, COL_OPERATION + " = ? and " + COL_OPERATION_CODE + " = ? ", new String[]{operation, operationCode});

        } catch (Throwable e){

        } finally {
            closeDatabase();
        }
    }


}
