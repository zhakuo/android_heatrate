package com.leg.yuandu.countaction;


import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.format.DateUtils;

import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.adslibrary.YLog;

import java.io.Serializable;
import java.util.Date;


public class CountActionTimesHelper implements Serializable {

    private static CountActionTimesHelper mInstance;

    private static SharedPreferences sharedPreferences;

    /**
     * 获取单例
     * @return
     */
    public synchronized static CountActionTimesHelper getInstance(){
        if(mInstance == null){
            mInstance = new CountActionTimesHelper();
            YLog.d("snow", "CountActionTimesHelper() new CountActionTimesHelper" );
        }

        if(sharedPreferences == null){
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(YdAppApplication.getContext());
            YLog.d("snow", "CountActionTimesHelper() new sharedPreferences" );
        }
        return mInstance;
    }

    public synchronized  static void init(){
        final long currentTime = System.currentTimeMillis();
        YLog.d("snow", "CountActionTimesHelper() init time =" + new Date(currentTime ) );
        sharedPreferences.edit().putLong(CountActionScheduleService.TARGET_TIME_LIVE, currentTime + DateUtils.MINUTE_IN_MILLIS * 10).apply();
        YLog.d("snow", "CountActionTimesHelper() init =" );
        for(int i = 2; i < 20; i++){
            sharedPreferences.edit().putInt(CountActionScheduleService.TARGET_TIME_AD + i, 0).apply();
        }
    }

    public synchronized  static int getCount(int position){
        final long currentTime = System.currentTimeMillis();
        long liveTime = sharedPreferences.getLong(CountActionScheduleService.TARGET_TIME_LIVE, 0);
        int organicTimes = 0;
        YLog.d("snow", "CountActionTimesHelper() liveTime =" + new Date(liveTime ) + " 5 min=" + new Date(currentTime ));
        if(liveTime > currentTime ){
            YLog.d("snow", "CountActionTimesHelper() getCount liveTime " +new Date(liveTime ) + " current=" +  new Date(currentTime ));
            organicTimes = sharedPreferences.getInt(CountActionScheduleService.TARGET_TIME_AD + position, 1);
        }else{
            sharedPreferences.edit().putInt(CountActionScheduleService.TARGET_TIME_AD + position, 0).apply();
        }
        YLog.d("snow", "CountActionTimesHelper() getCount position =" +position + " organicTimes=" + organicTimes);
        return organicTimes;
    }

    public synchronized  static void addCount(int position){
        int organicTimes = sharedPreferences.getInt(CountActionScheduleService.TARGET_TIME_AD + position, 1);
        sharedPreferences.edit().putInt(CountActionScheduleService.TARGET_TIME_AD + position, organicTimes + 1).apply();
        YLog.d("snow", "CountActionTimesHelper() addCount position =" +position + " organicTimes=" + (organicTimes + 1));
    }





}
