package com.leg.yuandu.countaction;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.leg.yuandu.adslibrary.YLog;


public class CountActionScheduleService extends IntentService {

    public static final String TARGET_TIME_FIRST = "UPLOAD_ACTION_PROTOCOL_TARGET_FIRST";
    public static final String TARGET_TIME_ORGANIC = "UPLOAD_ACTION_PROTOCOL_TARGET_TIME_ORGANIC";
    public static final String TARGET_TIME_LIVE = "UPLOAD_ACTION_PROTOCOL_TARGET_TIME_LIVE";
    public static final String TARGET_TIME_AD = "UPLOAD_ACTION_PROTOCOL_TARGET_TIME_AD";


    private static int maxtimes = 10;

    public static void startScheduleService(Context context) {
        try {
            Intent intent = new Intent(context, CountActionScheduleService.class);
            YLog.d("scheduleupload", "action maxtimes = " + maxtimes + "   ");
            context.startService(intent);
        }catch (Exception e){

        }
    }

    public static void resetTimes(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putInt(TARGET_TIME_ORGANIC, 1).apply();
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public CountActionScheduleService() {
        super(CountActionScheduleService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final long currentTime = System.currentTimeMillis();

        if (getTargetTime() < currentTime) {
            // 静态数据上传
            uploadStaticProtocol();
//            setTargetTime(currentTime);
        }

        /*NormalScheduleTask scheduleTask = new NormalScheduleTask(getApplicationContext());
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int organicTimes = sharedPreferences.getInt(TARGET_TIME_ORGANIC, 1);
        YLog.d("scheduleupload" , "action1 organicTimes = " + organicTimes ) ;
        if(organicTimes <= maxtimes){
            YLog.d("scheduleupload" , "action1 organicTimes < maxtimes  = " ) ;
            scheduleTask.startScheduleDelayTime(new Intent(this, CountActionScheduleService.class), currentTime, StatConstants.TIME_1_MINUTE * 2); //
        }else {
            if(intent != null) {
                YLog.d("scheduleupload" , "cancel scheduleTask  " ) ;
                scheduleTask.cancelSchedule(intent, NormalScheduleTask.SCHEDULE_REQUEST_CODE);
            }
        }*/
    }

    /**
     * 上传静态协议统计数据
     */
    private void uploadStaticProtocol() {
        // 定时数据上传
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int organicTimes = sharedPreferences.getInt(TARGET_TIME_ORGANIC, 1);
        YLog.d("scheduleupload" , "action uploadStaticProtocol organicTimes = " + organicTimes ) ;
        if(organicTimes < maxtimes) {
            CountActionManage.getInstance().uploadStatByPerMin();
        }/*else{
            CountActionManage.getInstance().uploadStatByPerDay();
        }*/
    }

    /*private void setTargetTime(long systemTime) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sharedPreferences != null) {
            long targetTime = systemTime + StatConstants.TIME_8_HOUR;
            int organicTimes = sharedPreferences.getInt(TARGET_TIME_ORGANIC, 1);
            if(organicTimes <= maxtimes ){ //2min circle, half day

                targetTime = systemTime +   2 * StatConstants.TIME_1_MINUTE ; //1 * StatisticsConstants.TIME_1_MINUTE
            }

            organicTimes += 1;
            YLog.d("scheduleupload" , "setTargetTime organicTimes = " + organicTimes ) ;
            sharedPreferences.edit().putInt(TARGET_TIME_ORGANIC, organicTimes).apply();
            sharedPreferences.edit().putLong(TARGET_TIME_FIRST, targetTime).apply();
        }
    }*/

    private long getTargetTime() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sharedPreferences != null) {
            return sharedPreferences.getLong(TARGET_TIME_FIRST, 0);
        }
        return 0;
    }


}
