package com.leg.yuandu.model;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by snow on 12/08/2019.
 */


@Entity
public class LangOffiline {


    long time;//cal time
    @Id
    String expr;//表达式

    String result;//结果

    int type;//普通算式1，方程式2
    @Generated(hash = 327437171)
    public LangOffiline(long time, String expr, String result, int type) {
        this.time = time;
        this.expr = expr;
        this.result = result;
        this.type = type;
    }
    @Generated(hash = 60377087)
    public LangOffiline() {
    }
    public long getTime() {
        return this.time;
    }
    public void setTime(long time) {
        this.time = time;
    }


    public String getExpr() {
        return expr;
    }

    public void setExpr(String expr) {
        this.expr = expr;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}