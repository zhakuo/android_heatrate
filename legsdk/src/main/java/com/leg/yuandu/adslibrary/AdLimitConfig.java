package com.leg.yuandu.adslibrary;

import android.content.Context;
import android.os.Bundle;

import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.utils.AppUtils;
import com.leg.yuandu.utils.RegionTool;
import com.workout.base.GlobalConst;
import com.workout.base.preferences.SPConstant;
import com.workout.base.preferences.SharedPreferencesUtil;

/**
 * Created by snow on 2018/12/29.
 */

public class AdLimitConfig {
    private Context mContext;
    private static AdLimitConfig sInstance;
    private static Object sLock = new Object();

    private int mAdIPValue = -1;

    private final static String TAG = "AdLimitConfig";

    private AdLimitConfig(Context context) {
        if (context == null) {
            context = YdAppApplication.getContext();
        }
        mContext = context.getApplicationContext();
        initializeData();
    }

    public static AdLimitConfig getsInstance(Context context) {
        if (sInstance == null) {
            synchronized (sLock) {
                if (sInstance == null) {
                    sInstance = new AdLimitConfig(context);
                }
            }
        }

        return sInstance;
    }

    private void initializeData() {
        try {
            SharedPreferencesUtil sp = new SharedPreferencesUtil(SPConstant.DEFAULT_SHAREPREFERENCES_FILE);

            mAdIPValue = sp.getInt(SPConstant.AD_IP_VALUE, 3);
        } catch (Exception e) {
            mAdIPValue = 3;
        }
    }

    public void getAdLimitValue() {

    }


    public boolean isAllowShowAd() {

        if (GlobalConst.DEBUG) {
            return true;
        }

        Bundle dailyBundle = new Bundle();


        if (RegionTool.isSgUser() || RegionTool.isCnUser()) {

            return false;
        }


        if (!AppUtils.hasSimCard(mContext)) {
            return false;
        }

        return true;
    }
}