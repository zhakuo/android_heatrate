package com.leg.yuandu.adslibrary;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.leg.adlibs.R;

/**
 * Created by snow on 16/8/23.
 */
public class AdvertTools {

    public static boolean isFbExist(Context context) {

        return isAppExist(context, "com.facebook.katana");

    }

    public static boolean isAppExist(final Context context,
                                     final String packageName) {
        if (context == null || packageName == null) {
            return false;
        }
        boolean result;
        try {
            context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SHARED_LIBRARY_FILES);
            result = true;
        } catch (PackageManager.NameNotFoundException e) {
            result = false;
        } catch (Throwable e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }


    public static boolean isNetworkOK(Context context) {
        boolean result = false;
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.isConnected()) {
                    result = true;
                }
            }
        }
        return result;
    }


}
