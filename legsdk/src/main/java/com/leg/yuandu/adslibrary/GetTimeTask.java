package com.leg.yuandu.adslibrary;

import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;

import com.leg.yuandu.config.SystemConfig;
import com.workout.base.preferences.SPConstant;
import com.workout.base.preferences.SharedPreferencesUtil;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by snow on 2017/7/5.
 */

public class GetTimeTask extends AsyncTask<Void, Void, Void> {

    private Context mContext;


    public GetTimeTask(Context context) {
        mContext = context;
    }


    @Override
    protected Void doInBackground(Void... voids) {
        long time = requestWebtime(mContext);
        if (time == -1){
            time = SystemConfig.getWebsiteDatetime();
        }
        if (time!=-1) {
            long webInitTime = SystemConfig.getInstance().getWebInitTime();
            if (webInitTime == 0) {
                SystemConfig.getInstance().setWebInitTime(time);
            }

            SystemConfig.getInstance().setSystemRealTime(SystemClock.elapsedRealtime());

            SystemConfig.getInstance().setWebCurrentTime(time);

            SharedPreferencesUtil sp = new SharedPreferencesUtil(SPConstant.DEFAULT_SHAREPREFERENCES_FILE);
            long times = sp.getLong(SPConstant.FIRST_START_TIME, -1l);
            if (times == -1) {
                long currentWebTime = SystemConfig.getInstance().getWebCurrentTime();
                if (currentWebTime != 0) {
                    sp.saveLong(SPConstant.FIRST_START_TIME, currentWebTime);
                }
            }
        }

        Date date = new Date(time);// 转换为标准时间对象
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);// 输出北京时间

        YLog.d("snow","web time........"+sdf.format(date));
        return null;
    }

    private long requestWebtime(Context context){
        try{
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(new HttpGet("https://google.com/"));
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z",Locale.ENGLISH);
                String mDateStr = response.getFirstHeader("Date").getValue();
                Date startDate = df.parse(mDateStr);
                Log.d("Response", "dateStr " + startDate.getTime());
                return startDate.getTime();

            } else{
                //Closes the connection.
                response.getEntity().getContent().close();
//                throw new IOException(statusLine.getReasonPhrase());
            }
        }catch (ParseException e) {
            Log.d("Response", e.getMessage());
        }catch (IOException e) {
            Log.d("Response", e.getMessage());
        }
        return -1;
    }


}