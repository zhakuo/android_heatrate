package com.leg.yuandu.adslibrary;


import android.util.Log;

import com.workout.base.GlobalConst;


/**
 * Created by snow on 2016/11/22.
 */

public class YLog {
//            public static boolean DEBUG = BuildConfig.DEBUG;

    public static boolean isD() {
        return GlobalConst.DEBUG;
    }

    public static void d(String tag, String s) {
        if (GlobalConst.DEBUG) {
            Log.d(tag, s);
        }
    }

    public static void d(String str) {
        if (GlobalConst.DEBUG) {
            Log.d("snow", str);
        }
    }

    public static boolean isDEBUG() {
        return GlobalConst.DEBUG;
    }
}