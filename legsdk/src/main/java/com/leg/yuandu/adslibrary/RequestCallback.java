package com.leg.yuandu.adslibrary;

/**
 * Created by snow on 16/8/23.
 */
public interface RequestCallback {

    public void onAdDataFinish(Object ad);
    public void onAdClicked(Object ad);
    public void onAdFail(String error);
    public void onAdLoadWithPos(Object ad, int adpos);
    public void onAdRewarded( int adpos);
    public void onAdDontRewarded(String reason);
}
