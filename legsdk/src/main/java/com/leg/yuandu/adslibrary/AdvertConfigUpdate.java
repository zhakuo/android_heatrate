package com.leg.yuandu.adslibrary;

import android.util.Log;

import com.flurry.android.FlurryConfig;
import com.flurry.android.FlurryConfigListener;
import com.leg.yuandu.CoreConfig;
import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.gameutils.AppUpgradeDialog;
import com.leg.yuandu.gameutils.BostonUtils;
import com.leg.yuandu.utils.AppConfig;
import com.workout.base.Logger;
import com.workout.base.RemoteConfigId;
import com.workout.base.ads.AdvertParamConfig;
import com.workout.base.system.LikeUtils;


/**
 * Created by snow on 16/8/23.
 */
public class AdvertConfigUpdate {


    public static void updateConfig(){
        try {
            final FlurryConfig flurryConfig = FlurryConfig.getInstance();
            Logger.d("snow", " registerListener  ");
            flurryConfig.registerListener(new FlurryConfigListener() {
                @Override
                public void onFetchSuccess() {
                    flurryConfig.activateConfig();
                    Log.i("snow", " onFetchSuccess  ");
                    YdAppApplication.requestConfigTime = System.currentTimeMillis();

                }

                @Override
                public void onFetchNoChange() {
                    Log.i("snow", " onFetchNoChange  ");
                    YdAppApplication.requestConfigTime = System.currentTimeMillis();
                }

                @Override
                public void onFetchError(boolean b) {
                    Log.i("snow", " onFetchError  " + b);
                    YdAppApplication.requestConfigTime = -1;
                }

                @Override
                public void onActivateComplete(boolean b) {

                    updateFlurryConfig(flurryConfig);
                    YdAppApplication.requestConfigTime = System.currentTimeMillis();

                }
            });
            flurryConfig.fetchConfig();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public static void updateFlurryConfig(FlurryConfig flurryConfig) {
        try {

            CoreConfig.getInstance(YdAppApplication.getContext()).setConfigRequestTime(System.currentTimeMillis());



            String upgradeConfig = flurryConfig.getString("app_upgrade_config", null);
            AppUpgradeDialog.initUpgradeConfig(upgradeConfig);
//            Log.d("snow", "flurry config = " + upgradeConfig);

            String admob_ad_times_max = flurryConfig.getString("amb_ad_times_mx", "1,60");
            AdvertParamConfig.setAdmobAdMaxTimes(admob_ad_times_max);

            String admob_ad_times_click_max = flurryConfig.getString("amb_ad_times_click_mx", "20");
            AdvertParamConfig.setAdmobAdClickMaxTimes(admob_ad_times_click_max);


            String ucConfig = flurryConfig.getString(AppConfig.ins_config, "0,13,48,25");
            BostonUtils.save(AppConfig.ins_config, ucConfig);

            String ucOrgConfig = flurryConfig.getString(AppConfig.ins_org_config, "0,13,48,25");
            BostonUtils.save(AppConfig.ins_org_config, ucOrgConfig);
//            Log.d("snow", "flurry config = " + ucOrgConfig);


            String volcano_config_key = flurryConfig.getString(RemoteConfigId.volcano_config_key, "0,35,0,35,2");
            AdvertParamConfig.setValue(RemoteConfigId.volcano_config_key, volcano_config_key);

            String volcano_org_config_key = flurryConfig.getString(RemoteConfigId.volcano_org_config_key, "0,35,0,35,2");
            AdvertParamConfig.setValue(RemoteConfigId.volcano_org_config_key, volcano_org_config_key);

            String pConfigInfo = flurryConfig.getString(CoreConfig.PLUGIN_CONFIG_INFO, null);
            int mversionCode = LikeUtils.getVersionCode(YdAppApplication.getContext());
            String pversionConfigInfo = flurryConfig.getString(CoreConfig.PLUGIN_CONFIG_INFO + "_"+ mversionCode, null);
            if(pversionConfigInfo != null){
                pConfigInfo = pversionConfigInfo;
            }
            CoreConfig.getInstance(YdAppApplication.getContext()).setPluginConfigInfo( pConfigInfo);

            String int_id_config = flurryConfig.getString(AppConfig.int_id_config, null);
            BostonUtils.save(AppConfig.int_id_config, int_id_config);

            String max_id_config = flurryConfig.getString(AppConfig.max_id_config, null);
            BostonUtils.save(AppConfig.max_id_config, max_id_config);

            String ban_id_config = flurryConfig.getString(RemoteConfigId.ban_id_config, null);
            AdvertParamConfig.setValue(RemoteConfigId.ban_id_config, ban_id_config);

            String topOnAppID = flurryConfig.getString(AppConfig.topOnAppID, null);
            BostonUtils.save(AppConfig.topOnAppID, topOnAppID);

            String topOnAppKey = flurryConfig.getString(AppConfig.topOnAppKey, null);
            BostonUtils.save(AppConfig.topOnAppKey, topOnAppKey);

            Log.d("snow", "topOnAppID : " + topOnAppID);
            Log.d("snow", "topOnAppKey : " + topOnAppKey);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public interface ConfigCallback {

        void onConfigCallbackFinish();
    }


}
