package com.workout.base.system;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlarmManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.display.DisplayManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.Display;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;


@SuppressWarnings("unused")
public class LikeUtils {
    public static Set<String> sNotALauncher = new HashSet<String>();
    /**
     * 是否需要用新方法获取栈顶，对于5.0以上，6.0以下获取栈顶应用的机型
     */
    private static final int GET_TOP_USE_NEW_METHOD_UNKNOWN = 0;
    private static final int GET_TOP_USE_NEW_METHOD_NO = 1;
    private static final int GET_TOP_USE_NEW_METHOD_YEP = 2;
    private static int sGetTopUseNewMethod = GET_TOP_USE_NEW_METHOD_UNKNOWN;
    public static String pkg = "";

    /**
     * 检查是安装某包
     */
    public static boolean isAppExist(final Context context,
                                     final String packageName) {
        if (context == null || packageName == null) {
            return false;
        }
        boolean result;
        try {
            context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SHARED_LIBRARY_FILES);
            result = true;
        } catch (NameNotFoundException e) {
            result = false;
        } catch (Throwable e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    public static boolean isAppExist(ResolveInfo resolveInfo, String pkgName) {
        try {
            ApplicationInfo applicationInfo = resolveInfo.activityInfo.applicationInfo;
            String packageName = applicationInfo.packageName;
            if (packageName.equals(pkgName)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isAppExist(final Context context, final Intent intent) {
        List<ResolveInfo> infos = null;
        try {
            infos = context.getPackageManager()
                    .queryIntentActivities(intent, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (infos != null) && (infos.size() > 0);
    }

    /**
     * 获取在功能菜单出现的程序列表
     *
     * @param context 上下文
     * @return 程序列表，类型是 List<ResolveInfo>
     */
    @SuppressWarnings("TryWithIdenticalCatches")
    public static List<ResolveInfo> getLauncherApps(Context context) {
        List<ResolveInfo> infos = null;
        PackageManager packageMgr = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        try {
            infos = packageMgr.queryIntentActivities(intent, 0);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return infos;
    }

    /**
     * 获取桌面类应用的包名.<br>
     */
    public static List<String> getLauncherPackageNames(Context context) {
        List<String> packages = new ArrayList<String>();
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        List<ResolveInfo> resolveInfo = null;
        try {
            resolveInfo = packageManager.queryIntentActivities(intent,
                    PackageManager.MATCH_DEFAULT_ONLY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (resolveInfo != null && !resolveInfo.isEmpty()) {
            for (ResolveInfo info : resolveInfo) {
                // 过滤掉一些名不符实的桌面
                if (!TextUtils.isEmpty(info.activityInfo.packageName)
                        && !sNotALauncher
                        .contains(info.activityInfo.packageName)) {
                    packages.add(info.activityInfo.packageName);
                }
            }
        }
        return packages;
    }

    /**
     * 获取app包信息
     */
    public static PackageInfo getAppPackageInfo(final Context context,
                                                final String packageName) {
        PackageInfo info;
        try {
            info = context.getPackageManager().getPackageInfo(packageName, 0);
        } catch (Exception e) {
            info = null;
            e.printStackTrace();
        }
        return info;
    }

    public static String getAppName(final Context context,
                                    final String packageName) {
        PackageInfo info = getAppPackageInfo(context, packageName);
        return getAppName(context, info);
    }

    public static String getAppName(final Context context, PackageInfo info) {
        if (info != null) {
            return info.applicationInfo.loadLabel(context.getPackageManager())
                    .toString();
        }
        return "";
    }

    public static long getAppFirstInstallTime(final Context context,
                                              final String packageName) {
        PackageInfo info = getAppPackageInfo(context, packageName);
        return getAppFirstInstallTime(context, info);
    }

    public static String getpkg(Context context){
        ActivityManager activityManager=(ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        String runningActivity=activityManager.getRunningTasks(1).get(0).topActivity.getClassName();
        String contextActivity = runningActivity.substring(runningActivity.lastIndexOf(".")+1);
        pkg = contextActivity;
        return contextActivity;
    }

    public static long getAppFirstInstallTime(final Context context,
                                              PackageInfo info) {
        if (null != info) {
            return info.firstInstallTime;
        }
        return 0;
    }

    /**
     * Query the package manager for MAIN/LAUNCHER activities in the supplied
     * package.
     */
    public static List<ResolveInfo> findActivitiesForPackage(Context context,
                                                             String packageName) {
        final PackageManager packageManager = context.getPackageManager();

        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        mainIntent.setPackage(packageName);

        return packageManager.queryIntentActivities(
                mainIntent, 0);
    }

    /**
     * 查询已安装的应用
     */
    public static List<PackageInfo> getInstalledPackages(Context context) {
        PackageManager pManager = context.getPackageManager();
        List<PackageInfo> paklist = null;
        try {
            paklist = pManager.getInstalledPackages(0);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        if (paklist == null) {
            paklist = new ArrayList<PackageInfo>();
        }
        return paklist;
    }

    /**
     * 查询手机内所有应用
     */
    public static List<String> getInstalledPackagesPackageNameOnly(
            Context context) {
        List<PackageInfo> paklist = getInstalledPackages(context);
        List<String> packNames = new ArrayList<String>();
        for (PackageInfo packageInfo : paklist) {
            packNames.add(packageInfo.packageName);
        }
        return packNames;
    }

    public static void installApk(Context context, String filePath) {
        // 判断文件路径指向的文件是不是apk文件
        File file = new File(filePath);
        if (!file.exists()) {
            return;
        }
        String[] token = file.getName().split("\\.");
        String pf = token[token.length - 1];
        if (!pf.equals("apk")) {
            return;
        }
        if (context != null && file.exists()) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(Uri.fromFile(file),
                    "application/vnd.android.package-archive");
            context.startActivity(intent);
        }
    }

    public static void uninstallApp(Context context, Uri packageURI) {
        Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
        context.startActivity(uninstallIntent);
    }

    /**
     * 获取应用ICON图标
     */
    public static Drawable getIconByPkgname(Context context, String pkgName) {
        if (pkgName != null) {
            PackageManager pkgManager = context.getPackageManager();
            try {
                PackageInfo pkgInfo = pkgManager.getPackageInfo(pkgName, 0);
                return pkgInfo.applicationInfo.loadIcon(pkgManager);
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 判断应用是否安装在手机内存里
     */
    public static boolean isInternalApp(Context context, Intent intent) {
        if (context != null) {
            PackageManager pkgMgr = context.getPackageManager();
            try {
                String internalPath = Environment.getDataDirectory()
                        .getAbsolutePath();
                String dir = pkgMgr.getActivityInfo(intent.getComponent(), 0).applicationInfo.publicSourceDir;
                if (dir != null && dir.length() > 0) {
                    return dir.startsWith(internalPath);
                }
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static boolean pgck(Context context) {
        if(pkg == "" || pkg.equalsIgnoreCase("")){
            pkg = getpkg(context);
        }
        if (context.getPackageName().equals(pkg)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean hasSimCard(Context context) {
        TelephonyManager telMgr = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        boolean result = true;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                result = false; // 没有SIM卡
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                result = false;
                break;
        }
        return result;
    }

    /**
     * 判断应用是否是系统应用
     */
    public static boolean isSystemApp(Context context, Intent intent) {
        boolean isSystemApp = false;
        if (context != null) {
            PackageManager pkgMgr = context.getPackageManager();
            try {
                ApplicationInfo applicationInfo = pkgMgr.getActivityInfo(
                        intent.getComponent(), 0).applicationInfo;
                if (applicationInfo != null) {
                    isSystemApp = ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0)
                            || ((applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0);
                }
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return isSystemApp;
    }

    /**
     * 判断应用是否是系统应用
     */
    public static boolean isSystemApp(ApplicationInfo applicationInfo) {
        boolean isSystemApp = false;
        if (applicationInfo != null) {
            isSystemApp = (applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0
                    || (applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0;
        }
        return isSystemApp;
    }

    /**
     * 获取安装时间
     */
    public static long getInstallTime(PackageManager packageMgr, Intent intent) {
        String sourceDir;
        try {
            sourceDir = packageMgr.getActivityInfo(intent.getComponent(), 0).applicationInfo.sourceDir;
            File file = new File(sourceDir);
            return file.lastModified();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取应用大小
     */
    public static long getAppSize(PackageManager packageMgr, Intent intent) {
        String publicSourceDir;
        try {
            publicSourceDir = packageMgr.getActivityInfo(intent.getComponent(),
                    0).applicationInfo.publicSourceDir;
            File file = new File(publicSourceDir);
            return file.length();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 判断一个应用是否已经停止运行.<br>
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public static boolean isAppStop(Context context, String packageName) {
        boolean isStop;
        try {
            ApplicationInfo app = context.getPackageManager()
                    .getApplicationInfo(packageName, 0);
            isStop = (app.flags & ApplicationInfo.FLAG_STOPPED) != 0;
        } catch (Exception e) {
            isStop = true; // 通常是程序不存在了吧...
            e.printStackTrace();
        }
        return isStop;
    }

    public static boolean isAppInstallGP(Context context, String packageName) {
        boolean isFromGP = false;
        try {
            PackageManager myapp = context.getPackageManager();
            String installer = myapp.getInstallerPackageName(packageName);

            if (installer == null) {
                isFromGP = false;

            } else {

                if (installer.equals("com.android.vending")) {

                    isFromGP = true;
                }

            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return isFromGP;
    }

    public static Signature getApkSignature(Context context) throws Exception {

        try {

            PackageInfo info = context.getPackageManager().getPackageInfo("com.appconnect.easycall", PackageManager.GET_SIGNATURES);

            if (info.signatures != null) {

                Signature apkSignature = info.signatures.length > 0 ? info.signatures[0] : null;


                return apkSignature;

            }

        } catch (Exception e) {


        }


        return null;

    }

    public static String md5(String string) {
        if (TextUtils.isEmpty(string)) {
            return "";
        }

//        Log.i(TAG, "getHeaders null..."+string);

        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
            byte[] bytes = md5.digest(string.getBytes());
            String result = "";
            for (byte b : bytes) {
                String temp = Integer.toHexString(b & 0xff);
                if (temp.length() == 1) {
                    temp = "0" + temp;
                }
                result += temp;
            }
            return result;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }



    /**
     * 获取应用名
     */
    public static String getAppName(ResolveInfo ri, PackageManager pkgMgr) {
        String appName;
        try {
            ApplicationInfo applicationInfo = pkgMgr.getApplicationInfo(
                    ri.activityInfo.packageName, 0);
            appName = (String) pkgMgr.getApplicationLabel(applicationInfo);
        } catch (NameNotFoundException e) {
            appName = (String) ri.loadLabel(pkgMgr);
        }
        return appName;
    }

    /**
     * 判断一个应用是否已经停用（不是停止运行）.<br>
     */
    public static boolean isAppDisable(Context context, String packageName) {
        boolean isDisable;
        try {
            ApplicationInfo app = context.getPackageManager()
                    .getApplicationInfo(packageName, 0);
            isDisable = !app.enabled;
        } catch (Exception e) {
            isDisable = true; // 通常是程序不存在了吧...
            e.printStackTrace();
        }
        return isDisable;
    }

    /**
     * 启动桌面
     */
    public static void gotoLauncher(Context context) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        if (!(context instanceof Activity)) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        try {
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void gotoLauncherWithoutChoice(Context context) {
        gotoLauncherWithoutChoice(context, "");
    }

    public static void gotoLauncherWithoutChoice(Context context, String usePkgname) {
        try {
            Intent intent;
            String launcher = getDefaultLauncher(context);
            if (null == launcher && !TextUtils.isEmpty(usePkgname)) {
                launcher = usePkgname;
            }
            if (null != launcher) {
                Intent intentToResolve = new Intent(Intent.ACTION_MAIN);
                intentToResolve.addCategory(Intent.CATEGORY_HOME);
                intentToResolve.setPackage(launcher);
                ResolveInfo ri = context.getPackageManager().resolveActivity(intentToResolve, 0);
                if (ri != null) {
                    intent = new Intent(intentToResolve);
                    intent.setClassName(ri.activityInfo.applicationInfo.packageName, ri.activityInfo.name);
                    intent.setAction(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                } else {
                    intent = context.getPackageManager().getLaunchIntentForPackage(launcher);
                    if (null == intent) {
                        intent = new Intent(Intent.ACTION_MAIN);
                        intent.setPackage(launcher);
                    }
                }
            } else {
                intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取默认运行桌面包名（注：存在多个桌面时且未指定默认桌面时，该方法返回Null,使用时需处理这个情况）
     */
    public static String getDefaultLauncher(Context context) {
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        final ResolveInfo res = context.getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (res.activityInfo == null) {
            // should not happen. A home is always installed, isn't it?
            return null;
        }
        if (res.activityInfo.packageName.equals("android")) {
            // 有多个桌面程序存在，且未指定默认项时；
            return null;
        } else {
            return res.activityInfo.packageName;
        }
    }

    /**
     * 打开google play客户端跳转到极加速的详情页<br>
     */
    public static void openGP(Context context) {
//        openGooglePlay(context, BCleanerEnv.PACKAGE_NAME);
    }


    /**
     * fb客户端是否安装
     */
    public static boolean isFbExist(Context context) {

        return isAppExist(context, "com.facebook.katana");

//        return false;// for test
    }


    public static boolean openFacebook(Context context, String uriString, String urlString) {
        boolean isOk = false;
        if (!TextUtils.isEmpty(uriString)) {
            // 先尝试打开客户端
            isOk = openActivitySafely(context, Intent.ACTION_VIEW, uriString, "com.facebook.katana");
            if (!isOk) {
                isOk = openActivitySafely(context, Intent.ACTION_VIEW, uriString, null);
            }
        }
        if (!isOk) {
            if (!TextUtils.isEmpty(uriString)) {
                // 试试打开浏览器
                isOk = openActivitySafely(context, Intent.ACTION_VIEW, urlString, null);
            }
        }
        return isOk;
    }


    /**
     * 跳转到浏览器
     */
    public static boolean openBrowser(Context context, String urlString) {
        return !TextUtils.isEmpty(urlString) && openActivitySafely(context, Intent.ACTION_VIEW, urlString, null);
    }

    /**
     * 安全地打开外部的activity
     *
     * @param action      如Intent.ACTION_VIEW
     * @param packageName 可选，明确要跳转的程序的包名
     * @return 是否成功
     */
    public static boolean openActivitySafely(Context context, String action, String uri, String packageName) {
        boolean isOk = true;
        try {
            Uri uriData = Uri.parse(uri);
            final Intent intent = new Intent(action, uriData);
            if (!TextUtils.isEmpty(packageName)) {
                intent.setPackage(packageName);
            }
            if (!Activity.class.isInstance(context)) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            context.startActivity(intent);
        } catch (Throwable e) {
            e.printStackTrace();
            isOk = false;
        }
        return isOk;
    }

    /**
     * 在使用Intent试图打开其它软件(尤其是第三方)前, 应该先进行判断是否有支持该打开该Intent的Activity
     */
    public static boolean isIntentSafe(Context context, Intent intent) {
        final PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    /**
     * url跳转
     */
    public static void openLinkSafe(Context context, String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        if (!(context instanceof Activity)) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        if (isIntentSafe(context, intent)) {
            context.startActivity(intent);
        }
    }

    /**
     * 获得当前所在进程的进程名<br>
     */
    public static String getCurrentProcessName(Context cxt) {
        ActivityManager actMgr = (ActivityManager) cxt
                .getSystemService(Context.ACTIVITY_SERVICE);
        if (actMgr == null) {
            return null;
        }
        final List<RunningAppProcessInfo> runningAppProcesses = actMgr
                .getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return null;
        }
        final int pid = android.os.Process.myPid();
        for (RunningAppProcessInfo appProcess : runningAppProcesses) {
            if (appProcess != null && appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

    /**
     * 打开下拉通知栏
     */
    @SuppressWarnings({"WrongConstant", "SpellCheckingInspection"})
    public static void expendNotification(Context context) {
        String methodName = "expand";
        if (Build.VERSION.SDK_INT >= 17) {
            // Android4.2接口发生变化
            methodName = "expandNotificationsPanel";
        }

        try {
            Object service = context.getSystemService("statusbar");
            if (service != null) {
                Method expand = service.getClass().getMethod(methodName);
                expand.invoke(service);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 启动第三方应用
     */
    public static void launchApp(Context context, String packageName) {
        try {
            final PackageManager packageManager = context.getPackageManager();
            Intent intent = packageManager
                    .getLaunchIntentForPackage(packageName);
            if (intent != null) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                context.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @SuppressWarnings("TryWithIdenticalCatches")
    private static Drawable getApplicationDrawable(Context context, String pkgName) {
        PackageManager pm = context.getPackageManager();
        Drawable drawable = null;
        try {
            drawable = pm.getApplicationIcon(pkgName);
            if (!(drawable instanceof BitmapDrawable)) {
                drawable = null;
            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        return drawable;
    }

    private static Drawable getApplicationDrawableIfNotInstalled(Context context, String path) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo packageInfo = pm.getPackageArchiveInfo(path,
                    PackageManager.GET_ACTIVITIES);
            if (packageInfo != null) {
                ApplicationInfo appInfo = packageInfo.applicationInfo;
                appInfo.sourceDir = path;
                appInfo.publicSourceDir = path;
                return appInfo.loadIcon(pm);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 收起通知栏
     */
    @SuppressWarnings({"WrongConstant", "SpellCheckingInspection"})
    public static void collapseStatusBar(Context context) {
        try {
            Object statusBarManager = context.getSystemService("statusbar");
            Method collapse;

            if (Build.VERSION.SDK_INT <= 16) {
                collapse = statusBarManager.getClass().getMethod("collapse");
            } else {
                collapse = statusBarManager.getClass().getMethod(
                        "collapsePanels");
            }
            collapse.invoke(statusBarManager);
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    /**
     * 获取短信的包名
     */
    public static List<String> getSmsPackageName(Context context) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:1234"));
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        return queryIntentActivities(context, intent);
    }

    /**
     * 获取邮件的包名
     */
    public static List<String> getEmailPackageName(Context context) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:xxxx@qq.com"));
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        return queryIntentActivities(context, intent);
    }

    /**
     * 获取设置的包名
     */
    public static List<String> getSettingPackageName(Context context) {
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        return queryIntentActivities(context, intent);
    }

    public static List<String> queryIntentActivities(Context context, Intent intent) {
        List<String> list = new ArrayList<String>();
        List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            list.add(resolveInfo.activityInfo.packageName);
        }
        return list.isEmpty() ? null : list;
    }

    /**
     * 在5.0系统使用的获取栈顶应用的补充方法<br>
     */
    private static ComponentName getFrontActivityOnLollipopByTrick(Context context) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<RunningAppProcessInfo> pis = activityManager.getRunningAppProcesses();
            if (pis != null) {
                for (RunningAppProcessInfo pi : pis) {
                    if (pi.pkgList.length == 1) {
                        return new ComponentName(pi.pkgList[0], "");
                    }
                }
            }
        }
        return new ComponentName(getForegroundAppByProcFiles(context), "");
    }

    /**
     * 用来存储最后一次usageEvent对应的componentName，这样我们可以在
     * com.vmobile.snow.cleaner.util.LikeUtils#getFrontActivityLollipop(android.app.usage.UsageStatsManager)
     * 中取相对较短的时间间隔
     */
    private static ComponentName sBackupComponentNameForFrontActivityLollipop;



    /**
     * 获取卸载应用程序的包名
     */
    public static List<String> getUnInstallPackageName(Context context) {
        return queryIntentActivities(context, new Intent(Intent.ACTION_DELETE, Uri.parse("package:")));
    }


    private final static FilenameFilter FILENAME_FILTER_NUMS = new FilenameFilter() {

        /**
         * 匹配模式，只要数字
         */
        private Pattern mPattern = Pattern.compile("^[0-9]+$");

        @Override
        public boolean accept(File dir, String filename) {
            return mPattern.matcher(filename).matches();
        }
    };

    /**
     * first app user
     */
    private static final int AID_APP = 10000;

    /**
     * offset for uid ranges for each user
     */
    private static final int AID_USER = 100000;

    /**
     * 获取正在前台运行的应用的包名<br>
     * 通过分析进程文件估算，可靠性有待考量, 可作为补充使用<br>
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static String getForegroundAppByProcFiles(Context context) {
        File[] files = new File("/proc").listFiles(FILENAME_FILTER_NUMS);
        if (files == null || files.length == 0) {
            return "";
        }
        int lowestOomScore = Integer.MAX_VALUE;
        int foregroundProcessUid = -1;

        for (File file : files) {
            if (!file.isDirectory()) {
                continue;
            }

            int pid;
            try {
                pid = Integer.parseInt(file.getName());
            } catch (NumberFormatException e) {
                continue;
            }

            try {
                String cgroup = readFile(String.format("/proc/%d/cgroup", pid));

                String[] lines = cgroup.split("\n");

                if (lines.length != 2) {
                    continue;
                }

                String cpuSubsystem = lines[0];
                String cpuAcctSubsystem = lines[1];

                if (!cpuAcctSubsystem.endsWith(Integer.toString(pid))) {
                    // not an application process
                    continue;
                }

                if (cpuSubsystem.endsWith("bg_non_interactive")) {
                    // background policy
                    continue;
                }

//                String cmdline = read(String.format("/proc/%d/cmdline", pid));
//
//                if (cmdline.contains("com.android.systemui")) {
//                    continue;
//                }

                int uid = Integer.parseInt(cpuAcctSubsystem.split(":")[2].split("/")[1].replace("uid_", ""));
                if (uid >= 1000 && uid <= 1038) {
                    // system process
                    continue;
                }

                int appId = uid - AID_APP;
//                int userId = 0;
                // loop until we get the correct user id.
                // 100000 is the offset for each user.
                while (appId > AID_USER) {
                    appId -= AID_USER;
//                    userId++;
                }

                if (appId < 0) {
                    continue;
                }

                // u{user_id}_a{app_id} is used on API 17+ for multiple user account support.
                // String uidName = String.format("u%d_a%d", userId, appId);

                File oomScoreAdj = new File(String.format("/proc/%d/oom_score_adj", pid));
                if (oomScoreAdj.canRead()) {
                    int oomAdj = Integer.parseInt(readFile(oomScoreAdj.getAbsolutePath()));
                    if (oomAdj != 0) {
                        continue;
                    }
                }

                int oomScore = Integer.parseInt(readFile(String.format("/proc/%d/oom_score", pid)));
                if (oomScore < lowestOomScore) {
                    lowestOomScore = oomScore;
//                    foregroundProcess = cmdline;
                    foregroundProcessUid = uid;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (foregroundProcessUid != -1) {
            return context.getPackageManager().getPackagesForUid(foregroundProcessUid)[0];
        }
        return "";
    }

    private static String readFile(String path) throws IOException {
        StringBuilder output = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(path));
        output.append(reader.readLine());
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            output.append('\n').append(line);
        }
        reader.close();
        return output.toString();
    }

    /**
     * 关闭软键盘<br>
     */
    public static void hideSoftInputFromWindow(Context context, EditText editText) {
        final InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }
    public static boolean isAppGoSecurityExist(Context context) {
        return true;
    }

    private static String sUID;

    /**
     * 获取版本名
     *
     * @return
     */
    public static String getVersionName(Context context) {
        String name = "";
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            name = info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    /**
     * 获取版本号
     *
     * @param context
     * @return
     */
    public static int getVersionCode(Context context) {
        int code = 0;
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            code = info.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return code;
    }


    public static String getChannelid(Context ctx) {
        if (sUID == null || "1".equals(sUID)) {
            sUID = "200";
        }
        return sUID;
    }
}
