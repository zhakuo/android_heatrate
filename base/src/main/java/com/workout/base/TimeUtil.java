package com.workout.base;

import android.content.SharedPreferences;

import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by chenhewen on 2018/7/10.
 */

public class TimeUtil {
    public static final long SECOND_IN_MILLIS = 1000L;
    public static final long MINUTE_IN_MILLIS = SECOND_IN_MILLIS * 60;
    public static final long HOUR_IN_MILLIS = MINUTE_IN_MILLIS * 60;
    public static final long DAY_IN_MILLIS = HOUR_IN_MILLIS * 24;

    /**
     * 距离1970年1月1日的天数，考虑了时区问题
     *
     * @param millis 例如system.currentMillis
     * @return 天数，该值+1为明天，-1为昨天
     */
    public static long dayFrom1970(long millis) {
        return (millis + TimeZone.getDefault().getRawOffset()) / TimeUtil.DAY_IN_MILLIS;
    }


    static public String getMillisToTimeStr(long millis) {
        String result;
        long h = TimeUnit.MILLISECONDS.toHours(millis);
        long m = TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1);
        long s = TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1);
        if (h != 0) {
            result = String.format(Locale.US, "%dh %dm %ds", h, m, s);
        } else {
            result = String.format(Locale.US, "%dm %ds", m, s);
        }
        return result;
    }

    /**
     * 距初次安装应用的是否已经过了几天
     *
     * @param days
     */
    public static boolean isInstallDaysAfter(int days) {
        SharedPreferences sp = BaseModulePreferenceStorage.getBaseSp();
        long currentTime = System.currentTimeMillis();
        long firstInstallTime = sp.getLong(BaseModulePreferenceId.APP_FIRST_INSTALL_MILLIS, currentTime);
        return (currentTime - firstInstallTime) > days * TimeUtil.DAY_IN_MILLIS;
    }
}
