package com.workout.base.preferences;

import android.content.Context;
import android.content.SharedPreferences;


public class SharePreferenceUtil {
    public static final String AUTHORITY = "workout.preference";

    public static SharedPreferences getSharedPreferences(String preferName, Context ctx) {
        return SharedPreferenceProxy.getSharedPreferences(ctx, preferName);
    }
}
