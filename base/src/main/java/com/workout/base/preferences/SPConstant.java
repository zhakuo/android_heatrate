package com.workout.base.preferences;

public class SPConstant {

    public static final String ABTEST_SP = "abtest";

    public static final String IS_NEW_USER_FOR_PL = "IS_NEW_USER_FOR_PL";



    public static final String DEFAULT_SHAREPREFERENCES_FILE = "default_sharepreferences_file_name";
    public static final long HOUR_MILLIS = 1000 * 60 * 60;

    /**
     * facebook ad的广告展示时间
     */
    public static final String SHOW_AD_TIME = "SHOW_AD_TIME";


    /**
     * facebook ad的广告上一次load成功时间
     */
    public static final String LAST_LOAD_AD_SUCCESS_TIME = "LAST_LOAD_AD_SUCCESS_TIME";

    /**
     * 首次启动时间
     */
    public static final String FIRST_START_TIME = "FIRST_START_TIME";

    public static final String UPDATE_IP_VALUE_TIME = "UPDATE_IP_VALUE_TIME";
    public static final String AD_IP_VALUE = "ad_ip_value";
    public static final String AD_DEEP_VALUE = "ad_deep_value";
    public static final String AD_IP_COUNTRY_VALUE = "ad_ip_country_value";
    public static final String AD_IP_CITY_VALUE = "ad_ip_city_value";
    public static final String VOLCANO_HAS_OPEN = "volcano_has_open";


    public static void saveVolcanoHasOpen(boolean enable) {
        SharedPreferencesUtil sp = new SharedPreferencesUtil(SPConstant.DEFAULT_SHAREPREFERENCES_FILE);
        sp.saveBoolean(VOLCANO_HAS_OPEN, enable);
    }

    public static boolean isVolcanoHasOpen() {
        SharedPreferencesUtil sp = new SharedPreferencesUtil(SPConstant.DEFAULT_SHAREPREFERENCES_FILE);
        return sp.getBoolean(VOLCANO_HAS_OPEN, false);
    }
}
