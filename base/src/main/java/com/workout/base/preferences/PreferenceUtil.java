package com.workout.base.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;


public class PreferenceUtil {
    public static final String METHOD_CONTAIN_KEY = "method_contain_key";
    public static final String AUTHORITY = "healthtrackercalendar.preference";
    public static final Uri URI = Uri.parse("content://" + AUTHORITY);
    public static final String METHOD_QUERY_VALUE = "method_query_value";
    public static final String METHOD_EIDIT_VALUE = "method_edit";
    public static final String METHOD_QUERY_PID = "method_query_pid";
    public static final String KEY_VALUES = "key_result";

    public static SharedPreferences getSharedPreferences(String preferName, Context ctx) {
        return SharedPreferenceProxy.getSharedPreferences(ctx, preferName);
    }
}

