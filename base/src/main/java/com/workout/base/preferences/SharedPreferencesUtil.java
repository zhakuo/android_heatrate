package com.workout.base.preferences;

import android.content.SharedPreferences;
import android.util.Log;

import com.workout.base.BaseModuleContext;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SharedPreferencesUtil {
    private SharedPreferences mStaticPreferences;
    private static Map<String, SharedPreferencesUtil> sInstance = new HashMap<String, SharedPreferencesUtil>();


    public static synchronized SharedPreferencesUtil get(String file) {
        SharedPreferencesUtil sharedPreferencesUtil = sInstance.get(file);
        if (sharedPreferencesUtil == null) {
            sharedPreferencesUtil = new SharedPreferencesUtil(file);
            sInstance.put(file, sharedPreferencesUtil);
        }
        return sharedPreferencesUtil;
    }

    public SharedPreferencesUtil(String sharePreferenceName) {
        mStaticPreferences = PreferenceUtil.getSharedPreferences(sharePreferenceName, BaseModuleContext.getContext());
    }

    public SharedPreferences getSharedPreferences() {
        return mStaticPreferences;
    }

    // 保存字符串数据
    public void saveString(String key, String value) {
        if (value == null) {
            mStaticPreferences.edit().putString(key, "").commit();
        } else {
            mStaticPreferences.edit().putString(key, value).commit();
        }
    }

    // 获取字符串数据
    public String getString(String key, String... defValue) {
        if (null != defValue && defValue.length > 0) {
            return mStaticPreferences.getString(key, defValue[0]);
        } else {
            return mStaticPreferences.getString(key, "");
        }
    }

    // 保存字符串集合set
    public void saveStringSet(String key, Set<String> set) {
        mStaticPreferences.edit().putStringSet(key, set).commit();
    }

    // 获取字符串集合set
    public Set<String> getStringSet(String key, Set<String> defValue) {
        return mStaticPreferences.getStringSet(key, defValue);
    }


    // 保存整型数据
    public void saveInt(String key, int value) {
        mStaticPreferences.edit().putInt(key, value).apply();
        mStaticPreferences.edit().putInt(key, value).commit();
    }

    // 获取整型数据
    public int getInt(String key, int defValue) {
        if (mStaticPreferences != null) {
            return mStaticPreferences.getInt(key, defValue);
        } else {
            return defValue;
        }
    }

    // 保存布尔值数据
    public synchronized void saveBoolean(String key, Boolean value) {
        mStaticPreferences.edit().putBoolean(key, value).commit();
    }

    // 获取布尔值数据
    public Boolean getBoolean(String key, Boolean... defValue) {
        if (defValue.length > 0) {
            return mStaticPreferences.getBoolean(key, defValue[0]);
        } else {
            return mStaticPreferences.getBoolean(key, false);
        }
    }

    // 保存long数据
    public void saveLong(String key, long value) {
        try {
            mStaticPreferences.edit().putLong(key, value).commit();
        } catch (Exception e) {
            Log.e("tell coder", e.getMessage());
        }

    }

    // 获取long数据
    public long getLong(String key, Long... defValue) {
        if (defValue.length > 0) {
            return mStaticPreferences.getLong(key, defValue[0]);
        } else {
            return mStaticPreferences.getLong(key, -1);
        }
    }

    public Map<String, ?> getAll() {
        return mStaticPreferences.getAll();
    }

    public void remove(String key) {
        mStaticPreferences.edit().remove(key).commit();
    }

    public void clear() {
        mStaticPreferences.edit().clear().commit();
    }
}
