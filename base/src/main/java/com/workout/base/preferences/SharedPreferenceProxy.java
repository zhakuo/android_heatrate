package com.workout.base.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class SharedPreferenceProxy{
    private static Map<String, SharedPreferenceProxy> sharedPreferenceProxyMap;

    private static AtomicInteger processFlag = new AtomicInteger(0);

    private Context ctx;
    private String preferName;

    private SharedPreferenceProxy(Context ctx, String name) {
        this.ctx = ctx.getApplicationContext();
        this.preferName = name;
    }

    public static SharedPreferences getSharedPreferences( Context ctx, String preferName) {
        //First check if the same process
        return getFromLocalProcess(ctx, preferName);
    }

    private static SharedPreferences getFromLocalProcess( Context ctx, String preferName) {
        return ctx.getSharedPreferences(preferName, Context.MODE_PRIVATE);
    }
}
