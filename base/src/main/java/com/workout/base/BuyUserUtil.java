package com.workout.base;

public class BuyUserUtil {

    public static void saveBuyUser(boolean isBuyUser) {
        BaseModulePreferenceStorage.getBaseSp().edit().putBoolean(BaseModulePreferenceId.IS_BUY_USER, isBuyUser).apply();
    }

    public static void isBuyUser() {
        BaseModulePreferenceStorage.getBaseSp().getBoolean(BaseModulePreferenceId.IS_BUY_USER, false);
    }

    /**
     * 步数通知栏是否开启
     */
    public static final String STEP_NOTIFICATION_ENABLE = "step_notification_enable";

    public static boolean isStepNotificationEnable() {
        return BaseModulePreferenceStorage.getBaseSp().getBoolean(STEP_NOTIFICATION_ENABLE, false);
    }
}
