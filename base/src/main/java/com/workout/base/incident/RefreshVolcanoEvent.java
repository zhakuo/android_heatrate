package com.workout.base.incident;

public class RefreshVolcanoEvent {

    public enum State {
        CONNECTED,
        DISCONNECTED,
        TIME_CHANGED,
        BATTERY_CHANGED
    }

    private State mState;

    public RefreshVolcanoEvent(State state) {
        mState = state;
    }

    public State getState() {
        return mState;
    }

    public static final RefreshVolcanoEvent CONNECTED = new RefreshVolcanoEvent(State.CONNECTED);
    public static final RefreshVolcanoEvent DISCONNECTED = new RefreshVolcanoEvent(State.DISCONNECTED);
    public static final RefreshVolcanoEvent BATTERY_CHANGED = new RefreshVolcanoEvent(State.BATTERY_CHANGED);
    public static final RefreshVolcanoEvent TIME_CHANGED = new RefreshVolcanoEvent(State.TIME_CHANGED);
}
