package com.workout.base.view;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;

public class StatusBarTool {
    public static boolean isSupport() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static int getHeight() {
        if (!isSupport()) {
            return 0;
        }
        return getStatusBarHeight();
    }

    public static int getStatusBarHeight() {
        Resources resources = Resources.getSystem();
        int resourceId = Resources.getSystem().getIdentifier(
                "status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public static void appendStatusBarTopMargin(View view) {
        if (isSupport()) {
            ViewGroup.LayoutParams sourceParams = view.getLayoutParams();
            if (sourceParams instanceof ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) sourceParams;
                layoutParams.topMargin += getHeight();
                view.setLayoutParams(layoutParams);
            }
        }
    }

    public static void appendStatusBarHeight(View view) {
        if (isSupport()) {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.height += getHeight();
            view.setLayoutParams(layoutParams);
        }
    }

    public static void changeStatusBarColor(Activity activity, int colorResId) {
        if (activity == null) {
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(activity.getResources().getColor(colorResId));
        }
    }
}
