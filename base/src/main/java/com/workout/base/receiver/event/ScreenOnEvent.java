package com.workout.base.receiver.event;

/**
 * Created by chenhewen on 2018/4/28.
 */

public class ScreenOnEvent {
    public static final ScreenOnEvent ENABLE = new ScreenOnEvent(true);
    public static final ScreenOnEvent DISABLE = new ScreenOnEvent(false);

    private boolean mEnable;

    public ScreenOnEvent(boolean enable) {
        mEnable = enable;
    }

    public boolean isEnable() {
        return mEnable;
    }

    public static ScreenOnEvent getEvent(boolean enable) {
        return enable ? ENABLE : DISABLE;
    }

}
