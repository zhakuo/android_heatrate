package com.workout.base.receiver.spec;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

import com.workout.base.receiver.BaseBroadcastReceiver;
import com.workout.base.receiver.event.BatteryChangedEvent;
import com.workout.base.receiver.event.BatteryConnectedChangedEvent;

import org.greenrobot.eventbus.EventBus;

public class BatteryChangedReceiver extends BaseBroadcastReceiver {

    private static final class BatteryChangedReceiverHolder {
        private static BatteryChangedReceiver sBatteryChangedReceiver = new BatteryChangedReceiver();
    }

    public static BatteryChangedReceiver getInstance() {
        return BatteryChangedReceiverHolder.sBatteryChangedReceiver;
    }

    private BatteryChangedReceiver() {

    }

    @Override
    protected void onStickyBroadcast(Intent intent) {
        if (intent != null) {
            setValues(intent);
        }
    }

    @Override
    protected IntentFilter createIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);

        return intentFilter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int batteryPreScale = mBatteryScale;
        int batteryPreLevel = mBatteryLevel;
        int batteryPrePct = mBatteryPct;
        boolean isPreConnected = mIsConnected;

        setValues(intent);

        // 事件
        if (isPreConnected != mIsConnected) {
            EventBus.getDefault().post(BatteryConnectedChangedEvent.getEvent(mIsConnected));
        }

        // 事件
        if (batteryPrePct != mBatteryPct) {
            EventBus.getDefault().post(new BatteryChangedEvent(batteryPrePct, mBatteryPct));
        }
    }

    private void setValues(Intent intent) {
        mBatteryScale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
        mBatteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        mBatteryPct = (int) (mBatteryLevel * 100f / mBatteryScale);
        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        mIsConnected = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;
        int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        mUseCharing = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        mAcCharging = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
    }

    private int mBatteryScale;

    private int mBatteryLevel;

    private int mBatteryPct;

    private boolean mIsConnected;

    private boolean mUseCharing;

    private boolean mAcCharging;

    public int getBatteryScale() {
        return mBatteryScale;
    }

    public int getBatteryLevel() {
        return mBatteryLevel;
    }

    /**
     * [0, 100]
     * @return
     */
    public int getBatteryPct() {
        return mBatteryPct;
    }

    public boolean isConnected() {
        return mIsConnected;
    }

    public boolean isUseCharing() {
        return mUseCharing;
    }

    public boolean isAcCharging() {
        return mAcCharging;
    }
}
