package com.workout.base.receiver.spec;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import org.greenrobot.eventbus.EventBus;

import com.workout.base.receiver.BaseBroadcastReceiver;
import com.workout.base.receiver.event.ScreenOnEvent;

/**
 * Created by chenhewen on 2018/4/28.
 */

public class ScreenOnReceiver extends BaseBroadcastReceiver {

    @Override
    protected IntentFilter createIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilter.setPriority(Integer.MAX_VALUE);
        return intentFilter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Intent.ACTION_SCREEN_ON.equals(action)) {
            EventBus.getDefault().post(ScreenOnEvent.ENABLE);
        } else if (Intent.ACTION_SCREEN_OFF.equals(action)) {
            EventBus.getDefault().post(ScreenOnEvent.DISABLE);
        }
    }
}
