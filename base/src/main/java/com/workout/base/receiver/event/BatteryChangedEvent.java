package com.workout.base.receiver.event;

public class BatteryChangedEvent {

    private int mPreBatteryPercentage;
    private int mBatteryPercentage;

    public BatteryChangedEvent(int preBatteryPercentage, int batteryPercentage) {
        mPreBatteryPercentage = preBatteryPercentage;
        mBatteryPercentage = batteryPercentage;
    }

    public int getBatteryPercentage() {
        return mBatteryPercentage;
    }

    public int getPreBatteryPercentage() {
        return mPreBatteryPercentage;
    }
}
