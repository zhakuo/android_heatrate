package com.workout.base.receiver.spec;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;

import org.greenrobot.eventbus.EventBus;

import com.workout.base.receiver.BaseBroadcastReceiver;
import com.workout.base.receiver.event.HomeKeyPressedEvent;
import com.workout.base.receiver.event.RecentAppKeyPressedEvent;

/**
 * SystemKeyReceiver
 * Created by chenhewen on 2018/3/16.
 */

public class SystemKeyReceiver extends BaseBroadcastReceiver {

    final String SYSTEM_DIALOG_REASON_KEY = "reason";
    final String SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS = "globalactions";
    final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
    final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";

    @Override
    protected IntentFilter createIntentFilter() {
        return new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (TextUtils.equals(action, Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
            String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
            if (reason != null) {
                if (reason.equals(SYSTEM_DIALOG_REASON_HOME_KEY)) {
                    // 事件
                    EventBus.getDefault().post(HomeKeyPressedEvent.SINGLETON);
                } else if (reason
                        .equals(SYSTEM_DIALOG_REASON_RECENT_APPS)) {
                    // 事件
                    EventBus.getDefault().post(RecentAppKeyPressedEvent.SINGLETON);
                }
            }
        }
    }


}
