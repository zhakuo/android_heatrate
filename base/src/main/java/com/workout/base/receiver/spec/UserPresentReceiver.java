package com.workout.base.receiver.spec;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.workout.base.receiver.BaseBroadcastReceiver;
import com.workout.base.receiver.event.UserPresentEvent;

import org.greenrobot.eventbus.EventBus;

public class UserPresentReceiver extends BaseBroadcastReceiver {
    @Override
    protected IntentFilter createIntentFilter() {
        return new IntentFilter(Intent.ACTION_USER_PRESENT);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        EventBus.getDefault().post(UserPresentEvent.SINGLETON);
    }
}
