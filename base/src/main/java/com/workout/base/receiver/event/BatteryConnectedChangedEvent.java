package com.workout.base.receiver.event;

public class BatteryConnectedChangedEvent {

    public static final BatteryConnectedChangedEvent ENABLE = new BatteryConnectedChangedEvent(true);
    public static final BatteryConnectedChangedEvent DISABLE = new BatteryConnectedChangedEvent(false);

    private boolean mEnable;

    public BatteryConnectedChangedEvent(boolean enable) {
        mEnable = enable;
    }

    public static BatteryConnectedChangedEvent getEvent(boolean enable) {
        return enable ? ENABLE : DISABLE;
    }

    public boolean isEnable() {
        return mEnable;
    }
}
