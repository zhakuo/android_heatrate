package com.workout.base.receiver.event;

/**
 * Created by chenhewen on 2018/4/29.
 */

public class TimeChangedEvent {

    private String mAction;

    public TimeChangedEvent(String action) {
        mAction = action;
    }

    public String getAction() {
        return mAction;
    }
}
