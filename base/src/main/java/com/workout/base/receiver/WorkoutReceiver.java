package com.workout.base.receiver;

import android.content.Context;

import com.workout.base.receiver.spec.BatteryChangedReceiver;
import com.workout.base.receiver.spec.ScreenOnReceiver;
import com.workout.base.receiver.spec.SystemKeyReceiver;
import com.workout.base.receiver.spec.TimeChangeReceiver;
import com.workout.base.receiver.spec.UserPresentReceiver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chenhewen on 2018/3/16.
 */

public class WorkoutReceiver {

    private List<BaseBroadcastReceiver> mReceiverList;

    public WorkoutReceiver() {
        mReceiverList = new ArrayList<>();
        mReceiverList.add(new ScreenOnReceiver());
        mReceiverList.add(new TimeChangeReceiver());
        mReceiverList.add(new SystemKeyReceiver());
        mReceiverList.add(BatteryChangedReceiver.getInstance());
        mReceiverList.add(new UserPresentReceiver());
    }

    public void onCreate(Context context) {
        for (BaseBroadcastReceiver receiver : mReceiverList) {
            receiver.register();
        }
    }

    public void onDestroy(Context context) {
        for (BaseBroadcastReceiver receiver : mReceiverList) {
            receiver.unregister();
        }
    }
}
