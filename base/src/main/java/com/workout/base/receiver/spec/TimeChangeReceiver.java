package com.workout.base.receiver.spec;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.workout.base.receiver.BaseBroadcastReceiver;
import com.workout.base.receiver.event.TimeChangedEvent;
import com.workout.base.receiver.event.TimeSetEvent;
import com.workout.base.receiver.event.TimeTickEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by chenhewen on 2018/4/29.
 */

public class TimeChangeReceiver extends BaseBroadcastReceiver {
    @Override
    protected IntentFilter createIntentFilter() {
        IntentFilter intent = new IntentFilter();
        intent.addAction(Intent.ACTION_TIME_TICK);
        intent.addAction(Intent.ACTION_TIME_CHANGED);
        intent.addAction(Intent.ACTION_LOCALE_CHANGED);
        intent.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        return intent;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Intent.ACTION_TIME_TICK.equals(action) ||
                Intent.ACTION_TIME_CHANGED.equals(action) ||
                Intent.ACTION_LOCALE_CHANGED.equals(action) ||
                Intent.ACTION_TIMEZONE_CHANGED.equals(action)) {

            EventBus.getDefault().post(new TimeChangedEvent(action));
        }

        if (Intent.ACTION_TIME_CHANGED.equals(action) ||
                Intent.ACTION_LOCALE_CHANGED.equals(action) ||
                Intent.ACTION_TIMEZONE_CHANGED.equals(action)) {

            EventBus.getDefault().post(TimeSetEvent.SINGLETON);
        }

        if (Intent.ACTION_TIME_CHANGED.equals(action)) {
            EventBus.getDefault().post(TimeTickEvent.SINGLETON);
        }

    }
}
