package com.workout.base.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.workout.base.BaseModuleContext;


/**
 * Created by chenhewen on 2018/3/16.
 */

public abstract class BaseBroadcastReceiver extends BroadcastReceiver {

    protected abstract IntentFilter createIntentFilter();

    protected void onStickyBroadcast(Intent intent) {

    }

    public void register() {
        Context context = BaseModuleContext.getContext();
        Intent intent = context.registerReceiver(this, createIntentFilter());
        onStickyBroadcast(intent);
    }

    public void unregister() {
        Context context = BaseModuleContext.getContext();
        context.unregisterReceiver(this);
    }
}
