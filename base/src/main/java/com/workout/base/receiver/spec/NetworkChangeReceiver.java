package com.workout.base.receiver.spec;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;
import com.workout.base.system.SafeToast;

import com.workout.base.BaseModuleContext;
import com.workout.base.receiver.BaseBroadcastReceiver;
import com.workout.base.receiver.event.ScreenOnEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by snow on 2019/3/23.
 */

public class NetworkChangeReceiver extends BaseBroadcastReceiver {

    private NetworkChangeReceiver networkChangeReceiver;
    @Override
    protected IntentFilter createIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        return intentFilter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager=(ConnectivityManager) BaseModuleContext.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo =connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isAvailable()) {
            SafeToast.makeText(context, "network is available", Toast.LENGTH_SHORT).show();
        } else {
            SafeToast.makeText(context,"newwork is unavailable",Toast.LENGTH_SHORT).show();
        }
    }

}
