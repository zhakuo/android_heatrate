package com.workout.base;

/**
 * remote config 后台配置与本地解析协议不同时抛出该异常
 */
public class RemoteConfigParseException extends RuntimeException {
}
