package com.workout.base;

public class BaseModulePreferenceId {

    /**
     * 应用首次安装时间
     */
    public static final String APP_FIRST_INSTALL_MILLIS = "app_first_install_millis";

    /**
     * 是否是买量用户
     */
    public static final String IS_BUY_USER = "is_buy_user";
}
