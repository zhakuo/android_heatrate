package com.workout.base;

/**
 * Created by chenhewen on 2018/7/7.
 */

public class GlobalConst {

    public static final boolean DEBUG = false  || BuildConfig.DEBUG;//;

    public static final String GOOGLE_PLAY_PREFIX = "https://play.google.com/store/apps/details?id=";
    public static final String GOOGLE_PLAY_APP_PREFIX = "market://details?id=";

}
