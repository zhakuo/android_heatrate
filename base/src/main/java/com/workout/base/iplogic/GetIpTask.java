package com.workout.base.iplogic;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.workout.base.BaseModuleContext;
import com.workout.base.Logger;

public class GetIpTask extends AsyncTask<Void, Void, Void> {

    private String TAG = "GetGeo";

    public GetIpTask() {

    }

    @Override
    protected Void doInBackground(Void... voids) {
        try{

            Logger.d(TAG,"get ip geo...start");

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(BaseModuleContext.getContext().getApplicationContext());


            long currentDay = System.currentTimeMillis() / (24 * 60 * 60 *1000l);

            if (preferences.getLong("IP_CURRENT_DAY", 0L) < currentDay || !IpUtils.getInstance().isSaveGeo()) {
                Logger.d(TAG,"get geo...a new day ");
                if (preferences.getLong("IP_CURRENT_DAY", 0L) < currentDay){

                    preferences.edit().putLong("IP_CURRENT_DAY", currentDay).commit();
                }

                Logger.d(TAG,"get geo...init data");

                try{
                    if (!IpUtils.getInstance().isSaveGeo()){

                        BaseModuleContext.post(new Runnable() {
                            @Override
                            public void run() {

                                RequestIpInfo.requestIpInfo();
                            }
                        });
                    }


                }catch (Exception e){


                }

            }


        }catch (Exception e){

        }



        return null;
    }

}
