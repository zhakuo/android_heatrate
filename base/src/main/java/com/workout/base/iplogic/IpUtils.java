package com.workout.base.iplogic;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.workout.base.BaseModuleContext;
import com.workout.base.global.GlobalPreferenceFileName;

public class IpUtils {


    private SharedPreferences mPreferences;

    public static final String IP_LATITUE = "ip_latitue";
    public static final String IP_LONGITUDE  = "ip_longitude";
    public static final String GEO_UPDATETIME  = "geo_updatetime";
    public static final String COUNTRY_CODE  = "country_code";
    public static final String VPN_STATE  = "vpn_state";
    public static final String VPN_UPDATETIME  = "vpn_updatetime";

    private static IpUtils sIpUtils;
    private final static Object LOCK = new Object();

    public static IpUtils getInstance() {
        if (sIpUtils == null) {
            synchronized (LOCK) {
                if (sIpUtils == null) {
                    sIpUtils = new IpUtils();
                }
            }
        }
        return sIpUtils;
    }

    private IpUtils() {
        init(BaseModuleContext.getContext());
    }

    public static void checkGeo(){
        try{
            new GetIpTask().execute();
        }catch (Exception e){

        }
    }
    public void init(Context context) {
        mPreferences = context.getSharedPreferences(GlobalPreferenceFileName.BASE_SP_FILE_NAME,Context.MODE_PRIVATE);
    }

    private void checkDefault() {
        if (mPreferences == null) {
            init(BaseModuleContext.getContext());
        }
    }
    public void saveGeo(double latitue,double longitude){
        checkDefault();
        mPreferences.edit().putString(IP_LATITUE, latitue+"").apply();
        mPreferences.edit().putString(IP_LONGITUDE, longitude+"").commit();


        saveGeoUpdatetime(System.currentTimeMillis());

    }

    public void saveGeoUpdatetime(long updatetime) {
        checkDefault();
        mPreferences.edit().putLong(GEO_UPDATETIME, updatetime).apply();
        mPreferences.edit().putLong(GEO_UPDATETIME, updatetime).commit();
    }

    public boolean isSaveGeo() {
        checkDefault();

        long updateime = mPreferences.getLong(GEO_UPDATETIME,-1);

        if (updateime+ 1*24*60*60*1000l>System.currentTimeMillis()){

            return true;

        }else{

            return false;
        }

    }

    public void saveCountryCode(String countryCode) {
        checkDefault();
        mPreferences.edit().putString(COUNTRY_CODE, countryCode).apply();
        mPreferences.edit().putString(COUNTRY_CODE, countryCode).commit();
    }

    public String getCountryCode() {
        checkDefault();
        return mPreferences.getString(COUNTRY_CODE, "UN");
    }

    public void saveVpnState(boolean state) {
        checkDefault();
        mPreferences.edit().putBoolean(VPN_STATE, state).apply();
        mPreferences.edit().putBoolean(VPN_STATE, state).commit();
    }

    public Boolean getVpnState() {
        checkDefault();
        return mPreferences.getBoolean(VPN_STATE, false);
    }

    public boolean isSaveVpn() {
        checkDefault();

        long updateime = mPreferences.getLong(VPN_UPDATETIME,-1);

        if (updateime+ 1*24*60*60*1000l>System.currentTimeMillis()){

            return true;

        }else{

            return false;
        }

    }

    public void saveVpnUpdatetime(long updatetime) {
        checkDefault();
        mPreferences.edit().putLong(VPN_UPDATETIME, updatetime).apply();
        mPreferences.edit().putLong(VPN_UPDATETIME, updatetime).commit();
    }

    public boolean checkSpeciaCountry(){
        String country = getCountryCode().toLowerCase();
        String passCountry = "br#ca#gb#au#jp#";
        Log.d("snow", "checkSpeciaCountry country " + country);
        if(passCountry.contains(country)){
            Log.d("snow", "contain country " + country);
            return true;
        }
        return false;
    }
}
