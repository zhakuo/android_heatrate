package com.workout.base.iplogic;


import com.tsy.sdk.myokhttp.MyOkHttp;
import com.tsy.sdk.myokhttp.response.RawResponseHandler;
import com.workout.base.Logger;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class RequestIpInfo {

    private static String TAG = "GetGeo";

    public static void requestIpInfo() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10000L, TimeUnit.MILLISECONDS)
                .readTimeout(10000L, TimeUnit.MILLISECONDS)
                //其他配置
                .build();

        MyOkHttp mMyOkhttp = new MyOkHttp(okHttpClient);

        String url = "https://get.geojs.io/v1/ip/geo.json";

        mMyOkhttp.get()
                .url(url)
                .enqueue(new RawResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, String response) {
//                        Logger.d(TAG, "doGet onSuccess:" + response);
//                        Logger.d(TAG, "doGet statusCode:" + statusCode);
                        if (response!=null&&!response.equals("")){

                            try{

                                JSONObject jsonObject=new JSONObject(response);

                                if (jsonObject!=null){

                                    if (statusCode == 200){

                                        Double lat = jsonObject.getDouble("latitude");
                                        Double lon = jsonObject.getDouble("longitude");

                                        if (lat!=null&&lon!=null){

                                            IpUtils.getInstance().saveGeo(lat,lon);
                                        }

//                                        Logger.d(TAG,"get ip geo.........."+lat+"----"+lon);

//                                        Logger.d(TAG,"get ip geo.........."+jsonObject.getString("city"));

                                        String country = jsonObject.getString("country_code");

                                        if (country!=null&&!country.equals("")){

                                            IpUtils.getInstance().saveCountryCode(country);
                                        }

                                    }
                                }


                            }catch (Exception e){
                                e.printStackTrace();
                            }



                        }
                    }

                    @Override
                    public void onFailure(int statusCode, String error_msg) {
                        Logger.d(TAG, "doGet onFailure:" + error_msg);
                    }
                });
    }


}
