package com.workout.base;

import android.content.Context;

/**
 * Created by chenhewen on 2017/11/7.
 */

public interface ILifeCircleManager {

    void onCreate(Context context);

    void onDestroy(Context context);
}
