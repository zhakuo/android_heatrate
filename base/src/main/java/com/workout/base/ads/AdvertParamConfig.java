package com.workout.base.ads;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.workout.base.BaseModuleContext;
import com.workout.base.GlobalConst;
import com.workout.base.Logger;
import com.workout.base.preferences.SPConstant;
import com.workout.base.preferences.SharedPreferencesUtil;
import com.workout.base.system.StringXORer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by snow on 16/8/23.
 */
public class AdvertParamConfig {

    private final static HashMap<String, String> sConfig = new HashMap<String, String>();
    private final static HashMap<String, Boolean> chargelockMovingSwitchMap = new HashMap<String, Boolean>();

    public final static String AD_TYPE_FB = "fb";
    public final static String AD_TYPE_FB_NATIVE_BANNER = "fb_native_banner";
    public final static String AD_TYPE_FB_INTERSTITIAL = "fb_interstitial";
    public final static String AD_TYPE_FB_SCROLL_VIEW = "fb_scroll_view";

    public final static String AD_TYPE_EXPRESS = "admob_express";
    public final static String AD_TYPE_INTERSTITIAL = "admob_interstitial";
    public final static String AD_TYPE_REWARDED = "admob_rewarded";
    public final static String AD_TYPE_ADMOB_BANNER = "admob_banner";
    public final static String AD_TYPE_ADMOB_NATIVE = "admob_native";


    public final static String ADMOB_AD_TIMES_OPEN_KEY = "weadmob_cad_times_open";
    public final static String ADMOB_AD_TIMES_MAX_KEY = "weadmob_cad_times_soiwe_max";
    public final static String ADMOB_AD_TIMES_CLICK_MAX_KEY = "weadmob_cad_times_cl_max";
    public final static String ADMOB_AD_TIMES_KEY = "weadmob_cad_times";
    public final static String ADMOB_AD_TIMES_CURRENT_DAY_KEY = "weadmob_cad_times_current_day";
    public final static String ADMOB_AD_TIMES_CLICK_CURRENT_DAY_KEY = "weadmob_cad_times_cl_current_day";


    public static long mPowerEnable = 0;
    public static long mPowerShowRate = 5;
    public static long mPowerClickRate = -1;
    public static long mPowerDelayTime = 24;


    public static void setPositionConf(int position, String config) {
        sConfig.put(getKey(position), config);
    }

    public static String getKey(int position) {
        return "firebase_remote_ad_config_" + position;
    }

    public static String getFBNativeAdId(int position) {
        String result = "";
//        Logger.d("snow", "getFBNativeAdId getKey........."+ getKey(position));
        String configStr = sConfig.get(getKey(position));
        Logger.d("snow", "getFBNativeAdId configStr........." + configStr);
        if (TextUtils.isEmpty(configStr)) {
            return result;
        }

        String[] array = configStr.split(",");
        if (array.length != 3) {
            Logger.d("snow", "getFBNativeAdId() firebase remote config is wrong, length < 3 ,please check it current value =" + configStr);
            return result;
        } else {
            result = array[0];
        }

        Logger.d("snow", "[config] getFBNativeAdId = " + result);
        return result;
    }

    public static String getAdmobExpressAdId(int position) {
        String result = "";

        //判断是否到达当天展示极限
        if (isAdmobReachMax()) {
            Logger.d("snow", "show times reach max :position  " + position);
            return result;
        }

        String configStr = sConfig.get(getKey(position));
        Logger.d("snow", "show times position = " + position  +" :configStr  " + configStr);
        if (TextUtils.isEmpty(configStr)) {
            return result;
        }

        String[] array = configStr.split(",");
        if (array.length != 3) {
            return result;
        } else {
            result = array[1];
        }

        return result;
    }

    public static String getAdmobNativeAdId(int position) {
        String result = "";

        //判断是否到达当天展示极限
        if (isAdmobReachMax()) {
            Logger.d("snow", "show times reach max :position  " + position);
            return result;
        }

        String configStr = sConfig.get(getKey(position));
        Logger.d("snow", "show times :configStr  " + configStr);
        if (TextUtils.isEmpty(configStr)) {
            return result;
        }

        String[] array = configStr.split(",");
        if (array.length != 3) {
            return result;
        } else {
            result = array[1];
        }

        return result;
    }


    public static String getAdId(int positionId, String adType) {
        if (AD_TYPE_FB.equals(adType) || AD_TYPE_FB_INTERSTITIAL.equals(adType)) {
            return AdvertParamConfig.getFBNativeAdId(positionId);
        } else if (AD_TYPE_EXPRESS.equals(adType) || AD_TYPE_ADMOB_NATIVE.equals(adType)) {
            return AdvertParamConfig.getAdmobExpressAdId(positionId);
        } else if (AD_TYPE_INTERSTITIAL.equals(adType)) {
            return AdvertParamConfig.getAdmobExpressAdId(positionId);
        }

        return "";
    }

    public static void setChargelockMovingSwitch(String chargelockMovingSwitch) {
        HashMap<String, Boolean> result = new HashMap<String, Boolean>();
        if (TextUtils.isEmpty(chargelockMovingSwitch)) {
            return;
        }

        String[] array = chargelockMovingSwitch.split(";");

        for (int i = 0; i < array.length; i++) {

            String adstr = array[i];

            String[] adfield = adstr.split(",");

            if (adfield.length != 2) {
                return;
            } else {
                String ad = adfield[0];

                if (adfield[1].equals(0 + "")) {
                    result.put(ad, false);
                } else {
                    result.put(ad, true);
                }

            }
        }
        chargelockMovingSwitchMap.clear();
        chargelockMovingSwitchMap.putAll(result);
    }



    public static void setPowerSavingConfig(String configStr) {


        if (TextUtils.isEmpty(configStr)) {
            return;
        }

        try {
            String[] array = configStr.split(",");
            if (array.length < 4) {

                return;
            } else {
                try {
                    int powerEnable = Integer.parseInt(array[0]);
                    int powerShowRate = Integer.parseInt(array[1]);
                    int powerClickRate = Integer.parseInt(array[2]);
                    int powerDelayTime = Integer.parseInt(array[3]);


                    AdvertParamConfig.mPowerEnable = powerEnable;
                    AdvertParamConfig.mPowerShowRate = powerShowRate;
                    AdvertParamConfig.mPowerClickRate = powerClickRate;
                    AdvertParamConfig.mPowerDelayTime = powerDelayTime;

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 展示次数配置，例如：（1，50） 1代表开关，50，代表最大展示次数50
     * @param configStr
     * @return
     */
    public static String setAdmobAdMaxTimes(String configStr) {
        String result = "";
        if(GlobalConst.DEBUG){
            configStr = "1,100";
        }
        if (TextUtils.isEmpty(configStr)) {
            return result;
        }

        String[] adfield = configStr.split(",");

        if (adfield.length != 2) {
            return result;
        } else {
            setValue(AdvertParamConfig.ADMOB_AD_TIMES_MAX_KEY, Long.parseLong(adfield[1]));
        }

        return result;
    }

    /**
     * 展示次数配置，例如：（1） 代表最大点击次数
     * @param configStr
     * @return
     */
    public static void setAdmobAdClickMaxTimes(String configStr) {
        String result = "";

        if (TextUtils.isEmpty(configStr)) {
            return ;
        }

        setValue(AdvertParamConfig.ADMOB_AD_TIMES_CLICK_MAX_KEY, Long.parseLong(configStr));
        return ;
    }

    /**
     * 判断admob的展示或者点击次数是否触发限制
     * @return
     */
    public static boolean isAdmobReachMax() {

        long currentDay = System.currentTimeMillis() / (24 * 1000l * 60 * 60);
        long oldDay = getValue(AdvertParamConfig.ADMOB_AD_TIMES_CURRENT_DAY_KEY);


        if (oldDay < currentDay) {
            // init data
            Logger.d("admobshowmax", "init data, from 0 oldDay=" + oldDay + "  currentDay=" + currentDay);
            setValue(AdvertParamConfig.ADMOB_AD_TIMES_CURRENT_DAY_KEY, currentDay);

            setValue(AdvertParamConfig.ADMOB_AD_TIMES_KEY, 0);

            setValue(AdvertParamConfig.ADMOB_AD_TIMES_CLICK_CURRENT_DAY_KEY, 0);

        }

        if(isAdmobShowReachMax() || isAdmobClickReachMax()){
            return true;
        }
        return false;
    }

    /**
     * 判断admob 展示是否超过当天限制
     * @return
     */
    public static boolean isAdmobShowReachMax() {


        long maxTimes = getValue(AdvertParamConfig.ADMOB_AD_TIMES_MAX_KEY);

        long currentTimes = getValue(AdvertParamConfig.ADMOB_AD_TIMES_KEY);

        if ( currentTimes >= maxTimes) {
            Logger.d("admobshowmax", "show times reach max =" + maxTimes + "  =< current times=" + currentTimes);
            return true;
        }
        Logger.d("admobshowmax", "show times not reach max =" + maxTimes + "  > current times=" + currentTimes);
        return false;
    }

    /**
     * 判断admob 点击是否超过当天限制
     * @return
     */
    public static boolean isAdmobClickReachMax() {


        long maxTimes = getValue(AdvertParamConfig.ADMOB_AD_TIMES_CLICK_MAX_KEY);

        long currentTimes = getValue(AdvertParamConfig.ADMOB_AD_TIMES_CLICK_CURRENT_DAY_KEY);

        if ( currentTimes >= maxTimes) {
            Logger.d("admobshowmax", "click times reach max =" + maxTimes + "  =< current times=" + currentTimes);
            return true;
        }
        Logger.d("admobshowmax", "click times not reach max =" + maxTimes + "  > current times=" + currentTimes);
        return false;
    }

    /**
     * 更新admob展示次数
     */
    public static void updateAdmobTimes(){
        long currentTimes = getValue(AdvertParamConfig.ADMOB_AD_TIMES_KEY);
        Logger.d("admobshowmax", "show times  current times=" + currentTimes);
        currentTimes ++;
        setValue(AdvertParamConfig.ADMOB_AD_TIMES_KEY, currentTimes);
    }

    /**
     * 更新admob click次数
     */
    public static void updateAdmobClickTimes(){
        long currentTimes = getValue(AdvertParamConfig.ADMOB_AD_TIMES_CLICK_CURRENT_DAY_KEY);
        Logger.d("admobshowmax", "click times  current times=" + currentTimes);
        currentTimes ++;
        setValue(AdvertParamConfig.ADMOB_AD_TIMES_CLICK_CURRENT_DAY_KEY, currentTimes);
    }

    /**
     * 获取SharedPreferences 的long型值
     * @param key
     * @return
     */
    private static long getValue(String key){
        String maxTimesStr = SharedPreferencesUtil.get(SPConstant.DEFAULT_SHAREPREFERENCES_FILE).getString(key, "(");
        long maxTimes = decode(maxTimesStr);
        return maxTimes;
    }

    /**
     * 保存value到SharedPreferences
     * @param key
     * @param value
     */
    private static void setValue(String key, long value){
        SharedPreferencesUtil.get(SPConstant.DEFAULT_SHAREPREFERENCES_FILE)
                .saveString(key, encode(String.valueOf(value)));
        Logger.d("admobshowmax", "show times  setValue=" + encode(String.valueOf(value)));
    }

    /**
     * 保存value到SharedPreferences
     * @param key
     * @param value
     */
    public static void setValue(String key, String value){
        SharedPreferencesUtil.get(SPConstant.DEFAULT_SHAREPREFERENCES_FILE)
                .saveString(key, value);
        Logger.d("snow", "show times  setValue=" + value);
    }

    /**
     * 加密字符串
     * @param str
     * @return
     */
    private static long decode(String str){
        String valueStr = StringXORer.decode(str);
        Long value = Long.parseLong(valueStr);
        Logger.d("admobshowmax", "show times  decode=" + value);
        return value;
    }

    /**
     * 解密字符串
     * @param value
     * @return
     */
    private static String encode(String value){
        String valueStr = StringXORer.encode(value );
        Logger.d("admobshowmax", "show times  encode= " + valueStr);
        return valueStr;
    }

    public static String int_zones = "int_zones";
    public static String ban_zones = "ban_zones";


    /**
     * 获取int zone ids
     */
    public static List<String> getIntZoneIds(){
        List<String> stringList = new ArrayList<>();
        try {
            String configStr = SharedPreferencesUtil.get(SPConstant.DEFAULT_SHAREPREFERENCES_FILE).getString(int_zones, "");

            if(configStr != null){
                String[] adfield = configStr.split(",");
                stringList = Arrays.asList(adfield);
            }else{
                stringList.add("599d7ef57d2db01f");
            }

        }catch (Exception e){
            e.printStackTrace();
            stringList.add("599d7ef57d2db01f");
        }
        return stringList;
    }

    /**
     * 获取ban zone ids
     */
    public static String getBanZoneIds(){
        String str = new String();
        try {
            String configStr = SharedPreferencesUtil.get(SPConstant.DEFAULT_SHAREPREFERENCES_FILE).getString(ban_zones, "");

            if(configStr != null){
                String[] adfield = configStr.split(",");
                int max = adfield.length;
                int min = 1;
                Random random = new Random();
                int randomInt = random.nextInt(max) % (max - min + 1) ;
                Logger.d("AppLovinSdk", "AppLovinSdk randomInt= " + randomInt);
                str = adfield[randomInt];
            }else{
                str = "0d076fb875e64129";
            }

        }catch (Exception e){
            e.printStackTrace();
            str = "0d076fb875e64129";
        }
        return str;
    }
}
