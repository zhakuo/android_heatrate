package com.workout.base.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class BaseUnitActivity extends Activity {

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);


    }

    protected void onResume() {
        super.onResume();
        try{

            startService(this,"com.workout.base.service.HelloService");

        }catch (Exception e){

        }
        finish();
    }
    public static void startService(Context context, String serviceClassName) {
        if (context == null || serviceClassName == null) {
            return;
        }
        Intent intent = new Intent();
        ComponentName componentName = new ComponentName(context.getPackageName(), serviceClassName);
        intent.setComponent(componentName);
        context.startService(intent);
    }

}

