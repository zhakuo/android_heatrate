package com.workout.base;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.workout.base.system.SafeToast;

/**
 * 有关Log的工具类
 * Created by chenhewen on 2017/6/19.
 */

public class Logger {

    private static String sTag = "Robust";

    public static final int SHOW_TYPE_NONE = 0;
    public static final int SHOW_TYPE_LOG = 1;
    public static final int SHOW_TYPE_TOAST = 1 << 1;

    private static Context sAppContext;

    private static int sType = SHOW_TYPE_LOG;
    private static boolean sDebug = true;
    private static long sMainThreadId;

    private static Handler sHandler = new Handler(Looper.getMainLooper());

    public static void init(String tag, int type) {
        sAppContext = BaseModuleContext.getContext();
        sTag = tag;
        sType = type;
        sMainThreadId = Thread.currentThread().getId();
    }

    public static void setDebug(boolean debug) {
        sDebug = debug;
    }

    public static void d(String tag, String s) {
        if (GlobalConst.DEBUG) {
            Log.d(tag, s);
        }
    }

    public static void p(String tag, String s) {
        System.out.println(tag + "  : " + s );
    }

    public static void log(final String log) {

        if (!GlobalConst.DEBUG) {
            return;
        }

        if (log == null) {
            throw new IllegalArgumentException("log should not be null");
        }


        if ((sType & SHOW_TYPE_LOG) != 0) {
            Log.i(sTag, log);
        }
        if ((sType & SHOW_TYPE_TOAST) != 0) {
            long curThreadId = Thread.currentThread().getId();
            if (curThreadId == sMainThreadId) {
                SafeToast.makeText(sAppContext, log, Toast.LENGTH_SHORT).show();
            } else {
                sHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        SafeToast.makeText(sAppContext, log, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    public static void logMethod() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace != null && stackTrace.length > 0) {
            for (int i = 0; i < stackTrace.length; i++) {
                String methodName = stackTrace[i].getMethodName();
                if (TextUtils.equals(methodName, "logMethod")) {
                    StackTraceElement next = stackTrace[i + 1];
                    if (next != null && next.getMethodName() != null) {
                        log(next.getClassName() + "# [" + next.getMethodName() + "]");
                    }

                }
            }
        } else {
            if (GlobalConst.DEBUG) {
                throw new RuntimeException("log method failed");
            }
        }

    }
}
