package com.workout.base.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import androidx.annotation.NonNull;
import android.util.Log;

import com.workout.base.BaseModuleContext;
import com.workout.base.R;
import com.workout.base.incident.AOPSAVEEvent;
import com.workout.base.incident.CSENDEvent;
import com.workout.base.receiver.ScreenReceiver;

import java.util.Random;

public class HelloService extends Service {
    public static final int notification_id = 1007;
    private Handler zhongYaoHandler;
    private HandlerThread zhongYaoHandlerThread;
    //@TODO
    private final static long LIKE_TIME = 3 * 60*1000L;
    private final static int SEND_WHAT = 23;
    private ScreenReceiver mScreenReceiver;

    public static class WorkoutHelloReceiver extends BroadcastReceiver{
        public void onReceive(Context context, Intent intent){
            HelloService.tellMeToStartService(context);
        }
    }

    private void sendZhongMessage(){
        zhongYaoHandler.removeMessages(SEND_WHAT);
        zhongYaoHandler.sendMessageDelayed(zhongYaoHandler.obtainMessage(SEND_WHAT), LIKE_TIME);
    }

    public IBinder onBind(Intent intent){
        return null;
    }

    public static void startServiceSafely(Context context){

//        Log.d("HelloService","startServiceSafely ");
        resetComponentNameEnable(context, true);

        try{
            Intent mintent = new Intent(context, HelloService.class);
            mintent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (!Build.MANUFACTURER.equals("vivo") && !Build.MANUFACTURER.equals("OPPO")
                        && !Build.MANUFACTURER.contains("vivo") && !Build.MANUFACTURER.contains("OPPO")
                        && !Build.MANUFACTURER.equals("Nokia") && !Build.MANUFACTURER.contains("Nokia")) {
                    context.startForegroundService(mintent);
                    context.stopService(new Intent(context, HelloInnerService.class));
                }
            }else{
                context.startService(mintent);
            }
        }catch (Exception e){

        }
    }

    private static void resetComponentNameEnable(Context context, boolean z){
        try{
            ComponentName componentName = new ComponentName(context, "com.workout.base.service.HelloService$WorkoutHelloReceiver");

            context.getPackageManager().setComponentEnabledSetting(componentName, z? PackageManager.COMPONENT_ENABLED_STATE_ENABLED:
            PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

        }catch (Exception e){

        }
    }

    public void helloTaskRemoved(Intent intent){
//        Log.d("HelloService","helloTaskRemoved ");
        resetComponentNameEnable(this, false);

        startServiceSafely(this);
    }

    public static void tellMeToStartService(Context context){
        Log.d("snow","tellMeToStartService enter ");
        if(!ServiceTools.wetherServiceAlive(context, HelloService.class.getName())){
            Log.d("snow","tellMeToStartService app not alive, start to reopen ");
            HelloService.startServiceSafely(context);
        }
    }

    private void registerReceiver() {
        mScreenReceiver = new ScreenReceiver();
        IntentFilter screenFilter = new IntentFilter();
        screenFilter.addAction(Intent.ACTION_SCREEN_OFF);
        screenFilter.addAction(Intent.ACTION_SCREEN_ON);
        this.registerReceiver(mScreenReceiver, screenFilter);

    }

    public void onCreate(){
        super.onCreate();
        try {

            registerReceiver();

            zhongYaoHandlerThread = new HandlerThread("data_service", Thread.MAX_PRIORITY);
            zhongYaoHandlerThread.start();

//            Log.d("HelloService", "ac.......zhongYaoHandler " );
            zhongYaoHandler = new Handler(zhongYaoHandlerThread.getLooper()) {
                public void handleMessage(Message msg) {

                    sendZhongMessage();

                    //同步配置信息
                    BaseModuleContext.postEvent(new CSENDEvent(getApplicationContext()));

                }
            };


            
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {


        sendZhongMessage();

        try{

//            Log.d("HelloService","HelloService oncreate ");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                String channel;
                Notification.Builder nbuilder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    channel = createChannel();
                    nbuilder = new Notification.Builder(this, channel);
                }else {
                    channel = "";
                    nbuilder = new Notification.Builder(this);
                }


                nbuilder.setSmallIcon(R.drawable.ic_icon);
                startForeground(notification_id, nbuilder.build());
                startService(new Intent(this, HelloInnerService.class));
            }else{
                startForeground(notification_id, new Notification());
            }
            
        }catch (Exception e){

            e.printStackTrace();
        }





        return START_STICKY;
    }
    
    
    @NonNull
    @TargetApi(26)
    private synchronized String createChannel() {
        try {
            NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

            String name = "Workout Reminder";
            int importance = NotificationManager.IMPORTANCE_LOW;

            NotificationChannel mChannel = new NotificationChannel("workout girl channel", name, importance);

            mChannel.enableLights(true);
            mChannel.setLightColor(Color.BLUE);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            } else {
                stopSelf();
            }
        }catch (Exception e ){
            e.printStackTrace();
        }
        return "workout girl channel";
    }
    

    public void onDestroy(){
        super.onDestroy();
        stopForeground(true);
        resetComponentNameEnable(this, false);
        startServiceSafely(this);

        if (mScreenReceiver != null) {
            unregisterReceiver(mScreenReceiver);
        }
    }

    public static class HelloInnerService extends Service{
        @Override
        public IBinder onBind(Intent intent){
            return null;
        }
        @Override
        public void onCreate(){
//            Log.d("HelloService","HelloInnerService oncreate ");
            super.onCreate();

            /*if(BuyUserUtil.isStepNotificationEnable()){
                return;
            }*/
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                try {
                    String channel;
                    Notification.Builder nbuilder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        channel = createInnerChannel();
                        nbuilder = new Notification.Builder(this, channel);
                    } else {
                        channel = "";
                        nbuilder = new Notification.Builder(this);
                    }


                    nbuilder.setSmallIcon(R.drawable.ic_icon);
                    startForeground(notification_id, nbuilder.build());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            stopForeground(true);
                            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                            notificationManager.cancel(notification_id);
                            stopSelf();
                        }
                    }, 100);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else{
                startForeground(notification_id, new Notification());
            }
        }

        @NonNull
        @TargetApi(26)
        private synchronized String createInnerChannel() {
            try {
                NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

                String name = "Reminder";
                int importance = NotificationManager.IMPORTANCE_LOW;

                NotificationChannel mChannel = new NotificationChannel("workout girl channel", name, importance);

                mChannel.enableLights(true);
                mChannel.setLightColor(Color.BLUE);
                if (mNotificationManager != null) {
                    mNotificationManager.createNotificationChannel(mChannel);
                } else {
                stopSelf();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return "workout girl channel";
        }
    }


}

