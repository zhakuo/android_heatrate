package com.workout.base.service;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

import java.util.List;

public class ServiceTools {

    public static boolean wetherServiceAlive(Context context, String className) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceInfos = activityManager.getRunningServices(150);

        if (null == serviceInfos || serviceInfos.size() < 1) {
            return false;
        }
//        Log.d("snow","className enter " + className);
        for (int i = 0; i < serviceInfos.size(); i++) {
//            Log.d("snow","serviceInfos.get(i).service.getPackageName() enter " + serviceInfos.get(i).service.getPackageName());
//            Log.d("snow","serviceInfos.get(i).service.getClassName() enter " + serviceInfos.get(i).service.getClassName());
            if (serviceInfos.get(i).service.getPackageName().equals(context.getPackageName())&&serviceInfos.get(i).service.getClassName().contains(className)) {
                isRunning = true;

                break;
            }
        }
        return isRunning ;
    }

}
