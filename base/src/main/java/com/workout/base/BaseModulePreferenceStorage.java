package com.workout.base;

import android.content.Context;
import android.content.SharedPreferences;

import com.workout.base.global.GlobalPreferenceFileName;

public class BaseModulePreferenceStorage {

    public static SharedPreferences getBaseSp() {
        Context appContext = BaseModuleContext.getContext();
        return appContext.getSharedPreferences(GlobalPreferenceFileName.BASE_SP_FILE_NAME, Context.MODE_PRIVATE);
    }
}
