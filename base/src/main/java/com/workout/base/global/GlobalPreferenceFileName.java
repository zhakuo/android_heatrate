package com.workout.base.global;

public class GlobalPreferenceFileName {

    public static final String BASE_SP_FILE_NAME = "base_sp";

    public static final String GLOBAL_SP_FILE_NAME = "global_sp";

    public static final String APP_VERSIONS_SP_FILE_NAME = "app_versions_sp";

    public static final String REMINDER_SP_FILE_NAME = "reminder_sp";
}
