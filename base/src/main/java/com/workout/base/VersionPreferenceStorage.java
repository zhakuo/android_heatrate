package com.workout.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import com.workout.base.global.GlobalPreferenceFileName;

import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class VersionPreferenceStorage {

    public static SharedPreferences getVersionSp() {
        Context appContext = BaseModuleContext.getContext();
        return appContext.getSharedPreferences(GlobalPreferenceFileName.APP_VERSIONS_SP_FILE_NAME, Context.MODE_PRIVATE);
    }

    private static String getVersionCode(Context context) {
        String version = null;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = String.valueOf(pInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }

    public static void saveIfNewVersion() {
        Context context = BaseModuleContext.getContext();

        String versionCode = getVersionCode(context);
        Version lastVersion = getLastVersion();
        if (versionCode != null) {
            if (lastVersion == null) {
                getVersionSp().edit().putLong(versionCode, System.currentTimeMillis()).apply();
            } else {
                String savedVersionCode = lastVersion.getVersionCode();
                if (!TextUtils.equals(versionCode, savedVersionCode)) {
                    getVersionSp().edit().putLong(versionCode, System.currentTimeMillis()).apply();
                }
            }
        }
    }

    private static SortedMap<String, Long> sSortedMap;

    @SuppressWarnings("unchecked")
    public static SortedMap<String, Long> getAllVersions() {

        if (sSortedMap != null) {
            return sSortedMap;
        }

        Map<String, Long> all = (Map<String, Long>) getVersionSp().getAll();
        if (all == null || all.isEmpty()) {
            return null;
        } else {
            sSortedMap = new TreeMap<>();
        }
        Iterator<Map.Entry<String, Long>> iterator = all.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Long> next = iterator.next();
            String versionCode = next.getKey();
            Long millis = next.getValue();

            sSortedMap.put(versionCode, millis);
        }

        return sSortedMap;
    }

    public static class Version {
        private String mVersionCode;
        private Long mUpdateMillis;

        public Version(String versionCode, Long updateMillis) {
            mVersionCode = versionCode;
            mUpdateMillis = updateMillis;
        }

        public String getVersionCode() {
            return mVersionCode;
        }

        public Long getUpdateMillis() {
            return mUpdateMillis;
        }
    }

    public static Version getLastVersion() {
        SortedMap<String, Long> allVersions = getAllVersions();
        if (allVersions == null || allVersions.isEmpty()) {
            return null;
        }

        String currentVersion = allVersions.lastKey();
        return new Version(currentVersion, allVersions.get(currentVersion));
    }

    public static Version getFirstVersion() {
        SortedMap<String, Long> allVersions = getAllVersions();
        if (allVersions == null || allVersions.isEmpty()) {
            return null;
        }

        String currentVersion = allVersions.firstKey();
        return new Version(currentVersion, allVersions.get(currentVersion));
    }

    public static boolean isUpdateUser() {
        SortedMap<String, Long> allVersions = getAllVersions();
        if (allVersions == null || allVersions.isEmpty() || allVersions.size() == 1) {
            return false;
        }

        return true;
    }

}
