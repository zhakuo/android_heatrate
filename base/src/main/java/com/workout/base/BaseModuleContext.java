package com.workout.base;

import android.content.Context;
import android.os.Handler;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

public class BaseModuleContext {

    private static Context sContext;
    private static boolean sDebug;
    private static Handler sHandler = new Handler();

    public static long B_REQUEST_TIME = -1;

    private final static EventBus GLOBAL_EVENT_BUS_YD = EventBus.getDefault();

    public static void init(Context context, boolean debug) {
        sContext = context;
        sDebug = debug;
    }

    public static Context getContext() {
        return sContext;
    }

    public static boolean isDebug() {
        return sDebug;
    }


    private static Map<String, Object> getDefaultConfigMap() {
        HashMap<String, Object> defaultConfigs = new HashMap<>();
        defaultConfigs.put(RemoteConfigId.volcano_config_key, RemoteConfigId.volcano_config_value);
        return defaultConfigs;
    }


    public static void post(Runnable runnable) {
        sHandler.post(runnable);
    }

    /**
     * 使用全局EventBus post一个事件<br>
     *
     * @param event
     */
    public static void postEvent(Object event) {
//        Log.d("snow", "ac.......postEvent " );
        GLOBAL_EVENT_BUS_YD.post(event);
    }
}
