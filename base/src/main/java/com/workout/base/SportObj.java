package com.workout.base;

import android.content.Context;

import java.util.concurrent.atomic.AtomicBoolean;

public class SportObj {

    static String sp;

    static Object alObj;

    static int al;

    static String mOrg;

    static String intId;
    static String banId;

    static Context mContext;

    public static boolean isCalling = false;

    static AtomicBoolean isSoundEffectDownload = new AtomicBoolean();

    static AtomicBoolean isSoundLockDownload = new AtomicBoolean();


    public static Context getmContext() {
        return mContext;
    }

    public static void setmContext(Context mContext) {
        SportObj.mContext = mContext;
    }


    public static String getOrg() {
        return mOrg;
    }

    public static void setOrg(String mBannerId) {
        SportObj.mOrg = mBannerId;
    }


    public static String getSp() {
        return sp;
    }

    public static void setSp(String sp) {
        SportObj.sp = sp;
    }

    public static Object getAlObj() {
        return alObj;
    }

    public static void setAlObj(Object alObj) {
        SportObj.alObj = alObj;
    }

    public static int getAl() {
        return al;
    }

    public static void setAl(int al) {
        SportObj.al = al;
    }

    public static boolean isSoundEffectDownload() {
        return isSoundEffectDownload.get();
    }

    public static void setSoundEffectDownload(boolean z) {
        isSoundEffectDownload.set(z);
    }

    public static boolean isSoundLockDownLoad(){return isSoundLockDownload.get();}

    public static void setSoundLockDownload(boolean z) {
        isSoundLockDownload.set(z);
    }

    public static String getIntId() {
        return intId;
    }

    public static void setIntId(String intId) {
        SportObj.intId = intId;
    }

    public static String getBanId() {
        return banId;
    }

    public static void setBanId(String banId) {
        SportObj.banId = banId;
    }
}
