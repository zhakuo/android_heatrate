# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
-renamesourcefileattribute SourceFile

-dontwarn org.joda.convert.**


-keep class com.kochava.android.tracker.** {*;}
-dontwarn com.kochava.android.tracker.**

-obfuscationdictionary dictionary_rules.txt
-classobfuscationdictionary dictionary_rules.txt
-packageobfuscationdictionary dictionary_rules.txt

# volley
-keep class com.android.volley.** {*;}
-keep class com.android.volley.toolbox.** {*;}
-keep class com.android.volley.Response$* { *; }
-keep class com.android.volley.Request$* { *; }
-keep class com.android.volley.RequestQueue$* { *; }
-keep class com.android.volley.toolbox.HurlStack$* { *; }
-keep class com.android.volley.toolbox.ImageLoader$* { *; }
-keep class com.android.volley.**{*;}


################################ okhttp start ################################

# JSR 305 annotations are for embedding nullability information.
-dontwarn javax.annotation.**

# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*

# OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform

################################ okhttp end ################################

###################### database annotation update start ######################
-keep @interface com.pjpj.heartrate.storage.annotation.Update

-keepclassmembers class * {
    @com.pjpj.heartrate.storage.annotation.Update *;
}
###################### database annotation update end ######################




###################### sdk start ######################
# Eventbus
-keepattributes *Annotation*,InnerClasses
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

#google play service sdk
-keep public class com.google.ads.** {*;}
-keep public class com.google.android.gms.** {*;}
#内购
-keep class com.android.vending.billing.**
#facebook sdk
-keep public class com.facebook.ads.** {*;}



-keep class com.kochava.android.tracker.** {*;}


###################### applovin start ######################
-keepattributes Signature,InnerClasses,Exceptions,Annotation
-keep public class com.applovin.sdk.AppLovinSdk{ *; }
-keep public class com.applovin.sdk.AppLovin* { public protected *; }
-keep public class com.applovin.nativeAds.AppLovin* { public protected *; }
-keep public class com.applovin.adview.* { public protected *; }
-keep public class com.applovin.mediation.* { public protected *; }
-keep public class com.applovin.mediation.ads.* { public protected *; }
-keep public class com.applovin.impl.*.AppLovin { public protected *; }
-keep public class com.applovin.impl.**.*Impl { public protected *; }
-keepclassmembers class com.applovin.sdk.AppLovinSdkSettings { private java.util.Map localSettings; }
-keep class com.applovin.mediation.adapters.** { *; }
-keep class com.applovin.mediation.adapter.**{ *; }
-keep class com.applovin.communicator.**{ *; }
###################### applovin end ######################

###################### ironsource start ######################
-keepclassmembers class com.ironsource.sdk.controller.IronSourceWebView$JSInterface {
    public *;
}
-keepclassmembers class * implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator *;
}
-keep public class com.google.android.gms.ads.** {
   public *;
}
-keep class com.ironsource.adapters.** { *;
}
-dontwarn com.ironsource.mediationsdk.**
-dontwarn com.ironsource.adapters.**
-keepattributes JavascriptInterface
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

###################### ironsource end ######################

###################### anythink start ######################
-keep public class com.anythink.network.**
-keepclassmembers class com.anythink.network.** {
   public *;
}

-dontwarn com.anythink.hb.**
-keep class com.anythink.hb.**{ *;}

-dontwarn com.anythink.china.api.**
-keep class com.anythink.china.api.**{ *;}

# new in v5.6.6
-keep class com.anythink.myoffer.ui.**{ *;}
-keepclassmembers public class com.anythink.myoffer.ui.** {
   public *;
}

###################### anythink end ######################

###################### verizon start ######################
-keepclassmembers class com.verizon.ads** {
public *;
}
-keep class com.verizon.ads**
###################### verizon end ######################

###################### plugin start ######################
-keep class com.ironsource.sdk.nginx.** { *; }
-keep class * implements com.ironsource.sdk.nginx.INginxAction{
public *;
}
###################### plugin start ######################

###################### base start ######################
-keep class com.workout.base.** { *; }

###################### base start ######################

###################### volcano start ######################
-keep class * implements com.workout.volcano.ui.VolcanoController { *; }
###################### volcano start ######################

###################### legsdk start ######################
-keep class * implements com.leg.yuandu.gameutils.WorkConstants{
public *;
}
###################### legsdk end ######################


###################### androidx start ######################
-keep class androidx.viewpager.widget.PagerAdapter {*;}
-dontwarn androidx.viewpager.widget.PagerAdapter

 -keep class com.google.android.material.** {*;}
 -keep class androidx.** {*;}
 -keep public class * extends androidx.**
 -keep interface androidx.** {*;}
 -dontwarn com.google.android.material.**
 -dontnote com.google.android.material.**
 -dontwarn androidx.**
###################### androidx end ######################


###################### flurry start ######################
-dontwarn com.flurry.**
-keep class com.flurry.**{ *;}
###################### flurry end ######################

###################### vungle start ######################
-dontwarn com.vungle.**
-keep class com.vungle.**{ *;}
###################### vungle end ######################

-keep class * implements androidx.viewbinding.ViewBinding {
    *;
}

-keep class com.pjpj.heartrate.data.**{ *;}