package com.pjpj.heartrate;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.leg.yuandu.storage.PreferenceId;
import com.leg.yuandu.storage.PreferenceStorage;
import com.leg.yuandu.utils.Utils;
import com.pjpj.heartrate.base.AppConfig;
import com.pjpj.heartrate.base.BaseViewBindingActivity;
import com.pjpj.heartrate.data.SituationData;
import com.pjpj.heartrate.databinding.ActivityMainBinding;
import com.pjpj.heartrate.databinding.MainActivityBinding;
import com.pjpj.heartrate.dialogs.ExitDialog;
import com.pjpj.heartrate.dialogs.RateDialog;
import com.pjpj.heartrate.music.I.IPlay;
import com.pjpj.heartrate.music.I.IPlayView;
import com.pjpj.heartrate.music.I.PlayImpl;
import com.pjpj.heartrate.music.IMusic;
import com.pjpj.heartrate.music.MusicService;
import com.pjpj.heartrate.music.MyService;
import com.pjpj.heartrate.music.model.PlayList;
import com.pjpj.heartrate.music.model.Song;
import com.pjpj.heartrate.ui.main.MainHistoryFragment;
import com.pjpj.heartrate.ui.main.MainHomeFragment;
import com.pjpj.heartrate.ui.main.MainInfoFragment;
import com.pjpj.heartrate.ui.main.MainMineFragment;
import com.pjpj.heartrate.ui.subscribe.SubscribeActivity;
import com.pjpj.heartrate.ui.user.UserGenderActivity;
import com.pjpj.heartrate.ui.user.UserSituationActivity;
import com.pjpj.heartrate.util.AppToastHelper;
import com.pjpj.heartrate.widget.MainNavigateTabBar;
import com.workout.base.system.SafeToast;

public class MainActivity extends BaseViewBindingActivity<MainActivityBinding>  {

    public static final String TAB_PAGE_HOME = "HEART";
    public static final String TAB_PAGE_INFO = "INFO";
    public static final String TAB_PAGE_HISTORY = "HISTORY";
    public static final String TAB_PAGE_MINE = "MINE";

    private SharedPreferences mSp;
    // 进入主界面次数
    private int mAppOpenTimes;
    // 评分引导弹出次数
    private int mRatePopTimes;
    private long mPendingExitTime = 0;

    public static void show(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected StatusBarStyle initStatusBar() {
        return StatusBarStyle.FULL;
    }

    @Override
    protected void initView() {
        //SubscribeActivity.show(mActivity);

        binding.mainNavigateTabbar.addTab(MainHomeFragment.class, new MainNavigateTabBar.TabParam(R.mipmap.ic_home_tab_home, R.mipmap.ic_home_tab_home_selected, TAB_PAGE_HOME));
        binding.mainNavigateTabbar.addTab(MainInfoFragment.class, new MainNavigateTabBar.TabParam(R.mipmap.ic_home_tab_info, R.mipmap.ic_home_tab_info_selected, TAB_PAGE_INFO));
        binding.mainNavigateTabbar.addTab(MainHistoryFragment.class, new MainNavigateTabBar.TabParam(R.mipmap.ic_home_tab_history, R.mipmap.ic_home_tab_history_selected, TAB_PAGE_HISTORY));
        binding.mainNavigateTabbar.addTab(MainMineFragment.class, new MainNavigateTabBar.TabParam(R.mipmap.ic_home_tab_mine, R.mipmap.ic_home_tab_mine_selected, TAB_PAGE_MINE));

        binding.mainNavigateTabbar.setTabClickListener(new MainNavigateTabBar.OnTabClickListener() {
            @Override
            public boolean onTabClick(MainNavigateTabBar.ViewHolder holder) {
                return true;
            }
        });
        binding.mainNavigateTabbar.init(getSupportFragmentManager());
    }

    @Override
    protected void initData() {
        super.initData();

        if (!AppConfig.isGenderSet()) {
            UserGenderActivity.show(mActivity);
        } else if (!AppConfig.isSituationSet()) {
            UserSituationActivity.show(mActivity);
        }

        mSp = PreferenceStorage.getGlobalSp();

        mAppOpenTimes = mSp.getInt(PreferenceId.APP_OPEN_TIMES, 0);
        mAppOpenTimes++;
        mSp.edit().putInt(PreferenceId.APP_OPEN_TIMES, mAppOpenTimes).apply();

        mRatePopTimes = mSp.getInt(PreferenceId.RATE_POP_TIMES, 0);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (mAppOpenTimes < 2 && mRatePopTimes < 1) {
                mRatePopTimes++;
                mSp.edit().putInt(PreferenceId.RATE_POP_TIMES, mRatePopTimes).apply();

                RateDialog dialog = new RateDialog(MainActivity.this);
                dialog.show();
            } else {
                if (Utils.isInstallDaysAfter(1)) {
                    try {
                        ExitDialog dialog = new ExitDialog(this);
                        dialog.setOnDialogListener(new ExitDialog.Listener() {
                            @Override
                            public void onOkClicked(Dialog dialog) {
                                finish();
                            }
                        });
                        dialog.show();
                    }catch (Exception e){

                    }
                } else {
                    long now = SystemClock.elapsedRealtime();
                    if (now - mPendingExitTime <= 2500) {
                        finish();
                    } else {
                        mPendingExitTime = now;
                        SafeToast.makeText(this, R.string.press_again_to_exit, Toast.LENGTH_SHORT).show();
                    }
                }

            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}