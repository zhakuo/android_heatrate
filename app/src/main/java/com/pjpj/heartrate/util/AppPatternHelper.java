package com.pjpj.heartrate.util;

import java.util.regex.Pattern;

/**
 * Des:
 * Create by: ye
 * On:        2021\02\24 9:27
 * Email:     yeshieh@163.com
 */
public class AppPatternHelper {
    public static boolean isEmail(String email) {
        return Pattern.compile("[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?").matcher(email).matches();
    }

    public static boolean isRegisterPwdInvalid(String pwd){
        String pwdPattern = "^(?=.*\\d)(?=.*[A-Za-z])[a-zA-Z0-9]{8,}$";
        return Pattern.compile(pwdPattern).matcher(pwd).matches();
    }

    public static boolean isNumber(String str) {
        return Pattern.compile("^([-+])?\\d+(\\.\\d+)?$").matcher(str).matches();
    }
}
