package com.pjpj.heartrate.util.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import java.io.Reader;
import java.lang.reflect.Type;

/**
 * Des:       GsonConvert
 * Create by: m122469119
 * On:        2018/4/7 11:29
 * Email:     122469119@qq.com
 */
public class GsonConvert {

    private static Gson GSON_INSTANCE;

    public synchronized static Gson create() {
        if (GSON_INSTANCE == null)
            GSON_INSTANCE = createGson();
        return GSON_INSTANCE;
    }


    private static Gson createGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory());
        // gsonBuilder.registerTypeAdapterFactory(new NullIntergerToEmptyAdapterFactory());
        return gsonBuilder.create();
    }

    public static <T> T fromJson(String json, Class<T> type) throws JsonIOException, JsonSyntaxException {
        return create().fromJson(json, type);
    }

    public static <T> T fromJson(String json, Type type) {
        return create().fromJson(json, type);
    }

    public static <T> T fromJson(JsonReader reader, Type typeOfT) throws JsonIOException, JsonSyntaxException {
        return create().fromJson(reader, typeOfT);
    }

    public static <T> T fromJson(Reader json, Class<T> classOfT) throws JsonSyntaxException, JsonIOException {
        return create().fromJson(json, classOfT);
    }

    public static <T> T fromJson(Reader json, Type typeOfT) throws JsonIOException, JsonSyntaxException {
        return create().fromJson(json, typeOfT);
    }

    public static String toJson(Object src) {
        return create().toJson(src);
    }

    public static String toJson(Object src, Type typeOfSrc) {
        return create().toJson(src, typeOfSrc);
    }


    public static <B> B modelA2B(Object modelA, Class<B> bClass) {
        try {

            String gsonA = create().toJson(modelA);
            B instanceB = create().fromJson(gsonA, bClass);

            return instanceB;
        } catch (Exception e) {
            return null;
        }
    }

}