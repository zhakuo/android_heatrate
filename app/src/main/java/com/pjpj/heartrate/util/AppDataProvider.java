package com.pjpj.heartrate.util;

import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Des:
 * Create by: ye
 * On:        2021\07\14 11:26
 * Email:     yeshieh@163.com
 */
public class AppDataProvider {
    public static final String APP_DATA = "Mock/noises.json";
    public static final String APP_DATA_FILE = "noises";
    public static final String APP_DATA_FILE_DECRYPT = "noises_decrypt";

    private static final AppDataProvider INSTANCE = new AppDataProvider();
    private String mNoiseDataStr;
//    private MusicList mOriginMusicListData;
//    private List<MusicList.MusicListData> mMusicListData;
//    private List<MusicData> mAllMusicListData;
//    private List<MusicData> mHomeMusicListData;
//    private List<MusicData> mFreeMusicListData;
//    private List<CustomSongList> mMyCustomListData;

    public static AppDataProvider getInstance() {
        return INSTANCE;
    }

    public void initData() {
//        //如果文件不存在就拷贝到存储目录下
//        AppFileOperateHelper.copyAppFileFromAssets(APP_DATA, APP_DATA_FILE_DECRYPT);
//        //从存储目录下读取文件
//        String encrypt = AppFileOperateHelper.getStringFromFilesDirFile(APP_DATA_FILE_DECRYPT);
//        mNoiseDataStr = EncryptHelper.decryptPassword(encrypt);
//
//        if (!TextUtils.isEmpty(mNoiseDataStr)) {
//            mOriginMusicListData = GsonConvert.fromJson(mNoiseDataStr, MusicList.class);
//            if (mOriginMusicListData != null) {
//                mMusicListData = mOriginMusicListData.data;
//            }
//        }
    }

//    public String getAssetStr(){
//        return mNoiseDataStr;
//    }
//
//    public MusicList getOriginMusicListData() {
//        if (mOriginMusicListData == null) {
//            initData();
//        }
//        if (mOriginMusicListData == null) {
//            return null;
//        }
//        return mOriginMusicListData;
//    }
//
//    public MusicList.MusicListData getTabListData(String title) {
//        if (mOriginMusicListData == null) {
//            initData();
//        }
//        if (mOriginMusicListData == null) {
//            return null;
//        }
//        for (int i = 0; i < mOriginMusicListData.data.size(); i++) {
//            MusicList.MusicListData listData = mOriginMusicListData.data.get(i);
//            if (listData.title.equals(title)) {
//                return listData;
//            }
//        }
//        return null;
//    }
//
//    public List<MusicData> getHomeListData() {
//        if (mHomeMusicListData != null) {
//            return mHomeMusicListData;
//        }
//        if (mMusicListData == null) {
//            initData();
//        }
//        if (mMusicListData == null) {
//            return null;
//        }
//        List<MusicData> homeListData = new ArrayList<>();
//        label:
//        for (int i = 0; i < mMusicListData.size(); i++) {
//            MusicList.MusicListData listData = mMusicListData.get(i);
//            if (listData != null) {
//                if ("All".equals(listData.title)) {
//                    for (int j = 0; j < listData.data.size(); j++) {
//                        homeListData.add(listData.data.get(j));
//                        if (j >= 5) {
//                            break label;
//                        }
//                    }
//                }
//            }
//        }
//        mHomeMusicListData = homeListData;
//        return homeListData;
//    }
//
//    /**
//     * 获取所有免费声音
//     *
//     * @return
//     */
//    public List<MusicData> getAllFreeListData() {
//        if (mFreeMusicListData != null) {
//            return mFreeMusicListData;
//        }
//        if (mMusicListData == null) {
//            initData();
//        }
//        if (mMusicListData == null) {
//            return null;
//        }
//        List<MusicData> freeListData = new ArrayList<>();
//        for (int i = 0; i < mMusicListData.size(); i++) {
//            MusicList.MusicListData listData = mMusicListData.get(i);
//            if (listData != null) {
//                if ("All".equals(listData.title)) {
//                    for (int j = 0; j < listData.data.size(); j++) {
//                        MusicData data = listData.data.get(j);
//                        if (!data.isIsVip()) {
//                            freeListData.add(data);
//                        }
//                    }
//                    break;
//                }
//            }
//        }
//        mFreeMusicListData = freeListData;
//        return freeListData;
//    }
//
//    /**
//     * 获取所有声音
//     *
//     * @return
//     */
//    public List<MusicData> getAllListData() {
//        if (mAllMusicListData != null) {
//            return mAllMusicListData;
//        }
//        if (mMusicListData == null) {
//            initData();
//        }
//        if (mMusicListData == null) {
//            return null;
//        }
//        for (int i = 0; i < mMusicListData.size(); i++) {
//            MusicList.MusicListData listData = mMusicListData.get(i);
//            if (listData != null) {
//                if ("All".equals(listData.title)) {
//                    mAllMusicListData = listData.data;
//                    break;
//                }
//            }
//        }
//        return mAllMusicListData;
//    }
//
//    public MusicData getMusicData(int id) {
//        if (mMusicListData == null) {
//            initData();
//        }
//        if (mMusicListData == null) {
//            return null;
//        }
//        if (mAllMusicListData == null) {
//            for (int i = 0; i < mMusicListData.size(); i++) {
//                MusicList.MusicListData listData = mMusicListData.get(i);
//                if (listData != null) {
//                    if ("All".equals(listData.title)) {
//                        mAllMusicListData = listData.data;
//                        break;
//                    }
//                }
//            }
//        }
//        for (int j = 0; j < mAllMusicListData.size(); j++) {
//            MusicData data = mAllMusicListData.get(j);
//            if (data.getTypeid() == id) {
//                return data;
//            }
//        }
//        return null;
//    }
//
//
//    public static ArrayList<String> getHours() {
//        ArrayList<String> resumeInfoParams = new ArrayList<>();
//        for (int i = 0; i < 24; i++) {
//            resumeInfoParams.add(getFull0(i));
//        }
//        return resumeInfoParams;
//    }
//
//    public static ArrayList<String> getMinutes() {
//        ArrayList<String> resumeInfoParams = new ArrayList<>();
//        for (int i = 0; i < 60; i++) {
//            resumeInfoParams.add(getFull0(i));
//        }
//        return resumeInfoParams;
//    }
//
//    public static ArrayList<String> getNapMinutes() {
//        ArrayList<String> resumeInfoParams = new ArrayList<>();
//        resumeInfoParams.add("1");
//        for (int i = 5; i <= 120; i++) {
//            if (i <= 60) {
//                if (i % 5 == 0) {
//                    resumeInfoParams.add(String.valueOf(i));
//                }
//            } else {
//                if (i % 10 == 0) {
//                    resumeInfoParams.add(String.valueOf(i));
//                }
//            }
//        }
//        return resumeInfoParams;
//    }
//
//    public static ArrayList<String> getFocusMinutes() {
//        ArrayList<String> resumeInfoParams = new ArrayList<>();
//        resumeInfoParams.add("1");
//        for (int i = 5; i <= 120; i++) {
//            if (i <= 60) {
//                if (i % 5 == 0) {
//                    resumeInfoParams.add(String.valueOf(i));
//                }
//            } else {
//                if (i % 10 == 0) {
//                    resumeInfoParams.add(String.valueOf(i));
//                }
//            }
//        }
//        return resumeInfoParams;
//    }
//
//    public static ArrayList<String> getBreathMinutes() {
//        ArrayList<String> resumeInfoParams = new ArrayList<>();
//        for (int i = 1; i <= 15; i++) {
//            resumeInfoParams.add(String.valueOf(i));
//        }
//        return resumeInfoParams;
//    }
//
//    public List<CustomSongList> getCustomSongList() {
//        if (mMyCustomListData != null) {
//            return mMyCustomListData;
//        }
//        mMyCustomListData = new ArrayList<>();
//        String json = AppConfig.getMyCustomMusic();
//        if (!TextUtils.isEmpty(json)) {
//            mMyCustomListData = GsonConvert.fromJson(json, new TypeToken<List<CustomSongList>>() {
//            }.getType());
//        }
//        return mMyCustomListData;
//    }
//
//    public void saveCustomSong(CustomSongList customSongList) {
//        List<CustomSongList> cacheList = getCustomSongList();
//        if (cacheList == null) {
//            cacheList = new ArrayList<>();
//        }
//        cacheList.remove(customSongList);
//        cacheList.add(0, customSongList);
//        AppConfig.setMyCustomMusic(cacheList);
//    }
//
//    public void deleteCustomSong(CustomSongList customSongList) {
//        List<CustomSongList> cacheList = getCustomSongList();
//        if (cacheList == null) {
//            cacheList = new ArrayList<>();
//        }
//        cacheList.remove(customSongList);
//        AppConfig.setMyCustomMusic(cacheList);
//    }
//
//    public String getRandomListCover() {
//        int random = new Random().nextInt(11) + 1;
//        return "file:///android_asset/newsound/newsound" + random + ".jpg";
//    }
//
//    public int getRandomListId() {
//        return new Random().nextInt(Integer.MAX_VALUE);
//    }
//
//    public static String getFull0(int number) {
//        return number >= 10 ? String.valueOf(number) : "0" + number;
//    }
}
