package com.pjpj.heartrate.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Des:
 * Create by: m122469119
 * On:        2018\07\02 17:22
 * Email:     122469119@qq.com
 */
public class AppFileOperateHelper {
    public final static int FILE_BUFFER_MB_LENGTH = 1 * 1024 * 1024; // 1MB

    private static Context sContext;

    private AppFileOperateHelper() {

    }

    public static synchronized void init(Context context) {
        if (context != null) {
            sContext = context.getApplicationContext();
        }
    }

    public static Context getContext() {
        return sContext;
    }

    /**
     * 获取文件的存储文件夹,会随着App删除而删除
     * /data/data/com.mix.filetext.filetext/files/
     **/
    public static String getAppFilesDirPath() {
        return getContext().getFilesDir().getAbsolutePath() + File.separator;

    }


    /**
     * 获取文件路径
     *
     * @param fileName
     * @return
     */
    public static String getAppFilesDirFilePath(String fileName) {
        return getAppFilesDirPath() + fileName;
    }


    /**
     * 获取媒体文件的保存路径
     * /storage/emulated/0/xx招聘/
     * /data/data/包名/files/xx招聘/
     *
     * @return
     */
    public static String getAppMediaDirPath() {
        //  return getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath() + File.separator;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File externalStorageAppImgDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + File.separator
                    + AppSystemHelper.getAppName(getContext()));
            if (!externalStorageAppImgDir.exists()) {
                boolean mkdirs = externalStorageAppImgDir.mkdirs();
                if (!mkdirs) {
                    File appImgDir = new File(getContext().getFilesDir().getAbsolutePath()
                            + File.separator
                            + Environment.DIRECTORY_PICTURES
                            + File.separator
                            + AppSystemHelper.getAppName(getContext()));
                    if (!appImgDir.exists()) {
                        appImgDir.mkdirs();
                    }
                    return appImgDir.getAbsolutePath() + File.separator;
                }
            }
            return externalStorageAppImgDir.getAbsolutePath() + File.separator;

        } else {
            File appImgDir = new File(getContext().getFilesDir().getAbsolutePath()
                    + File.separator
                    + Environment.DIRECTORY_PICTURES
                    + File.separator
                    + AppSystemHelper.getAppName(getContext()));
            if (!appImgDir.exists()) {
                appImgDir.mkdirs();
            }
            return appImgDir.getAbsolutePath() + File.separator;

        }
    }

    public static String getAppMediaDirFilePath(String fileName) {
        return getAppMediaDirPath() + fileName;
    }

    /**
     * 获取下载文件的保存路径
     * /storage/emulated/0/Download/xx招聘/
     * /data/data/包名/files/xx招聘/
     *
     * @return
     */
    public static String getAppDownloadDirPath() {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File externalStorageAppImgDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS + File.separator
                    + AppSystemHelper.getAppName(getContext()));
            if (!externalStorageAppImgDir.exists()) {
                boolean mkdirs = externalStorageAppImgDir.mkdirs();
                if (!mkdirs) {
                    File appImgDir = new File(getContext().getFilesDir().getAbsolutePath()
                            + File.separator
                            + Environment.DIRECTORY_DOWNLOADS
                            + File.separator
                            + AppSystemHelper.getAppName(getContext()));
                    if (!appImgDir.exists()) {
                        appImgDir.mkdirs();
                    }
                    return appImgDir.getAbsolutePath() + File.separator;
                }
            }
            return externalStorageAppImgDir.getAbsolutePath() + File.separator;

        } else {
            File appImgDir = new File(getContext().getFilesDir().getAbsolutePath()
                    + File.separator
                    + Environment.DIRECTORY_DOWNLOADS
                    + File.separator
                    + AppSystemHelper.getAppName(getContext()));
            if (!appImgDir.exists()) {
                appImgDir.mkdirs();
            }
            return appImgDir.getAbsolutePath() + File.separator;

        }
    }


    /**
     * 从文件中获取字符串
     *
     * @param fileName
     * @return
     */
    public static String getStringFromFilesDirFile(String fileName) {
        File jsonFile = new File(getAppFilesDirFilePath(fileName));
        FileInputStream fis = null;
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        try {
            fis = new FileInputStream(jsonFile);
            FileChannel channel = fis.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(1 << 13);
            int i1;
            while ((i1 = channel.read(buffer)) != -1) {
                buffer.flip();
                bao.write(buffer.array(), 0, i1);
                buffer.clear();
            }

            return bao.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(fis, bao);
        }
        return "";
    }


    /**
     * 将字符串写入文件(带删除)
     *
     * @param str
     * @param fileName
     */
    public static void writeStringToFilesDirFile(String str, String fileName) {
        File saveFile = new File(getAppFilesDirFilePath(fileName));
        if (saveFile.exists()) {
            saveFile.delete();
        }
        InputStream is = null;
        BufferedOutputStream fos = null;
        try {
            is = new ByteArrayInputStream(str.getBytes());
            fos = new BufferedOutputStream(new FileOutputStream(saveFile));
            byte[] buffer = new byte[1024];
            int count = 0;
            while ((count = is.read(buffer)) > 0) {
                fos.write(buffer, 0, count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(is, fos);

        }
    }


    /**
     * 路径拷贝到路径
     *
     * @param copyFilePath
     * @param saveFileName
     * @return
     */
    public boolean copyFile(String copyFilePath, String saveFileName) {
        BufferedInputStream inputStream = null;
        BufferedOutputStream outputStream = null;
        try {
            inputStream = new BufferedInputStream(new FileInputStream(new File(copyFilePath)));
            outputStream = new BufferedOutputStream(new FileOutputStream(new File(getAppFilesDirFilePath(saveFileName))));
            byte[] buffer = new byte[1024 * 4];
            int len;
            while ((len = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len);
            }
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(inputStream, outputStream);
        }
        return true;
    }

    /**
     * 文件拷贝到路径
     *
     * @param copyFile
     * @param saveFileName
     * @return
     */
    public static boolean copyFile(File copyFile, String saveFileName) {
        BufferedInputStream inputStream = null;
        BufferedOutputStream outputStream = null;
        try {
            inputStream = new BufferedInputStream(new FileInputStream(copyFile));
            outputStream = new BufferedOutputStream(new FileOutputStream(new File(getAppFilesDirFilePath(saveFileName))));
            byte[] buffer = new byte[1024 * 4];
            int len;
            while ((len = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len);
            }
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(inputStream, outputStream);
        }
        return true;
    }


    /**
     * 文件拷贝到文件
     *
     * @param srcFile
     * @param saveFile
     * @return
     */
    public static boolean copyFile(final File srcFile, final File saveFile) {
        BufferedInputStream inputStream = null;
        BufferedOutputStream outputStream = null;
        try {
            inputStream = new BufferedInputStream(new FileInputStream(srcFile));
            outputStream = new BufferedOutputStream(new FileOutputStream(saveFile));
            byte[] buffer = new byte[1024 * 4];
            int len;
            while ((len = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len);
            }
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(inputStream, outputStream);
        }
        return true;
    }


    /**
     * assets文件拷贝到内部存储
     *
     * @param assetsName
     * @param saveFileName
     */
    public static void copyAppFileFromAssets(String assetsName, String saveFileName) {
        File saveFile = new File(getAppFilesDirFilePath(saveFileName));
        if (!saveFile.exists()) {
            InputStream is = null;
            BufferedOutputStream fos = null;
            try {
                is = getContext().getResources().getAssets().open(assetsName);
                fos = new BufferedOutputStream(new FileOutputStream(saveFile));
                byte[] buffer = new byte[1024];
                int count;
                while ((count = is.read(buffer)) > 0) {
                    fos.write(buffer, 0, count);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                close(is, fos);
            }
        }
    }


    /**
     * 保存图片并返回路径
     *
     * @param mBitmap
     * @param filename
     * @return
     */
    public static String saveBitmap(Bitmap mBitmap, String filename) {
        File filePic = new File(getAppFilesDirFilePath(filename));
        if (filePic.exists()) {
            filePic.delete();
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filePic);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            close(fos);
        }
        return filePic.getAbsolutePath();
    }


    /**
     * 删除文件
     *
     * @param filePath
     * @return
     */
    public static boolean deleteFile(final String filePath) {
        return deleteFile(new File(filePath));
    }

    /**
     * 删除文件
     *
     * @param file
     * @return
     */
    public static boolean deleteFile(final File file) {
        return file != null && (!file.exists() || file.isFile() && file.delete());
    }

    /**
     * 关闭流
     *
     * @param closeables
     */
    public static void close(Closeable... closeables) {
        if (closeables == null || closeables.length == 0)
            return;
        for (Closeable closeable : closeables) {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static String getRealFilePath(final Uri uri) {
        if (null == uri)
            return null;
        final String scheme = uri.getScheme();
        String data = null;
        if (scheme == null)
            data = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            data = uri.getPath();
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            Cursor cursor = sContext.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    if (index > -1) {
                        data = cursor.getString(index);
                    }
                }
                cursor.close();
            }
        }
        return data;
    }


    public static byte[] file2Byte(File file) {
        byte[] buffer = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }

}
