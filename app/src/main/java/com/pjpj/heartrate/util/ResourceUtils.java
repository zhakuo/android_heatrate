package com.pjpj.heartrate.util;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;

/**
 * Des:
 * Create by: ye
 * On:        2021\07\16 15:42
 * Email:     yeshieh@163.com
 */
public class ResourceUtils {
    public static final String RES_ID = "id";
    public static final String RES_STRING = "string";
    public static final String RES_DRAWABLE = "drawable";
    public static final String RES_MIPMAP = "mipmap";
    public static final String RES_LAYOUT = "layout";
    public static final String RES_STYLE = "style";
    public static final String RES_COLOR = "color";
    public static final String RES_DIMEN = "dimen";
    public static final String RES_ANIM = "anim";
    public static final String RES_MENU = "menu";
    /**
     * 获取资源文件ID
     * @param context
     * @param resName
     * @param defType
     * @return
     */
    public static int getResId(Context context, String resName, String defType){
        return context.getResources().getIdentifier(resName, defType, context.getPackageName());
    }

    public static byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, output);
        if (needRecycle) {
            bmp.recycle();
        }

        byte[] result = output.toByteArray();
        try {
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
