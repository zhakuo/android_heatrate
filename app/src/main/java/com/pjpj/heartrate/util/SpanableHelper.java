package com.pjpj.heartrate.util;

import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Des:       SpanableHelper
 * Create by: m122469119
 * On:        2018/7/17 14:34
 * Email:     122469119@qq.com
 */
public class SpanableHelper {

    /**
     * 关键字高亮变色
     *
     * @param color    变化的色值
     * @param text     文字
     * @param keywords 文字中的关键字
     * @return
     */
    public static SpannableString setLightKeyWord(int color, String text, String... keywords) {
        SpannableString s = new SpannableString(text);
        for (String keyword : keywords) {
            try {
                Pattern p = Pattern.compile(keyword);
                Matcher m = p.matcher(s);
                while (m.find()) {
                    int start = m.start();
                    int end = m.end();
                    s.setSpan(new ForegroundColorSpan(color), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            } catch (Exception e) {
                s = new SpannableString(text);
            }
        }

        return s;
    }

    /**
     * 关键字高亮变色
     *
     * @param color    变化的色值
     * @param s        格式化
     * @param keywords 文字中的关键字
     * @return
     */
    public static SpannableString setLightKeyWord(int color, SpannableString s, String... keywords) {
        for (String keyword : keywords) {
            try {
                Pattern p = Pattern.compile(keyword);
                Matcher m = p.matcher(s);
                while (m.find()) {
                    int start = m.start();
                    int end = m.end();
                    s.setSpan(new ForegroundColorSpan(color), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            } catch (Exception e) {
            }
        }

        return s;
    }

    /**
     * 关键字 字体大小
     *
     * @param size     字体大小
     * @param s        格式化
     * @param keywords 文字中的关键字
     * @return
     */
    public static SpannableString setKeyWordTextSize(int size, SpannableString s, String... keywords) {
        for (String keyword : keywords) {
            try {
                Pattern p = Pattern.compile(keyword);
                Matcher m = p.matcher(s);
                while (m.find()) {
                    int start = m.start();
                    int end = m.end();
                    s.setSpan(new AbsoluteSizeSpan(size, true), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            } catch (Exception e) {
            }
        }

        return s;
    }

    public static String replaceBlank(String str) {
        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n|\r\n|");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }

    public static SpannableStringBuilder setKeyWordClickable(SpannableStringBuilder style, String keyword, final int color, ClickableSpan clickableSpan) {
        try {
            Pattern p = Pattern.compile(keyword);
            Matcher m = p.matcher(style);
            while (m.find()) {
                int start = m.start();
                int end = m.end();
                style.setSpan(clickableSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                style.setSpan(new ForegroundColorSpan(color), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        } catch (Exception e) {
        }
        return style;
    }
}
