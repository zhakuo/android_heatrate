/**
 *
 */
package com.pjpj.heartrate.util;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.pjpj.heartrate.R;

public class AppToastHelper {

    private static Context mContext;
    private static Toast mLastToast;
    private static int mPaddingVertical;
    private static int mPaddingHorizontal;

    public static void init(Context context) {
        mContext = context.getApplicationContext();
        mPaddingVertical = UIHelper.dpToPx(22);
        mPaddingHorizontal = UIHelper.dpToPx(12);
    }

    public static synchronized void show(int resId) {
        if (mContext == null) {
            throw new IllegalArgumentException("ToastUtils Context is not init");
        }
        show(mContext.getString(resId));
    }

    public static synchronized void show(String message) {
        if (mContext == null) {
            throw new IllegalArgumentException("ToastUtils Context is not init");
        }
        TextView mTvToast = new TextView(mContext);// 显示的提示文字
        mTvToast.setText(message);
        mTvToast.setTextColor(Color.WHITE);
        mTvToast.setBackgroundResource(R.drawable.bg_toast);
        mTvToast.setShadowLayer(2.75f, 0, 0, Color.parseColor("#BB000000"));
        mTvToast.setPadding(80, mPaddingHorizontal, 80, mPaddingHorizontal);
        Toast mToast = new Toast(mContext);
        mToast.setGravity(Gravity.CENTER_VERTICAL, 0, mPaddingVertical);
        mToast.setView(mTvToast);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mTvToast.setText(message);
        try {
            if (mLastToast != null) {
                mLastToast.cancel();
            }
            mToast.show();
            mLastToast = mToast;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showLong(String message) {
        if (mContext == null) {
            throw new IllegalArgumentException("ToastUtils Context is not init");
        }
        TextView mTvToast = new TextView(mContext);// 显示的提示文字
        mTvToast.setText(message);
        mTvToast.setTextColor(Color.WHITE);
        mTvToast.setBackgroundResource(R.drawable.bg_toast);
        mTvToast.setShadowLayer(2.75f, 0, 0, Color.parseColor("#BB000000"));
        mTvToast.setPadding(80, 20, 80, 20);

        Toast mToast = new Toast(mContext);
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.BOTTOM, 0, 250);
        mToast.setView(mTvToast);
        mToast.setDuration(Toast.LENGTH_LONG);
        mTvToast.setText(message);
        try {
            if (mLastToast != null) {
                mLastToast.cancel();
            }
            mToast.show();
            mLastToast = mToast;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
