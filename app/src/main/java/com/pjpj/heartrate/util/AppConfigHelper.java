package com.pjpj.heartrate.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Des:
 * Create by: ye
 * On:        2021\01\26 14:48
 * Email:     yeshieh@163.com
 */
public final class AppConfigHelper {

    private static final String APP_CONFIG_FILE = "app_config_file";

    private static SharedPreferences mPref;

    private AppConfigHelper() {

    }

    public static void init(Context context) {
        mPref = context.getSharedPreferences(APP_CONFIG_FILE, Context.MODE_PRIVATE);
    }

    /**
     * 设置字符串
     *
     * @param key
     * @param value
     */
    public static void setValue(String key, String value) {
        mPref.edit().putString(key, value).apply();
    }

    /**
     * 獲取字符串
     *
     * @param key
     * @param defValue
     * @return
     */
    public static String getValue(String key, String defValue) {
        return mPref.getString(key, defValue);
    }


    /**
     * 设置float类型
     *
     * @param key
     * @param value
     */
    public static void setValue(String key, float value) {
        mPref.edit().putFloat(key, value).apply();
    }


    /**
     * 获取float类型
     *
     * @param key
     * @param defValue
     * @return
     */
    public static float getValue(String key, float defValue) {
        return mPref.getFloat(key, defValue);
    }


    /**
     * 设置boolean类型
     *
     * @param key
     * @param value
     */
    public static void setValue(String key, boolean value) {

        mPref.edit().putBoolean(key, value).apply();
    }

    /**
     * 获取boolean类型
     *
     * @param key
     * @param defValue
     * @return
     */
    public static boolean getValue(String key, boolean defValue) {

        return mPref.getBoolean(key, defValue);
    }


    /**
     * 设置int类型
     *
     * @param key
     * @param value
     */
    public static void setValue(String key, int value) {
        mPref.edit().putInt(key, value).apply();
    }


    /**
     * 获取int类型
     *
     * @param key
     * @param defValue
     * @return
     */
    public static int getValue(String key, int defValue) {

        return mPref.getInt(key, defValue);
    }

    /**
     * 设置long类型
     *
     * @param key
     * @param value
     */
    public static void setValue(String key, long value) {
        mPref.edit().putLong(key, value).apply();
    }

    /**
     * 获取long类型
     *
     * @param key
     * @param defValue
     * @return
     */
    public static long getValue(String key, long defValue) {
        return mPref.getLong(key, defValue);
    }

    /**
     * 设置double类型
     *
     * @param key
     * @param value
     */
    public static void setValue(String key, double value) {
        mPref.edit().putLong(key, Double.doubleToLongBits(value)).apply();
    }

    /**
     * 獲取double类型
     *
     * @param key
     * @param defValue
     * @return
     */
    public static double getValue(String key, double defValue) {
        return Double.longBitsToDouble(mPref.getLong(key, Double.doubleToLongBits(defValue)));
    }


}
