package com.pjpj.heartrate.util;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * Des:       CountDownTimerComponent
 * Create by: m122469119
 * On:        2018/8/21 10:31
 * Email:     122469119@qq.com
 */
public class CountDownTimerComponent implements LifecycleObserver {

    protected Context mContext;
    protected CountDownTimer mCountDownTimer;
    private long mRemainTime;
    private long mLastInterval;

    public interface CountDownTimerListener {
        void onStart();

        /**
         * Callback fired on regular interval.
         *
         * @param millisUntilFinished The amount of time until finished.
         */
        void onTick(long millisUntilFinished);

        /**
         * Callback fired when the time is up.
         */
        void onFinish();

    }

    private CountDownTimerListener mCountDownTimerListener;

    public CountDownTimerListener getCountDownTimerListener() {
        return mCountDownTimerListener;
    }

    public void setCountDownTimerListener(CountDownTimerListener countDownTimerListener) {
        mCountDownTimerListener = countDownTimerListener;
    }

    public CountDownTimerComponent(long millisInFuture, long countDownInterval) {
        mRemainTime = millisInFuture;
        mLastInterval = countDownInterval;
        mCountDownTimer = new CountDownTimer(millisInFuture, countDownInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
                mRemainTime = millisUntilFinished;
                Log.e("CountDownTimerComponent", "onTick:" + millisUntilFinished);
                if (mCountDownTimerListener != null) {
                    mCountDownTimerListener.onTick(millisUntilFinished);
                }
            }

            @Override
            public void onFinish() {
                mRemainTime = -1;
                if (mCountDownTimerListener != null) {
                    mCountDownTimerListener.onFinish();
                }
            }
        };

    }

    public void pause() {
        mCountDownTimer.cancel();
    }

    public void resume() {
        if (mRemainTime > 0) {
            mCountDownTimer = new CountDownTimer(mRemainTime, mLastInterval) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mRemainTime = millisUntilFinished;
                    if (mCountDownTimerListener != null) {
                        mCountDownTimerListener.onTick(millisUntilFinished);
                    }
                }

                @Override
                public void onFinish() {
                    mRemainTime = -1;
                    if (mCountDownTimerListener != null) {
                        mCountDownTimerListener.onFinish();
                    }
                }
            };
            mCountDownTimer.start();
        }
    }

    public void start() {
        if (mCountDownTimerListener != null) {
            mCountDownTimerListener.onStart();
        }
        mCountDownTimer.start();
    }

    public void cancel() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    void OnDestory() {
        mCountDownTimer.cancel();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void onStop() {
        mCountDownTimer.cancel();
    }
}
