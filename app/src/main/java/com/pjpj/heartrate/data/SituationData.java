package com.pjpj.heartrate.data;

import com.pjpj.heartrate.App;
import com.pjpj.heartrate.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\25 17:26
 * Email:     yeshieh@163.com
 */
public class SituationData implements Serializable {

    public SituationData(int sstatus, String statusStr,
                         int min, DiagnosisType minDiagnosis, String minResultDiagnosis, String minHypertension, String minCardiopulmonary, String minHistory, int minSuggest1, int minSuggest2,
                         int min1, int max, DiagnosisType diagnosis, String resultDiagnosis, String hypertension, String cardiopulmonary, String history, int suggest1, int suggest2,
                         int lMin, int lMax, DiagnosisType lDiagnosis, String lResultDiagnosis, String lHypertension, String lCardiopulmonary, String llHistory, int lSuggest1, int lSuggest2,
                         int max1, DiagnosisType maxDiagnosis, String maxResultDiagnosis, String maxHypertension, String maxCardiopulmonary, String maxHistory, int maxSuggest1, int maxSuggest2) {
        this.sstatus = sstatus;
        this.sstatusStr = statusStr;
        this.min = min;
        this.minDiagnosis = minDiagnosis;
        this.minResultDiagnosis = minResultDiagnosis;
        this.minHypertension = minHypertension;
        this.minCardiopulmonary = minCardiopulmonary;
        this.minHistory = minHistory;
        this.minSuggest1 = minSuggest1;
        this.minSuggest2 = minSuggest2;

        mMin = min1;
        mMax = max;
        mDiagnosis = diagnosis;
        mResultDiagnosis = resultDiagnosis;
        mHypertension = hypertension;
        mCardiopulmonary = cardiopulmonary;
        mHistory = history;
        mSuggest1 = suggest1;
        mSuggest2 = suggest2;

        this.lMin = lMin;
        this.lMax = lMax;
        this.lDiagnosis = lDiagnosis;
        this.lResultDiagnosis = lResultDiagnosis;
        this.lHypertension = lHypertension;
        this.lCardiopulmonary = lCardiopulmonary;
        this.llHistory = llHistory;
        this.lSuggest1 = lSuggest1;
        this.lSuggest2 = lSuggest2;

        this.max = max1;
        this.maxDiagnosis = maxDiagnosis;
        this.maxResultDiagnosis = maxResultDiagnosis;
        this.maxHypertension = maxHypertension;
        this.maxCardiopulmonary = maxCardiopulmonary;
        this.maxHistory = maxHistory;
        this.maxSuggest1 = maxSuggest1;
        this.maxSuggest2 = maxSuggest2;
    }

    public int sstatus;
    public String sstatusStr;
    public int min;
    public DiagnosisType minDiagnosis;//下限诊断
    public String minResultDiagnosis; //文案
    public String minHypertension; //高血压文案
    public String minCardiopulmonary;//心肺文案
    public String minHistory;//历史记录文案
    public int minSuggest1; //推荐文章
    public int minSuggest2; //推荐文章

    public int mMin; //第一个区间的下限
    public int mMax; //第一个区间的上限
    public DiagnosisType mDiagnosis; //第一个区间的诊断
    public String mResultDiagnosis; //文案
    public String mHypertension; //高血压文案
    public String mCardiopulmonary;//心肺文案
    public String mHistory;//历史记录文案
    public int mSuggest1; //推荐文章
    public int mSuggest2; //推荐文章

    public int lMin; //第二个区间的下限
    public int lMax; //第二个区间的上限
    public DiagnosisType lDiagnosis; //第二个区间的诊断
    public String lResultDiagnosis; //文案
    public String lHypertension; //高血压文案
    public String lCardiopulmonary;//心肺文案
    public String llHistory;//历史记录文案
    public int lSuggest1; //推荐文章
    public int lSuggest2; //推荐文章

    public int max; // 上限
    public DiagnosisType maxDiagnosis; //上限诊断
    public String maxResultDiagnosis; //文案
    public String maxHypertension; //高血压文案
    public String maxCardiopulmonary;//心肺文案
    public String maxHistory;//历史记录文案
    public int maxSuggest1; //推荐文章
    public int maxSuggest2; //推荐文章

    public static List<SituationData> situationList = new ArrayList<SituationData>() {
        {
            add(new SituationData(0, string(R.string.home_situarion_to_sleep), 45, DiagnosisType.retest, string(R.string.home_m101), string(R.string.home_m999), string(R.string.home_m998), string(R.string.home_m101), 10, 11,
                    45, 90, DiagnosisType.normal, string(R.string.home_m103), string(R.string.home_m203), string(R.string.home_m998), string(R.string.home_m701), 7, 8,
                    0, 0, DiagnosisType.nil, string(R.string.home_m103), string(R.string.home_m203), string(R.string.home_m998), string(R.string.home_m701), 1, 2,
                    90, DiagnosisType.too_fast, string(R.string.home_m104), string(R.string.home_m204), string(R.string.home_m998), string(R.string.home_m702), 4, 9));

            add(new SituationData(1, string(R.string.home_situarion_wake_up), 45, DiagnosisType.retest, string(R.string.home_m101), string(R.string.home_m999), string(R.string.home_m998), string(R.string.home_m101), 4, 12,
                    45, 90, DiagnosisType.normal, string(R.string.home_m103), string(R.string.home_m203), string(R.string.home_m998), string(R.string.home_m703), 5, 7,
                    0, 0, DiagnosisType.nil, string(R.string.home_m103), string(R.string.home_m203), string(R.string.home_m998), string(R.string.home_m703), 1, 2,
                    90, DiagnosisType.too_fast, string(R.string.home_m104), string(R.string.home_m204), string(R.string.home_m998), string(R.string.home_m704), 12, 9));

            add(new SituationData(2, string(R.string.home_situarion_working_hard), 45, DiagnosisType.retest, string(R.string.home_m101), string(R.string.home_m999), string(R.string.home_m998), string(R.string.home_m703), 11, 6,
                    45, 60, DiagnosisType.too_slow, string(R.string.home_m102), string(R.string.home_m202), string(R.string.home_m998), string(R.string.home_m704), 1, 6,
                    60, 100, DiagnosisType.normal, string(R.string.home_m103), string(R.string.home_m203), string(R.string.home_m998), string(R.string.home_m703), 8, 5,
                    100, DiagnosisType.too_fast, string(R.string.home_m104), string(R.string.home_m204), string(R.string.home_m998), string(R.string.home_m705), 12, 9));

            add(new SituationData(3, string(R.string.home_situarion_finish_meal), 45, DiagnosisType.retest, string(R.string.home_m101), string(R.string.home_m999), string(R.string.home_m998), string(R.string.home_m101), 2, 3,
                    45, 60, DiagnosisType.too_slow, string(R.string.home_m102), string(R.string.home_m202), string(R.string.home_m998), string(R.string.home_m706), 2, 3,
                    60, 100, DiagnosisType.normal, string(R.string.home_m103), string(R.string.home_m203), string(R.string.home_m998), string(R.string.home_m707), 11, 10,
                    100, DiagnosisType.too_fast, string(R.string.home_m104), string(R.string.home_m204), string(R.string.home_m998), string(R.string.home_m705), 9, 2));

            add(new SituationData(4, string(R.string.home_situarion_running), 45, DiagnosisType.retest, string(R.string.home_m101), string(R.string.home_m999), string(R.string.home_m998), string(R.string.home_m101), 11, 9,
                    45, 60, DiagnosisType.too_slow, string(R.string.home_m102), string(R.string.home_m202), string(R.string.home_m998), string(R.string.home_m708), 6, 2,
                    60, 120, DiagnosisType.normal, string(R.string.home_m103), string(R.string.home_m203), string(R.string.home_m998), string(R.string.home_m709), 8, 11,
                    120, DiagnosisType.too_fast, string(R.string.home_m104), string(R.string.home_m204), string(R.string.home_m998), string(R.string.home_m710), 3, 4));

            add(new SituationData(5, string(R.string.home_situarion_yoga), 45, DiagnosisType.retest, string(R.string.home_m101), string(R.string.home_m999), string(R.string.home_m998), string(R.string.home_m101), 10, 1,
                    45, 60, DiagnosisType.too_slow, string(R.string.home_m102), string(R.string.home_m202), string(R.string.home_m998), string(R.string.home_m708), 6, 2,
                    60, 100, DiagnosisType.normal, string(R.string.home_m103), string(R.string.home_m203), string(R.string.home_m998), string(R.string.home_m709), 10, 7,
                    100, DiagnosisType.too_fast, string(R.string.home_m104), string(R.string.home_m204), string(R.string.home_m998), string(R.string.home_m705), 3, 5));

            add(new SituationData(6, string(R.string.home_situarion_walking), 45, DiagnosisType.retest, string(R.string.home_m101), string(R.string.home_m999), string(R.string.home_m998), string(R.string.home_m101), 1, 2,
                    45, 60, DiagnosisType.too_slow, string(R.string.home_m102), string(R.string.home_m202), string(R.string.home_m998), string(R.string.home_m708), 1, 2,
                    60, 120, DiagnosisType.normal, string(R.string.home_m103), string(R.string.home_m203), string(R.string.home_m998), string(R.string.home_m709), 8, 7,
                    120, DiagnosisType.too_fast, string(R.string.home_m104), string(R.string.home_m204), string(R.string.home_m998), string(R.string.home_m705), 7, 9));

            add(new SituationData(7, string(R.string.home_situarion_relaxing), 45, DiagnosisType.retest, string(R.string.home_m101), string(R.string.home_m999), string(R.string.home_m998), string(R.string.home_m101), 1, 4,
                    45, 90, DiagnosisType.normal, string(R.string.home_m103), string(R.string.home_m203), string(R.string.home_m998), string(R.string.home_m703), 8, 7,
                    0, 0, DiagnosisType.nil, string(R.string.home_m103), string(R.string.home_m203), string(R.string.home_m998), string(R.string.home_m703), 1, 2,
                    90, DiagnosisType.too_fast, string(R.string.home_m104), string(R.string.home_m204), string(R.string.home_m998), string(R.string.home_m705), 9, 12));
        }
    };

    private static String string(int resID) {
        return App.app.getString(resID);
    }

    enum DiagnosisType {
        retest, normal, too_slow, too_fast, nil
    }
}
