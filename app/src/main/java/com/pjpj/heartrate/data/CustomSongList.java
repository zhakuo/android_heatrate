package com.pjpj.heartrate.data;

import com.pjpj.heartrate.music.model.Song;

import java.util.List;

/**
 * Des:
 * Create by: ye
 * On:        2021\07\19 11:37
 * Email:     yeshieh@163.com
 */
public class CustomSongList {
    private int id;
    private List<Song> songList;
    private String title;
    private String cover;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Song> getSongList() {
        return songList;
    }

    public void setSongList(List<Song> songList) {
        this.songList = songList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomSongList that = (CustomSongList) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
