package com.pjpj.heartrate.data;

import com.pjpj.heartrate.App;
import com.pjpj.heartrate.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\25 11:11
 * Email:     yeshieh@163.com
 */
public class ArticleData {

    public String image;
    public String title;
    public String text;
    public String date;
    public boolean isVip;
    public boolean isShow;

    public ArticleData() {
    }

    public ArticleData(String image, String title, String text, String date, boolean isVip, boolean isShow) {
        this.image = image;
        this.title = title;
        this.text = text;
        this.date = date;
        this.isVip = isVip;
        this.isShow = isShow;
    }

    public static List<ArticleData> articleList = new ArrayList<ArticleData>(){
        {
            add(new ArticleData("rate_info1",string(R.string.article_title_1),string(R.string.article_1),"JAN 1",false,false));
            add(new ArticleData("rate_info2",string(R.string.article_title_2),string(R.string.article_2),"OCT 17",false,false));
            add(new ArticleData("rate_info3",string(R.string.article_title_3),string(R.string.article_3),"AUG 27",true,false));
            add(new ArticleData("rate_info4",string(R.string.article_title_4),string(R.string.article_4),"JUNE 26",true,false));
            add(new ArticleData("rate_info5",string(R.string.article_title_5),string(R.string.article_5),"JUNE 11",true,false));
            add(new ArticleData("rate_info6",string(R.string.article_title_6),string(R.string.article_6),"JUNE 11",true,false));
            add(new ArticleData("rate_info7",string(R.string.article_title_7),string(R.string.article_7),"JUNE 11",true,false));
            add(new ArticleData("rate_info8",string(R.string.article_title_8),string(R.string.article_8),"JUNE 11",true,false));
            add(new ArticleData("rate_info9",string(R.string.article_title_9),string(R.string.article_9),"JUNE 11",true,false));
            add(new ArticleData("rate_info10",string(R.string.article_title_10),string(R.string.article_10),"JUNE 11",true,false));
            add(new ArticleData("rate_info11",string(R.string.article_title_11),string(R.string.article_11),"JUNE 11",true,false));
            add(new ArticleData("rate_info12",string(R.string.article_title_12),string(R.string.article_12),"JUNE 11",true,false));
        }
    };

    private static String string(int resID) {
        return App.app.getString(resID);
    }
}
