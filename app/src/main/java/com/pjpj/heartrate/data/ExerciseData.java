package com.pjpj.heartrate.data;

import android.util.Log;

import com.pjpj.heartrate.R;

import java.util.ArrayList;
import java.util.List;

public class ExerciseData {
    public final static int Plank = 1;
    public final static int PILE_SQUAT = 2;
    public final static int WALL_PUSH_UP = 3;
    public final static int MEDITATION = 4;

    public int exerciseRes;
    public int exerciseRes1;
    public int type;
    public String typeName;
    public int totalSec;//总时间
    public int totalCount;//总次数 如果是-1 就不需要次数

    public ExerciseData(int type, String typeName, int exerciseRes, int exerciseRes1, int totalSec, int totalCount) {
        this.exerciseRes = exerciseRes;
        this.exerciseRes1 = exerciseRes1;
        this.typeName = typeName;
        this.type = type;
        this.totalSec = totalSec;
        this.totalCount = totalCount;
    }

    public static List<ExerciseData> mExerciseList = new ArrayList<ExerciseData>() {
        {
            Log.e("Exercise", "add exercise data");
            add(new ExerciseData(Plank, "PLANK", R.mipmap.bg_exercise_plank, R.mipmap.ic_exercise_plank, 30, -1));
            add(new ExerciseData(WALL_PUSH_UP, "WALL PUSH UP", R.mipmap.bg_exercise_push_up, R.mipmap.ic_exercise_wallpushup, 40, 20));
            add(new ExerciseData(MEDITATION, "MEDITATION", R.mipmap.bg_exercise_breath, R.mipmap.ic_exercise_meditation, 180, -1));
            add(new ExerciseData(PILE_SQUAT, "PILE SQUAT", R.mipmap.bg_exercise_pile_squat, R.mipmap.ic_exercise_pilesquat, 30, 10));
        }
    };

    public static ExerciseData getData(int type) {
        for (int i = 0; i < mExerciseList.size(); i++) {
            ExerciseData data = mExerciseList.get(i);
            if (data.type == type) {
                return data;
            }
        }
        return null;
    }
}
