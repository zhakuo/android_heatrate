package com.pjpj.heartrate.data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

/**
 * Des:
 * Create by: ye
 * On:        2021\03\06 16:23
 * Email:     yeshieh@163.com
 */
@Entity(tableName = "search_history")
public class SearchHistoryData implements Serializable {

    @PrimaryKey
    @NonNull
    private String content = "";
    private int type = -1;
    @Ignore
    private String subType;
    private int fileType = -1;

    public SearchHistoryData() {
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public @NonNull
    String getContent() {
        return content;
    }

    public void setContent(@NonNull String content) {
        this.content = content;
    }
}
