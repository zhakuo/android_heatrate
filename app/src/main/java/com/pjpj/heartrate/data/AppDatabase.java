package com.pjpj.heartrate.data;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {RateData.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase database;

    public static AppDatabase getDatabase() {
        return database;
    }

    public static void initDataBase(Context context) {
        database = Room.databaseBuilder(context, AppDatabase.class, "heartrate.db")
                .allowMainThreadQueries()
                .build();
    }

    public abstract RateDao rateDao();
}
