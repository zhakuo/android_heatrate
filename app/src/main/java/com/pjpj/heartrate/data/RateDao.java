package com.pjpj.heartrate.data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\17 16:36
 * Email:     yeshieh@163.com
 */
@Dao
public interface RateDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<RateData> data);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(RateData data);

    @Delete
    void delete(RateData data);

    @Query("DELETE FROM rate_data")
    void deleteAll();

    @Query("SELECT * FROM rate_data")
    List<RateData> getAll();

    @Query("SELECT * FROM rate_data WHERE rateId= :rateId")
    RateData getRate(String rateId);
}
