package com.pjpj.heartrate.base.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.pjpj.heartrate.base.type.RequestFootType;

/**
 * Des:       BaseListViewModel
 * Create by: m122469119
 * On:        2018\03\22 15:47
 * Email:     122469119@qq.com
 */
public class BaseListViewModel extends ViewModel {


    protected MutableLiveData<RequestFootType> mRequestFootTypeMutableLiveData = new MutableLiveData<>();




    protected MutableLiveData<Integer> mPageMutableLiveData = new MutableLiveData<>();


    public BaseListViewModel() {
        //  mRequestTypeMutableLiveData.setValue(RequestType.TYPE_LOADING);
        mPageMutableLiveData.setValue(1);
    }




    public MutableLiveData<RequestFootType> getRequestFootTypeMutableLiveData() {
        return mRequestFootTypeMutableLiveData;
    }

    public void setRequestFootTypeMutableLiveData(RequestFootType requestFootType) {
        mRequestFootTypeMutableLiveData.setValue(requestFootType);
    }


    public MutableLiveData<Integer> getPageMutableLiveData() {
        return mPageMutableLiveData;
    }

    public void setPageMutableLiveData(Integer integer) {
        mPageMutableLiveData.setValue(integer);
    }

}
