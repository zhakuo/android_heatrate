package com.pjpj.heartrate.base;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewbinding.ViewBinding;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.pjpj.heartrate.base.type.RequestType;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Objects;

@SuppressWarnings("unchecked")
public abstract class BaseViewModelFragment<VB extends ViewBinding, VM extends BaseViewModel> extends Fragment {

    protected Handler handler = new Handler(Looper.getMainLooper());

    protected VB binding;
    protected VM viewModel;
    protected Bundle mBundle;
    protected boolean isInit = false;
    protected boolean isLoad = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Activity.class.isAssignableFrom(bindContext())) {
            viewModel = ViewModelProviders.of(getActivity()).get(getVMClass());
        } else {
            viewModel = ViewModelProviders.of(this).get(getVMClass());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            Class<VB> clazz = getVBClass();
            Method method = clazz.getDeclaredMethod("inflate", LayoutInflater.class);
            binding = (VB) method.invoke(null, getLayoutInflater());
            return Objects.requireNonNull(binding).getRoot();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //初始化View
        initView();

        //初始化观察者
        initLifeCycles(getLifecycle());

        //初始化参数
        if ((mBundle = getArguments()) != null) {
            initArguments(mBundle);
        }

        isInit = true;
        isCanLoadData();
        //初始化数据层
        initData();
    }

    protected SwipeRefreshLayout getRefreshLayout() {
        return null;
    }

    protected BaseQuickAdapter getAdapter() {
        return null;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isCanLoadData();
    }

    private void isCanLoadData() {
        if (!isInit) {
            return;
        }
        if (getUserVisibleHint()) {
            if (!isLoad) {
                lazyLoad();
                isLoad = true;
            }
        } else {
            if (isLoad) {
                stopLoad();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isInit = false;
        isLoad = false;
    }


    protected abstract void initView();


    protected void initArguments(Bundle bundle) {
    }

    protected void initLifeCycles(Lifecycle lifecycle) {
        viewModel.getRequestTypeMutableLiveData().observe(this, new Observer<RequestType>() {
            @Override
            public void onChanged(RequestType requestType) {

                if (requestType == RequestType.TYPE_ERROR) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(false);
                    }
                    if (getAdapter() != null) {
                        getAdapter().loadMoreFail();
                    }
                } else if (requestType == RequestType.TYPE_NET_ERROR) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(false);
                    }
                    if (getAdapter() != null) {
                        getAdapter().loadMoreFail();
                    }
                } else if (requestType == RequestType.TYPE_SUCCESS) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(false);
                    }
                    if (getAdapter() != null) {
                        getAdapter().loadMoreComplete();
                    }
                } else if (requestType == RequestType.TYPE_EMPTY) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(false);
                    }
                } else if (requestType == RequestType.TYPE_LOADING) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(true);
                    }
                } else if (requestType == RequestType.TYPE_NO_MORE) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(false);
                    }
                    if (getAdapter() != null) {
                        getAdapter().loadMoreEnd();
                    }
                }
            }
        });
    }

    protected void initData() {
    }

    protected void lazyLoad() {
    }

    protected void stopLoad() {
    }

    public Class<? extends LifecycleOwner> bindContext() {
        return this.getClass();
    }

    public Class<VM> getVMClass() {
        return (Class<VM>) ((ParameterizedType) Objects.requireNonNull(getClass().getGenericSuperclass())).getActualTypeArguments()[1];
    }

    public Class<VB> getVBClass() {
        return (Class<VB>) ((ParameterizedType) Objects.requireNonNull(getClass().getGenericSuperclass())).getActualTypeArguments()[0];
    }

    protected void runOnUIThread(Runnable runnable) {
        handler.post(runnable);
    }

    protected void runOnSubThread(Runnable runnable) {
        AsyncTask.THREAD_POOL_EXECUTOR.execute(runnable);
    }
}
