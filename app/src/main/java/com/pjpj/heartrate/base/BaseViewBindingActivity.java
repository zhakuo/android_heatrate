package com.pjpj.heartrate.base;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.viewbinding.ViewBinding;

import com.pjpj.heartrate.R;
import com.pjpj.heartrate.util.AppSystemBarHelper;
import com.workout.base.HomeKeyDetect;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Objects;

@SuppressWarnings("unchecked")
public abstract class BaseViewBindingActivity<VB extends ViewBinding> extends AppCompatActivity {

    public enum StatusBarStyle {
        NORMAL, FULL, FULLDARK, CLOSE, TRANSPARENT, TRANSPARENTDARK,BLUE
    }

    protected AppCompatActivity mActivity;
    protected VB binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mActivity = this;
        super.onCreate(savedInstanceState);
        try {
            Class<VB> clazz = getVBClass();
            Method method = clazz.getDeclaredMethod("inflate", LayoutInflater.class);
            binding = (VB) method.invoke(null, getLayoutInflater());
            setContentView(Objects.requireNonNull(binding).getRoot());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        if (getIntent() != null) {
            initArgument(getIntent().getExtras());
        }
        initWindow(initStatusBar());
        initView();
        initLife(getLifecycle());
        initData();

        initHomekey();
    }

    protected StatusBarStyle initStatusBar() {
        return StatusBarStyle.NORMAL;
    }

    public void initWindow(StatusBarStyle statusBarStyle) {
        switch (statusBarStyle) {
            case FULL:
                AppSystemBarHelper.immersiveStatusBar(this, 0);
                break;
            case FULLDARK:
                AppSystemBarHelper.immersiveStatusBar(this, 0);
                AppSystemBarHelper.setStatusBarDarkMode(this);
                break;
            case NORMAL:
            case BLUE:
                AppSystemBarHelper.tintStatusBar(this, getResources().getColor(R.color.app_primary_color), 0);
                AppSystemBarHelper.setStatusBarLightMode(this);
                break;
            case TRANSPARENT:
                AppSystemBarHelper.tintStatusBar(this, getResources().getColor(R.color.transparent), 0);
                break;
            case TRANSPARENTDARK:
                AppSystemBarHelper.tintStatusBar(this, getResources().getColor(R.color.transparent), 0);
                AppSystemBarHelper.setStatusBarDarkMode(this);
                break;
            case CLOSE:
                break;
        }

    }

    protected void initArgument(Bundle bundle) {
    }

    protected abstract void initView() ;

    protected void initData() {
    }

    protected void initLife(Lifecycle lifecycle) {
    }

    @Override
    public void setContentView(int layoutResID) {
    }

    public Class<VB> getVBClass() {
        return (Class<VB>) ((ParameterizedType) Objects.requireNonNull(getClass().getGenericSuperclass())).getActualTypeArguments()[0];
    }

    private HomeKeyDetect mHomeKeyMonitor = null;
    private boolean isClickRecent = false;

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try{

            if (mHomeKeyMonitor!=null){

                mHomeKeyMonitor.unregisterReceiver();
            }

        }catch (Exception e){

        }
    }

    private void initHomekey(){
        try {


            mHomeKeyMonitor = new HomeKeyDetect(this, new HomeKeyDetect.OnHomeKeyListener() {
                @Override
                public void onRecentApps() {

                    closeDialogActivity();
                }

                @Override
                public void onLock() {
                }

                @Override
                public void onHome() {

                    closeDialogActivity();



                }


            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void closeDialogActivity() {
        if (this !=null) {
            try {
                if (Build.VERSION.SDK_INT >= 21) {
                    finishAndRemoveTask();
                } else {
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
