package com.pjpj.heartrate.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.viewbinding.ViewBinding;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Objects;

/**
 * Des:       BaseViewHolder
 * Create by: m122469119
 * On:        2018\03\17 16:38
 * Email:     122469119@qq.com
 */
public abstract class BaseViewHolder<VB extends ViewBinding> {

    protected Context mContext;
    protected View mView;
    protected VB binding;

    public BaseViewHolder(Context context) {
        this.mContext = context;
        try {
            Class<VB> clazz = getVBClass();
            Method method = clazz.getDeclaredMethod("inflate", LayoutInflater.class);
            binding = (VB) method.invoke(null, LayoutInflater.from(mContext));
            mView = binding.getRoot();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        initView();
        initData();
    }

    private Class<VB> getVBClass() {
        return (Class<VB>) ((ParameterizedType) Objects.requireNonNull(getClass().getGenericSuperclass())).getActualTypeArguments()[0];
    }

    public View getView() {
        return mView;
    }

    protected abstract void initView();

    protected void initData() {

    }

}
