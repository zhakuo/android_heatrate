package com.pjpj.heartrate.base;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewbinding.ViewBinding;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.pjpj.heartrate.base.type.RequestType;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.view.dialog.AppLoadingDialog;
import com.workout.base.HomeKeyDetect;

import java.lang.reflect.ParameterizedType;
import java.util.Objects;

/**
 * Des:
 * Create by: ye
 * On:        2021\02\22 14:30
 * Email:     yeshieh@163.com
 */
public abstract class BaseViewModelActivity<VB extends ViewBinding, VM extends BaseViewModel> extends BaseViewBindingActivity<VB> {

    protected VM viewModel;
    private AppLoadingDialog mLoadingDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this).get(getVMClass());
        initHomekey();
        super.onCreate(savedInstanceState);
    }

    public Class<? extends LifecycleOwner> bindContext() {
        return this.getClass();
    }

    public Class<VM> getVMClass() {
        return (Class<VM>) ((ParameterizedType) Objects.requireNonNull(getClass().getGenericSuperclass())).getActualTypeArguments()[1];
    }

    protected SwipeRefreshLayout getRefreshLayout() {
        return null;
    }

    protected BaseQuickAdapter getAdapter() {
        return null;
    }

    @Override
    protected void initLife(Lifecycle lifecycle) {
        super.initLife(lifecycle);
        initDialogAction();
        viewModel.getRequestTypeMutableLiveData().observe(this, new Observer<RequestType>() {
            @Override
            public void onChanged(RequestType requestType) {

                if (requestType == RequestType.TYPE_ERROR) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(false);
                    }
                    if (getAdapter() != null) {
                        getAdapter().loadMoreFail();
                    }
                } else if (requestType == RequestType.TYPE_NET_ERROR) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(false);
                    }
                    if (getAdapter() != null) {
                        getAdapter().loadMoreFail();
                    }
                } else if (requestType == RequestType.TYPE_SUCCESS) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(false);
                    }
                    if (getAdapter() != null) {
                        getAdapter().loadMoreComplete();
                    }
                } else if (requestType == RequestType.TYPE_EMPTY) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(false);
                    }
                } else if (requestType == RequestType.TYPE_LOADING) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(true);
                    }
                } else if (requestType == RequestType.TYPE_NO_MORE) {
                    if (getRefreshLayout() != null) {
                        getRefreshLayout().setRefreshing(false);
                    }
                    if (getAdapter() != null) {
                        getAdapter().loadMoreEnd();
                    }
                }
            }
        });
    }

    protected void initDialogAction() {
        viewModel.getDialogActionMutableLiveData().observe(this, new Observer<BaseViewModel.DialogAction>() {
            @Override
            public void onChanged(@Nullable BaseViewModel.DialogAction action) {
                switch (action) {
                    case SHOW:
                    case SHOW_STICKY:
                        if (mLoadingDialog == null) {
                            mLoadingDialog = new AppLoadingDialog(BaseViewModelActivity.this);
                            mLoadingDialog.setCanceledOnTouchOutside(action == BaseViewModel.DialogAction.SHOW);
                        }
                        mLoadingDialog.show();
                        break;
                    case DISMISS:
                        if (mLoadingDialog != null) {
                            mLoadingDialog.dismiss();
                            mLoadingDialog = null;
                        }
                        break;
                }
            }
        });
    }
    private HomeKeyDetect mHomeKeyMonitor = null;
    private boolean isClickRecent = false;

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try{

            if (mHomeKeyMonitor!=null){

                mHomeKeyMonitor.unregisterReceiver();
            }

        }catch (Exception e){

        }
    }

    private void initHomekey(){
        try {


            mHomeKeyMonitor = new HomeKeyDetect(this, new HomeKeyDetect.OnHomeKeyListener() {
                @Override
                public void onRecentApps() {

                    closeDialogActivity();
                }

                @Override
                public void onLock() {
                }

                @Override
                public void onHome() {

                    closeDialogActivity();



                }


            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void closeDialogActivity() {
        if (this !=null) {
            try {
                if (Build.VERSION.SDK_INT >= 21) {
                    finishAndRemoveTask();
                } else {
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
