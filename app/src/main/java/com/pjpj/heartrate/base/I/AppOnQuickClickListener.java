package com.pjpj.heartrate.base.I;

import android.view.View;

import com.pjpj.heartrate.util.AppToastHelper;

/**
 * Des:       OnQuickClickListener
 * Create by: m122469119
 * On:        2018/3/9 08:46
 * Email:     122469119@qq.com
 */
public abstract class AppOnQuickClickListener extends AppOnNetworkClickListener {

    // 防止快速点击默认等待时长为500ms
    private long DELAY_TIME = 350;
    private long lastClickTime;

    protected long getDelayTime(){
        return DELAY_TIME;
    }

    private boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < getDelayTime()) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    @Override
    public void onNetworkClick(View v) {
        // 判断当前点击事件与前一次点击事件时间间隔是否小于阙值
        if (isFastDoubleClick()) {
            return;
        }
        onFastClick(v);
    }


    @Override
    public void onNoNetworkClick(View v) {
        super.onNoNetworkClick(v);
        AppToastHelper.show("请检查网络");
    }

    /**
     * 设置默认快速点击事件时间间隔
     *
     * @param delay_time
     * @return
     */
    public AppOnQuickClickListener setLastClickTime(long delay_time) {

        this.DELAY_TIME = delay_time;

        return this;
    }

    /**
     * 快速点击事件回调方法
     *
     * @param v
     */
    public abstract void onFastClick(View v);

}
