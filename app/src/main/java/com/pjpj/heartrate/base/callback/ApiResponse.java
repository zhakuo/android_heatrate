package com.pjpj.heartrate.base.callback;

import com.google.gson.annotations.SerializedName;

/**
 * Des:
 * Create by: ye
 * On:        2021\02\22 14:59
 * Email:     yeshieh@163.com
 */
public class ApiResponse<T> {

    /**
     * code : 10000
     * msg : SUCCESS
     * data : []
     */

    @SerializedName("code")
    private int mCode;
    @SerializedName("msg")
    private String mMsg;
    @SerializedName("data")
    private T mData;

    public int getCode() {
        return mCode;
    }

    public void setCode(int code) {
        mCode = code;
    }

    public String getMsg() {
        return mMsg;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public T getData() {
        return mData;
    }

    public void setData(T data) {
        mData = data;
    }

    public boolean isSuccess() {
        return mCode == 0;
    }
}
