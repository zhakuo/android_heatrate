package com.pjpj.heartrate.base.I;

import android.text.Editable;
import android.text.TextWatcher;

public abstract class SimpleTextListener implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
