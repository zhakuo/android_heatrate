package com.pjpj.heartrate.base.type;

/**
 * Des:       RequestType
 * Create by: 柊
 * On:        2018/8/21 10:28
 * Email:     122469119@qq.com
 */
public enum RequestType {
    //成功
    TYPE_SUCCESS,
    //加载
    TYPE_LOADING,
    //服务器错误
    TYPE_ERROR,
    //网络错误
    TYPE_NET_ERROR,
    //空数据
    TYPE_EMPTY,
    TYPE_NO_MORE,
    TYPE_LOCATION
}