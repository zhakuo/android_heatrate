package com.pjpj.heartrate.base.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.pjpj.heartrate.base.type.RequestType;

/**
 * Des:       基础模型
 * Create by: m122469119
 * On:        2018\03\22 15:25
 * Email:     122469119@qq.com
 */
public class BaseViewModel extends BaseListViewModel {
    public enum DialogAction {
        //请求
        SHOW("0"),
        //显示 不可取消
        SHOW_STICKY("2"),
        //成功
        DISMISS("1");

        // 构造方法
        DialogAction(String id) {
            this.Id = id;
        }

        private String Id;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

    }

    protected MutableLiveData<DialogAction> mDialogActionMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<DialogAction> getDialogActionMutableLiveData() {
        return mDialogActionMutableLiveData;
    }

    public void setDialogActionMutableLiveData(DialogAction dialogAction) {
        mDialogActionMutableLiveData.setValue(dialogAction);
    }


    protected MutableLiveData<RequestType> mRequestTypeMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<RequestType> getRequestTypeMutableLiveData() {
        return mRequestTypeMutableLiveData;
    }

    public void setRequestTypeMutableLiveData(RequestType requestType) {
        mRequestTypeMutableLiveData.setValue(requestType);
    }


    public MutableLiveData<String> mErrorMessageLiveData = new MutableLiveData<>();

    public MutableLiveData<String> getErrorMessageLiveData() {
        return mErrorMessageLiveData;
    }


}
