package com.pjpj.heartrate.base;

/**
 * Des:
 * Create by: ye
 * On:        2021\01\26 16:19
 * Email:     yeshieh@163.com
 */
public class LiveEventConfig {
    public final static String EVENT_SONG_PLAY = "song_play";
    public final static String EVENT_SONG_STATUS_CHANGE = "song_change";
    public final static String EVENT_SONG_COVER_ADD = "song_cover_add";
    public final static String EVENT_SONG_COVER_REMOVE = "song_cover_remove";

    public final static String EVENT_LIST_SONG_CHANGE = "song_change";
    public final static String EVENT_LIST_CUSTOM_SONG_CHANGE = "custom_song_change";
}
