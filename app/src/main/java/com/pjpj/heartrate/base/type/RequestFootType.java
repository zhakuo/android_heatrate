package com.pjpj.heartrate.base.type;


/**
 * Des:       RequestFootType
 * Create by: 柊
 * On:        2018/3/21 10:28
 * Email:     122469119@qq.com
 */
public enum RequestFootType {
    //没有更多数据并隐藏加载更多
    TYPE_NO_MORE_AND_GONE_VIEW,
    //成功
    TYPE_SUCCESS,
    //服务器错误
    TYPE_ERROR,
    //网络错误
    TYPE_NET_ERROR,
    //没有更多数据不隐藏加载更多
    TYPE_NO_MORE,
}