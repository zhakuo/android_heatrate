package com.pjpj.heartrate.base;

import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;

import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.music.I.IPlay;
import com.pjpj.heartrate.music.I.IPlayView;
import com.pjpj.heartrate.music.I.PlayImpl;
import com.pjpj.heartrate.music.IMusic;
import com.pjpj.heartrate.music.MusicService;
import com.pjpj.heartrate.music.model.PlayList;
import com.pjpj.heartrate.music.model.Song;
import com.pjpj.heartrate.util.AppToastHelper;
import com.workout.base.HomeKeyDetect;

/**
 * Des:
 * Create by: ye
 * On:        2021\07\16 15:48
 * Email:     yeshieh@163.com
 */
public abstract class BaseMusicActivity<VB extends ViewBinding, VM extends BaseViewModel> extends BaseViewModelActivity<VB, VM> implements IPlayView<IPlay>, IMusic.Callback {
    protected IPlay mIPlay;
    protected IMusic mIMusic;
    protected Song mPendingSong;//等待播放

    @Override
    protected void initData() {
        super.initData();
        new PlayImpl(this, this).subscribe();
    }

    @Override
    public void setPresenter(IPlay iPlay) {
        mIPlay = iPlay;
    }

    @Override
    public void handleError(Throwable error) {
        AppToastHelper.show(error.getMessage());
    }

    @Override
    public void onPlaybackServiceBound(MusicService service) {
        mIMusic = service;
        mIMusic.registerCallback(this);
        if (mPendingSong != null) {
            playSong(mPendingSong);
        }
    }

    @Override
    public void onPlaybackServiceUnbound() {
        mIMusic.unregisterCallback(this);
        mIMusic = null;
    }

    @Override
    public void onSongUpdated(@Nullable Song song) {
    }

    /*Music Service*/
    @Override
    public void onPlayStatusChanged(boolean isPlaying) {
    }

    protected void playSong(Song song) {
        if (mIMusic == null) {
            mPendingSong = song;
            return;
        }
        PlayList playList = new PlayList(song);
        playList.setPlayMode(AppConfig.lastPlayMode());
        mIMusic.play(playList, 0);
    }

    protected void stopMusic() {
        if (mIMusic != null) {
            mIMusic.pause();
        }
    }

    protected void resumeMusic() {
        if (mIMusic != null) {
            mIMusic.play();
        }
    }
}
