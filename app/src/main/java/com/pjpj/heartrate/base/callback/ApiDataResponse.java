package com.pjpj.heartrate.base.callback;

import com.google.gson.annotations.SerializedName;

/**
 * Des:
 * Create by: ye
 * On:        2021\07\06 10:42
 * Email:     yeshieh@163.com
 */
public class ApiDataResponse<T> extends ApiResponse<ApiDataResponse.Data<T>> {

    public static class Data<T> {
        @SerializedName(value = "data")
        public T data;
    }
}
