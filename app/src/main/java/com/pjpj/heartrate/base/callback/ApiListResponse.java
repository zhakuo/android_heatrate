package com.pjpj.heartrate.base.callback;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Des:
 * Create by: ye
 * On:        2021\03\02 11:50
 * Email:     yeshieh@163.com
 */
public class ApiListResponse<T> extends ApiResponse<ApiListResponse.Data<T>>{

    public static class Data<T>{
        @SerializedName(value = "datas",alternate = "data")
        public List<T> list;
        public Page pages;
        public int count;
    }

    public static class Page{

        /**
         * page : 0
         * totalCount : 3
         * pageCount : 1
         * pagesize : 15
         */

        @SerializedName("page")
        private Integer mPage;
        @SerializedName("totalCount")
        private String mTotalCount;
        @SerializedName("pageCount")
        private Integer mPageCount;
        @SerializedName("pagesize")
        private Integer mPagesize;

        public Integer getPage() {
            return mPage;
        }

        public void setPage(Integer page) {
            mPage = page;
        }

        public String getTotalCount() {
            return mTotalCount;
        }

        public void setTotalCount(String totalCount) {
            mTotalCount = totalCount;
        }

        public Integer getPageCount() {
            return mPageCount;
        }

        public void setPageCount(Integer pageCount) {
            mPageCount = pageCount;
        }

        public Integer getPagesize() {
            return mPagesize;
        }

        public void setPagesize(Integer pagesize) {
            mPagesize = pagesize;
        }
    }
}
