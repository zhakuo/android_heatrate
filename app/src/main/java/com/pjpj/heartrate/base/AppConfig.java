package com.pjpj.heartrate.base;

import com.pjpj.heartrate.BuildConfig;
import com.pjpj.heartrate.music.PlayMode;
import com.pjpj.heartrate.util.AppConfigHelper;

import java.util.List;

/**
 * Des:
 * Create by: ye
 * On:        2021\01\26 15:06
 * Email:     yeshieh@163.com
 */
public class AppConfig {
    public final static boolean DEBUG = true;
    public final static String FILE_PROVIDER = BuildConfig.APPLICATION_ID + ".fileprovider";

    public static boolean mIsShowSubscribe = false;

    private static final String KEY_FIRST_ENTER = "first_enter";

    public static boolean isFirstEnter() {
//        return true;
        return AppConfigHelper.getValue(KEY_FIRST_ENTER, true);
    }

    public static void setFirstEnter() {
        AppConfigHelper.setValue(KEY_FIRST_ENTER, false);
    }

    /**
     * Play mode from the last time.
     */
    private static final String KEY_PLAY_MODE = "playMode";

    /**
     * {@link #KEY_PLAY_MODE}
     */
    public static PlayMode lastPlayMode() {
        String playModeName = AppConfigHelper.getValue(KEY_PLAY_MODE, null);
        if (playModeName != null) {
            return PlayMode.valueOf(playModeName);
        }
        return PlayMode.getDefault();
    }

    /**
     * {@link #KEY_PLAY_MODE}
     */
    public static void setPlayMode(PlayMode playMode) {
        AppConfigHelper.setValue(KEY_PLAY_MODE, playMode.name());
    }


    private static final String KEY_GENDER_SET = "gender_set";

    private static final String KEY_STATEMENT_SET = "statement_set";

    public static boolean isStatementSet() {
        return AppConfigHelper.getValue(KEY_STATEMENT_SET, 0) != 0;
    }

    public static void setStatement(int statement) {
        AppConfigHelper.setValue(KEY_STATEMENT_SET, statement);
    }

    public static boolean isGenderSet() {
        return AppConfigHelper.getValue(KEY_GENDER_SET, 0) != 0;
    }

    public static void setGender(int gender) {
        AppConfigHelper.setValue(KEY_GENDER_SET, gender);
    }

    public static int getGender(){
        return AppConfigHelper.getValue(KEY_GENDER_SET, 0);
    }

    private static final String KEY_USER_ID = "user_id";

    public static void setUserId(int userId) {
        AppConfigHelper.setValue(KEY_USER_ID, userId);
    }

    public static int getUserId(){
        return AppConfigHelper.getValue(KEY_USER_ID, 0);
    }

    private static final String KEY_SITUATION_SET = "situation_set";

    public static boolean isSituationSet() {
        return AppConfigHelper.getValue(KEY_SITUATION_SET, 0) != 0;
    }

    public static void setSituation(int situation) {
        AppConfigHelper.setValue(KEY_SITUATION_SET, situation);
    }

}
