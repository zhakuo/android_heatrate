package com.pjpj.heartrate;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.multidex.MultiDex;

import com.leg.yuandu.YdAppApplication;
import com.leg.yuandu.promote.KochavarHelper;
import com.leg.yuandu.storage.DbHelper;
import com.leg.yuandu.utils.Utils;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheEntity;
import com.lzy.okgo.cache.CacheMode;
import com.pjpj.heartrate.data.AppDatabase;
import com.pjpj.heartrate.util.AppConfigHelper;
import com.pjpj.heartrate.util.AppDataProvider;
import com.pjpj.heartrate.util.AppExecutor;
import com.pjpj.heartrate.util.AppFileOperateHelper;
import com.pjpj.heartrate.util.AppToastHelper;
import com.pjpj.heartrate.util.UIHelper;
import com.pjpj.heartrate.widget.AppHttpLoggingInterceptor;


import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;

import static com.lzy.okgo.OkGo.DEFAULT_MILLISECONDS;

/**
 * Des:
 * Create by: ye
 * On:        2021\07\12 13:49
 * Email:     yeshieh@163.com
 */
public class App extends YdAppApplication {
    public static App app;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;
        AppExecutor.getInstance().runWorker(new Runnable() {
            @Override
            public void run() {
                initOkGo();
                AppDatabase.initDataBase(app);
                AppFileOperateHelper.init(app);
                AppDataProvider.getInstance().initData();
                UIHelper.init(app);
                AppToastHelper.init(app);
                AppConfigHelper.init(app);
                Log.e("Exercise", "app init");
            }
        });

        /**只在主进程里初始化主进程相关的东西，避免在守护进程也初始化**/
        if (isMainProcess()) {

            // 检查并记录首次安装时间点
            Utils.checkSetFirstInstallTime();
            Utils.copyFirstInstallTimeToBaseModule();

            // 初始化数据库
            DbHelper.init(this);

            // 配置广告
//            initAd();


            KochavarHelper.initTracker();
        }
    }

    private void initOkGo() {
        OkGo.getInstance().init(this)
                .setOkHttpClient(getOkGOClient())               //建议设置OkHttpClient，不设置会使用默认的
                .setCacheMode(CacheMode.NO_CACHE)               //全局统一缓存模式，默认不使用缓存，可以不传
                .setCacheTime(CacheEntity.CACHE_NEVER_EXPIRE)   //全局统一缓存时间，默认永不过期，可以不传
                .setRetryCount(2);                             //全局统一超时重连次数，默认为三次，那么最差的情况会请求4次(一次原始请求，三次重连请求)，不需要可以设置为0
    }

    private OkHttpClient mOkGoHttpClient;

    private OkHttpClient getOkGOClient() {
        if (mOkGoHttpClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(createSSLSocketFactory());
            builder.hostnameVerifier(new TrustAllHostnameVerifier());
            AppHttpLoggingInterceptor interceptor = new AppHttpLoggingInterceptor();
            interceptor.setLevel(BuildConfig.DEBUG ? AppHttpLoggingInterceptor.Level.BODY : AppHttpLoggingInterceptor.Level.NONE);
            builder.addInterceptor(interceptor);
            builder.readTimeout(DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);
            builder.writeTimeout(DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);
            builder.connectTimeout(DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);
            mOkGoHttpClient = builder.build();
        }
        return mOkGoHttpClient;
    }

    private static class TrustAllCerts implements X509TrustManager {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

    private static class TrustAllHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }


    private static SSLSocketFactory createSSLSocketFactory() {
        SSLSocketFactory ssfFactory = null;

        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, new TrustManager[]{new TrustAllCerts()}, new SecureRandom());
            ssfFactory = sc.getSocketFactory();
        } catch (Exception e) {
        }

        return ssfFactory;
    }


}
