package com.pjpj.heartrate.ui.statement;

import android.content.Context;
import android.view.View;

import com.pjpj.heartrate.ui.permission.CameraPermissionDialog;
import com.pjpj.heartrate.util.permission.PermissionHelper;
import com.pjpj.heartrate.util.permission.request.IRemider;
import com.pjpj.heartrate.util.permission.request.RequestExecutor;

import java.util.List;

public class StatementTipsReminder implements IRemider {
    @Override
    public void showGuide(Context context, List<String> permissions, RequestExecutor executor) {
        StatementTipsDialog dialog = new StatementTipsDialog(context);
        dialog.setOnAccessListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                executor.execute();
            }
        });
        dialog.show();
    }

    @Override
    public void showRationale(Context context, List<String> permissions, RequestExecutor executor) {
        StatementTipsDialog dialog = new StatementTipsDialog(context);
        dialog.setOnAccessListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                executor.execute();
            }
        });
        dialog.show();
    }

    @Override
    public void showDenied(Context context, List<String> permissions, RequestExecutor executor) {
        StatementTipsDialog dialog = new StatementTipsDialog(context);
        dialog.setOnAccessListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                PermissionHelper.with(context).permissionSetting().execute();
                executor.execute();
            }
        });
        dialog.show();
    }

    @Override
    public void showInstall(Context context, RequestExecutor executor) {

    }
}
