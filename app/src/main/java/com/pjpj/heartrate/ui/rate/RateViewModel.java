package com.pjpj.heartrate.ui.rate;

import androidx.lifecycle.MutableLiveData;

import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.data.AppDatabase;
import com.pjpj.heartrate.data.RateData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\18 11:37
 * Email:     yeshieh@163.com
 */
public class RateViewModel extends BaseViewModel {
    public MutableLiveData<List<RateData>> mHistoryLiveData = new MutableLiveData<>();
    public MutableLiveData<List<Integer>> mReportYearLiveData = new MutableLiveData<>();
    public MutableLiveData<List<Integer>> mReportMonthLiveData = new MutableLiveData<>();

    public void loadHistory() {
        List<RateData> historyList = AppDatabase.getDatabase().rateDao().getAll();
        Collections.reverse(historyList);
        mHistoryLiveData.setValue(historyList);
    }

    public void loadReportMonth() {
        List<RateData> historyList = AppDatabase.getDatabase().rateDao().getAll();
        Map<String, Integer> resultMap = new HashMap<>();
        for (int i = 0; i < historyList.size(); i++) {
            RateData data = historyList.get(i);
            String date = data.getYear() + "" + data.getMonth() + ""+ data.getDay();
            if (!resultMap.containsKey(date)) {
                resultMap.put(date, data.getRate());
            } else {
                int addResult = resultMap.get(date) + data.getRate();
                int averageRate = (int) (addResult / 2f);
                resultMap.put(date, averageRate);
            }
        }
        List<Integer> result = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : resultMap.entrySet()) {
            result.add(entry.getValue());
        }
        mReportMonthLiveData.setValue(result);
    }

    public void loadReportYear() {
        List<RateData> historyList = AppDatabase.getDatabase().rateDao().getAll();
        Map<String, Integer> resultMap = new HashMap<>();
        for (int i = 0; i < historyList.size(); i++) {
            RateData data = historyList.get(i);
            String date = data.getYear() + "" + data.getMonth();
            if (!resultMap.containsKey(date)) {
                resultMap.put(date, data.getRate());
            } else {
                int addResult = resultMap.get(date) + data.getRate();
                int averageRate = (int) (addResult / 2f);
                resultMap.put(date, averageRate);
            }
        }
        List<Integer> result = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : resultMap.entrySet()) {
            result.add(entry.getValue());
        }
        mReportYearLiveData.setValue(result);
    }
}
