package com.pjpj.heartrate.ui.statement;

import static com.pjpj.heartrate.ui.rate.RateTestActivity.ARGUMENT_INT_BEFORE_EXERCISE_RATE;
import static com.pjpj.heartrate.ui.rate.RateTestActivity.ARGUMENT_INT_EXERCISE_TYPE;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.lifecycle.Lifecycle;

import com.leg.yuandu.utils.Utils;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.base.BaseViewModelActivity;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.data.ExerciseData;
import com.pjpj.heartrate.databinding.ActivityStatementBinding;
import com.pjpj.heartrate.databinding.ExerciseActivityBinding;
import com.pjpj.heartrate.ui.AboutUsActivity;
import com.pjpj.heartrate.ui.rate.RateTestActivity;
import com.pjpj.heartrate.util.CountDownTimerComponent;
import com.pjpj.heartrate.util.UIHelper;

/**
 * Des:
 * Create by: snow
 * On:        2022\01\20 18:14
 * Email:     snow@imageview.cc
 */
public class StatementActivity extends BaseViewModelActivity<ActivityStatementBinding, BaseViewModel> {


    public static void show(Context context) {
        Intent intent = new Intent(context, StatementActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected StatusBarStyle initStatusBar() {
        return StatusBarStyle.FULL;
    }

    @Override
    protected void initArgument(Bundle bundle) {
        super.initArgument(bundle);

    }

    @Override
    protected void initView() {

        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        binding.tvContent3.getPaint().setUnderlineText(true);
        binding.tvContent3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Utils.openActivitySafely(StatementActivity.this, Intent.ACTION_VIEW,
                        "https://dl.acm.org/doi/10.1145/2491148.2491156", null);
            }
        });

        binding.tvContent5.getPaint().setUnderlineText(true);
        binding.tvContent5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.openActivitySafely(StatementActivity.this, Intent.ACTION_VIEW,
                        "https://en.wikipedia.org/wiki/Heart_rate", null);
            }
        });
    }

    @Override
    protected void initLife(Lifecycle lifecycle) {
        super.initLife(lifecycle);

    }

    @Override
    protected void initData() {
        super.initData();
    }


}
