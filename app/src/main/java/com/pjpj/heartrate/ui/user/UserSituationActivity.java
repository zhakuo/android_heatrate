package com.pjpj.heartrate.ui.user;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.pjpj.heartrate.base.AppConfig;
import com.pjpj.heartrate.base.BaseViewModelActivity;
import com.pjpj.heartrate.databinding.UserSelectSituationActivityBinding;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\17 15:23
 * Email:     yeshieh@163.com
 */
public class UserSituationActivity extends BaseViewModelActivity<UserSelectSituationActivityBinding, UserViewModel> {
    public static int SITUATION_GYM = 1;
    public static int SITUATION_WORKING = 2;
    public static int SITUATION_HOME = 3;
    public static int SITUATION_STUDENT = 4;
    public static int SITUATION_LEISURE = 5;

    private int mSituation = 1;

    public static void show(Context context) {
        Intent intent = new Intent(context, UserSituationActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected StatusBarStyle initStatusBar() {
        return StatusBarStyle.FULL;
    }

    @Override
    protected void initView() {
        binding.tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.llGym.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSituation = SITUATION_GYM;
                updateCheckView();
            }
        });
        binding.llWorking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSituation = SITUATION_WORKING;
                updateCheckView();
            }
        });
        binding.llHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSituation = SITUATION_HOME;
                updateCheckView();
            }
        });
        binding.llStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSituation = SITUATION_STUDENT;
                updateCheckView();
            }
        });
        binding.llLeisure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSituation = SITUATION_LEISURE;
                updateCheckView();
            }
        });
        binding.llDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppConfig.setSituation(mSituation);
                finish();
            }
        });
    }

    private void updateCheckView() {
        binding.ivGymCheck.setVisibility(mSituation == SITUATION_GYM ? View.VISIBLE : View.GONE);
        binding.ivWorkingCheck.setVisibility(mSituation == SITUATION_WORKING ? View.VISIBLE : View.GONE);
        binding.ivHomeCheck.setVisibility(mSituation == SITUATION_HOME ? View.VISIBLE : View.GONE);
        binding.ivStudentCheck.setVisibility(mSituation == SITUATION_STUDENT ? View.VISIBLE : View.GONE);
        binding.ivLeisureCheck.setVisibility(mSituation == SITUATION_LEISURE ? View.VISIBLE : View.GONE);
    }
}
