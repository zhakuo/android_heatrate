package com.pjpj.heartrate.ui.statement;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.pjpj.heartrate.databinding.StatementTipsDialogBinding;
import com.pjpj.heartrate.view.dialog.BaseDialog;

public class StatementTipsDialog extends BaseDialog<StatementTipsDialog> {
    StatementTipsDialogBinding binding;

    public StatementTipsDialog(Context context) {
        super(context);
    }

    @Override
    public View onCreateView() {
        binding = StatementTipsDialogBinding.inflate(LayoutInflater.from(mContext));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view) {
        super.onViewCreated(view);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        binding.llAccess.setOnClickListener(onAccessListener);
    }

    View.OnClickListener onAccessListener;

    public void setOnAccessListener(View.OnClickListener onAccessListener) {
        this.onAccessListener = onAccessListener;
    }

    @Override
    public void setUiBeforeShow() {
        widthScale(0.9f);
    }
}
