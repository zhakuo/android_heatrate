package com.pjpj.heartrate.ui.rate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.leg.yuandu.gameutils.BostonUtils;
import com.leg.yuandu.utils.AppConfig;
import com.pjpj.heartrate.base.BaseViewModelActivity;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.databinding.ReportActivityBinding;
import com.workout.base.GlobalConst;
import com.workout.base.SportObj;

import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;

public class RateReportActivity extends BaseViewModelActivity<ReportActivityBinding, RateViewModel> {

    Calendar mCalendar = Calendar.getInstance();

    private MaxInterstitialAd interstitialAd;

    public static void show(Context context) {
        Intent intent = new Intent(context, RateReportActivity.class);
        context.startActivity(intent);
    }

    private boolean isMonth = true;

    @Override
    protected void initView() {
        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (interstitialAd != null && interstitialAd.isReady()){
                    interstitialAd.showAd();
                }
                finish();
            }
        });
        binding.llMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isMonth = true;
                binding.llMonth.getDelegate().showGradient(true);
                binding.llYear.getDelegate().showGradient(false);
                binding.tvMonth.setTextColor(Color.parseColor("#ffffff"));
                binding.tvYear.setTextColor(Color.parseColor("#6DB5FF"));
                viewModel.loadReportMonth();
            }
        });
        binding.llYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isMonth = false;
                binding.llYear.getDelegate().showGradient(true);
                binding.llMonth.getDelegate().showGradient(false);
                binding.tvYear.setTextColor(Color.parseColor("#ffffff"));
                binding.tvMonth.setTextColor(Color.parseColor("#6DB5FF"));
                viewModel.loadReportYear();
            }
        });
        binding.llMonth.getDelegate().showGradient(true);
        binding.llYear.getDelegate().showGradient(false);
        binding.tvMonth.setTextColor(Color.parseColor("#ffffff"));
        binding.tvYear.setTextColor(Color.parseColor("#6DB5FF"));
        binding.ivTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RateTestActivity.show(mActivity);
                finish();
            }
        });
        initChart();
    }

    @Override
    protected void initLife(Lifecycle lifecycle) {
        super.initLife(lifecycle);
        viewModel.mReportMonthLiveData.observe(this, new Observer<List<Integer>>() {
            @Override
            public void onChanged(List<Integer> integers) {
                binding.chart.getData().clearValues();
                for (int i = 0; i < integers.size(); i++) {
                    addEntry(integers.get(i));
                }
                binding.chart.animateX(0);
            }
        });
        viewModel.mReportYearLiveData.observe(this, new Observer<List<Integer>>() {
            @Override
            public void onChanged(List<Integer> integers) {
                binding.chart.getData().clearValues();
                for (int i = 0; i < integers.size(); i++) {
                    addEntry(integers.get(i));
                }
                binding.chart.animateX(0);
            }
        });
    }

    @Override
    protected void initData() {
        super.initData();
        viewModel.loadReportMonth();
        String interstitialTopOnPlacementID = BostonUtils.getString(AppConfig.max_id_config);
        createInterstitialAd(interstitialTopOnPlacementID, this);
    }


    private void initChart() {
        binding.chart.getDescription().setEnabled(false);
        binding.chart.setTouchEnabled(false);
        binding.chart.setDragEnabled(true);
        binding.chart.setScaleEnabled(false);
        binding.chart.setDrawGridBackground(false);
        binding.chart.setHighlightPerDragEnabled(false);
        binding.chart.setDrawBorders(false);
        binding.chart.setExtraBottomOffset(10f);

        binding.chart.setBackgroundColor(Color.TRANSPARENT);

        // get the legend (only possible after setting data)
        Legend l = binding.chart.getLegend();
        l.setEnabled(false);

        XAxis xAxis = binding.chart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1);
        /*又说不要了*/
        xAxis.setEnabled(false);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                if (isMonth) {
                    return (int) (value + 1) + "号";
                } else {
                    return (int) (value + 1) + "月";
                }
            }
        });

        YAxis leftAxis = binding.chart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setTextColor(Color.parseColor("#b331404D"));
        leftAxis.setTextSize(14);
        leftAxis.setDrawLabels(true);
        leftAxis.setLabelCount(5, true);//坐标
        leftAxis.setDrawGridLines(true);
        leftAxis.enableGridDashedLine(10, 10, 0);
        leftAxis.setGranularityEnabled(false);
        leftAxis.setAxisMinimum(40);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setAxisMaximum(120);
        leftAxis.setEnabled(true);

        YAxis rightAxis = binding.chart.getAxisRight();
        rightAxis.setEnabled(false);

        LineData data = new LineData();
        data.setValueTextColor(Color.WHITE);

        // add empty data
        binding.chart.setData(data);
    }

    public void addEntry(double y) {
        LineData data = binding.chart.getData();
        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            data.addEntry(new Entry(set.getEntryCount(), (float) y), 0);
            data.notifyDataChanged();

            // let the chart know it's data has changed
            binding.chart.notifyDataSetChanged();

            // limit the number of visible entries
            binding.chart.setVisibleXRangeMaximum(7);
            binding.chart.setVisibleXRangeMinimum(7);
        }
    }

    private LineDataSet createSet() {
        LineDataSet set = new LineDataSet(null, "rate Data");
        set.setDrawCircles(true);
        set.setCircleColor(Color.parseColor("#6DB5FF"));
        set.setCircleRadius(2);
        set.setDrawValues(false);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(ColorTemplate.getHoloBlue());
        set.setLineWidth(2);
        set.setCubicIntensity(0.3f);
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        return set;
    }

    private int retryAttempt;
    private void createInterstitialAd(String adUnitId, Activity context)
    {
        interstitialAd = new MaxInterstitialAd( adUnitId, context );
        interstitialAd.setListener(new MaxAdListener() {
            @Override
            public void onAdLoaded(MaxAd ad) {
                retryAttempt = 0;
//                Log.d("snow", "MaxAd onAdLoaded");
                SportObj.setAlObj(interstitialAd);
                SportObj.isCalling = false;
            }

            @Override
            public void onAdDisplayed(MaxAd ad) {

            }

            @Override
            public void onAdHidden(MaxAd ad) {
                interstitialAd.loadAd();
            }

            @Override
            public void onAdClicked(MaxAd ad) {

            }

            @Override
            public void onAdLoadFailed(String adUnitId, MaxError error) {
                retryAttempt++;
                long delayMillis = TimeUnit.SECONDS.toMillis( (long) Math.pow( 2, Math.min( 3, retryAttempt ) ) );

                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        interstitialAd.loadAd();
                    }
                }, delayMillis );
            }

            @Override
            public void onAdDisplayFailed(MaxAd ad, MaxError error) {
                interstitialAd.loadAd();
            }
        });

        // Load the first ad
        interstitialAd.loadAd();
//        Log.d("snow", "MaxAd loadAd" );
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
//                Log.d("snow", "onKeyDown");
                if (interstitialAd != null && interstitialAd.isReady()){
                    interstitialAd.showAd();
                }
                finish();
                break;
        }
        return super.onKeyDown(keyCode, event);
    }
}
