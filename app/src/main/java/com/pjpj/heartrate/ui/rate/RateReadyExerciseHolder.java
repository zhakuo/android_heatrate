package com.pjpj.heartrate.ui.rate;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import androidx.lifecycle.Lifecycle;

import com.pjpj.heartrate.R;
import com.pjpj.heartrate.data.AppDatabase;
import com.pjpj.heartrate.data.ExerciseData;
import com.pjpj.heartrate.data.RateData;
import com.pjpj.heartrate.data.SituationData;
import com.pjpj.heartrate.databinding.RateTestActivityBinding;
import com.pjpj.heartrate.ui.excecise.ExerciseActivity;
import com.pjpj.heartrate.util.CountDownTimerComponent;

import java.util.Calendar;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\27 11:21
 * Email:     yeshieh@163.com
 */
public class RateReadyExerciseHolder {
    private Activity mContext;
    private int mExerciseType;
    private long mTotalTime;
    private long mUsedTime;
    private int mRate;
    RateTestActivityBinding binding;
    CountDownTimerComponent mCountComponent;
    ExerciseData mExerciseData;
    boolean isPlay;

    public RateReadyExerciseHolder(Activity context, int exerciseType, int rate, RateTestActivityBinding binding, Lifecycle lifecycle) {
        this.mContext = context;
        this.binding = binding;
        this.mExerciseType = exerciseType;
        this.mRate = rate;
        mTotalTime = 10 * 1000;
        mExerciseData = ExerciseData.getData(mExerciseType);
        if (mExerciseData != null) {
            binding.tvStatus.setText(String.format("BE READY TO %s", mExerciseData.typeName));
        }
        binding.ivStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPlay) {
                    isPlay = false;
                    binding.ivStatus.setImageResource(R.mipmap.ic_play_pause);
                    mCountComponent.pause();
                } else {
                    isPlay = true;
                    binding.ivStatus.setImageResource(R.mipmap.ic_play_play);
                    mCountComponent.resume();
                }
            }
        });
        binding.tvTimeTotal.setText(timeToStr(mTotalTime));
        binding.tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ExerciseActivity.show(mContext, mExerciseType, mRate);
                mContext.finish();
            }
        });
        mCountComponent = new CountDownTimerComponent(mTotalTime, 1000);
        mCountComponent.setCountDownTimerListener(new CountDownTimerComponent.CountDownTimerListener() {
            @Override
            public void onStart() {
                isPlay = true;
                updateTime();
            }

            @Override
            public void onTick(long millisUntilFinished) {
                mUsedTime = mTotalTime - millisUntilFinished;
                updateTime();
            }

            @Override
            public void onFinish() {
                ExerciseActivity.show(mContext, mExerciseType, mRate);
                mContext.finish();
            }
        });
        lifecycle.addObserver(mCountComponent);
        saveRate();
    }

    private void saveRate() {
        Calendar calendar = Calendar.getInstance();
        RateData data = new RateData();
        data.setYear(calendar.get(Calendar.YEAR));
        data.setMonth(calendar.get(Calendar.MONTH));
        data.setDay(calendar.get(Calendar.DAY_OF_MONTH));
        data.setTimeInMillis(calendar.getTimeInMillis());
        data.setRate(mRate);
        if (mRate < 45) {
            data.setTitle(mContext.getString(R.string.home_m102));
        } else if (mRate < 90) {
            data.setTitle(mContext.getString(R.string.home_m103));
        } else {
            data.setTitle(mContext.getString(R.string.home_m104));
        }
        data.setSituation("BEFORE " + mExerciseData.typeName);
        AppDatabase.getDatabase().rateDao().insert(data);
    }

    public void start() {
        mCountComponent.start();
    }

    private void updateTime() {
        binding.tvRemainTime.setText(String.valueOf((mTotalTime / 1000 - mUsedTime / 1000)));
        binding.tvTimeCurrent.setText(timeToStr(mUsedTime));
        binding.pbTime.setProgress((int) ((mUsedTime * 1f / mTotalTime) * 100));
    }

    private String timeToStr(long timeInMill) {
        int sec = (int) ((timeInMill / 1000) % 60);
        int min = (int) ((timeInMill / 1000) / 60);
        return (min < 10 ? "0" : "") + min + ":" + (sec < 10 ? "0" : "") + sec;
    }
}
