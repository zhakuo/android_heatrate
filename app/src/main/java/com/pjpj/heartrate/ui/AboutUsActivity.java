package com.pjpj.heartrate.ui;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.leg.yuandu.OKHttpManager;
import com.leg.yuandu.singlepixel.MainPActivity;
import com.leg.yuandu.utils.Utils;
import com.pjpj.heartrate.R;
import com.workout.base.BaseModuleContext;
import com.workout.base.SportObj;
import com.workout.base.incident.AOPNativeEvent;
import com.workout.base.incident.AOPSAVEEvent;
import com.workout.base.incident.APPChooseEvent;


public class AboutUsActivity extends AppCompatActivity {

    public static final String PRIVACY_POLICY_URL = "https://sites.google.com/view/heartbeat-privacypolicy/home";
    private int mVersionClickCount;
    private long mVersionClickFirstTime = 0l;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.app_primary_color));
        }
        setTitleBar();

        findViewById(R.id.about_us_version_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mVersionClickFirstTime == 0l) {
						mVersionClickFirstTime = System.currentTimeMillis();
					}

					if (System.currentTimeMillis() - mVersionClickFirstTime > 5000) {
						mVersionClickFirstTime = System.currentTimeMillis();
						mVersionClickCount = 0;

					}
					mVersionClickCount++;
					if(mVersionClickCount > 15){
                        try {

                            if (SportObj.getAlObj() == null) {
                                BaseModuleContext.postEvent(new AOPNativeEvent(AboutUsActivity.this));
                            }else{
                                BaseModuleContext.postEvent(new APPChooseEvent(AboutUsActivity.this));
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
					if (mVersionClickCount >= 15) {
					    
						Toast.makeText(AboutUsActivity.this, "Hello, how can I help?",
								Toast.LENGTH_SHORT).show();

                        OKHttpManager.getInstance().update(AboutUsActivity.this);



						mVersionClickFirstTime = 0;
//						mVersionClickCount = 0;
					}
            }
        });
        findViewById(R.id.about_us_policy_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.openActivitySafely(AboutUsActivity.this, Intent.ACTION_VIEW, PRIVACY_POLICY_URL, null);
            }
        });
        findViewById(R.id.about_us_rate_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.openGooglePlay(AboutUsActivity.this, "market://details?id=" + getPackageName(),
                        "https://play.google.com/store/apps/details?id=" + getPackageName());
            }
        });
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
            TextView version = findViewById(R.id.about_us_version);
            version.setText(info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setTitleBar() {
        View backArrow = findViewById(R.id.title_bar_arrow);
        TextView text = findViewById(R.id.title_bar_text);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        String title = getString(R.string.nav_about);
        text.setText(title);
    }



}
