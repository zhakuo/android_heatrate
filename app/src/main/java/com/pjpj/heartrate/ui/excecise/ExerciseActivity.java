package com.pjpj.heartrate.ui.excecise;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.lifecycle.Lifecycle;

import com.pjpj.heartrate.R;
import com.pjpj.heartrate.base.BaseViewModelActivity;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.data.ExerciseData;
import com.pjpj.heartrate.databinding.ExerciseActivityBinding;
import com.pjpj.heartrate.ui.rate.RateTestActivity;
import com.pjpj.heartrate.util.CountDownTimerComponent;
import com.pjpj.heartrate.util.UIHelper;

import static com.pjpj.heartrate.ui.rate.RateTestActivity.ARGUMENT_INT_BEFORE_EXERCISE_RATE;
import static com.pjpj.heartrate.ui.rate.RateTestActivity.ARGUMENT_INT_EXERCISE_TYPE;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\26 17:33
 * Email:     yeshieh@163.com
 */
public class ExerciseActivity extends BaseViewModelActivity<ExerciseActivityBinding, BaseViewModel> {

    private CountDownTimerComponent mCountComponent;

    private int mExerciseType;
    private ExerciseData mExerciseData;
    private long mTotalTime;
    private long mUsedTime;
    private int mTotalCount;

    private int mBeforeRate;

    private boolean mIsPlay = false;

    public static void show(Context context, int exerciseType, int rate) {
        Intent intent = new Intent(context, ExerciseActivity.class);
        intent.putExtra(ARGUMENT_INT_EXERCISE_TYPE, exerciseType);
        intent.putExtra(ARGUMENT_INT_BEFORE_EXERCISE_RATE, rate);
        context.startActivity(intent);
    }

    @Override
    protected StatusBarStyle initStatusBar() {
        return StatusBarStyle.FULL;
    }

    @Override
    protected void initArgument(Bundle bundle) {
        super.initArgument(bundle);
        mExerciseType = bundle.getInt(ARGUMENT_INT_EXERCISE_TYPE);
        mBeforeRate = bundle.getInt(ARGUMENT_INT_BEFORE_EXERCISE_RATE);
        mExerciseData = ExerciseData.getData(mExerciseType);
        if (mExerciseData == null) {
            finish();
        }
    }

    @Override
    protected void initView() {
        ((LinearLayout.LayoutParams) binding.llBack.getLayoutParams()).topMargin = UIHelper.getStatusBarHeight(mActivity);

        mTotalTime = mExerciseData.totalSec * 1000;
        mTotalCount = mExerciseData.totalCount;
        if (mExerciseType == ExerciseData.MEDITATION) {
            binding.bgExercise.setVisibility(View.GONE);
            binding.bgBreath.setVisibility(View.VISIBLE);
            binding.bgBreath.setAnimation("anim/breath_data.json");
            binding.bgBreath.setRepeatCount(ValueAnimator.INFINITE);
            binding.bgBreath.playAnimation();
        } else {
            binding.bgExercise.setVisibility(View.VISIBLE);
            binding.bgBreath.setVisibility(View.GONE);
            binding.bgExercise.setImageResource(mExerciseData.exerciseRes);
        }

        binding.tvStatus.setText(mExerciseData.typeName);
        binding.tvTimeTotal.setText(timeToStr(mTotalTime));

        binding.ivStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIsPlay) {
                    mIsPlay = false;
                    binding.ivStatus.setImageResource(R.mipmap.ic_play_pause);
                    mCountComponent.pause();
                } else {
                    mIsPlay = true;
                    binding.ivStatus.setImageResource(R.mipmap.ic_play_play);
                    mCountComponent.resume();
                }
            }
        });
        binding.tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RateTestActivity.showAfterExercise(mActivity, mBeforeRate, mExerciseType);
                finish();
            }
        });
    }

    @Override
    protected void initLife(Lifecycle lifecycle) {
        super.initLife(lifecycle);
        mCountComponent = new CountDownTimerComponent(mTotalTime, 1000);
        mCountComponent.setCountDownTimerListener(new CountDownTimerComponent.CountDownTimerListener() {
            @Override
            public void onStart() {
                mIsPlay = true;
                updateTime();
            }

            @Override
            public void onTick(long millisUntilFinished) {
                mUsedTime = mTotalTime - millisUntilFinished;
                updateTime();
            }

            @Override
            public void onFinish() {
                RateTestActivity.showAfterExercise(mActivity, mBeforeRate, mExerciseType);
                finish();
            }
        });
        lifecycle.addObserver(mCountComponent);
    }

    @Override
    protected void initData() {
        super.initData();
        mCountComponent.start();
    }

    private void updateTime() {
        binding.tvTimeCurrent.setText(timeToStr(mUsedTime));
        binding.pbTime.setProgress((int) ((mUsedTime * 1f / mTotalTime) * 100));
        if (mTotalCount > 0) {
            long timePerCount = mTotalTime / mTotalCount;
            int currentCount = (int) (mUsedTime / timePerCount + 1);
            binding.tvStatus.setText(currentCount + "/" + mTotalCount + " " + mExerciseData.typeName);
        }
    }

    private String timeToStr(long timeInMill) {
        int sec = (int) ((timeInMill / 1000) % 60);
        int min = (int) ((timeInMill / 1000) / 60);
        return (min < 10 ? "0" : "") + min + ":" + (sec < 10 ? "0" : "") + sec;
    }

}
