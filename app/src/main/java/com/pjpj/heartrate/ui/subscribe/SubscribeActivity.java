package com.pjpj.heartrate.ui.subscribe;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.pjpj.heartrate.R;
import com.pjpj.heartrate.base.BaseViewModelActivity;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.databinding.SubscribeActivityBinding;
import com.pjpj.heartrate.ui.web.WebActivity;

public class SubscribeActivity extends BaseViewModelActivity<SubscribeActivityBinding, BaseViewModel> {

    private boolean isTrail;

    public static void show(Context context) {
        Intent intent = new Intent(context, SubscribeActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected StatusBarStyle initStatusBar() {
        return StatusBarStyle.FULLDARK;
    }

    @Override
    protected void initView() {
        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.flSubscribeYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        binding.flSubscribeWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        binding.tvSubscribeText.setText(getString(R.string.subscribe_text, "200", "Year", "32", "week"));
        binding.ivOpenTrial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isTrail) {
                    isTrail = false;
                    binding.ivOpenTrial.setImageResource(R.mipmap.ic_subsribe_off);
                } else {
                    isTrail = true;
                    binding.ivOpenTrial.setImageResource(R.mipmap.ic_subsribe_on);
                }
            }
        });
        binding.tvPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebActivity.openWebSafely(mActivity, getString(R.string.app_privacy_url));
            }
        });
        binding.tvUserTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebActivity.openWebSafely(mActivity, getString(R.string.app_terms_url));
            }
        });
    }
}
