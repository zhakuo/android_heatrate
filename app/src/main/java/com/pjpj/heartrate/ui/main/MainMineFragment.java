package com.pjpj.heartrate.ui.main;

import android.content.Intent;
import android.view.View;

import com.pjpj.heartrate.R;
import com.pjpj.heartrate.base.AppConfig;
import com.pjpj.heartrate.base.BaseViewModelFragment;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.databinding.MainMineFragmentBinding;
import com.pjpj.heartrate.ui.AboutUsActivity;
import com.pjpj.heartrate.ui.FeedbackActivity;
import com.pjpj.heartrate.ui.web.WebActivity;

import java.util.Calendar;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\11 15:38
 * Email:     yeshieh@163.com
 */
public class MainMineFragment extends BaseViewModelFragment<MainMineFragmentBinding, BaseViewModel> {
    @Override
    protected void initView() {
        boolean isMale = AppConfig.getGender() == 1;
        binding.ivHead.setImageResource(isMale ? R.mipmap.ic_gender_male : R.mipmap.ic_gender_female);
        //binding.tvName.setText("ADMIN_" + AppConfig.getUserId());
        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        if (hourOfDay > 5 && hourOfDay < 12) {
            binding.tvGreet.setText("hi~Good Morning");
        } else if (hourOfDay >= 12 && hourOfDay < 19) {
            binding.tvGreet.setText("hi~Good Afternoon");
        } else {
            binding.tvGreet.setText("hi~Good Everning");
        }

        binding.llPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebActivity.openWebSafely(getActivity(), getString(R.string.app_privacy_url));
            }
        });
        binding.llTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebActivity.openWebSafely(getActivity(), getString(R.string.app_terms_url));
            }
        });

        binding.llAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AboutUsActivity.class);
                getActivity().startActivity(i);
            }
        });
    }
}
