package com.pjpj.heartrate.ui.web;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
/**
 * Des:
 * Create by: ye
 * On:        2021\08\25 15:53
 * Email:     yeshieh@163.com
 */
public class WebActivity {

    private final static String ARGUMENT_STRING_URL = "url";
    private final static String ARGUMENT_STRING_TITLE = "title";

    public static void show(Context context, String url, String title) {
        Intent intent = new Intent(context, WebActivity.class);
        intent.putExtra(ARGUMENT_STRING_URL, url);
        intent.putExtra(ARGUMENT_STRING_TITLE, title);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static boolean openWebSafely(Context context, String uri) {
        boolean isOk = true;
        try {
            Uri uriData = Uri.parse(uri);
            final Intent intent = new Intent(Intent.ACTION_VIEW, uriData);
            if (!Activity.class.isInstance(context)) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            context.startActivity(intent);
        } catch (Throwable e) {
            e.printStackTrace();
            isOk = false;
        }
        return isOk;
    }

}
