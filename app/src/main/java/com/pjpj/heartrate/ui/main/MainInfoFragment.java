package com.pjpj.heartrate.ui.main;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.pjpj.heartrate.base.BaseViewModelFragment;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.data.ArticleData;
import com.pjpj.heartrate.databinding.MainInfoFragmentBinding;
import com.pjpj.heartrate.ui.article.ArticleAdapter;
import com.pjpj.heartrate.ui.article.ArticleDetailDialog;
import com.pjpj.heartrate.ui.article.ArticleDetailDialog1;
import com.pjpj.heartrate.util.UIHelper;
import com.pjpj.heartrate.widget.AppRvSpaceDecorator;

import java.util.ArrayList;
import java.util.List;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\11 15:36
 * Email:     yeshieh@163.com
 */
public class MainInfoFragment extends BaseViewModelFragment<MainInfoFragmentBinding, BaseViewModel> {
    ArticleAdapter mAdapter;

    @Override
    protected void initView() {
        binding.rvList.addItemDecoration(new AppRvSpaceDecorator(UIHelper.dpToPx(20), UIHelper.dpToPx(20), 0, true));
        binding.rvList.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvList.setAdapter(mAdapter = new ArticleAdapter());

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                ArticleData article = mAdapter.getItem(i);
                ArticleDetailDialog1 dialog1 = new ArticleDetailDialog1();
                dialog1.setArticleData(article);
                dialog1.show(getChildFragmentManager(), "article");
            }
        });
    }

    @Override
    protected void initData() {
        super.initData();
        mAdapter.setNewData(ArticleData.articleList);
    }
}
