package com.pjpj.heartrate.ui.rate;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.data.RateDao;
import com.pjpj.heartrate.data.RateData;

import java.text.SimpleDateFormat;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\25 16:06
 * Email:     yeshieh@163.com
 */
public class RateHistoryAdapter extends BaseQuickAdapter<RateData, BaseViewHolder> {
    public RateHistoryAdapter() {
        super(R.layout.rate_history_item_view);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, RateData rateData) {
        if (getData().size() == 1) {
            baseViewHolder.setGone(R.id.lineTop, false);
            baseViewHolder.setGone(R.id.lineBottom, false);
        } else {
            if (baseViewHolder.getAdapterPosition() == 0) {
                baseViewHolder.setGone(R.id.lineTop, false);
                baseViewHolder.setGone(R.id.lineBottom, true);
            } else if (baseViewHolder.getAdapterPosition() == getData().size() - 1) {
                baseViewHolder.setGone(R.id.lineTop, true);
                baseViewHolder.setGone(R.id.lineBottom, false);
            } else {
                baseViewHolder.setGone(R.id.lineTop, true);
                baseViewHolder.setGone(R.id.lineBottom, true);
            }
        }
        String time = new SimpleDateFormat("yyyy/MM/dd\nHH:mm:ss").format(rateData.getTimeInMillis());
        baseViewHolder.setText(R.id.tvRate, String.valueOf(rateData.getRate()));
        baseViewHolder.setText(R.id.tvTime, time);
        baseViewHolder.setText(R.id.tvTitle, rateData.getSituation());
        if (rateData.getTitle().contains("%s")) {
            baseViewHolder.setText(R.id.tvSubTitle, String.format(rateData.getTitle(), rateData.getRate()));
        } else {
            baseViewHolder.setText(R.id.tvSubTitle, rateData.getTitle());
        }

    }
}
