package com.pjpj.heartrate.ui.article;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.data.ArticleData;
import com.pjpj.heartrate.util.ResourceUtils;

import java.util.List;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\25 11:11
 * Email:     yeshieh@163.com
 */
public class ArticleAdapter extends BaseQuickAdapter<ArticleData, BaseViewHolder> {
    public ArticleAdapter() {
        super(R.layout.rate_article_item_view);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ArticleData articleData) {
        //baseViewHolder.setGone(R.id.llVip, articleData.isVip);
        baseViewHolder.setText(R.id.tvTitle, articleData.title);
        baseViewHolder.setText(R.id.tvContent, articleData.text);
        baseViewHolder.setImageResource(R.id.ivContent, ResourceUtils.getResId(mContext, articleData.image, "mipmap"));
    }
}
