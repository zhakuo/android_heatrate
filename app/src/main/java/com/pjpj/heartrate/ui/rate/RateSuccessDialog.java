package com.pjpj.heartrate.ui.rate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.pjpj.heartrate.data.AppDatabase;
import com.pjpj.heartrate.data.RateData;
import com.pjpj.heartrate.data.SituationData;
import com.pjpj.heartrate.databinding.RateSuccessDialogBinding;
import com.pjpj.heartrate.ui.statement.StatementActivity;
import com.pjpj.heartrate.util.UIHelper;
import com.pjpj.heartrate.view.dialog.BottomBaseDialog;
import com.pjpj.heartrate.widget.AppRvSpaceDecorator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\24 17:52
 * Email:     yeshieh@163.com
 */
public class RateSuccessDialog extends BottomBaseDialog<RateSuccessDialog> {
    RateSuccessDialogBinding binding;
    RateSituationAdapter mAdapter;
    private int mRate;
    private ItemClick mItemClick;

    public void setItemClick(ItemClick itemClick) {
        mItemClick = itemClick;
    }

    public void setRate(int rate) {
        mRate = rate;
    }

    public RateSuccessDialog(Context context) {
        super(context);
    }

    @Override
    public View onCreateView() {
        binding = RateSuccessDialogBinding.inflate(LayoutInflater.from(mContext));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view) {
        super.onViewCreated(view);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        binding.rvSituation.setLayoutManager(new GridLayoutManager(mContext, 2));
        binding.rvSituation.addItemDecoration(new AppRvSpaceDecorator(UIHelper.dpToPx(16), 2));
        mAdapter = new RateSituationAdapter();
        binding.rvSituation.setAdapter(mAdapter);

        mAdapter.setNewData(SituationData.situationList);

        binding.llSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                SituationData situationData = mAdapter.getSelectedItem();
                RateData data = new RateData();
                data.setYear(calendar.get(Calendar.YEAR));
                data.setMonth(calendar.get(Calendar.MONTH));
                data.setDay(calendar.get(Calendar.DAY_OF_MONTH));
                data.setTimeInMillis(calendar.getTimeInMillis());
                data.setRate(mRate);
                if (mRate < situationData.min) {
                    data.setTitle(situationData.minHistory);
                } else if (mRate >= situationData.mMin && mRate < situationData.mMax) {
                    data.setTitle(situationData.mHistory);
                } else if (mRate >= situationData.lMin && mRate < situationData.lMax) {
                    data.setTitle(situationData.llHistory);
                } else if (mRate >= situationData.max) {
                    data.setTitle(situationData.maxHistory);
                }
                data.setSituation(mAdapter.getSelectedItem().sstatusStr);
                AppDatabase.getDatabase().rateDao().insert(data);
                if (mItemClick != null) {
                    mItemClick.onItemClick(mAdapter.getSelectedItem());
                }
                dismiss();
            }
        });

        binding.statementImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StatementActivity.show(mContext);
            }
        });
    }

    public interface ItemClick {
        void onItemClick(SituationData data);
    }

    @Override
    public void setUiBeforeShow() {
        binding.tvRate.setText(String.valueOf(mRate));
    }
}
