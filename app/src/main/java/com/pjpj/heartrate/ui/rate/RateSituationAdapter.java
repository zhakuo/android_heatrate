package com.pjpj.heartrate.ui.rate;

import android.graphics.Color;
import android.view.View;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.flyco.roundview.RoundFrameLayout;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.data.SituationData;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\24 18:00
 * Email:     yeshieh@163.com
 */
public class RateSituationAdapter extends BaseQuickAdapter<SituationData, BaseViewHolder> {
    private int mSelectedIndex;

    public RateSituationAdapter() {
        super(R.layout.rate_situation_item_view);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, SituationData data) {
        baseViewHolder.setText(R.id.tv_content,data.sstatusStr);
        RoundFrameLayout rvView = baseViewHolder.getView(R.id.rl_content);
        if (baseViewHolder.getAdapterPosition() == mSelectedIndex) {
            rvView.getDelegate().setBackgroundColor(Color.parseColor("#6DB5FF"));
            baseViewHolder.setTextColor(R.id.tv_content, Color.parseColor("#ffffff"));
        } else {
            rvView.getDelegate().setBackgroundColor(Color.parseColor("#4d6DB5FF"));
            baseViewHolder.setTextColor(R.id.tv_content, Color.parseColor("#31404D"));
        }
        baseViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectedIndex = baseViewHolder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });
    }

    public SituationData getSelectedItem() {
        return getItem(mSelectedIndex);
    }
}
