package com.pjpj.heartrate.ui;

import android.animation.Animator;
import android.animation.ValueAnimator;

import com.airbnb.lottie.RenderMode;
import com.pjpj.heartrate.MainActivity;
import com.pjpj.heartrate.base.BaseViewModelActivity;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.databinding.SplashActivityBinding;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\26 16:17
 * Email:     yeshieh@163.com
 */
public class SplashActivity extends BaseViewModelActivity<SplashActivityBinding, BaseViewModel> {

    @Override
    protected StatusBarStyle initStatusBar() {
        return StatusBarStyle.FULL;
    }

    @Override
    protected void initView() {
        binding.animateView.setImageAssetsFolder("anim/splash/images");
        binding.animateView.setAnimation("anim/splash/welcome_data.json");
        binding.animateView.setRenderMode(RenderMode.SOFTWARE);
        binding.animateView.setRepeatCount(0);

        binding.animateView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                MainActivity.show(mActivity);
                finish();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        MainActivity.show(mActivity);
        finish();
        //binding.animateView.playAnimation();
    }
}
