package com.pjpj.heartrate.ui.rate;

import android.content.Context;
import android.content.Intent;
import android.graphics.BlurMaskFilter;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.MaskFilterSpan;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.pjpj.heartrate.base.BaseViewModelActivity;
import com.pjpj.heartrate.data.ArticleData;
import com.pjpj.heartrate.data.SituationData;
import com.pjpj.heartrate.databinding.RateResultActivityBinding;
import com.pjpj.heartrate.ui.article.ArticleAdapter;
import com.pjpj.heartrate.ui.article.ArticleDetailDialog1;
import com.pjpj.heartrate.util.UIHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\25 11:21
 * Email:     yeshieh@163.com
 */
public class RateResultActivity extends BaseViewModelActivity<RateResultActivityBinding, RateViewModel> {
    private final static String ARGUMENT_INT_RATE = "rate";
    private final static String ARGUMENT_CLASS_SITUATION = "situation";

    private SituationData mSituationData;
    private int mRate;
    private ArticleAdapter mAdapter;

    public static void show(Context context, int rate, SituationData data) {
        Intent intent = new Intent(context, RateResultActivity.class);
        intent.putExtra(ARGUMENT_INT_RATE, rate);
        intent.putExtra(ARGUMENT_CLASS_SITUATION, data);
        context.startActivity(intent);
    }

    @Override
    protected StatusBarStyle initStatusBar() {
        return StatusBarStyle.FULL;
    }

    @Override
    protected void initArgument(Bundle bundle) {
        super.initArgument(bundle);
        mRate = bundle.getInt(ARGUMENT_INT_RATE);
        mSituationData = (SituationData) bundle.getSerializable(ARGUMENT_CLASS_SITUATION);
    }

    @Override
    protected void initView() {
        binding.llContainer.setPadding(0, UIHelper.getStatusBarHeight(mActivity), 0, 0);
        binding.llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.tvRate.setText(String.valueOf(mRate));

        String title = "";
        String hypertensionTitle = "";
        String cardiopulmonaryTitle = "";
        int suggest1 = 0;
        int suggest2 = 0;
        if (mRate < mSituationData.min) {
            title = mSituationData.minResultDiagnosis;
            hypertensionTitle = mSituationData.minHypertension;
            cardiopulmonaryTitle = mSituationData.minCardiopulmonary;
            suggest1 = mSituationData.minSuggest1;
            suggest2 = mSituationData.minSuggest2;
        } else if (mRate >= mSituationData.mMin && mRate < mSituationData.mMax) {
            title = mSituationData.mResultDiagnosis;
            hypertensionTitle = mSituationData.mHypertension;
            cardiopulmonaryTitle = mSituationData.mCardiopulmonary;
            suggest1 = mSituationData.mSuggest1;
            suggest2 = mSituationData.mSuggest2;
        } else if (mRate >= mSituationData.lMin && mRate < mSituationData.lMax) {
            title = mSituationData.lResultDiagnosis;
            hypertensionTitle = mSituationData.lHypertension;
            cardiopulmonaryTitle = mSituationData.lCardiopulmonary;
            suggest1 = mSituationData.lSuggest1;
            suggest2 = mSituationData.lSuggest2;
        } else if (mRate >= mSituationData.max) {
            title = mSituationData.maxResultDiagnosis;
            hypertensionTitle = mSituationData.maxHypertension;
            cardiopulmonaryTitle = mSituationData.maxCardiopulmonary;
            suggest1 = mSituationData.maxSuggest1;
            suggest2 = mSituationData.maxSuggest2;
        }
        if (title.contains("%s")) {
            title = String.format(title, mRate);
        }
        binding.tvRateTip.setText(title);
        binding.tvHypertensionTip.setText(hypertensionTitle);
        binding.tvObesityTip.setText(cardiopulmonaryTitle);

        binding.rvList.setLayoutManager(new LinearLayoutManager(mActivity));
        binding.rvList.setAdapter(mAdapter = new ArticleAdapter());

        binding.tvPercent.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        SpannableString stringBuilder = new SpannableString(String.format(" %s ", new Random().nextInt(100)));
//        stringBuilder.setSpan(new MaskFilterSpan(new BlurMaskFilter(40f, BlurMaskFilter.Blur.NORMAL)),
//                0, stringBuilder.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        binding.tvPercent.setText(stringBuilder);

        binding.tvPercent1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        SpannableString stringBuilder1 = new SpannableString(String.format(" %s ", new Random().nextInt(100)));
//        stringBuilder1.setSpan(new MaskFilterSpan(new BlurMaskFilter(40f, BlurMaskFilter.Blur.NORMAL)),
//                0, stringBuilder1.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        binding.tvPercent1.setText(stringBuilder1);

        List<ArticleData> list = new ArrayList<>();
        list.add(ArticleData.articleList.get(suggest1 - 1));
        list.add(ArticleData.articleList.get(suggest2 - 1));
        mAdapter.setNewData(list);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                ArticleData article = mAdapter.getItem(i);
                ArticleDetailDialog1 dialog1 = new ArticleDetailDialog1();
                dialog1.setArticleData(article);
                dialog1.show(getSupportFragmentManager(), "article");
            }
        });
    }

}
