package com.pjpj.heartrate.ui.rate;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;

import com.airbnb.lottie.RenderMode;
import com.pjpj.heartrate.ImageProcessing;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.base.AppConfig;
import com.pjpj.heartrate.base.BaseViewModelActivity;
import com.pjpj.heartrate.data.ExerciseData;
import com.pjpj.heartrate.data.SituationData;
import com.pjpj.heartrate.databinding.RateTestActivityBinding;
import com.pjpj.heartrate.ui.chart.ChartHelper;
import com.pjpj.heartrate.ui.chart.ChartHelper1;
import com.pjpj.heartrate.ui.permission.CameraPermissionDialog;
import com.pjpj.heartrate.ui.permission.RatePermissionReminder;
import com.pjpj.heartrate.ui.statement.StatementActivity;
import com.pjpj.heartrate.ui.statement.StatementTipsDialog;
import com.pjpj.heartrate.util.AppToastHelper;
import com.pjpj.heartrate.util.permission.Permission;
import com.pjpj.heartrate.util.permission.PermissionHelper;
import com.pjpj.heartrate.util.permission.request.Action;
import com.pjpj.heartrate.view.RoundSurfaceView;
import com.pjpj.heartrate.widget.PermissionReminder;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import androidx.core.app.ActivityCompat;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\18 11:23
 * Email:     yeshieh@163.com
 */
public class RateTestActivity extends BaseViewModelActivity<RateTestActivityBinding, RateViewModel> {
    public final static String ARGUMENT_INT_EXERCISE_TYPE = "exercise";
    public final static String ARGUMENT_INT_BEFORE_EXERCISE_RATE = "before_rate";
    private final static String ARGUMENT_BOOLEAN_BEFORE = "before";

    private Timer timer = new Timer();
    private TimerTask task;
    private static int gx;
    private static int j;
    private static double flag = 1;
    private ChartHelper1 mChartHelper;
    private Handler handler;
    private String title = "pulse";
    private int addX = -1;
    double addY;
    int[] hua = new int[]{9, 10, 11, 12, 13, 14, 13, 12, 11, 10, 9, 8, 7, 6, 7, 8, 9, 10, 11, 10, 10};

    private static final AtomicBoolean processing = new AtomicBoolean(false);
    private Camera camera = null;
    private static SurfaceHolder previewHolder = null;
    private static PowerManager.WakeLock wakeLock = null;

    private static int averageIndex = 0;
    private static final int averageArraySize = 6;
    private static final int[] averageArray = new int[averageArraySize];

    public enum TYPE {
        GREEN, RED
    }

    //设置默认类型
    private TYPE currentType = TYPE.GREEN;

    //    //心跳下标值
//    private static int beatsIndex = 0;
//    //心跳数组的大小
//    private static final int beatsArraySize = 3;
//    //心跳数组
//    private static final int[] beatsArray = new int[beatsArraySize];
    //心跳脉冲
    private double beats = 0;
    //开始时间
    private long startTime = 0;
    private long lastTime = 0;

    private int mBeforeRate;//运动前的心率 要和运动后做对比
    private int mExerciseType;
    private boolean mBeforeExercise;
    private List<Integer> mSuccessRateList = new ArrayList<>();

    public static void show(Context context) {
        Intent intent = new Intent(context, RateTestActivity.class);
        context.startActivity(intent);
    }

    public static void showBeforeExercise(Context context, int type) {
        Intent intent = new Intent(context, RateTestActivity.class);
        intent.putExtra(ARGUMENT_INT_EXERCISE_TYPE, type);
        intent.putExtra(ARGUMENT_BOOLEAN_BEFORE, true);
        context.startActivity(intent);
    }

    public static void showAfterExercise(Context context, int rate, int type) {
        Intent intent = new Intent(context, RateTestActivity.class);
        intent.putExtra(ARGUMENT_INT_EXERCISE_TYPE, type);
        intent.putExtra(ARGUMENT_INT_BEFORE_EXERCISE_RATE, rate);
        intent.putExtra(ARGUMENT_BOOLEAN_BEFORE, false);
        context.startActivity(intent);
    }

    @Override
    protected StatusBarStyle initStatusBar() {
        return StatusBarStyle.FULL;
    }

    @Override
    protected void initArgument(Bundle bundle) {
        super.initArgument(bundle);
        if (bundle == null) {
            return;
        }
        mBeforeRate = bundle.getInt(ARGUMENT_INT_BEFORE_EXERCISE_RATE);
        mExerciseType = bundle.getInt(ARGUMENT_INT_EXERCISE_TYPE);
        mBeforeExercise = bundle.getBoolean(ARGUMENT_BOOLEAN_BEFORE);
    }

    @Override
    protected void initView() {
        binding.animateView.setImageAssetsFolder("anim/rateguide/images");
        binding.animateView.setAnimation("anim/rateguide/data.json");
        binding.animateView.setRenderMode(RenderMode.SOFTWARE);
        binding.animateView.setRepeatCount(ValueAnimator.INFINITE);
        binding.animateView.playAnimation();
        binding.tvPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
            }
        });
        binding.llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timer.cancel();
                camera.setPreviewCallback(null);
                camera.stopPreview();
                camera.release();
                camera = null;
                finish();
            }
        });
        binding.statementTipsBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StatementActivity.show(RateTestActivity.this);
            }
        });

    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            binding.llGuide.setVisibility(View.GONE);
            binding.llTestContainer.setVisibility(View.GONE);
            binding.llPermissionGuide.setVisibility(View.VISIBLE);
            binding.tvTip.setText("CANNOT USE HEART RATE TEST NOW.\nTHE PERMISSION OF CAMERA SHOULD BE ALLOW");
        }

        PermissionHelper.with(this).permission(Permission.GROUP.CAMERA)
                .reminder(new RatePermissionReminder())
                .onCallback(new Action() {
                    @Override
                    public void onSuccess() {
                        if(!AppConfig.isStatementSet()){
                            StatementTipsDialog dialog = new StatementTipsDialog(RateTestActivity.this);
                            dialog.setOnAccessListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.cancel();
                                }
                            });
                            dialog.show();
                            AppConfig.setStatement(1);
                        }


                        camera = Camera.open();
                        startTime = System.currentTimeMillis();
                        binding.llGuide.setVisibility(View.VISIBLE);
                        binding.llTestContainer.setVisibility(View.GONE);
                        binding.llPermissionGuide.setVisibility(View.GONE);
                        initConfig();
                    }

                    @Override
                    public void onCancel() {
                        AppToastHelper.show("permission needed");
                        binding.llGuide.setVisibility(View.GONE);
                        binding.llTestContainer.setVisibility(View.GONE);
                        binding.llPermissionGuide.setVisibility(View.VISIBLE);
                        binding.tvTip.setText("CANNOT USE HEART RATE TEST NOW.\nTHE PERMISSION OF CAMERA SHOULD BE ALLOW");
                    }
                })
                .start();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (wakeLock != null) {
            wakeLock.acquire();
        }
        if (camera == null) {
            requestPermission();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (wakeLock != null) {
            wakeLock.release();
        }
        if (camera == null) {
            return;
        }
        camera.setPreviewCallback(null);
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    /**
     * 初始化配置
     */
    @SuppressLint("InvalidWakeLockTag")
    @SuppressWarnings("deprecation")
    private void initConfig() {

        //这里获得main界面上的布局，下面会把图表画在这个布局里面
        mChartHelper = new ChartHelper1(binding.chart);
        binding.llTestContainer.setVisibility(View.GONE);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                //刷新图表
                updateChart();
                super.handleMessage(msg);
            }
        };

        task = new TimerTask() {
            @Override
            public void run() {
                Message message = new Message();
                message.what = 1;
                handler.sendMessage(message);
            }
        };

        try {
            timer.schedule(task, 1, 25);           //曲线
        }catch (IllegalStateException e){
            e.printStackTrace();
        }
        RoundSurfaceView surfaceView = new RoundSurfaceView(mActivity);
        surfaceView.setBackgroundResource(R.color.transparent);
        binding.flSurface.addView(surfaceView);
        previewHolder = surfaceView.getHolder();
        previewHolder.addCallback(surfaceCallback);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");

        wakeLock.acquire();
    }

    /**
     * 更新图标信息
     */
    private void updateChart() {
        //设置好下一个需要增加的节点
        if (flag == 1) {
            addY = 10;
        } else {
            flag = 1;
            if (gx < 200) {
                if (hua[20] > 1) {
                    mSuccessRateList.clear();
                    binding.llGuide.setVisibility(View.VISIBLE);
                    binding.llTestContainer.setVisibility(View.GONE);
                    binding.llPermissionGuide.setVisibility(View.GONE);
                    binding.tvTip.setText("HEARTBEAT IS NOT DETECTED,PLEASE KEEP\nYOUR FINGERS ON THE CAMERA");
                    binding.pbRate.setProgress(0);
                    hua[20] = 0;
                }
                hua[20]++;
                return;
            } else {
                hua[20] = 10;
            }
            j = 0;
        }
        if (j < 20) {
            addY = hua[j];
            j++;
        }
        mChartHelper.addEntry(addY);
    }

    private void onTestSuccess() {
        timer.cancel();
        camera.setPreviewCallback(null);
        camera.stopPreview();
        camera.release();
        camera = null;

        int averageRate;
        double totalRate = 0;
        for (int i = 0; i < mSuccessRateList.size(); i++) {
            int rate = mSuccessRateList.get(i);
            totalRate += rate;
        }
        averageRate = (int) (totalRate / mSuccessRateList.size());
        if (mExerciseType > 0) {
            if (mBeforeExercise) {
                binding.llGuide.setVisibility(View.GONE);
                binding.llTestContainer.setVisibility(View.GONE);
                binding.llPermissionGuide.setVisibility(View.GONE);
                binding.llExercise.setVisibility(View.VISIBLE);
                binding.tvTip.setText(String.format("YOUR HEART RATE IS %sBMP,\nREADY TO NEXT TEST", averageRate));
                RateReadyExerciseHolder holder = new RateReadyExerciseHolder(mActivity, mExerciseType, averageRate, binding, getLifecycle());
                holder.start();
            } else {
                //运动后
                RateResultExerciseActivity.show(mActivity, mExerciseType, mBeforeRate, averageRate);
                finish();
            }
        } else {
            RateSuccessDialog dialog = new RateSuccessDialog(mActivity);
            dialog.setRate(averageRate);
            dialog.setItemClick(new RateSuccessDialog.ItemClick() {
                @Override
                public void onItemClick(SituationData data) {
                    RateResultActivity.show(mActivity, averageRate, data);
                    finish();
                }
            });
            dialog.show();
        }
    }

    /**
     * 相机预览方法
     * 这个方法中实现动态更新界面UI的功能，
     * 通过获取手机摄像头的参数来实时动态计算平均像素值、脉冲数，从而实时动态计算心率值。
     */
    private Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera cam) {
            if (data == null) {
                throw new NullPointerException();
            }
            Camera.Size size = cam.getParameters().getPreviewSize();
            if (size == null) {
                throw new NullPointerException();
            }
            calculate(size.width, size.height, data);
        }
    };

    private synchronized void calculate(int width, int height, byte[] data) {
        if (!processing.compareAndSet(false, true)) {
            return;
        }

        //图像处理
        int imgAvg = ImageProcessing.decodeYUV420SPtoRedAvg(data.clone(), height, width);
        gx = imgAvg;

        if (imgAvg == 255 || imgAvg < 200) {
            processing.set(false);
            flag = 0;
            return;
        }
        //计算平均值
        int averageArrayAvg = 0;
        int averageArrayCnt = 0;
        for (int i = 0; i < averageArray.length; i++) {
            if (averageArray[i] > 0) {
                averageArrayAvg += averageArray[i];
                averageArrayCnt++;
            }
        }

        //计算平均值
        int rollingAverage = (averageArrayCnt > 0) ? (averageArrayAvg / averageArrayCnt) : 0;
        TYPE newType = currentType;
        if (imgAvg < rollingAverage - 1) {
            newType = TYPE.RED;
            if (newType != currentType) {
                beats++;
                flag = 0;
            }
        } else if (imgAvg > rollingAverage) {
            newType = TYPE.GREEN;
        }


        if (averageIndex == averageArraySize) {
            averageIndex = 0;
        }

        averageArray[averageIndex] = imgAvg;
        averageIndex++;

        if (newType != currentType) {
            currentType = newType;
        }

        //获取系统结束时间（ms）
        long endTime = System.currentTimeMillis();
        long diffTIme = endTime - lastTime;

        Log.e("RateTestActivity", "当前测试数值 imgAvg:" + imgAvg + " newType：" + newType + " beats:" + beats + " diffTIme:" + diffTIme);
        if (diffTIme < 1000) {
            processing.set(false);
            return;
        }
        double totalTimeInSecs = (endTime - startTime) / 1000d;

        if (totalTimeInSecs >= 2) {
            double bps = (beats / totalTimeInSecs);
            int dpm = (int) (bps * 60d);//一分钟几下心跳
            if (dpm < 60 || dpm > 130 || imgAvg < 200) {
                //获取系统开始时间（ms）
                startTime = System.currentTimeMillis();
                lastTime = System.currentTimeMillis();
                //beats心跳总数
                beats = 0;
                processing.set(false);
                return;
            }
//
//            if (beatsIndex == beatsArraySize) {
//                beatsIndex = 0;
//            }
//            beatsArray[beatsIndex] = dpm;
//            beatsIndex++;
//
//            int beatsArrayAvg = 0;
//            int beatsArrayCnt = 0;
//            for (int i = 0; i < beatsArray.length; i++) {
//                if (beatsArray[i] > 0) {
//                    beatsArrayAvg += beatsArray[i];
//                    beatsArrayCnt++;
//                }
//            }
            binding.llGuide.setVisibility(View.GONE);
            binding.llTestContainer.setVisibility(View.VISIBLE);
            binding.llPermissionGuide.setVisibility(View.GONE);
            int beatsAvg = dpm;
            binding.tvTip.setText("CALCULATING...\nPLEASE DO NOT REMOVE YOUR FINGER");

            Log.e("RateTestActivity", "您的心率是" + String.valueOf(beatsAvg));
            //获取系统时间（ms）
            mSuccessRateList.add(beatsAvg);
            binding.pbRate.setProgress((int) (mSuccessRateList.size() / 6f * 100));
            if (mSuccessRateList.size() == 6) {
                onTestSuccess();
            }
            lastTime = System.currentTimeMillis();
        }
        processing.set(false);
    }

    /**
     * 预览回调接口
     */
    private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        //创建时调用
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(previewHolder);
                camera.setPreviewCallback(previewCallback);
            } catch (Throwable t) {
                Log.e("RateTestActivity", "Exception in setPreviewDisplay()", t);
            }
        }

        //当预览改变的时候回调此方法
        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            Camera.Size size = getSmallestPreviewSize(width, height, parameters);
            setCameraDisplayOrientation(mActivity, 0, camera);
            if (size != null) {
                parameters.setPreviewSize(size.width, size.height);
            }
            camera.setParameters(parameters);
            camera.startPreview();
        }

        //销毁的时候调用
        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {

        }
    };

    /**
     * 获取相机最小的预览尺寸
     */
    private static Camera.Size getSmallestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;
        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;
                    if (newArea < resultArea) {
                        result = size;
                    }
                }
            }
        }
        return result;
    }

    public static void setCameraDisplayOrientation(Activity activity, int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;   // compensate the mirror
        } else {
            // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

}
