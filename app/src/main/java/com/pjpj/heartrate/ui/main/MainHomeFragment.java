package com.pjpj.heartrate.ui.main;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.pjpj.heartrate.base.BaseViewModelFragment;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.data.AppDatabase;
import com.pjpj.heartrate.data.ExerciseData;
import com.pjpj.heartrate.databinding.MainHomeFragmentBinding;
import com.pjpj.heartrate.ui.TestActivity;
import com.pjpj.heartrate.ui.excecise.ExerciseActivity;
import com.pjpj.heartrate.ui.excecise.ExerciseAdapter;
import com.pjpj.heartrate.ui.rate.RateResultExerciseActivity;
import com.pjpj.heartrate.ui.rate.RateTestActivity;
import com.pjpj.heartrate.ui.subscribe.SubscribeActivity;
import com.pjpj.heartrate.util.AppCollectionHelper;
import com.pjpj.heartrate.util.UIHelper;
import com.pjpj.heartrate.widget.AppRvSpaceDecorator;

import androidx.recyclerview.widget.GridLayoutManager;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\11 15:35
 * Email:     yeshieh@163.com
 */
public class MainHomeFragment extends BaseViewModelFragment<MainHomeFragmentBinding, BaseViewModel> {

    ExerciseAdapter mAdapter;

    @Override
    protected void initView() {
        binding.ivVip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubscribeActivity.show(getActivity());
            }
        });
        binding.llStartTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                TestActivity.show(getActivity());
                RateTestActivity.show(getActivity());
//                RateResultExerciseActivity.show(getActivity(),ExerciseData.Plank,80,110);
            }
        });
        binding.rvList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        binding.rvList.setAdapter(mAdapter = new ExerciseAdapter());
        binding.rvList.addItemDecoration(new AppRvSpaceDecorator(UIHelper.dpToPx(20), 2));
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                ExerciseData data = mAdapter.getItem(i);
                RateTestActivity.showBeforeExercise(getActivity(), data.type);
            }
        });
    }

    @Override
    protected void initData() {
        super.initData();
        mAdapter.setNewData(ExerciseData.mExerciseList);
    }
}
