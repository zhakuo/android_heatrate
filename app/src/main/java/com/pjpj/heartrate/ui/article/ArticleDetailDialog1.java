package com.pjpj.heartrate.ui.article;

import android.app.Dialog;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.data.ArticleData;
import com.pjpj.heartrate.util.ResourceUtils;
import com.pjpj.heartrate.util.UIHelper;
import com.pjpj.heartrate.widget.BaseBottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.DialogFragment;

public class ArticleDetailDialog1 extends BaseBottomSheetDialogFragment<com.pjpj.heartrate.databinding.ArticleDetailDialogBinding, BaseViewModel> {

    ArticleData articleData;

    public void setArticleData(ArticleData articleData) {
        this.articleData = articleData;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Dialog dialog = getDialog();
        if (dialog != null) {
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = (int) (UIHelper.getScreenHeight() * 8 / 9);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        final View view = getView();
        view.post(new Runnable() {
            @Override
            public void run() {
                View parent = (View) view.getParent();
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
                CoordinatorLayout.Behavior behavior = params.getBehavior();
                BottomSheetBehavior<View> mBottomSheetBehavior = (BottomSheetBehavior) behavior;
                mBottomSheetBehavior.setPeekHeight(view.getHeight());
            }
        });
    }


    @Override
    public void initView() {
        super.initView();
        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        binding.tvTitle.setText(articleData.title);
        binding.tvContent.setText(articleData.text);
        binding.ivContent.setImageResource(ResourceUtils.getResId(getContext(), articleData.image, "mipmap"));
    }
}
