package com.pjpj.heartrate.ui.excecise;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.data.ExerciseData;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ExerciseAdapter extends BaseQuickAdapter<ExerciseData, BaseViewHolder> {
    public ExerciseAdapter() {
        super(R.layout.exercise_item_view);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, ExerciseData data) {
        baseViewHolder.setImageResource(R.id.ivContent, data.exerciseRes1);
        baseViewHolder.setText(R.id.tvContent, data.typeName);
    }
}
