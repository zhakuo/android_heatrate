package com.pjpj.heartrate.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.widget.FrameLayout;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.base.BaseViewModelActivity;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.databinding.TestActivityBinding;
import com.pjpj.heartrate.ui.chart.ChartHelper;
import com.pjpj.heartrate.util.UIHelper;

import org.achartengine.GraphicalView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\28 10:59
 * Email:     yeshieh@163.com
 */
public class TestActivity extends BaseViewModelActivity<TestActivityBinding, BaseViewModel> {
    private Timer timer;

    private GraphicalView mView;
    private ChartHelper mChartHelper;

    public static void show(Context context) {
        Intent intent = new Intent(context, TestActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        initChart2();
        feedMultiple();
    }

    private void initChart1(){
        mChartHelper = new ChartHelper(this);
        mChartHelper.setXYMultipleSeriesDataset("");
        mChartHelper.setXYMultipleSeriesRenderer(Color.RED, Color.RED,getResources().getColor(R.color.app_primary_color), Color.parseColor("#707070"));
        mView = mChartHelper.getGraphicalView();
        binding.flContainer.addView(mView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT));

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.sendMessage(handler.obtainMessage());
            }
        }, 10, 1000);
    }

    private void initChart2(){
        binding.chart1.getDescription().setEnabled(false);
        binding.chart1.setTouchEnabled(false);
        binding.chart1.setDragEnabled(false);
        binding.chart1.setScaleEnabled(false);
        binding.chart1.setDrawGridBackground(false);
        binding.chart1.setHighlightPerDragEnabled(false);
        binding.chart1.setDrawBorders(false);

        binding.chart1.setBackgroundColor(Color.TRANSPARENT);
        binding.chart1.setViewPortOffsets(0f, 0f, 0f, 0f);
        binding.chart1.setAutoScaleMinMaxEnabled(false);

        // get the legend (only possible after setting data)
        Legend l = binding.chart1.getLegend();
        l.setEnabled(false);

        XAxis xAxis = binding.chart1.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setEnabled(false);

        YAxis leftAxis = binding.chart1.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setDrawGridLines(false);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setAxisMaximum(100f);
        leftAxis.setYOffset(-9f);
        leftAxis.setTextColor(Color.rgb(255, 192, 56));
        leftAxis.setEnabled(false);

        YAxis rightAxis = binding.chart1.getAxisRight();
        rightAxis.setEnabled(false);

        LineData data = new LineData();
        data.setValueTextColor(Color.WHITE);

        // add empty data
        binding.chart1.setData(data);
    }

    private int t = 0;
    private Handler handler = new Handler() {
        @Override
        //定时更新图表
        public void handleMessage(Message msg) {
            mChartHelper.updateChart(t, Math.random() * 100);
            t += 5;
        }
    };

    private Thread thread;

    private void feedMultiple() {

        if (thread != null)
            thread.interrupt();

        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                addEntry();
            }
        };

        thread = new Thread(new Runnable() {

            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {

                    // Don't generate garbage runnables inside the loop.
                    runOnUiThread(runnable);

                    try {
                        Thread.sleep(25);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
    }

    private void addEntry() {

        LineData data = binding.chart1.getData();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            data.addEntry(new Entry(set.getEntryCount(), (float) (Math.random() * 40) + 30f), 0);
            data.notifyDataChanged();

            // let the chart know it's data has changed
            binding.chart1.notifyDataSetChanged();

            // limit the number of visible entries
            binding.chart1.setVisibleXRangeMaximum(100);
            binding.chart1.setVisibleXRangeMinimum(100);

            // move to the latest entry
            binding.chart1.moveViewToX(data.getEntryCount());

            // this automatically refreshes the chart (calls invalidate())
            // chart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);
        }
    }

    private LineDataSet createSet() {
        LineDataSet set = new LineDataSet(null, "rate Data");
        set.setDrawCircles(false);
        set.setDrawValues(false);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(ColorTemplate.getHoloBlue());
        set.setLineWidth(2);
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        return set;
    }
}
