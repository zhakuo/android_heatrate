package com.pjpj.heartrate.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.pjpj.heartrate.R;
import com.workout.base.HomeKeyDetect;
import com.workout.base.system.SafeToast;


public class FeedbackActivity extends AppCompatActivity {

    private EditText mEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.app_primary_color));
        }
        setTitleBar();

        mEditText = findViewById(R.id.feedback_edit_text);
        View sentBtn = findViewById(R.id.feedback_send_btn);
        sentBtn.setActivated(true);
        sentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = mEditText.getText().toString().trim();
                if (input.length() == 0) {
                    SafeToast.makeText(FeedbackActivity.this, getString(R.string.common_empty_alert), Toast.LENGTH_SHORT).show();
                    return;
                }
                sendEmailFeedBack(input);
            }
        });

        initHomekey();
    }

    private void setTitleBar() {
        View backArrow = findViewById(R.id.title_bar_arrow);
        TextView text = findViewById(R.id.title_bar_text);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        String title = getString(R.string.feedback);
        text.setText(title);
    }


    public void sendEmailFeedBack(String content) {

        String to = "categoryhbb@gmail.com";

        String titleContent = "Feedback - " + getString(R.string.app_name);

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.putExtra(Intent.EXTRA_TEXT, content);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, titleContent);
        Uri uri = Uri.parse("mailto:" + to);
        emailIntent.setAction(Intent.ACTION_SENDTO);
        emailIntent.setData(uri);

        try {
            startActivity(emailIntent);
        } catch (Exception ex) {
        }
        finish();
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    private HomeKeyDetect mHomeKeyMonitor = null;
    private boolean isClickRecent = false;

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try{

            if (mHomeKeyMonitor!=null){

                mHomeKeyMonitor.unregisterReceiver();
            }

        }catch (Exception e){

        }
    }

    private void initHomekey(){
        try {


            mHomeKeyMonitor = new HomeKeyDetect(this, new HomeKeyDetect.OnHomeKeyListener() {
                @Override
                public void onRecentApps() {

                    closeDialogActivity();
                }

                @Override
                public void onLock() {
                }

                @Override
                public void onHome() {

                    closeDialogActivity();



                }


            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void closeDialogActivity() {
        if (this !=null) {
            try {
                if (Build.VERSION.SDK_INT >= 21) {
                    finishAndRemoveTask();
                } else {
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
