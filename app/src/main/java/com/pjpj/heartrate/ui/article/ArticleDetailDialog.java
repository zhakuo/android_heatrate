package com.pjpj.heartrate.ui.article;

import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;

import com.pjpj.heartrate.R;
import com.pjpj.heartrate.data.ArticleData;
import com.pjpj.heartrate.databinding.ArticleDetailDialogBinding;
import com.pjpj.heartrate.util.ResourceUtils;
import com.pjpj.heartrate.view.dialog.BottomBaseDialog;

public class ArticleDetailDialog extends BottomBaseDialog<ArticleDetailDialog> {
    ArticleDetailDialogBinding binding;
    ArticleData articleData;

    public void setArticleData(ArticleData articleData) {
        this.articleData = articleData;
    }

    public ArticleDetailDialog(Context context) {
        super(context);
    }

    @Override
    public View onCreateView() {
        binding = ArticleDetailDialogBinding.inflate(LayoutInflater.from(mContext));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view) {
        super.onViewCreated(view);
        binding.tvTitle.setText(articleData.title);
        binding.tvContent.setText(articleData.text);
        binding.ivContent.setImageResource(ResourceUtils.getResId(mContext, articleData.image, "mipmap"));
        binding.tvContent.setMovementMethod(new ScrollingMovementMethod());
        binding.tvContent.setScrollbarFadingEnabled(false);
    }

    @Override
    public void setUiBeforeShow() {

    }
}
