package com.pjpj.heartrate.ui.rate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.pjpj.heartrate.R;
import com.pjpj.heartrate.base.BaseViewModelActivity;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.data.AppDatabase;
import com.pjpj.heartrate.data.ExerciseData;
import com.pjpj.heartrate.data.RateData;
import com.pjpj.heartrate.databinding.RateResultExerciseActivityBinding;
import com.pjpj.heartrate.util.UIHelper;

import java.util.Calendar;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\27 17:07
 * Email:     yeshieh@163.com
 */
public class RateResultExerciseActivity extends BaseViewModelActivity<RateResultExerciseActivityBinding, BaseViewModel> {
    private final static String ARGUMENT_INT_BEFORE = "before";
    private final static String ARGUMENT_INT_AFTER = "after";
    private final static String ARGUMENT_INT_EXERCISE = "exercise";

    private int mExerciseType;
    private int mBeforeRate;
    private int mAfterRate;
    ExerciseData mExerciseData;

    public static void show(Context context, int exerciseType, int beforeRate, int afterRate) {
        Intent intent = new Intent(context, RateResultExerciseActivity.class);
        intent.putExtra(ARGUMENT_INT_BEFORE, beforeRate);
        intent.putExtra(ARGUMENT_INT_AFTER, afterRate);
        intent.putExtra(ARGUMENT_INT_EXERCISE, exerciseType);
        context.startActivity(intent);
    }

    @Override
    protected StatusBarStyle initStatusBar() {
        return StatusBarStyle.FULL;
    }

    @Override
    protected void initArgument(Bundle bundle) {
        super.initArgument(bundle);
        mExerciseType = bundle.getInt(ARGUMENT_INT_EXERCISE);
        mBeforeRate = bundle.getInt(ARGUMENT_INT_BEFORE);
        mAfterRate = bundle.getInt(ARGUMENT_INT_AFTER);
    }

    @Override
    protected void initView() {
        ((ConstraintLayout.LayoutParams) binding.llBack.getLayoutParams()).topMargin = UIHelper.getStatusBarHeight(mActivity);
        binding.llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.tvRate.setText(String.valueOf(mAfterRate));
        mExerciseData = ExerciseData.getData(mExerciseType);
        calculate();
    }

    @Override
    protected void initData() {
        super.initData();
        saveRate();
    }

    private void calculate() {
        int diff = mAfterRate - mBeforeRate;
        String title = "";
        switch (mExerciseType) {
            case ExerciseData.Plank:
            case ExerciseData.PILE_SQUAT:
            case ExerciseData.WALL_PUSH_UP:
                if (diff < 10) {
                    title = getString(R.string.home_m601);
                } else if (diff < 20) {
                    title = getString(R.string.home_m602);
                } else {
                    title = getString(R.string.home_m603);
                }
                break;
            case ExerciseData.MEDITATION:
                if (diff >= -10) {
                    title = getString(R.string.home_m603);
                } else if (diff > -20) {
                    title = getString(R.string.home_m602);
                } else {
                    title = getString(R.string.home_m601);
                }
                break;
        }
        binding.tvTip.setText(title);

        Calendar calendar = Calendar.getInstance();
        RateData data = new RateData();
        data.setYear(calendar.get(Calendar.YEAR));
        data.setMonth(calendar.get(Calendar.MONTH));
        data.setDay(calendar.get(Calendar.DAY_OF_MONTH));
        data.setTimeInMillis(calendar.getTimeInMillis());
        data.setRate(mAfterRate);
        data.setTitle(title);

        data.setSituation("AFTER " + mExerciseData.typeName);
        AppDatabase.getDatabase().rateDao().insert(data);
    }

    private void saveRate() {
    }

}
