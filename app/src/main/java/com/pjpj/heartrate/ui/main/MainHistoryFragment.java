package com.pjpj.heartrate.ui.main;

import android.view.View;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pjpj.heartrate.base.BaseViewModelFragment;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.data.RateData;
import com.pjpj.heartrate.databinding.MainHistoryFragmentBinding;
import com.pjpj.heartrate.ui.rate.RateHistoryAdapter;
import com.pjpj.heartrate.ui.rate.RateReportActivity;
import com.pjpj.heartrate.ui.rate.RateViewModel;

import java.util.List;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\11 15:37
 * Email:     yeshieh@163.com
 */
public class MainHistoryFragment extends BaseViewModelFragment<MainHistoryFragmentBinding, RateViewModel> {
    RateHistoryAdapter mAdapter;

    @Override
    protected void initView() {
        binding.tvReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RateReportActivity.show(getActivity());
            }
        });
        binding.tvReport.getPaint().setUnderlineText(true);
        binding.rvList.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvList.setAdapter(mAdapter = new RateHistoryAdapter());
    }

    @Override
    protected void initLifeCycles(Lifecycle lifecycle) {
        super.initLifeCycles(lifecycle);
        viewModel.mHistoryLiveData.observe(this, new Observer<List<RateData>>() {
            @Override
            public void onChanged(List<RateData> data) {
                mAdapter.setNewData(data);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.loadHistory();
    }
}
