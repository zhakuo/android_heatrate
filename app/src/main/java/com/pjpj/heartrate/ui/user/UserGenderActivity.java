package com.pjpj.heartrate.ui.user;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.pjpj.heartrate.base.AppConfig;
import com.pjpj.heartrate.base.BaseViewModelActivity;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.databinding.UserSelectGenderActivityBinding;

import java.util.Random;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\17 11:48
 * Email:     yeshieh@163.com
 */
public class UserGenderActivity extends BaseViewModelActivity<UserSelectGenderActivityBinding, UserViewModel> {

    private int mGender = 1;

    public static void show(Context context) {
        Intent intent = new Intent(context, UserGenderActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected StatusBarStyle initStatusBar() {
        return StatusBarStyle.FULL;
    }

    @Override
    protected void initView() {
        binding.tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!AppConfig.isSituationSet()) {
                    UserSituationActivity.show(mActivity);
                }
                finish();
            }
        });
        binding.ivFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mGender = 2;
                binding.ivFemaleCheck.setVisibility(View.VISIBLE);
                binding.ivMaleCheck.setVisibility(View.GONE);
            }
        });
        binding.ivMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mGender = 1;
                binding.ivMaleCheck.setVisibility(View.VISIBLE);
                binding.ivFemaleCheck.setVisibility(View.GONE);
            }
        });
        binding.llDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppConfig.setGender(mGender);
                if (!AppConfig.isSituationSet()) {
                    UserSituationActivity.show(mActivity);
                }
                AppConfig.setUserId(1000 + new Random().nextInt(9000));
                finish();
            }
        });
    }
}
