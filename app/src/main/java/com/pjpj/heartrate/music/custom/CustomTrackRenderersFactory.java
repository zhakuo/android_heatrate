package com.pjpj.heartrate.music.custom;

import android.content.Context;
import android.os.Handler;

import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.Renderer;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.audio.AudioSink;
import com.google.android.exoplayer2.mediacodec.MediaCodecSelector;

import java.util.ArrayList;

public class CustomTrackRenderersFactory extends DefaultRenderersFactory {

    CustomMediaCodecAudioRenderer mCodecAudioRenderer;

    public CustomTrackRenderersFactory(Context context) {
        super(context);
    }

    @Override
    protected void buildAudioRenderers(Context context, int extensionRendererMode,
                                       MediaCodecSelector mediaCodecSelector, boolean enableDecoderFallback, AudioSink audioSink, Handler eventHandler,
                                       AudioRendererEventListener eventListener, ArrayList<Renderer> out) {
        mCodecAudioRenderer = new CustomMediaCodecAudioRenderer(context, MediaCodecSelector.DEFAULT);
        out.add(mCodecAudioRenderer);
    }

    public void setProgressChange(ProgressChange progressChange) {
        if (mCodecAudioRenderer != null) {
            mCodecAudioRenderer.setProgressChange(progressChange);
        }
    }

    public void setTotalDurationUs(long durationUs) {
        if (mCodecAudioRenderer != null) {
            mCodecAudioRenderer.setTotalDuration(durationUs);
        }
    }

    public interface ProgressChange {
        void almostFinish();
    }
}
