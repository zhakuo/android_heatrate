package com.pjpj.heartrate.music;

import android.content.res.AssetManager;
import android.media.MediaPlayer;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.BaseMediaSource;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.MediaSourceFactory;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.SilenceMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.pjpj.heartrate.App;
import com.pjpj.heartrate.data.CustomSongList;
import com.pjpj.heartrate.music.model.PlayList;
import com.pjpj.heartrate.music.model.Song;
import com.pjpj.heartrate.music.multi.MultiTrackRenderersFactory;
import com.pjpj.heartrate.music.multi.MultiTrackSelector;

import java.util.ArrayList;
import java.util.List;

/**
 * Des:
 * Create by: ye
 * On:        2021\07\17 16:52
 * Email:     yeshieh@163.com
 */
public class EPlayer implements IMusic, MediaPlayer.OnCompletionListener {

    private static final String TAG = "Player";

    private static final int MULTIPLE_SIZE = 6;
    private static volatile EPlayer sInstance;

    private ExoPlayer mPlayer;
    private DataSource.Factory mMediaDataSourceFactory;
    private MediaSourceFactory mMediaSourceFactory;
    private MultiTrackRenderersFactory mMediaRenderFactory;
    private AssetManager mAssetManager;


    private PlayList mPlayList;
    // Default size 2: for service and UI
    private List<Callback> mCallbacks = new ArrayList<>(2);

    // Player status
    private boolean isPaused;

    private boolean mMultiple;

    private EPlayer() {
        mMediaRenderFactory = new MultiTrackRenderersFactory(MULTIPLE_SIZE, App.app);
        mMediaDataSourceFactory = new DefaultDataSourceFactory(App.app, Util.getUserAgent(App.app, "whiteNoise"));
        mMediaSourceFactory = new DefaultMediaSourceFactory(mMediaDataSourceFactory);
        mPlayer = new SimpleExoPlayer.Builder(App.app, mMediaRenderFactory)
                .setMediaSourceFactory(mMediaSourceFactory)
                .setTrackSelector(new MultiTrackSelector())
                .build();
        mPlayer.setPlayWhenReady(true);
        mPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
        mPlayList = new PlayList();
    }

    public static EPlayer getInstance() {
        if (sInstance == null) {
            synchronized (EPlayer.class) {
                if (sInstance == null) {
                    sInstance = new EPlayer();
                }
            }
        }
        return sInstance;
    }

    @Override
    public void setPlayList(PlayList list) {
        if (list == null) {
            list = new PlayList();
        }
        mPlayList = list;
    }

    @Override
    public boolean play() {
        if (isPaused) {
            mPlayer.play();
            notifyPlayStatusChanged(true);
            return true;
        }
        if (mPlayList.prepare()) {
            int songListSize = mPlayList.getSongs().size();
            BaseMediaSource[] mediaSources = new BaseMediaSource[MULTIPLE_SIZE];
            for (int i = 0; i < MULTIPLE_SIZE; i++) {
                BaseMediaSource mediaSource;
                if (i < songListSize) {
                    Song song = mPlayList.getSongs().get(i);
                    mediaSource = new ProgressiveMediaSource.Factory(mMediaDataSourceFactory).createMediaSource(MediaItem.fromUri(song.getPath()));
                } else {
                    mediaSource = new SilenceMediaSource(20);
                }
                mediaSources[i] = mediaSource;
            }
            mPlayer.setMediaSource(new MergingMediaSource(mediaSources));
            mPlayer.prepare();
            mPlayer.setPlayWhenReady(true);
            notifyPlayStatusChanged(true);

            return true;
        }
        return false;
    }

    @Override
    public boolean play(PlayList list) {
        if (list == null) return false;

        isPaused = false;
        setPlayList(list);
        return play();
    }

    @Override
    public boolean play(PlayList list, int startIndex) {
        if (list == null || startIndex < 0 || startIndex >= list.getNumOfSongs()) return false;

        isPaused = false;
        list.setPlayingIndex(startIndex);
        setPlayList(list);
        return play();
    }

    @Override
    public boolean play(Song song) {
        if (song == null) return false;

        isPaused = false;
        mPlayList.getSongs().clear();
        mPlayList.getSongs().add(song);
        return play();
    }

    @Override
    public boolean addSong(Song song) {
        if (song == null) return false;

        isPaused = false;
        if(mPlayList.getSongs().contains(song)){
            return false;
        }
        mPlayList.getSongs().add(song);
        return play();
    }

    @Override
    public boolean removeSong(Song song) {
        if (song == null) return false;

        isPaused = false;
        if (!mPlayList.getSongs().contains(song)) {
            return false;
        }
        mPlayList.getSongs().remove(song);
        if (mPlayList.getSongs().size() == 0) {
            pause();
        }
        return play();
    }

    @Override
    public boolean playMultiSong(CustomSongList customSong) {
        return false;
    }

    @Override
    public PlayList getPlayList() {
        return mPlayList;
    }

    @Override
    public boolean playLast() {
        isPaused = false;
        boolean hasLast = mPlayList.hasLast();
        if (hasLast) {
            Song last = mPlayList.last();
            play();
            notifyPlayLast(last);
            return true;
        }
        return false;
    }

    @Override
    public boolean playNext() {
        isPaused = false;
        boolean hasNext = mPlayList.hasNext(false);
        if (hasNext) {
            Song next = mPlayList.next();
            play();
            notifyPlayNext(next);
            return true;
        }
        return false;
    }

    @Override
    public boolean pause() {
        if (mPlayer.isPlaying()) {
            mPlayer.pause();
            isPaused = true;
            notifyPlayStatusChanged(false);
            return true;
        }
        return false;
    }

    @Override
    public boolean isPlaying() {
        return mPlayer.isPlaying();
    }

    @Override
    public int getProgress() {
        return 0;
    }

    @Nullable
    @Override
    public Song getPlayingSong() {
        return mPlayList.getCurrentSong();
    }

    @Override
    public List<Song> getSongList() {
        return mPlayList.getSongs();
    }

    @Override
    public void setVolume(Song song) {

    }

    @Override
    public boolean seekTo(int progress) {
        if (mPlayList.getSongs().isEmpty()) return false;

        Song currentSong = mPlayList.getCurrentSong();
        if (currentSong != null) {
            if (currentSong.getDuration() <= progress) {
                // onCompletion(mPlayer);
            } else {
                mPlayer.seekTo(progress);
            }
            return true;
        }
        return false;
    }

    @Override
    public void setPlayMode(PlayMode playMode) {
        mPlayList.setPlayMode(playMode);
    }

    // Listeners

    @Override
    public void onCompletion(MediaPlayer mp) {
        Song next = null;
        // There is only one limited play mode which is list, player should be stopped when hitting the list end
        if (mPlayList.getPlayMode() == PlayMode.LIST && mPlayList.getPlayingIndex() == mPlayList.getNumOfSongs() - 1) {
            // In the end of the list
            // Do nothing, just deliver the callback
        } else if (mPlayList.getPlayMode() == PlayMode.SINGLE) {
            next = mPlayList.getCurrentSong();
            play();
        } else {
            boolean hasNext = mPlayList.hasNext(true);
            if (hasNext) {
                next = mPlayList.next();
                play();
            }
        }
        notifyComplete(next);
    }

    @Override
    public void releasePlayer() {
        mPlayList = null;
        mPlayer.release();
        mPlayer = null;
        sInstance = null;
    }

    @Override
    public void multiple(boolean multiple) {
        mMultiple = multiple;
    }

    // Callbacks

    @Override
    public void registerCallback(Callback callback) {
        mCallbacks.add(callback);
    }

    @Override
    public void unregisterCallback(Callback callback) {
        mCallbacks.remove(callback);
    }

    @Override
    public void removeCallbacks() {
        mCallbacks.clear();
    }

    private void notifyPlayStatusChanged(boolean isPlaying) {
        for (Callback callback : mCallbacks) {
            callback.onPlayStatusChanged(isPlaying);
        }
    }

    private void notifyPlayLast(Song song) {
//        for (Callback callback : mCallbacks) {
//            callback.onSwitchLast(song);
//        }
    }

    private void notifyPlayNext(Song song) {
//        for (Callback callback : mCallbacks) {
//            callback.onSwitchNext(song);
//        }
    }

    private void notifyComplete(Song song) {
//        for (Callback callback : mCallbacks) {
//            callback.onComplete(song);
//        }
    }
}