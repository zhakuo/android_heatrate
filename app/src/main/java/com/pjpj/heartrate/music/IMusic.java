package com.pjpj.heartrate.music;

import com.pjpj.heartrate.data.CustomSongList;
import com.pjpj.heartrate.music.model.PlayList;
import com.pjpj.heartrate.music.model.Song;

import java.util.List;

public interface IMusic {

    void setPlayList(PlayList list);

    boolean play();

    boolean play(PlayList list);

    boolean play(PlayList list, int startIndex);

    boolean play(Song song);

    boolean addSong(Song song);

    boolean removeSong(Song song);

    boolean playMultiSong(CustomSongList customSong);

    PlayList getPlayList();

    boolean playLast();

    boolean playNext();

    boolean pause();

    boolean isPlaying();

    int getProgress();

    Song getPlayingSong();

    List<Song> getSongList();

    void setVolume(Song song);

    boolean seekTo(int progress);

    void setPlayMode(PlayMode playMode);

    void registerCallback(Callback callback);

    void unregisterCallback(Callback callback);

    void removeCallbacks();

    void releasePlayer();

    void multiple(boolean multiple);

    interface Callback {

//        void onSwitchLast(@Nullable Song last);
//
//        void onSwitchNext(@Nullable Song next);
//
//        void onComplete(@Nullable Song next);

        void onPlayStatusChanged(boolean isPlaying);
    }
}
