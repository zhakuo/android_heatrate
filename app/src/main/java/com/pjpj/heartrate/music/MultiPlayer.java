package com.pjpj.heartrate.music;

import android.media.MediaPlayer;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.BaseMediaSource;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.MediaSourceFactory;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.pjpj.heartrate.App;
import com.pjpj.heartrate.data.CustomSongList;
import com.pjpj.heartrate.music.custom.CustomTrackRenderersFactory;
import com.pjpj.heartrate.music.model.PlayList;
import com.pjpj.heartrate.music.model.Song;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MultiPlayer implements IMusic, MediaPlayer.OnCompletionListener {

    private static final String TAG = "MultiPlayer";

    private static final int MULTIPLE_SIZE = 6;
    private static volatile MultiPlayer sInstance;


    private PlayList mPlayList;
    // Default size 2: for service and UI
    private List<Callback> mCallbacks = new ArrayList<>(2);
    private Map<Song, MediaHolder> mPlayerList = new HashMap<>(MULTIPLE_SIZE);

    // Player status
    private boolean isPaused;

    private MultiPlayer() {
        mPlayList = new PlayList();
    }

    private MediaHolder initPlayer() {
        CustomTrackRenderersFactory renderersFactory = new CustomTrackRenderersFactory(App.app);
        DataSource.Factory mediaDataSourceFactory = new DefaultDataSourceFactory(App.app, Util.getUserAgent(App.app, "whiteNoise"));
        MediaSourceFactory mMediaSourceFactory = new DefaultMediaSourceFactory(mediaDataSourceFactory);
        SimpleExoPlayer player = new SimpleExoPlayer.Builder(App.app, renderersFactory)
                .setMediaSourceFactory(mMediaSourceFactory)
                .build();
        player.setPlayWhenReady(true);
        player.setRepeatMode(Player.REPEAT_MODE_ONE);

        MediaHolder mediaHolder = new MediaHolder();
        mediaHolder.exoPlayer = player;
        mediaHolder.mediaDataSourceFactory = mediaDataSourceFactory;

//        player.addListener(new Player.Listener() {
//            @Override
//            public void onPlaybackStateChanged(int state) {
//                if (state == Player.STATE_READY) {
//                    long totalDuration = player.getDuration();
//                    renderersFactory.setTotalDurationUs(totalDuration * 1000);
//                }
//
//            }
//        });
//        renderersFactory.setProgressChange(new CustomTrackRenderersFactory.ProgressChange() {
//            @Override
//            public void almostFinish() {
//                AppExecutor.getInstance().runUI(new Runnable() {
//                    @Override
//                    public void run() {
//                        player.seek(0);
//                    }
//                });
//            }
//        });
        mediaHolder.mRenderersFactory = renderersFactory;
        return mediaHolder;
    }

    public static MultiPlayer getInstance() {
        if (sInstance == null) {
            synchronized (EPlayer.class) {
                if (sInstance == null) {
                    sInstance = new MultiPlayer();
                }
            }
        }
        return sInstance;
    }

    @Override
    public void setPlayList(PlayList list) {
        if (list == null) {
            list = new PlayList();
        }
        mPlayList = list;
    }

    @Override
    public boolean play() {
        if (isPaused) {
            Iterator<Song> iterator = mPlayerList.keySet().iterator();
            while (iterator.hasNext()) {
                Song song = iterator.next();
                MediaHolder player = mPlayerList.get(song);
                if (player != null) {
                    player.exoPlayer.play();
                }
            }
            isPaused = false;
            notifyPlayStatusChanged(true);
            return true;
        }
        if (mPlayList.prepare()) {
            int songListSize = mPlayList.getSongs().size();
            for (int i = 0; i < songListSize; i++) {
                Song song = mPlayList.getSongs().get(i);
                if (!mPlayerList.containsKey(song)) {
                    mPlayerList.put(song, initPlayer());
                }
            }
            Iterator<Song> iterator = mPlayerList.keySet().iterator();
            while (iterator.hasNext()) {
                Song song = iterator.next();
                MediaHolder player = mPlayerList.get(song);
                if (!mPlayList.getSongs().contains(song)) {
                    player.release();
                    iterator.remove();
                    mPlayerList.remove(song);
                    continue;
                }
                if (player.exoPlayer.isPlaying()) {
                    continue;
                }
                BaseMediaSource mediaSource = new ProgressiveMediaSource.Factory(player.mediaDataSourceFactory).createMediaSource(MediaItem.fromUri(song.getPath()));
                player.exoPlayer.setMediaSource(new MergingMediaSource(mediaSource));
                player.exoPlayer.setVolume(song.getVolume());
                player.exoPlayer.prepare();
                player.exoPlayer.setPlayWhenReady(true);
            }

            notifyPlayStatusChanged(true);

            return true;
        } else {
            Iterator<Song> iterator = mPlayerList.keySet().iterator();
            while (iterator.hasNext()) {
                Song song = iterator.next();
                MediaHolder player = mPlayerList.get(song);
                if (!mPlayList.getSongs().contains(song)) {
                    player.release();
                    iterator.remove();
                    mPlayerList.remove(song);
                }
            }
            notifyPlayStatusChanged(false);
        }
        return false;
    }

    @Override
    public boolean play(PlayList list) {
        if (list == null) return false;

        mPlayList.setMultiPlay(false);
        isPaused = false;
        setPlayList(list);
        return play();
    }

    @Override
    public boolean play(PlayList list, int startIndex) {
        if (list == null || startIndex < 0 || startIndex >= list.getNumOfSongs()) return false;

        mPlayList.setMultiPlay(false);
        isPaused = false;
        list.setPlayingIndex(startIndex);
        setPlayList(list);
        return play();
    }

    @Override
    public boolean play(Song song) {
        if (song == null) return false;

        mPlayList.setMultiPlay(false);
        isPaused = false;
        List<Song> list = new ArrayList<>();
        list.add(song);
        mPlayList.setSongs(list);
        return play();
    }

    @Override
    public boolean addSong(Song song) {
        if (song == null) return false;
        isPaused = false;
        if (mPlayList.isMultiPlay()) {
            //前面是播放保存的混合音乐
            //清空一下
            mPlayList.getSongs().clear();
        }
        if (mPlayList.getSongs().contains(song)) {
            return false;
        }
        mPlayList.setMultiPlay(false);
        mPlayList.getSongs().add(song);
        return play();
    }

    @Override
    public boolean removeSong(Song song) {
        if (song == null) return false;

        isPaused = false;
        if (!mPlayList.getSongs().contains(song)) {
            return false;
        }
        //mPlayList.setMultiPlay(false);
        mPlayList.getSongs().remove(song);
        if (mPlayList.getSongs().size() == 0) {
            mPlayList.setPlayingIndex(PlayList.NO_POSITION);
        }
        return play();
    }

    @Override
    public boolean playMultiSong(CustomSongList customSong) {
        mPlayList.setMultiPlay(true);
        mPlayList.setId(customSong.getId());
        mPlayList.setName(customSong.getTitle());
        mPlayList.setCover(customSong.getCover());
        List<Song> list = new ArrayList<>();
        list.addAll(customSong.getSongList());
        mPlayList.setSongs(list);
        return play();
    }

    @Override
    public PlayList getPlayList() {
        return mPlayList;
    }

    @Override
    public boolean playLast() {
        isPaused = false;
        boolean hasLast = mPlayList.hasLast();
        if (hasLast) {
            Song last = mPlayList.last();
            play();
            notifyPlayLast(last);
            return true;
        }
        return false;
    }

    @Override
    public boolean playNext() {
        isPaused = false;
        boolean hasNext = mPlayList.hasNext(false);
        if (hasNext) {
            Song next = mPlayList.next();
            play();
            notifyPlayNext(next);
            return true;
        }
        return false;
    }

    @Override
    public boolean pause() {
        if (!isPaused) {
            for (Map.Entry<Song, MediaHolder> entry : mPlayerList.entrySet()) {
                MediaHolder player = entry.getValue();
                if (player != null) {
                    player.exoPlayer.pause();
                }
            }
            isPaused = true;
            notifyPlayStatusChanged(false);
            return true;
        }
        return false;
    }

    @Override
    public boolean isPlaying() {
        return !isPaused;
    }

    @Override
    public int getProgress() {
        return 0;
    }

    @Nullable
    @Override
    public Song getPlayingSong() {
        return mPlayList.getCurrentSong();
    }

    @Override
    public List<Song> getSongList() {
        return mPlayList.getSongs();
    }

    @Override
    public void setVolume(Song song) {
        for (Map.Entry<Song, MediaHolder> entry : mPlayerList.entrySet()) {
            MediaHolder player = entry.getValue();
            Song media = entry.getKey();
            if (player != null && media.equals(song)) {
                player.exoPlayer.setVolume(song.getVolume());
            }
        }
    }

    @Override
    public boolean seekTo(int progress) {
        if (mPlayList.getSongs().isEmpty()) return false;

        Song currentSong = mPlayList.getCurrentSong();
        if (currentSong != null) {
            if (currentSong.getDuration() <= progress) {
                // onCompletion(mPlayer);
            } else {
                // mPlayer.seekTo(progress);
            }
            return true;
        }
        return false;
    }

    @Override
    public void setPlayMode(PlayMode playMode) {
        mPlayList.setPlayMode(playMode);
    }

    // Listeners

    @Override
    public void onCompletion(MediaPlayer mp) {
        Song next = null;
        // There is only one limited play mode which is list, player should be stopped when hitting the list end
        if (mPlayList.getPlayMode() == PlayMode.LIST && mPlayList.getPlayingIndex() == mPlayList.getNumOfSongs() - 1) {
            // In the end of the list
            // Do nothing, just deliver the callback
        } else if (mPlayList.getPlayMode() == PlayMode.SINGLE) {
            next = mPlayList.getCurrentSong();
            play();
        } else {
            boolean hasNext = mPlayList.hasNext(true);
            if (hasNext) {
                next = mPlayList.next();
                play();
            }
        }
        notifyComplete(next);
    }

    @Override
    public void releasePlayer() {
        mPlayList = null;

        for (Map.Entry<Song, MediaHolder> entry : mPlayerList.entrySet()) {
            MediaHolder player = entry.getValue();
            if (player != null) {
                player.exoPlayer.release();
                player.exoPlayer = null;
            }
        }
        sInstance = null;
    }

    @Override
    public void multiple(boolean multiple) {

    }

    // Callbacks

    @Override
    public void registerCallback(Callback callback) {
        mCallbacks.add(callback);
    }

    @Override
    public void unregisterCallback(Callback callback) {
        mCallbacks.remove(callback);
    }

    @Override
    public void removeCallbacks() {
        mCallbacks.clear();
    }

    private void notifyPlayStatusChanged(boolean isPlaying) {
        for (Callback callback : mCallbacks) {
            callback.onPlayStatusChanged(isPlaying);
        }
    }

    private void notifyPlayLast(Song song) {
//        for (Callback callback : mCallbacks) {
//            callback.onSwitchLast(song);
//        }
    }

    private void notifyPlayNext(Song song) {
//        for (Callback callback : mCallbacks) {
//            callback.onSwitchNext(song);
//        }
    }

    private void notifyComplete(Song song) {
//        for (Callback callback : mCallbacks) {
//            callback.onComplete(song);
//        }
    }

    class MediaHolder {
        public ExoPlayer exoPlayer;
        public DataSource.Factory mediaDataSourceFactory;
        public CustomTrackRenderersFactory mRenderersFactory;

        public void release() {
            if (exoPlayer != null) {
                exoPlayer.stop();
                exoPlayer.release();
                exoPlayer = null;
            }
            mediaDataSourceFactory = null;
        }
    }
}