package com.pjpj.heartrate.music.I;

public interface IPlay {
    void retrieveLastPlayMode();

    void bindPlaybackService();

    void unbindPlaybackService();

    void subscribe();

    void unsubscribe();
}
