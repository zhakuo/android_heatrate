package com.pjpj.heartrate.music.multi;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.audio.MediaCodecAudioRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecAdapter;
import com.google.android.exoplayer2.mediacodec.MediaCodecSelector;
import com.google.android.exoplayer2.util.MediaClock;

import java.nio.ByteBuffer;

public class MultiMediaCodecAudioRenderer extends MediaCodecAudioRenderer {
    private final int index;

    public MultiMediaCodecAudioRenderer(int index, Context context, MediaCodecSelector mediaCodecSelector) {
        super(context, mediaCodecSelector);
        this.index = index;
    }

    public void setVolume(float volume) {
        try {
            handleMessage(MSG_SET_VOLUME, volume);
        } catch (ExoPlaybackException e) {
            e.printStackTrace();
        }
    }

    @Override
    public MediaClock getMediaClock() {
        if (index == 0) {
            return super.getMediaClock();
        }
        return null;
    }

    @Override
    protected boolean processOutputBuffer(long positionUs, long elapsedRealtimeUs, @Nullable MediaCodecAdapter codec, @Nullable ByteBuffer buffer, int bufferIndex, int bufferFlags, int sampleCount, long bufferPresentationTimeUs, boolean isDecodeOnlyBuffer, boolean isLastBuffer, Format format) throws ExoPlaybackException {
        Log.e("MultiMediaCodec", "inedx:" + index + " buffer:" + buffer);
        return super.processOutputBuffer(positionUs, elapsedRealtimeUs, codec, buffer, bufferIndex, bufferFlags, sampleCount, bufferPresentationTimeUs, isDecodeOnlyBuffer, isLastBuffer, format);
    }
}
