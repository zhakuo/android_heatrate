package com.pjpj.heartrate.music.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.pjpj.heartrate.App;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.util.ResourceUtils;

import java.util.Objects;

public class Song implements Parcelable {
    public final static int FILE_TYPE_ASSETS = 1;
    public final static int FILE_TYPE_RAW = 2;
    public final static int FILE_TYPE_FILE = 3;
    public final static int FILE_TYPE_ONLINE = 4;

    private int id;

    private String title;

    private String displayName;

    private String artist;

    private String album;

    private String path;

    private int duration;

    private int size;

    private boolean favorite;

    private int resourceType;

    private float volume = 0.5f;

    public Song() {
        // Empty
    }

    public Song(Parcel in) {
        readFromParcel(in);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public Object getRealImg() {
        if (TextUtils.isEmpty(album)) {
            return R.mipmap.ic_launcher;
        }
        if (album.startsWith("http")) {
            return album;
        } else {
            int redId = ResourceUtils.getResId(App.app, album, ResourceUtils.RES_MIPMAP);
            return redId;
        }

    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public int getResourceType() {
        return resourceType;
    }

    public void setResourceType(int resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.displayName);
        dest.writeString(this.artist);
        dest.writeString(this.album);
        dest.writeString(this.path);
        dest.writeInt(this.duration);
        dest.writeInt(this.size);
        dest.writeInt(this.favorite ? 1 : 0);
        dest.writeInt(this.resourceType);
        dest.writeFloat(this.volume);
    }

    public void readFromParcel(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.displayName = in.readString();
        this.artist = in.readString();
        this.album = in.readString();
        this.path = in.readString();
        this.duration = in.readInt();
        this.size = in.readInt();
        this.favorite = in.readInt() == 1;
        this.resourceType = in.readInt();
        this.volume = (float) in.readDouble();
    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel source) {
            return new Song(source);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return id == song.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}