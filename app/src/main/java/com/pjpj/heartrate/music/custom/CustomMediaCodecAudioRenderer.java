package com.pjpj.heartrate.music.custom;

import android.content.Context;

import androidx.annotation.Nullable;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.audio.MediaCodecAudioRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecAdapter;
import com.google.android.exoplayer2.mediacodec.MediaCodecSelector;

import java.nio.ByteBuffer;

public class CustomMediaCodecAudioRenderer extends MediaCodecAudioRenderer {

    private CustomTrackRenderersFactory.ProgressChange mProgressChange;
    private long mTotalDurationUs;

    public CustomMediaCodecAudioRenderer(Context context, MediaCodecSelector mediaCodecSelector) {
        super(context, mediaCodecSelector);
    }

    public void setProgressChange(CustomTrackRenderersFactory.ProgressChange progressChange) {
        mProgressChange = progressChange;
    }

    public void setTotalDuration(long duration) {
        this.mTotalDurationUs = duration;
    }

    @Override
    protected boolean processOutputBuffer(long positionUs, long elapsedRealtimeUs, @Nullable MediaCodecAdapter codec, @Nullable ByteBuffer buffer, int bufferIndex, int bufferFlags, int sampleCount, long bufferPresentationTimeUs, boolean isDecodeOnlyBuffer, boolean isLastBuffer, Format format) throws ExoPlaybackException {
//        Log.e("processOutputBuffer", "positionUs:" + positionUs + " elapsedRealtimeUs:" + elapsedRealtimeUs + " isLastBuffer:" + isLastBuffer);
        boolean result = super.processOutputBuffer(positionUs, elapsedRealtimeUs, codec, buffer, bufferIndex, bufferFlags, sampleCount, bufferPresentationTimeUs, isDecodeOnlyBuffer, isLastBuffer, format);
        boolean customLastBuffer = mTotalDurationUs > 0 && mTotalDurationUs - positionUs < 1000000;//一秒后就结束
        if (customLastBuffer && !mIsLastBuffer) {
            mIsLastBuffer = true;
            mProgressChange.almostFinish();
        }
        mIsLastBuffer = customLastBuffer;
        return result;
    }

    private boolean mIsLastBuffer;
}
