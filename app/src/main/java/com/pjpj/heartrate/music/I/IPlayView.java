package com.pjpj.heartrate.music.I;

import androidx.annotation.Nullable;

import com.pjpj.heartrate.music.MusicService;
import com.pjpj.heartrate.music.model.Song;

public interface IPlayView<T> {
    void setPresenter(T t);

    void handleError(Throwable error);

    void onPlaybackServiceBound(MusicService service);

    void onPlaybackServiceUnbound();

    void onSongUpdated(@Nullable Song song);

}
