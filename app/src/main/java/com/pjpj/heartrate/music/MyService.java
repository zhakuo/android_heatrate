package com.pjpj.heartrate.music;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.pjpj.heartrate.music.I.IPlay;
import com.pjpj.heartrate.music.I.IPlayView;
import com.pjpj.heartrate.music.I.PlayImpl;
import com.pjpj.heartrate.music.model.Song;

/**
 * Des:
 * Create by: ye
 * On:        2021\08\19 9:42
 * Email:     yeshieh@163.com
 */
public class MyService extends Service  implements IPlayView<IPlay> {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        new PlayImpl(this, this).subscribe();
    }

    @Override
    public void setPresenter(IPlay iPlay) {

    }

    @Override
    public void handleError(Throwable error) {

    }

    @Override
    public void onPlaybackServiceBound(MusicService service) {

    }

    @Override
    public void onPlaybackServiceUnbound() {

    }

    @Override
    public void onSongUpdated(@Nullable Song song) {

    }
}
