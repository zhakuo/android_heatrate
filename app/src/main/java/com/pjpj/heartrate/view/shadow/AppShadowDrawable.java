package com.pjpj.heartrate.view.shadow;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;

import androidx.annotation.Nullable;

import static com.pjpj.heartrate.view.shadow.AppShadowLayout.BOTTOM;
import static com.pjpj.heartrate.view.shadow.AppShadowLayout.LEFT;
import static com.pjpj.heartrate.view.shadow.AppShadowLayout.RIGHT;
import static com.pjpj.heartrate.view.shadow.AppShadowLayout.TOP;

/**
 * Des:
 * Create by: ye
 * On:        2019\07\31 17:35
 * Email:     yeshieh@163.com
 */
public class AppShadowDrawable extends ShapeDrawable {

    private Paint mShadowPaint;
    private float mShadowRadius;
    private float mShadowRadiusL;
    private float mShadowRadiusT;
    private float mShadowRadiusR;
    private float mShadowRadiusB;
    private float mOffsetX;
    private float mOffsetY;
    private int mShadowSide;

    public AppShadowDrawable(boolean dstAtTop, int shadowColor, float shadowRadius,
                             float shadowRadiusL,
                             float shadowRadiusT,
                             float shadowRadiusR,
                             float shadowRadiusB,
                             float offsetX, float offsetY, int shapeSide, RoundRectShape rectShape) {
        super(rectShape);
        this.mShadowRadius = shadowRadius;
        this.mShadowRadiusL = shadowRadiusL;
        this.mShadowRadiusT = shadowRadiusT;
        this.mShadowRadiusR = shadowRadiusR;
        this.mShadowRadiusB = shadowRadiusB;
        this.mOffsetX = offsetX;
        this.mOffsetY = offsetY;
        this.mShadowSide = shapeSide;

        mShadowPaint = new Paint();
        mShadowPaint.setColor(Color.TRANSPARENT);
        //mShadowPaint.setAntiAlias(true);
        mShadowPaint.setShadowLayer(shadowRadius, offsetX, offsetY, shadowColor);
        if (dstAtTop) {
            mShadowPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_ATOP));
        }
    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        if ((mShadowSide & LEFT) == LEFT) {
            left = (int) (left + mShadowRadiusL - mOffsetX);
        }
        if ((mShadowSide & TOP) == TOP) {
            top = (int) (top + mShadowRadiusT - mOffsetY);
        }
        if ((mShadowSide & RIGHT) == RIGHT) {
            right = (int) (right - mShadowRadiusR - mOffsetX);
        }
        if ((mShadowSide & BOTTOM) == BOTTOM) {
            bottom = (int) (bottom - mShadowRadiusB - mOffsetY);
        }
        super.setBounds(left, top, right, bottom);
    }

    @Override
    protected void onDraw(Shape shape, Canvas canvas, Paint paint) {
        shape.draw(canvas, mShadowPaint);
    }

    @Override
    public void setAlpha(int alpha) {
        mShadowPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        mShadowPaint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }
}
