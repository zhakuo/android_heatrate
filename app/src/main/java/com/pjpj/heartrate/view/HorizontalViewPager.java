package com.pjpj.heartrate.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

public class HorizontalViewPager extends ViewPager {

    public HorizontalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HorizontalViewPager(Context context) {
        super(context);
    }

    /**
     * 事件分发, 请求父控件及祖宗控件是否拦截事件
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        if (getCurrentItem() == 0 || getCurrentItem() == getAdapter().getCount() - 1) {
            //请求父控件拦截
            getParent().requestDisallowInterceptTouchEvent(false);
        } else {
            //不要拦截
            getParent().requestDisallowInterceptTouchEvent(true);// 拦截
        }
        return super.dispatchTouchEvent(ev);
    }

}