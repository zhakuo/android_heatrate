package com.pjpj.heartrate.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.pjpj.heartrate.R;

public class AppSettingView extends FrameLayout {

    TextView tvSubTitle;

    public AppSettingView(@NonNull Context context) {
        super(context);
        init(context, null);
    }

    public AppSettingView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attributeSet) {
        LayoutInflater.from(context).inflate(R.layout.app_setting_view, this);
        if (attributeSet != null) {
            TypedArray array = context.obtainStyledAttributes(attributeSet, R.styleable.AppSettingView);
            boolean showArrow = array.getBoolean(R.styleable.AppSettingView_setting_arrow, true);
            boolean showLine = array.getBoolean(R.styleable.AppSettingView_setting_line, true);
            String title = array.getString(R.styleable.AppSettingView_setting_title);
            String subTitle = array.getString(R.styleable.AppSettingView_setting_subtitle);

            array.recycle();

            TextView tvTitle = findViewById(R.id.tv_title);
            tvSubTitle = findViewById(R.id.tv_sub_title);
            View arrow = findViewById(R.id.iv_arrow);
            View line = findViewById(R.id.view_line);

            arrow.setVisibility(showArrow ? View.VISIBLE : View.INVISIBLE);
            line.setVisibility(showLine ? View.VISIBLE : View.INVISIBLE);
            tvTitle.setText(title);
            tvSubTitle.setText(subTitle);
        }
    }
}
