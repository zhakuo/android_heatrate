package com.pjpj.heartrate.view.animate;

import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;

/**
 * Des:
 * Create by: ye
 * On:        2019\07\12 15:01
 * Email:     yeshieh@163.com
 */
public class HangOver extends BaseAnimatorSet{
    @Override
    public void setAnimation(View view) {
        animatorSet.setInterpolator(new AnticipateOvershootInterpolator());
        animatorSet.playTogether(//
                ObjectAnimator.ofFloat(view, "alpha", 1, 0).setDuration(duration),
                ObjectAnimator.ofFloat(view,"translationY",0,-500));
    }
}
