package com.pjpj.heartrate.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.util.UIHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.jessyan.autosize.utils.AutoSizeUtils;

/**
 * Des:
 * Create by: ye
 * On:        2021\07\17 10:51
 * Email:     yeshieh@163.com
 */
public class MusicCoverContainer extends FrameLayout {
    private static final int DEFAULT_CHILD_GRAVITY = Gravity.TOP | Gravity.START;
    private int mDefaultMargin;
    private int mCoverWidth;
    private int mCoverGravity;

    private Map<Object, RoundedImageView> mCoverViewList = new HashMap<>();

    public MusicCoverContainer(@NonNull Context context) {
        super(context);
        init(context, null);
    }

    public MusicCoverContainer(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MusicCoverContainer(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.MusicCoverContainer);
            mCoverWidth = ta.getDimensionPixelSize(R.styleable.MusicCoverContainer_cover_height, AutoSizeUtils.dp2px(getContext(), 56));
            mDefaultMargin = ta.getDimensionPixelSize(R.styleable.MusicCoverContainer_cover_margin, AutoSizeUtils.dp2px(getContext(), 8));
            mCoverGravity = ta.getInt(R.styleable.MusicCoverContainer_cover_gravity, 0);
        }
    }

    public void updateData(List<Object> imgList) {
        removeAllViews();
        for (int i = 0; i < imgList.size(); i++) {
            addImage(imgList.get(i));
        }
    }

    public void addImage(Object img) {
        if (mCoverViewList.containsKey(img)) {
            View view = mCoverViewList.get(img);
            addView(view);
        } else {
            RoundedImageView imageView = new RoundedImageView(getContext());
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setCornerRadius(UIHelper.dpToPx(8));
            Glide.with(getContext()).load(img).into(imageView);
            LayoutParams lp = new LayoutParams(mCoverWidth, mCoverWidth);
            if (mCoverGravity == 2) {
                lp.gravity = Gravity.CENTER_HORIZONTAL;
            } else if (mCoverGravity == 1) {
                lp.gravity = Gravity.RIGHT;
            }
            addView(imageView, lp);
            mCoverViewList.put(img, imageView);
        }
    }

    public void removeImage(Object img) {
        if (mCoverViewList.containsKey(img)) {
            View view = mCoverViewList.get(img);
            removeView(view);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        layoutChildren(left, top, right, bottom, true);
    }

    void layoutChildren(int left, int top, int right, int bottom, boolean forceLeftGravity) {
        final int count = getChildCount();

        final int parentLeft = getPaddingLeft();
        final int parentRight = right - left - getPaddingRight();

        final int parentTop = getPaddingTop();
        final int parentBottom = bottom - top - getPaddingBottom();

        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                final LayoutParams lp = (LayoutParams) child.getLayoutParams();

                final int width = (int) (mCoverWidth * (1 - ((count - i - 1) * 0.05f)));
                final int height = (int) (mCoverWidth * (1 - ((count - i - 1) * 0.05f)));
//                Log.e("MusicCoverContainer", "width:" + width);
//                final int width =(mCoverWidth);
//                final int height = (mCoverWidth);

                int childLeft;
                int childTop;

                int gravity = lp.gravity;
                if (gravity == -1) {
                    gravity = DEFAULT_CHILD_GRAVITY;
                }

                final int layoutDirection = getLayoutDirection();
                final int absoluteGravity = Gravity.getAbsoluteGravity(gravity, layoutDirection);
                final int verticalGravity = gravity & Gravity.VERTICAL_GRAVITY_MASK;

                switch (absoluteGravity & Gravity.HORIZONTAL_GRAVITY_MASK) {
                    case Gravity.CENTER_HORIZONTAL:
                        childLeft = parentLeft + (parentRight - parentLeft - width) / 2 +
                                lp.leftMargin - lp.rightMargin + (count - i - 1) * mDefaultMargin;
                        break;
                    case Gravity.RIGHT:
                    case Gravity.LEFT:
                    default:
                        childLeft = parentLeft + lp.leftMargin + (count - i - 1) * mDefaultMargin;
                }

                switch (verticalGravity) {
                    case Gravity.TOP:
                    case Gravity.CENTER_VERTICAL:
                    case Gravity.BOTTOM:
                    default:
                        childTop = parentTop + (parentBottom - parentTop - height) / 2 +
                                lp.topMargin - lp.bottomMargin;
                }


                child.layout(childLeft, childTop, childLeft + width, childTop + height);
            }
        }
    }
}
