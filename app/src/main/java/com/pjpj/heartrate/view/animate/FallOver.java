package com.pjpj.heartrate.view.animate;

import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.OvershootInterpolator;


/**
 * Des:
 * Create by: ye
 * On:        2019\07\12 14:57
 * Email:     yeshieh@163.com
 */
public class FallOver extends BaseAnimatorSet {
    @Override
    public void setAnimation(View view) {
        animatorSet.setDuration(750);
        view.setAlpha(0f);
        animatorSet.setStartDelay(250);
        animatorSet.setInterpolator(new OvershootInterpolator());
        animatorSet.playTogether(//
                ObjectAnimator.ofFloat(view, "alpha", 0, 1).setDuration(duration),
                ObjectAnimator.ofFloat(view, "translationY", -750, 0));
    }
}
