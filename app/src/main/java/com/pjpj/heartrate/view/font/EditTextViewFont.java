package com.pjpj.heartrate.view.font;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.Nullable;

import com.pjpj.heartrate.R;

/**
 * Des:
 * Create by: ye
 * On:        2021\07\19 11:19
 * Email:     yeshieh@163.com
 */
public class EditTextViewFont extends androidx.appcompat.widget.AppCompatEditText {
    public EditTextViewFont(Context context) {
        super(context);
        init(context, null);
    }

    public EditTextViewFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public EditTextViewFont(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        if (isInEditMode())
            return;
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.EditTextViewFont);
        if (ta != null) {
            boolean bold = ta.getBoolean(R.styleable.EditTextViewFont_font_bold_et, false);
            Typeface tf;
            int style = Typeface.NORMAL;
            if (bold) {
                tf = FontManager.getInstance().getFont("fonts/MaximSansSemibold.otf");
            } else {
                tf = FontManager.getInstance().getFont("fonts/MaximSansRegular.otf");
            }
            Log.i("TextViewFont", "tf:" + tf);
            if (getTypeface() != null) {
                style = getTypeface().getStyle();
            }
            if (tf != null) {
                setTypeface(tf, style);
            }
        }
    }
}
