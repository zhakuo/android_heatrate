package com.pjpj.heartrate.view.dialog;

import android.animation.Animator;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;

import com.pjpj.heartrate.view.animate.BaseAnimatorSet;

/**
 * Des:       BaseDialog
 * Create by: m122469119
 * On:        2018/7/31 09:43
 * Email:     122469119@qq.com
 */
public abstract class BaseDialog<T extends BaseDialog> extends Dialog {
    /**
     * mContext(上下文)
     */
    protected Context mContext;

    /**
     * (DisplayMetrics)设备密度
     */
    protected DisplayMetrics mDisplayMetrics;

    /**
     * enable dismiss outside dialog(设置点击对话框以外区域,是否dismiss)
     */
    protected boolean mCancel;

    /**
     * dialog width scale(宽度比例)
     */
    protected float mWidthScale = 0;

    /**
     * dialog height scale(高度比例)
     */
    protected float mHeightScale = 0;

    /**
     * mShowAnim(对话框显示动画)
     */
    private BaseAnimatorSet mShowAnim;

    /**
     * mDismissAnim(对话框消失动画)
     */
    private BaseAnimatorSet mDismissAnim;

    /**
     * top container(最上层容器)
     */
    protected LinearLayout mContentWrapper;

    /**
     * container to control dialog height(用于控制对话框高度)
     */
    protected LinearLayout mContentView;

    /**
     * the child of mContentView you create.(创建出来的mLlControlHeight的直接子View)
     */
    protected View mOnCreateView;

    /**
     * is mShowAnim running(显示动画是否正在执行)
     */
    private boolean mIsShowAnim;

    /**
     * is DismissAnim running(消失动画是否正在执行)
     */
    private boolean mIsDismissAnim;

    /**
     * max height(最大高度)
     */
    protected float mMaxHeight;


    /**
     * automatic dimiss dialog after given delay(在给定时间后,自动消失)
     */
    private boolean mAutoDismiss;

    /**
     * delay (milliseconds) to dimiss dialog(对话框消失延时时间,毫秒值)
     */
    private long mAutoDismissDelay = 1500;

    private Handler mHandler = new Handler(Looper.getMainLooper());


    protected boolean mIsEditMode = false;

    /**
     * method execute order:
     * show:constrouctor---show---oncreate---onStart---onAttachToWindow
     * dismiss:dismiss---onDetachedFromWindow---onStop
     */
    public BaseDialog(Context context) {
        super(context);
        mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);// android:windowNoTitle
        setCanceledOnTouchOutside(true);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));// android:windowBackground
        getWindow().addFlags(LayoutParams.FLAG_DIM_BEHIND);// android:backgroundDimEnabled默认是true的
    }

    /**
     * method execute order:
     * show:constrouctor---show---oncreate---onStart---onAttachToWindow
     * dismiss:dismiss---onDetachedFromWindow---onStop
     */
    public BaseDialog(Context context, int style) {
        super(context, style);
        mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);// android:windowNoTitle
        setCanceledOnTouchOutside(true);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));// android:windowBackground
        getWindow().addFlags(LayoutParams.FLAG_DIM_BEHIND);// android:backgroundDimEnabled默认是true的
    }


    public abstract View onCreateView();


    /**
     * set Ui data or logic opreation before attatched window(在对话框显示之前,设置界面数据或者逻辑)
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mDisplayMetrics = mContext.getResources().getDisplayMetrics();


        mOnCreateView = onCreateView();

        mContentView = new LinearLayout(mContext);
        mContentView.setOrientation(LinearLayout.VERTICAL);
        mContentView.addView(mOnCreateView);

        mContentWrapper = new LinearLayout(mContext);
        mContentWrapper.setGravity(Gravity.CENTER);
        mContentWrapper.addView(mContentView);

        if (!mIsEditMode) {
            setContentView(mContentWrapper, new ViewGroup.LayoutParams(mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels));
        } else {
            setContentView(mContentWrapper, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

        onViewCreated(mOnCreateView);

        mContentWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCancel) {
                    dismiss();
                }
            }
        });

        mOnCreateView.setClickable(true);
    }

    public void onViewCreated(View view) {
    }

    /**
     * when dailog attached to window,set dialog width and height and show anim
     */
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        setUiBeforeShow();

        int width;
        if (mWidthScale == 0) {
            width = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            width = (int) (mDisplayMetrics.widthPixels * mWidthScale);
        }

        int height;
        if (mHeightScale == 0) {
            height = ViewGroup.LayoutParams.WRAP_CONTENT;
        } else {
            height = (int) (mDisplayMetrics.heightPixels * mHeightScale);
        }

        mContentView.setLayoutParams(new LinearLayout.LayoutParams(width, height));

        if (mShowAnim != null) {
            mShowAnim.listener(new BaseAnimatorSet.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    mIsShowAnim = true;
                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    mIsShowAnim = false;
                    delayDismiss();
                }

                @Override
                public void onAnimationCancel(Animator animator) {
                    mIsShowAnim = false;
                }
            }).playOn(mContentView);
        } else {
            BaseAnimatorSet.reset(mContentView);
            delayDismiss();
        }
    }

    public abstract void setUiBeforeShow();

    @Override
    public void setCanceledOnTouchOutside(boolean cancel) {
        this.mCancel = cancel;
        super.setCanceledOnTouchOutside(cancel);
    }

    @Override
    public void show() {
        super.show();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    @Override
    public void dismiss() {
        if (mDismissAnim != null) {
            mDismissAnim.listener(new BaseAnimatorSet.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    mIsDismissAnim = true;
                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    mIsDismissAnim = false;
                    superDismiss();
                }

                @Override
                public void onAnimationCancel(Animator animator) {
                    mIsDismissAnim = false;
                    superDismiss();
                }
            }).playOn(mContentView);
        } else {
            superDismiss();
        }
    }

    /**
     * dismiss without anim(无动画dismiss)
     */
    public void superDismiss() {
        super.dismiss();
    }

    /**
     * dialog anim by styles(动画弹出对话框,style动画资源)
     */
    public void show(int animStyle) {
        Window window = getWindow();
        window.setWindowAnimations(animStyle);
        show();
    }


    /**
     * set window dim or not(设置背景是否昏暗)
     */
    public T dimEnabled(boolean isDimEnabled) {
        if (isDimEnabled) {
            getWindow().addFlags(LayoutParams.FLAG_DIM_BEHIND);
        } else {
            getWindow().clearFlags(LayoutParams.FLAG_DIM_BEHIND);
        }
        return (T) this;
    }

    /**
     * set window dim 黑暗程度
     */
    public T dimAlpha(float alpha) {
        LayoutParams lp = getWindow().getAttributes();
        lp.dimAmount = alpha;
        getWindow().setAttributes(lp);
        getWindow().addFlags(LayoutParams.FLAG_DIM_BEHIND);
        return (T) this;
    }

    /**
     * set dialog width scale:0-1(设置对话框宽度,占屏幕宽的比例0-1)
     */
    public T widthScale(float widthScale) {
        this.mWidthScale = widthScale;
        return (T) this;
    }

    /**
     * set dialog height scale:0-1(设置对话框高度,占屏幕宽的比例0-1)
     */
    public T heightScale(float heightScale) {
        mHeightScale = heightScale;
        return (T) this;
    }


    public T maxHeight(float height) {
        mMaxHeight = height;
        return (T) this;
    }

    /**
     * set show anim(设置显示的动画)
     */
    public T showAnim(BaseAnimatorSet showAnim) {
        mShowAnim = showAnim;
        return (T) this;
    }

    /**
     * set dismiss anim(设置隐藏的动画)
     */
    public T dismissAnim(BaseAnimatorSet dismissAnim) {
        mDismissAnim = dismissAnim;
        return (T) this;
    }

    /**
     * automatic dimiss dialog after given delay(在给定时间后,自动消失)
     */
    public T autoDismiss(boolean autoDismiss) {
        mAutoDismiss = autoDismiss;
        return (T) this;
    }

    /**
     * set dealy (milliseconds) to dimiss dialog(对话框消失延时时间,毫秒值)
     */
    public T autoDismissDelay(long autoDismissDelay) {
        mAutoDismissDelay = autoDismissDelay;
        return (T) this;
    }

    private void delayDismiss() {
        if (mAutoDismiss && mAutoDismissDelay > 0) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismiss();
                }
            }, mAutoDismissDelay);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mIsDismissAnim || mIsShowAnim || mAutoDismiss) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        if (mIsDismissAnim || mIsShowAnim || mAutoDismiss) {
            return;
        }
        super.onBackPressed();
    }

    /**
     * dp to px
     */
    protected int dp2px(float dp) {
        final float scale = mContext.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}
