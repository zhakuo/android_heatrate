package com.pjpj.heartrate.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Region;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceView;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\18 17:36
 * Email:     yeshieh@163.com
 */
public class RoundSurfaceView extends SurfaceView {
    public RoundSurfaceView(Context context) {
        super(context);
    }

    public RoundSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RoundSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void draw(Canvas canvas) {
        Path path = new Path();
        path.addCircle(getMeasuredWidth() / 2, getMeasuredHeight() / 2, getMeasuredWidth() / 2, Path.Direction.CCW);
        if (Build.VERSION.SDK_INT >= 28) {
            canvas.clipPath(path);
        } else {
            canvas.clipPath(path, Region.Op.REPLACE);
        }
        super.draw(canvas);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

}
