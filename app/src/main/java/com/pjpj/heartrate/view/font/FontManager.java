package com.pjpj.heartrate.view.font;

import android.content.res.AssetManager;
import android.graphics.Typeface;

import com.pjpj.heartrate.App;

import java.util.HashMap;
import java.util.Map;

/**
 * Des:
 * Create by: ye
 * On:        2021\07\15 11:51
 * Email:     yeshieh@163.com
 */
public class FontManager {
    private static FontManager instance;

    private AssetManager mgr;

    private Map<String, Typeface> fonts;

    private FontManager(AssetManager _mgr) {
        mgr = _mgr;
        fonts = new HashMap<String, Typeface>();
    }

    public static void init(AssetManager mgr) {
        instance = new FontManager(mgr);
    }

    public static FontManager getInstance() {
        if (instance == null) {
            // App.getContext() is just one way to get a Context here
            // getContext() is just a method in an Application subclass
            // that returns the application context
            AssetManager assetManager = App.app.getAssets();
            init(assetManager);
        }

        return instance;
    }

    public Typeface getFont(String asset) {
        if (fonts.containsKey(asset))
            return fonts.get(asset);

        Typeface font = null;

        try {
            font = Typeface.createFromAsset(mgr, asset);
            fonts.put(asset, font);
        } catch (Exception e) {

        }

        return font;
    }

}
