package com.pjpj.heartrate.view.dialog;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import com.pjpj.heartrate.R;

/**
 * Des:
 * Create by: ye
 * On:        2021\02\25 16:55
 * Email:     yeshieh@163.com
 */
public class AppLoadingDialog extends BaseDialog<AppLoadingDialog> {
    private ProgressBar mLoadingView;

    public AppLoadingDialog(Context context) {
        super(context);
    }

    @Override
    public View onCreateView() {
        View view = View.inflate(mContext, R.layout.app_dialog_loading, null);
        mLoadingView = view.findViewById(R.id.progressBar1);
        return view;
    }

    @Override
    public void setUiBeforeShow() {
    }

    @Override
    public void show() {
        super.show();
        mLoadingView.setIndeterminate(true);
    }

    @Override
    public void dismiss() {
        mLoadingView.setIndeterminate(false);
        super.dismiss();
    }
}
