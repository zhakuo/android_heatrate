package com.pjpj.heartrate.view.font;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.pjpj.heartrate.R;

/**
 * Des:
 * Create by: ye
 * On:        2021\07\15 11:42
 * Email:     yeshieh@163.com
 */
public class TextViewFont extends androidx.appcompat.widget.AppCompatTextView {
    public TextViewFont(@NonNull Context context) {
        this(context, null);
    }

    public TextViewFont(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextViewFont(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        if (isInEditMode())
            return;
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TextViewFont);
        if (ta != null) {
            boolean bold = ta.getBoolean(R.styleable.TextViewFont_font_bold, false);
            Typeface tf;
            int style = Typeface.NORMAL;
            if (bold) {
                tf = FontManager.getInstance().getFont("fonts/Bebas-Regular.ttf");
            } else {
                tf = FontManager.getInstance().getFont("fonts/Abel-Regular.ttf");
            }
            Log.i("TextViewFont", "tf:" + tf);
            if (getTypeface() != null) {
                style = getTypeface().getStyle();
            }
            if (tf != null) {
                setTypeface(tf, style);
            }
        }
    }
}
