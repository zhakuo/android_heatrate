package com.pjpj.heartrate.view.shadow;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;

import com.pjpj.heartrate.R;


/**
 * Des:阴影Layout 使用该layout会导致子view的宽高减少阴影的宽度 为了显示阴影 设置了padding
 * childView.height=shadowlayout.height-shadowradius
 * childView.width=shadowlayout.width-shadowradius
 * <p>
 * Create by: ye
 * On:        2019\07\31 17:29
 * Email:     yeshieh@163.com
 */
public class AppShadowLayout extends FrameLayout {

    protected static final int ALL = 0x1111;

    protected static final int LEFT = 0x0001;

    protected static final int TOP = 0x0010;

    protected static final int RIGHT = 0x0100;

    protected static final int BOTTOM = 0x1000;

    public static final int SHAPE_RECTANGLE = 0x0001;

    //TODO 还没实现
    public static final int SHAPE_OVAL = 0x0010;

    /**
     * 阴影的颜色
     */
    private int mShadowColor = Color.TRANSPARENT;

    /**
     * 阴影的大小范围
     */
    private float mShadowRadius = 0;

    private float mShadowRadiusLeft = 0;
    private float mShadowRadiusTop = 0;
    private float mShadowRadiusRight = 0;
    private float mShadowRadiusBottom = 0;

    /**
     * 阴影的圆角
     */
    private float mCornerRadius = 0;

    /**
     * 阴影的圆角top left
     */
    private float mCornerRadiusTopLeft = 0;

    /**
     * 阴影的圆角top right
     */
    private float mCornerRadiusTopRight = 0;

    /**
     * 阴影的圆角bottom left
     */
    private float mCornerRadiusBottomLeft = 0;

    /**
     * 阴影的圆角bottom right
     */
    private float mCornerRadiusBottomRight = 0;

    /**
     * 阴影 x 轴的偏移量
     */
    private float mShadowDx = 0;

    /**
     * 阴影 y 轴的偏移量
     */
    private float mShadowDy = 0;

    /**
     * 阴影显示的边界
     */
    private int mShadowSide = ALL;

    /**
     * 阴影的形状，圆形/矩形
     */
    private int mShadowShape = SHAPE_RECTANGLE;

    private AppShadowDrawable mShadowDrawable;

    public AppShadowLayout(@NonNull Context context) {
        this(context, null, 0);
    }

    public AppShadowLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AppShadowLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    private void initialize(@Nullable AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.AppShadowLayout);
        boolean dstAtTop = true;
        if (typedArray != null) {
            mShadowShape = typedArray.getInt(R.styleable.AppShadowLayout_shadowShape, SHAPE_RECTANGLE);
            mShadowRadius = typedArray.getDimension(R.styleable.AppShadowLayout_shadowRadius, 0);
            mShadowRadiusLeft = typedArray.getDimension(R.styleable.AppShadowLayout_shadowRadiusLeft, 0);
            mShadowRadiusTop = typedArray.getDimension(R.styleable.AppShadowLayout_shadowRadiusTop, 0);
            mShadowRadiusRight = typedArray.getDimension(R.styleable.AppShadowLayout_shadowRadiusRight, 0);
            mShadowRadiusBottom = typedArray.getDimension(R.styleable.AppShadowLayout_shadowRadiusBottom, 0);
            mShadowColor = typedArray.getColor(R.styleable.AppShadowLayout_shadowColor,
                    getContext().getResources().getColor(android.R.color.black));
            mShadowDx = typedArray.getDimension(R.styleable.AppShadowLayout_shadowDx, 0);
            mShadowDy = typedArray.getDimension(R.styleable.AppShadowLayout_shadowDy, 0);
            mCornerRadiusTopLeft = typedArray.getDimension(R.styleable.AppShadowLayout_shadowCornerTL, 0);
            mCornerRadiusTopRight = typedArray.getDimension(R.styleable.AppShadowLayout_shadowCornerTR, 0);
            mCornerRadiusBottomLeft = typedArray.getDimension(R.styleable.AppShadowLayout_shadowCornerBL, 0);
            mCornerRadiusBottomRight = typedArray.getDimension(R.styleable.AppShadowLayout_shadowCornerBR, 0);
            mCornerRadius = typedArray.getDimension(R.styleable.AppShadowLayout_shadowCornerRadius, 0);
            mShadowSide = typedArray.getInt(R.styleable.AppShadowLayout_shadowSide, ALL);
            dstAtTop = typedArray.getBoolean(R.styleable.AppShadowLayout_shadowDstATop, true);

            typedArray.recycle();
        }

        if (mCornerRadius > 0) {
            mCornerRadiusTopLeft = mCornerRadius;
            mCornerRadiusTopRight = mCornerRadius;
            mCornerRadiusBottomLeft = mCornerRadius;
            mCornerRadiusBottomRight = mCornerRadius;
        }
        boolean radiusPendingCalculate = mShadowRadius == 0;
        if (mShadowRadiusLeft == 0 && ((mShadowSide & LEFT) == LEFT)) {
            mShadowRadiusLeft = mShadowRadius;
        } else if (radiusPendingCalculate) {
            if (mShadowRadius < mShadowRadiusLeft) {
                mShadowRadius = mShadowRadiusLeft;
            }
        }
        if (mShadowRadiusTop == 0 && ((mShadowSide & TOP) == TOP)) {
            mShadowRadiusTop = mShadowRadius;
        }
        if (radiusPendingCalculate) {
            if (mShadowRadius < mShadowRadiusTop) {
                mShadowRadius = mShadowRadiusTop;
            }
        }
        if (mShadowRadiusRight == 0 && ((mShadowSide & RIGHT) == RIGHT)) {
            mShadowRadiusRight = mShadowRadius;
        }
        if (radiusPendingCalculate) {
            if (mShadowRadius < mShadowRadiusRight) {
                mShadowRadius = mShadowRadiusRight;
            }
        }
        if (mShadowRadiusBottom == 0 && ((mShadowSide & BOTTOM) == BOTTOM)) {
            mShadowRadiusBottom = mShadowRadius;
        }
        if (radiusPendingCalculate) {
            if (mShadowRadius < mShadowRadiusBottom) {
                mShadowRadius = mShadowRadiusBottom;
            }
        }

        float[] outerR = new float[]{mCornerRadiusTopLeft, mCornerRadiusTopLeft, mCornerRadiusTopRight, mCornerRadiusTopRight,
                mCornerRadiusBottomLeft, mCornerRadiusBottomLeft, mCornerRadiusBottomRight, mCornerRadiusBottomRight};
        RoundRectShape shape = new RoundRectShape(outerR, null, null);
        mShadowDrawable = new AppShadowDrawable(dstAtTop, mShadowColor, mShadowRadius,
                mShadowRadiusLeft,
                mShadowRadiusTop,
                mShadowRadiusRight,
                mShadowRadiusBottom,
                mShadowDx, mShadowDy, mShadowSide, shape);
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        setPadding((int) mShadowRadiusLeft, (int) mShadowRadiusTop, (int) mShadowRadiusRight, (int) mShadowRadiusBottom);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, (int) (left), (int) (top), (int) (right), (int) (bottom));
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        ViewCompat.setBackground(AppShadowLayout.this, mShadowDrawable);
    }

    @Override
    public void setPadding(int left, int top, int right, int bottom) {
        super.setPadding(left, top, right, bottom);
        // NO OP
    }

    @Override
    public void setPaddingRelative(int start, int top, int end, int bottom) {
        super.setPaddingRelative(start, top, end, bottom);
        // NO OP
    }
}
