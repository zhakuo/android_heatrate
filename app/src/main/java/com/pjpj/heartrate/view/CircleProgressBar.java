package com.pjpj.heartrate.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.pjpj.heartrate.R;

/**
 * Des:
 * Create by: ye
 * On:        2021\09\27 17:35
 * Email:     yeshieh@163.com
 */
public class CircleProgressBar extends View {


    // 画圆环的画笔
    private Paint mRingPaint;
    // 圆环颜色
    private int mRingColor;
    // 半径
    private float mRadius;
    // 圆环宽度
    private float mStrokeWidth;
    // 总进度
    private int mTotalProgress = 100;
    // 当前进度
    private int mProgress;

    public CircleProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        // 获取自定义的属性
        initAttrs(context, attrs);
        initVariable();
    }

    private void initAttrs(Context context, AttributeSet attrs) {
        TypedArray typeArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircleProgressBar, 0, 0);
        mStrokeWidth = typeArray.getDimension(R.styleable.CircleProgressBar_progress_strokeWidth, 10);
        mRingColor = typeArray.getColor(R.styleable.CircleProgressBar_progress_ringColor, 0xFFFFFFFF);
    }

    private void initVariable() {
        mRingPaint = new Paint();
        mRingPaint.setAntiAlias(true);
        mRingPaint.setColor(mRingColor);
        mRingPaint.setStyle(Paint.Style.STROKE);
        mRingPaint.setStrokeWidth(mStrokeWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (mProgress > 0) {
            RectF oval = new RectF();
            oval.left = mStrokeWidth / 2;
            oval.top = mStrokeWidth / 2;
            oval.right = getWidth() - mStrokeWidth / 2;
            oval.bottom = getHeight() - mStrokeWidth / 2;
            canvas.drawArc(oval, -90, ((float) mProgress / mTotalProgress) * 360, false, mRingPaint); //
        }
    }

    public void setProgress(int progress) {
        mProgress = progress;
        postInvalidate();
    }

}