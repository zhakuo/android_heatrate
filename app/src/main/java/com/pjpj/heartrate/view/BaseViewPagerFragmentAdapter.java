package com.pjpj.heartrate.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;

import androidx.collection.ArrayMap;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;
import java.util.Map;

/**
 * Des:       BaseViewPagerFragmentAdapter
 * Create by: m122469119
 * On:        2018/5/24 10:57
 * Email:     122469119@qq.com
 */
public class BaseViewPagerFragmentAdapter extends FragmentStatePagerAdapter {
    private List<FragmentPageInfo> mFragmentInfos;
    private Map<String, Fragment> mFragments = new ArrayMap<>();

    private Fragment mCurFragment;
    private Context mContext;

    public BaseViewPagerFragmentAdapter(Context context, FragmentManager fm, List<FragmentPageInfo> infoList) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mContext = context;
        mFragmentInfos = infoList;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        if (object instanceof Fragment) {
            mCurFragment = (Fragment) object;
        }
    }

    public Fragment getCurFragment() {
        return mCurFragment;
    }

    @Override
    public Fragment getItem(int position) {
        FragmentPageInfo fragmentPageInfo = mFragmentInfos.get(position);

        Log.e("fragmentPageInfo tag",fragmentPageInfo.getTag());
        Fragment fragment = mFragments.get(fragmentPageInfo.getTag());
        if (fragment == null) {
            fragment = Fragment.instantiate(mContext, fragmentPageInfo.getClx().getName(), fragmentPageInfo.getArgs());
            mFragments.put(fragmentPageInfo.getTag(), fragment);
        }

        return fragment;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }


    @Override
    public int getCount() {
        return mFragmentInfos.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentInfos.get(position).title;
    }


    public void addTab(String title, String tag, Class<? extends Fragment> clss, Bundle args) {
        FragmentPageInfo pageInfo = new FragmentPageInfo(title, tag, clss, args);
        mFragmentInfos.add(pageInfo);
        notifyDataSetChanged();
    }


    public List<FragmentPageInfo> getFragmentInfos() {
        return mFragmentInfos;
    }


    public Map<String, Fragment> getFragments() {
        return mFragments;
    }

    public void addAllTab(List<FragmentPageInfo> mTabs) {
        mFragmentInfos.addAll(mTabs);
        notifyDataSetChanged();
    }


    /**
     * 移除一个tab
     *
     * @param index 备注：如果index小于0，则从第一个开始删 如果大于tab的数量值则从最后一个开始删除
     */
    public void remove(int index) {
        if (mFragmentInfos.isEmpty()) {
            return;
        }

        if (index < 0) {
            index = 0;
        }

        if (index >= mFragmentInfos.size()) {
            index = mFragmentInfos.size() - 1;
        }

        FragmentPageInfo fragmentPageInfo = mFragmentInfos.get(index);
        if (mFragments.containsKey(fragmentPageInfo.getTag())) {
            mFragments.remove(fragmentPageInfo.getTag());
        }

        mFragmentInfos.remove(index);
        notifyDataSetChanged();
    }

    /**
     * 移除所有的tab
     */
    public void removeAll() {
        if (mFragmentInfos.isEmpty()) {
            return;
        }
        mFragments.clear();
        mFragmentInfos.clear();
        notifyDataSetChanged();
    }

    public static class FragmentPageInfo {
        private String title;
        private Class<? extends Fragment> clx;
        private Bundle args;
        private String tag;

        public FragmentPageInfo(String title, String tag, Class<? extends Fragment> clx, Bundle args) {
            this.title = title;
            this.clx = clx;
            this.args = args;
            this.tag = tag;
        }

        public String getTitle() {
            return title;
        }


        public Class<? extends Fragment> getClx() {
            return clx;
        }


        public Bundle getArgs() {
            return args;
        }


        public String getTag() {
            return tag;
        }


    }
}