package com.pjpj.heartrate.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;

import com.leg.yuandu.utils.Utils;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.ui.FeedbackActivity;

/**
 * 评分引导弹窗，只有点击右上角关闭按钮才能退出
 */
public class RateDialog extends BaseDialog {

    public RateDialog(@NonNull final Context context) {
        super(context);
        setCanceledOnTouchOutside(false);
        setContentView(R.layout.rate_dialog);
        findViewById(R.id.rate_dialog_feedback_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, FeedbackActivity.class);
                context.startActivity(i);
                dismiss();
            }
        });
        findViewById(R.id.rate_dialog_rate_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.openGooglePlay(context, "market://details?id=" + context.getPackageName(),
                        "https://play.google.com/store/apps/details?id=" + context.getPackageName());
                dismiss();
            }
        });
        findViewById(R.id.rate_dialog_close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    Log.d("RateDialog", "back btn press, do nothing");
                }
                return true;
            }
        });
    }


}
