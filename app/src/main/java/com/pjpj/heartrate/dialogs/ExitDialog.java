package com.pjpj.heartrate.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;

import com.pjpj.heartrate.R;


public class ExitDialog extends BaseDialog {

    private Listener mListener;

    public ExitDialog(Context context) {
        super(context);

        setContentView(R.layout.dialog_exit_fb);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setExit();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    private void setExit() {
        View cancel = findViewById(R.id.exit_cancel);
        View ok = findViewById(R.id.exit_ok);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onOkClicked(ExitDialog.this);
                }
            }
        });
    }

    public interface Listener {
        public void onOkClicked(Dialog dialog);
    }

    public void setOnDialogListener(Listener listener) {
        mListener = listener;
    }
}
