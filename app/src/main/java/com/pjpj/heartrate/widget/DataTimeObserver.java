package com.pjpj.heartrate.widget;

import android.os.Handler;
import android.os.Message;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

/**
 * Des:
 * Create by: ye
 * On:        2021\01\26 10:08
 * Email:     yeshieh@163.com
 */
public abstract class DataTimeObserver implements LifecycleObserver {

    /**
     * The interval in millis that the user receives callbacks
     */
    private final long mCountdownInterval;

    /**
     * boolean representing if the timer was cancelled
     */
    private boolean mCancelled = false;

    /**
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick()} callbacks.
     */
    public DataTimeObserver(long countDownInterval) {
        mCountdownInterval = countDownInterval;
    }

    /**
     * Cancel the countdown.
     */
    public synchronized final void cancel() {
        mCancelled = true;
        mHandler.removeMessages(MSG);
    }

    /**
     * Start the countdown.
     */
    public synchronized final DataTimeObserver start() {
        mCancelled = false;
        mHandler.sendMessage(mHandler.obtainMessage(MSG));
        return this;
    }


    /**
     * Callback fired on regular interval.
     */
    public abstract void onTick();

    private static final int MSG = 1;

    // handles counting down
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            synchronized (DataTimeObserver.this) {
                if (mCancelled) {
                    return;
                }
                onTick();
                sendMessageDelayed(obtainMessage(MSG), mCountdownInterval);
            }
        }
    };

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    void OnDestory() {
        cancel();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    void onPause() {
        cancel();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    void onResume() {
        start();
    }
}
