package com.pjpj.heartrate.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Des:
 * Create by: ye
 * On:        2020\04\20 10:27
 * Email:     yeshieh@163.com
 */
public class RoundGradientLayout extends FrameLayout {
    private RoundGradientViewDelegate delegate;

    public RoundGradientLayout(Context context) {
        this(context, null);
    }

    public RoundGradientLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        delegate = new RoundGradientViewDelegate(this, context, attrs);
    }

    /** use delegate to set attr */
    public RoundGradientViewDelegate getDelegate() {
        return delegate;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (delegate.isWidthHeightEqual() && getWidth() > 0 && getHeight() > 0) {
            int max = Math.max(getWidth(), getHeight());
            int measureSpec = MeasureSpec.makeMeasureSpec(max, MeasureSpec.EXACTLY);
            super.onMeasure(measureSpec, measureSpec);
            return;
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (delegate.isRadiusHalfHeight()) {
            delegate.setCornerRadius(getHeight() / 2);
        }else {
            delegate.setBgSelector();
        }
    }
}

