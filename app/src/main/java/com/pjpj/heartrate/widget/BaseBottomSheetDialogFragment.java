package com.pjpj.heartrate.widget;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewbinding.ViewBinding;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.pjpj.heartrate.R;
import com.pjpj.heartrate.base.viewmodel.BaseViewModel;
import com.pjpj.heartrate.util.UIHelper;
import com.pjpj.heartrate.view.dialog.AppLoadingDialog;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Objects;

/**
 * Des:
 * Create by: ye
 * On:        2020\01\18 11:14
 * Email:     yeshieh@163.com
 */
public abstract class BaseBottomSheetDialogFragment<VB extends ViewBinding, VM extends BaseViewModel> extends BottomSheetDialogFragment {
    protected View mRoot;
    protected Bundle mBundle;
    protected VM mViewModel;
    protected VB binding;

    protected boolean isInit = false;
    protected boolean isLoad = false;

    private AppLoadingDialog mLoadingDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Type type = getVMClass();
        if (Activity.class.isAssignableFrom(bindContext())) {
            mViewModel = ViewModelProviders.of(getActivity()).get((Class<VM>) type);
        } else {
            mViewModel = ViewModelProviders.of(this).get((Class<VM>) type);
        }
        setStyle(DialogFragment.STYLE_NORMAL, 0);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            Class<VB> clazz = getVBClass();
            Method method = clazz.getDeclaredMethod("inflate", LayoutInflater.class);
            binding = (VB) method.invoke(null, getLayoutInflater());
            mRoot = binding.getRoot();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return mRoot;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isCanLoadData();
    }

    private void isCanLoadData() {
        if (!isInit) {
            return;
        }
        if (getUserVisibleHint()) {
            if (!isLoad) {
                lazyLoad();
                isLoad = true;
            }
        } else {
            if (isLoad) {
                stopLoad();
            }
        }
    }

    protected void lazyLoad() {

    }

    protected void stopLoad() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //初始化参数
        if ((mBundle = getArguments()) != null) {
            initArguments(mBundle);
        }

        //初始化View
        initView();

        //初始化观察者
        initLifeCycles(getLifecycle());

        isInit = true;
        isCanLoadData();
        //初始化数据层
        initData();
        initDialogAction();
    }

    protected void initDialogAction() {
        mViewModel.getDialogActionMutableLiveData().observe(this, new Observer<BaseViewModel.DialogAction>() {
            @Override
            public void onChanged(@Nullable BaseViewModel.DialogAction action) {
                switch (action) {
                    case SHOW:
                    case SHOW_STICKY:
                        if (mLoadingDialog == null) {
                            mLoadingDialog = new AppLoadingDialog(getActivity());
                            mLoadingDialog.setCanceledOnTouchOutside(action == BaseViewModel.DialogAction.SHOW);
                        }
                        mLoadingDialog.show();
                        break;
                    case DISMISS:
                        if (mLoadingDialog != null) {
                            mLoadingDialog.dismiss();
                            mLoadingDialog = null;
                        }
                        break;
                }
            }
        });
    }

    public Class<? extends LifecycleOwner> bindContext() {
        return this.getClass();
    }


    public Class<VB> getVBClass() {
        return (Class<VB>) ((ParameterizedType) Objects.requireNonNull(getClass().getGenericSuperclass())).getActualTypeArguments()[0];
    }

    public Class<VM> getVMClass() {
        return (Class<VM>) ((ParameterizedType) Objects.requireNonNull(getClass().getGenericSuperclass())).getActualTypeArguments()[1];
    }

    public void initView() {
//        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    protected void initLifeCycles(Lifecycle lifecycle) {
    }

    protected void initArguments(Bundle bundle) {
    }

    public void initData() {

    }
}
