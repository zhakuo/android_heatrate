package com.pjpj.heartrate.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.pjpj.heartrate.util.UIHelper;

import androidx.annotation.Nullable;

public class MyGridView extends View {
    Paint mPaint;

    public MyGridView(Context context) {
        super(context);
        init();
    }

    public MyGridView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.parseColor("#f6f7f7"));
        mPaint.setStrokeWidth(UIHelper.dpToPx(1));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        int perWidth = width / 9;
        for (int i = 1; i < 9; i++) {
            canvas.drawLine(0, i * perWidth, width, i * perWidth, mPaint);
            canvas.drawLine(i * perWidth, 0, i * perWidth, height, mPaint);
        }
    }
}
