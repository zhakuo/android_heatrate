package com.pjpj.heartrate.widget;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Des:
 * Create by: ye
 * On:        2019\10\17 11:51
 * Email:     yeshieh@163.com
 */
public class AppRvSpaceDecorator extends RecyclerView.ItemDecoration {
    private int mFirstTopSpace, mLastBottomSpace, mNormalSpace;
    private int mGridSpanCount = 0;
    private boolean mVertical = true;

    public AppRvSpaceDecorator(int firstTop, int lastBottom, int normal, boolean vertical) {
        this.mFirstTopSpace = firstTop;
        this.mLastBottomSpace = lastBottom;
        this.mNormalSpace = normal;
        this.mVertical = vertical;
    }

    public AppRvSpaceDecorator(int space, int spanCount) {
        this.mNormalSpace = space;
        this.mGridSpanCount = spanCount;
    }

    public AppRvSpaceDecorator(int space, int spanCount, int firstTop, int lastBottom) {
        this.mNormalSpace = space;
        this.mGridSpanCount = spanCount;
        this.mLastBottomSpace = lastBottom;
        this.mFirstTopSpace = firstTop;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        if (parent.getAdapter() == null) {
            return;
        }

        if (mGridSpanCount == 0) {
            if (mVertical) {
                outRect.top = mNormalSpace;
                outRect.bottom = 0;
                if (parent.getChildAdapterPosition(view) == 0) {
                    outRect.top = mFirstTopSpace;
                    outRect.bottom = 0;
                } else if (parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1) {
                    outRect.top = mNormalSpace;
                    outRect.bottom = mLastBottomSpace;
                } else {
                    outRect.top = mNormalSpace;
                    outRect.bottom = 0;
                }
            } else {
                outRect.left = mNormalSpace;
                outRect.right = 0;
                if (parent.getChildAdapterPosition(view) == 0) {
                    outRect.left = mFirstTopSpace;
                    outRect.right = 0;
                } else if (parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1) {
                    outRect.left = mNormalSpace;
                    outRect.right = mLastBottomSpace;
                } else {
                    outRect.left = mNormalSpace;
                    outRect.right = 0;
                }
            }
        } else {
            int pos = parent.getChildAdapterPosition(view);
            int column = (pos) % mGridSpanCount;// 计算这个child 处于第几列

            outRect.left = (column * mNormalSpace / mGridSpanCount);
            outRect.right = mNormalSpace - (column + 1) * mNormalSpace / mGridSpanCount;

            boolean mIsFirstLine = false;
            boolean mIsLastLine = false;
            int lastLine = (parent.getAdapter().getItemCount() - 1) / mGridSpanCount;

            for (int i = 0; i < mGridSpanCount; i++) {
                if (pos == i) {
                    mIsFirstLine = true;
                }
                if ((pos - i) / mGridSpanCount == lastLine) {
                    mIsLastLine = true;
                }
            }
            if(mFirstTopSpace!=0 && mIsFirstLine){
                outRect.top = mFirstTopSpace;
            }else if(mLastBottomSpace!=0 && mIsLastLine){
                outRect.top = mNormalSpace;
                outRect.bottom = mLastBottomSpace;
            }else if(pos>=mGridSpanCount){
                outRect.top = mNormalSpace;
            }

//            if (mLastBottomSpace != 0 && (pos == parent.getAdapter().getItemCount() - 1 || pos == parent.getAdapter().getItemCount() - 2)) {
//                outRect.top = mNormalSpace;
//                outRect.bottom = mLastBottomSpace;
//            } else if (pos >= mGridSpanCount) {
//                outRect.top = mNormalSpace;
//            }
        }
    }
}
