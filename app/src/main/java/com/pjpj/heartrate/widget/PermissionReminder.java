package com.pjpj.heartrate.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;

import com.pjpj.heartrate.util.permission.Permission;
import com.pjpj.heartrate.util.permission.PermissionHelper;
import com.pjpj.heartrate.util.permission.request.IRemider;
import com.pjpj.heartrate.util.permission.request.RequestExecutor;

import java.util.List;

/**
 * Des:
 * Create by: ye
 * On:        2020\06\30 15:38
 * Email:     yeshieh@163.com
 */
public class PermissionReminder implements IRemider {

    @Override
    public void showRationale(Context context, List<String> permissions, final RequestExecutor executor) {
        List<String> permissionNames = Permission.transformText(context, permissions);
        Spanned message = getString3(TextUtils.join(" ", permissionNames));

        new AlertDialog.Builder(context).setTitle("获取权限")
                .setMessage(message)
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        executor.cancel();
                    }
                })
                .setPositiveButton("重新授权", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        executor.execute();
                    }
                }).show();
    }

    @Override
    public void showGuide(Context context, List<String> permissions, final RequestExecutor executor) {
        List<String> permissionNames = Permission.transformText(context, permissions);
        Spanned message = getString(TextUtils.join(" ", permissionNames));

        new AlertDialog.Builder(context).setTitle("获取权限")
                .setMessage(message)
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        executor.cancel();
                    }
                })
                .setPositiveButton("去授权", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        executor.execute();
                    }
                }).show();

    }


    @Override
    public void showDenied(Context context, List<String> permissions, final RequestExecutor executor) {
        List<String> permissionNames = Permission.transformText(context, permissions);
        Spanned message = getString2(TextUtils.join(" ", permissionNames));
        new AlertDialog.Builder(context).setTitle("获取权限")
                .setMessage(message)
                .setNegativeButton("我已了解", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        executor.cancel();
                    }
                })
                .setPositiveButton("前往设置", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        PermissionHelper.with(context).permissionSetting().execute();
                        executor.execute();
                    }
                }).show();
    }

    @Override
    public void showInstall(Context context, RequestExecutor executor) {
        new AlertDialog.Builder(context).setTitle("获取权限")
                .setMessage("App更新需要授予安装权限")
                .setNegativeButton("取消安装", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        executor.cancel();
                    }
                })
                .setPositiveButton("立即开启", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        executor.execute();
                    }
                }).show();
    }


    private Spanned getString(String permission) {
        return Html.fromHtml(String.format("完善App功能,我们需要获取<font color='#0378d8'>%s</font>权限，以便为您提供相应服务。", permission));
    }

    private Spanned getString3(String permission) {
        return Html.fromHtml(String.format("您拒绝了我们App的权限申请请求,我们需要获取<font color='#0378d8'>%s</font>权限，否则您将无法正常使用该功能", permission));
    }

    private Spanned getString2(String permission) {
        return Html.fromHtml(String.format("由于无法获取<font color='#0378d8'>%s</font>权限，请手动开启该该权限，以便获取更好的体验<br><br>设置路径：系统设置->Heart Rate->权限", permission));

    }
}
